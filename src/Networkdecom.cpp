/*
 * Networkdecom.cpp
 *
 *  Created on: 2022年12月11日
 *      Author: Administrator
 */

#include "Networkdecom.h"
#include "math.h"
#include <iostream>;
#include<time.h>
#include <stack>
#include "fstream"
#include <algorithm>
#include<vector>

#include <direct.h>
#include <malloc.h>




bool find_pGraph_V1(HashGraphmap *phashmap, int primarykey,
		GraphNode* pgraph,string &keystring) {


	bool ishiting = false;


//   cout<<"keystring:::"<<keystring<<endl;

	if (phashmap[primarykey].existence != false) {
//		cout<<"primarykey existence------true"<<endl;
//		cout<<"m_graphNodeCluster.col_ReserveSize:"<<phashmap[primarykey].m_graphNodeCluster.col_ReserveSize<<endl;

		unordered_map<string, GraphNode*>::iterator noderesultit =
				phashmap[primarykey].mapstring_relia.find(keystring);

		if (noderesultit != phashmap[primarykey].mapstring_relia.end()) {

			phashmap[primarykey].hit++;

			ishiting = true;

			noderesultit->second->PreVarRelia+=pgraph->PreVarRelia;

//			cout<<"hit------------------"<<endl;
//			cout<<"relia:"<<noderesultit->second->PreVarRelia<<endl;

		}else{

			phashmap[primarykey].mapstring_relia[keystring] =pgraph;


		}

	} else {

		phashmap[primarykey].mapstring_relia[keystring] =pgraph;

		phashmap[primarykey].existence = true;

	}

//	cout<<"fined ..."<<ishiting<<endl;

	return ishiting;

}




bool find_pGraph_2D(HashGraphmap &phashmap,GraphNode* pgraph,string &keystring) {


	bool ishiting = false;


//   cout<<"keystring:::"<<keystring<<endl;



	if (phashmap.existence != false) {
//		cout<<"primarykey existence------true"<<endl;
//		cout<<"m_graphNodeCluster.col_ReserveSize:"<<phashmap[primarykey].m_graphNodeCluster.col_ReserveSize<<endl;

		unordered_map<string, GraphNode*>::iterator noderesultit =
				phashmap.mapstring_relia.find(keystring);

		if (noderesultit != phashmap.mapstring_relia.end()) {

			phashmap.hit++;

			ishiting = true;

			noderesultit->second->PreVarRelia+=pgraph->PreVarRelia;

//			cout<<"hit------------------"<<endl;
//			cout<<"relia:"<<noderesultit->second->PreVarRelia<<endl;

			if(noderesultit->first.size()!=keystring.size()){
				cout<<"need debug"<<endl;
			}

			int size=keystring.size();
			for(int i=0;i<size;i++){


				if(noderesultit->first[i]!=keystring[i]){
					cout<<"need debug"<<endl;
				}
			}

		}else{

			phashmap.mapstring_relia[keystring] =pgraph;


		}

	} else {

		phashmap.mapstring_relia[keystring] =pgraph;

		phashmap.existence = true;

	}

//	cout<<"fined ..."<<ishiting<<endl;

	return ishiting;

}








set<int> NeiborsOfNodeV1(Graph &graph, int nodeID) {
	set<int> neiborVector;
	map<int, double>::iterator neiborit = graph.adj_relia[nodeID].begin();
	for (; neiborit != graph.adj_relia[nodeID].end(); ++neiborit) {
		if(neiborit->first!=nodeID){
		neiborVector.insert(neiborit->first);}
	}

	return neiborVector;
}

int importanceOfTargetID(Graph &graph, int nodeID,set<int>&VisitedSet) {
	int importantindex=0;
	set<int> neiborVector;
	map<int, double>::iterator neiborit = graph.adj_relia[nodeID].begin();
	for (; neiborit != graph.adj_relia[nodeID].end(); ++neiborit) {
		neiborVector.insert(neiborit->first);
		if(::isContains(VisitedSet, neiborit->first)==true){
			importantindex++;
			cout<<"importantindex"<<importantindex<<endl;
		}
	}

	//importantindex=-1*importantindex*10;
	importantindex=neiborVector.size()+importantindex*10;




	return importantindex;
}





set<int> NeiborsOfNodeV2(Graph &graph, int nodeID) {
	set<int> neiborVector;
	map<int, double>::iterator neiborit = graph.adj_reliaV2[nodeID].begin();
	for (; neiborit != graph.adj_reliaV2[nodeID].end(); ++neiborit) {
		neiborVector.insert(neiborit->first);
	}

	return neiborVector;
}



vector<int> NeiborsOfNode(Graph &graph, int nodeID) {
	vector<int> neiborVector;
	map<int, double>::iterator neiborit = graph.adj_relia[nodeID].begin();
	for (; neiborit != graph.adj_relia[nodeID].end(); ++neiborit) {
		neiborVector.push_back(neiborit->first);
	}

	return neiborVector;
}

void printNeiborsOfId(Graph &graph, int nodeID) {
	vector<int> neiborVector = NeiborsOfNode(graph, nodeID);

	vector<int>::iterator neiborit = neiborVector.begin();
	cout << "the neibors of :" << nodeID << "   :";
	for (; neiborit != neiborVector.end(); neiborit++) {
		cout << *neiborit << "--";

	}
	cout << endl;
}

void DFSBasedEdgeListPtrV2(Graph &graph, list<int> &bfsNodeslist,
		list<Edge> &pedgelist) {

	cout << "DFSBasedEdgeListPtrV2" << endl;

	list<int>::iterator listit = bfsNodeslist.begin();
	cout << "dfs resulst as follows" << endl;
	for (; listit != bfsNodeslist.end(); listit++) {
		cout << *listit << "--";

	}

	list<int>::iterator it = bfsNodeslist.begin();

	list<int>::iterator j = bfsNodeslist.begin();
	//
	//		for (; j != bfsNodeslist.end(); ++j)
	//			cout << *j << endl;
//bool s[graph.nVerts][graph.nVerts] = { 0 };    // 判断是否已存入该边到S集合中



	int ** s = new int*[graph.nVerts];
	for (int i = 0; i < graph.nVerts; i++) {
		s[i] = new int[graph.nVerts];

	}



	for (int i = 0; i < graph.nVerts; i++) {
		for (int j = 0; j < graph.nVerts; j++) {
			s[i][j] = false;
		}
	}



	list<int> Stack = bfsNodeslist;

	while (!Stack.empty())	//只要队列不空，遍历就没有结束
	{
		int startID = Stack.front();
		//取出对头元素
		Stack.pop_front();

		vector<int> neibors = NeiborsOfNode(graph, startID);

		vector<int>::iterator neiborit;
		for (neiborit = neibors.begin(); neiborit != neibors.end();
				neiborit++) {
			if (s[*neiborit][startID] == false) {
				s[*neiborit][startID] = true;
				s[startID][*neiborit] = true;

				int endID = *neiborit;
				Edge edge;
				edge.StartID = startID;
				edge.EndID = endID;
				edge.relia = graph.adj_relia[startID][endID];
				//add to the pedgelist
				pedgelist.push_back(edge);
				//add to stack
//				Stack.push_back(endID);
			}

		}
	}


	for (int i = 0; i < graph.nVerts; i++) {
		delete s[i];

	}

	delete[]s;

}

void BFSBasedEdgeVectorPtrV2(Graph &graph, list<int> &bfsNodeslist,
		vector<Edge> &vectorEdge) {

	list<int>::iterator it = bfsNodeslist.begin();

	list<int>::iterator j = bfsNodeslist.begin();
	//
	//		for (; j != bfsNodeslist.end(); ++j)
	//			cout << *j << endl;

	int** s = new int* [graph.nVerts];
	for (int i = 0; i < graph.nVerts; i++) {
		s[i] = new int[graph.nVerts];

	}



	for (int i = 0; i < graph.nVerts; i++) {
		for (int j = 0; j < graph.nVerts; j++) {
			s[i][j] = false;
		}
	}

	// 判断是否已存入该边到S集合中
	list<int> Stack;
	Stack.push_back(*it);

	while (!Stack.empty())	//只要队列不空，遍历就没有结束
	{
		int startID = Stack.front();
		//取出对头元素
		Stack.pop_front();

		vector<int> neibors = NeiborsOfNode(graph, startID);

		vector<int>::iterator neiborit = neibors.begin();
		for (; neiborit != neibors.end(); neiborit++) {
//			cout<<"arc"<<*neiborit<<"-->"<<startID<<s[*neiborit][startID]<<endl;
			if (s[*neiborit][startID] == false) {
				s[*neiborit][startID] = true;
				s[startID][*neiborit] = true;
//				cout<<"arc"<<*neiborit<<"--"<<startID<<s[*neiborit][startID]<<endl;
//				cout<<"arc"<<startID<<"--"<<*neiborit<<s[startID][*neiborit]<<endl;
				int endID = *neiborit;
				Edge edge;
				edge.StartID = startID;
				edge.EndID = endID;
				edge.relia = graph.adj_relia[startID][endID];
				//add to the pedgelist
				vectorEdge.push_back(edge);
//				add to stack
				Stack.push_back(endID);
			}

		}
	}

	for (int i = 0; i < graph.nVerts; i++) {
		delete s[i];

	}

	delete[]s;

}

void BFSBasedEdgeVectorPtrV3(Graph &graph, list<int> &bfsNodeslist) {

	list<int>::iterator it = bfsNodeslist.begin();

	list<int>::iterator j = bfsNodeslist.begin();
	//
	//		for (; j != bfsNodeslist.end(); ++j)
	//			cout << *j << endl;
	int** s = new int* [graph.nVerts];
	for (int i = 0; i < graph.nVerts; i++) {
		s[i] = new int[graph.nVerts];

	}



	for (int i = 0; i < graph.nVerts; i++) {
		for (int j = 0; j < graph.nVerts; j++) {
			s[i][j] = false;
		}
	}


	// 判断是否已存入该边到S集合中
	list<int> Stack;
	Stack.push_back(*it);

	while (!Stack.empty())	//只要队列不空，遍历就没有结束
	{
		int startID = Stack.front();
		//取出对头元素
		Stack.pop_front();

		vector<int> neibors = NeiborsOfNode(graph, startID);

		vector<int>::iterator neiborit = neibors.begin();
		for (; neiborit != neibors.end(); neiborit++) {
//			cout<<"arc"<<*neiborit<<"-->"<<startID<<s[*neiborit][startID]<<endl;
			if (s[*neiborit][startID] == false) {
				s[*neiborit][startID] = true;
				s[startID][*neiborit] = true;
//				cout<<"arc"<<*neiborit<<"--"<<startID<<s[*neiborit][startID]<<endl;
//				cout<<"arc"<<startID<<"--"<<*neiborit<<s[startID][*neiborit]<<endl;
				int endID = *neiborit;
				Edge edge;
				edge.StartID = startID;
				edge.EndID = endID;
				edge.relia = graph.adj_relia[startID][endID];
				//add to the pedgelist
				graph.vectorEdge.push_back(edge);
//				add to stack
				Stack.push_back(endID);
			}

		}
	}
	for (int i = 0; i < graph.nVerts; i++) {
		delete s[i];

	}

	delete[]s;

}




void BFSBasedEdgeVectorPtr_generalV1(Graph &graph, list<int> &bfsNodeslist,
		double nodefuncs) {

	list<int>::iterator it = bfsNodeslist.begin();

	list<int>::iterator j = bfsNodeslist.begin();
	//
	//		for (; j != bfsNodeslist.end(); ++j)
	//			cout << *j << endl;
	int** s = new int* [graph.nVerts];
	for (int i = 0; i < graph.nVerts; i++) {
		s[i] = new int[graph.nVerts];

	}



	for (int i = 0; i < graph.nVerts; i++) {
		for (int j = 0; j < graph.nVerts; j++) {
			s[i][j] = false;
		}
	}

	// 判断是否已存入该边到S集合中
	list<int> Stack;
	Stack.push_back(*it);

	while (!Stack.empty())	//只要队列不空，遍历就没有结束
	{
		int startID = Stack.front();
		//取出对头元素
		Stack.pop_front();
		//add the nodeself
		if (s[startID][startID] == false) {
			s[startID][startID] = true;

			Edge edge;
			edge.StartID = startID;
			edge.EndID = startID;
			edge.relia = nodefuncs;
			graph.vectorEdge.push_back(edge);
		}

		vector<int> neibors = NeiborsOfNode(graph, startID);

		vector<int>::iterator neiborit = neibors.begin();
		for (; neiborit != neibors.end(); neiborit++) {
//			cout<<"arc"<<*neiborit<<"-->"<<startID<<s[*neiborit][startID]<<endl;
			if (s[*neiborit][startID] == false) {
				s[*neiborit][startID] = true;
				s[startID][*neiborit] = true;
//				cout<<"arc"<<*neiborit<<"--"<<startID<<s[*neiborit][startID]<<endl;
//				cout<<"arc"<<startID<<"--"<<*neiborit<<s[startID][*neiborit]<<endl;
				int endID = *neiborit;
				Edge edge;
				edge.StartID = startID;
				edge.EndID = endID;
				edge.relia = graph.adj_relia[startID][endID];
				//add to the pedgelist
				graph.vectorEdge.push_back(edge);
//				add to stack
				Stack.push_back(endID);
			}

		}
	}
	for (int i = 0; i < graph.nVerts; i++) {
		delete s[i];

	}

	delete[]s;

}


void BFSBasedEdgeVectorPtr_generalV2(Graph &graph, list<int> &bfsNodeslist,
		double nodefuncs) {

	list<int>::iterator it = bfsNodeslist.begin();

	list<int>::iterator j = bfsNodeslist.begin();
	//
	//		for (; j != bfsNodeslist.end(); ++j)
	//			cout << *j << endl;

	const int n= graph.nVerts;
//	int (*s)[len] = new int[len][len]();

//	auto s = new int[n][n];
//
	int **s=new int*[n];
	for(int i=0;i< n;i++){
		s[i]=new int[n];
	}


	for (int i = 1; i < graph.nVerts; i++) {
		for (int j = 1; j < graph.nVerts; j++) {

			s[i][j] = false;
		}
	}

	// 判断是否已存入该边到S集合中
	list<int> Stack=bfsNodeslist;


	while (!Stack.empty())	//只要队列不空，遍历就没有结束
	{
		int startID = Stack.front();
		//取出对头元素
		Stack.pop_front();
		cout<<"node:"<<startID<<" is popped"<<endl;
		//add the nodeself
//		if (s[startID][startID] == false) {
//			s[startID][startID] = true;
//            if(::isContains(graph.KterminalSet, startID)==false){
//			Edge edge;
//			edge.StartID = startID;
//			edge.EndID = startID;
//			edge.relia = nodefuncs;
//			graph.vectorEdge.push_back(edge);}
//		}

		vector<int> neibors = NeiborsOfNode(graph, startID);

		vector<int>::iterator neiborit = neibors.begin();
		for (; neiborit != neibors.end(); neiborit++) {
//			cout<<"arc"<<*neiborit<<"-->"<<startID<<s[*neiborit][startID]<<endl;
			if (s[*neiborit][startID] == false) {
				s[*neiborit][startID] = true;
				s[startID][*neiborit] = true;
//				cout<<"arc"<<*neiborit<<"--"<<startID<<s[*neiborit][startID]<<endl;
//				cout<<"arc"<<startID<<"--"<<*neiborit<<s[startID][*neiborit]<<endl;
				int endID = *neiborit;
				Edge edge;
				edge.StartID = startID;
				edge.EndID = endID;
				edge.relia = graph.adj_relia[startID][endID];
				//add to the pedgelist
				graph.vectorEdge.push_back(edge);
////				add to stack
//				Stack.push_back(endID);
			}

		}
	}

	cout<<"s bool array is released"<<endl;
	for (int i = 0; i < n; i++) {
		delete s[i];
	}
	delete []s;

}



void BFSBasedEdgeVectorPtr_generalV3(Graph &graph, list<int> &bfsNodeslist,
		double nodefuncs) {


	//
	//		for (; j != bfsNodeslist.end(); ++j)
	//			cout << *j << endl;

	const int n= graph.nVerts;
//	int (*s)[len] = new int[len][len]();

//	auto s = new int[n][n];
//
	int **s=new int*[n];
	for(int i=0;i< n;i++){
		s[i]=new int[n];
	}


	for (int i = 1; i < graph.nVerts; i++) {
		for (int j = 1; j < graph.nVerts; j++) {

			s[i][j] = false;
		}
	}

	// 判断是否已存入该边到S集合中
	list<int> Stack=bfsNodeslist;


	while (!Stack.empty())	//只要队列不空，遍历就没有结束
	{
		int startID = Stack.front();
		//取出对头元素
		Stack.pop_front();
		cout<<"node:"<<startID<<" is popped"<<endl;
		//add the nodeself
//		if (s[startID][startID] == false) {
//			s[startID][startID] = true;
//            if(::isContains(graph.KterminalSet, startID)==false){
//			Edge edge;
//			edge.StartID = startID;
//			edge.EndID = startID;
//			edge.relia = nodefuncs;
//			graph.vectorEdge.push_back(edge);}
//		}

		set<int> neibors = NeiborsOfNodeV2(graph, startID);

		set<int>::iterator neiborit = neibors.begin();
		for (; neiborit != neibors.end(); neiborit++) {
//			cout<<"arc"<<*neiborit<<"-->"<<startID<<s[*neiborit][startID]<<endl;
			if (s[*neiborit][startID] == false) {
				s[*neiborit][startID] = true;
				s[startID][*neiborit] = true;
//				cout<<"arc"<<*neiborit<<"--"<<startID<<s[*neiborit][startID]<<endl;
//				cout<<"arc"<<startID<<"--"<<*neiborit<<s[startID][*neiborit]<<endl;
				int endID = *neiborit;
				Edge edge;
				edge.StartID = startID;
				edge.EndID = endID;
				edge.relia = graph.adj_reliaV2[startID][endID];
				//add to the pedgelist
				graph.vectorEdge.push_back(edge);
////				add to stack
//				Stack.push_back(endID);
			}

		}


	}

	cout<<"s bool array is released"<<endl;
	for (int i = 0; i < n; i++) {
		delete s[i];
	}
	delete []s;

}



void BFSBasedEdgeVectorPtr_generalV4(Graph &graph, list<int> &bfsNodeslist,
		double nodefuncs) {


	//
	//		for (; j != bfsNodeslist.end(); ++j)
	//			cout << *j << endl;

	const int n= graph.nVerts;
//	int (*s)[len] = new int[len][len]();

//	auto s = new int[n][n];
//
	int **s=new int*[n];
	for(int i=0;i< n;i++){
		s[i]=new int[n];
	}


	for (int i = 1; i < graph.nVerts; i++) {
		for (int j = 1; j < graph.nVerts; j++) {

			s[i][j] = false;
		}
	}

	// 判断是否已存入该边到S集合中
	list<int> Stack=bfsNodeslist;


	while (!Stack.empty())	//只要队列不空，遍历就没有结束
	{
		int startID = Stack.front();
		//取出对头元素
		Stack.pop_front();
		cout<<"node:"<<startID<<" is popped"<<endl;
//		add the nodeself
		if (s[startID][startID] == false) {
			s[startID][startID] = true;

			Edge edge;
			edge.StartID = startID;
			edge.EndID = startID;
			edge.relia = nodefuncs;
			graph.vectorEdge.push_back(edge);
		}

		set<int> neibors = NeiborsOfNodeV2(graph, startID);

		set<int>::iterator neiborit = neibors.begin();
		for (; neiborit != neibors.end(); neiborit++) {
//			cout<<"arc"<<*neiborit<<"-->"<<startID<<s[*neiborit][startID]<<endl;

			//add the ends node self first
			if (s[*neiborit][*neiborit] == false) {
				s[*neiborit][*neiborit] = true;

				Edge edge;
				edge.StartID = *neiborit;
				edge.EndID = *neiborit;
				edge.relia = nodefuncs;
				graph.vectorEdge.push_back(edge);
			}




			if (s[*neiborit][startID] == false) {
				s[*neiborit][startID] = true;
				s[startID][*neiborit] = true;
//				cout<<"arc"<<*neiborit<<"--"<<startID<<s[*neiborit][startID]<<endl;
//				cout<<"arc"<<startID<<"--"<<*neiborit<<s[startID][*neiborit]<<endl;
				int endID = *neiborit;
				Edge edge;
				edge.StartID = startID;
				edge.EndID = endID;
				edge.relia = graph.adj_reliaV2[startID][endID];
				//add to the pedgelist
				graph.vectorEdge.push_back(edge);
////				add to stack
//				Stack.push_back(endID);
			}

		}


	}

	cout<<"s bool array is released"<<endl;
	for (int i = 0; i < n; i++) {
		delete s[i];
	}
	delete []s;

}










void BFSBasedEdgeListPtrV2(Graph &graph, list<int> &bfsNodeslist,
		list<Edge> &pedgelist) {

	list<int>::iterator it = bfsNodeslist.begin();

	list<int>::iterator j = bfsNodeslist.begin();
	//
	//		for (; j != bfsNodeslist.end(); ++j)
	//			cout << *j << endl;
	int** s = new int* [graph.nVerts];
	for (int i = 0; i < graph.nVerts; i++) {
		s[i] = new int[graph.nVerts];

	}



	for (int i = 0; i < graph.nVerts; i++) {
		for (int j = 0; j < graph.nVerts; j++) {
			s[i][j] = false;
		}
	}

	// 判断是否已存入该边到S集合中
	list<int> Stack;
	Stack.push_back(*it);

	while (!Stack.empty())	//只要队列不空，遍历就没有结束
	{
		int startID = Stack.front();
		//取出对头元素
		Stack.pop_front();

		vector<int> neibors = NeiborsOfNode(graph, startID);

		vector<int>::iterator neiborit = neibors.begin();
		for (; neiborit != neibors.end(); neiborit++) {
//			cout<<"arc"<<*neiborit<<"-->"<<startID<<s[*neiborit][startID]<<endl;
			if (s[*neiborit][startID] == false) {
				s[*neiborit][startID] = true;
				s[startID][*neiborit] = true;
//				cout<<"arc"<<*neiborit<<"--"<<startID<<s[*neiborit][startID]<<endl;
//				cout<<"arc"<<startID<<"--"<<*neiborit<<s[startID][*neiborit]<<endl;
				int endID = *neiborit;
				Edge edge;
				edge.StartID = startID;
				edge.EndID = endID;
				edge.relia = graph.adj_relia[startID][endID];
				//add to the pedgelist
				pedgelist.push_back(edge);
//				add to stack
				Stack.push_back(endID);
			}

		}
	}

	for (int i = 0; i < graph.nVerts; i++) {
		delete s[i];

	}

	delete[]s;



}

void BFSBasedEdgeListPtr(Graph &graph, list<int> &bfsNodeslist,
		list<Edge> &pedgelist) {
//	pedgelist.pop_front();
//    pedgelist=new list<Edge>;
//	list<int> bfsNodeslist=BFSTraverse(sNode);
	list<int>::iterator it = bfsNodeslist.begin();

	cout << "bfsNodeslist size:" << bfsNodeslist.size() << endl;

//
//		for (; j != bfsNodeslist.end(); ++j)
//			cout << *j << endl;
	int** s = new int* [graph.nVerts];
	for (int i = 0; i < graph.nVerts; i++) {
		s[i] = new int[graph.nVerts];

	}



	for (int i = 0; i < graph.nVerts; i++) {
		for (int j = 0; j < graph.nVerts; j++) {
			s[i][j] = false;
		}
	}

	list<int> Stack;
	Stack.push_back(*it);

	while (!Stack.empty())	//只要队列不空，遍历就没有结束
	{
		int startID = Stack.front();
		//取出对头元素
		Stack.pop_front();

		vector<int> neibors = NeiborsOfNode(graph, startID);

		vector<int>::iterator neiborit;
		for (neiborit = neibors.begin(); neiborit != neibors.end();
				neiborit++) {
			if (s[*neiborit][startID] == false) {
				s[*neiborit][startID] = true;
				s[startID][*neiborit] = true;

				int endID = *neiborit;
				Edge edge;
				edge.StartID = startID;
				edge.EndID = endID;
				edge.relia = 0.9;
				//add to the pedgelist
				pedgelist.push_back(edge);
				//add to stack
				Stack.push_back(endID);
			}

		}
	}
	for (int i = 0; i < graph.nVerts; i++) {
		delete s[i];

	}

	delete[]s;

}


inline int mergeinteger_int(int a, int b)
{
	int sum = 0;
	//if (b >= 0)
	int mid = b;
	sum += a * 10;
	/*while (mid /= 10)
	{
		sum *= 10;
	}*/
	for (;mid/=10;sum*=10);
	sum += b;
	return sum;
}




inline void ClusterBListToCombineString2D(Graph& G,string &keystring,int &auxKey,int cols) {

//    cout<<"recording......"<<endl;
//    cout<<G.mClusterBounaryList.size()<<endl;
	int edgeLevel=0;
	int nodeindex=0;
	int ssize=0;

	int maxsize=0;


	int realhead=-1;


//	graph.BoundaryString.reserve(8);

	//source-----    1
	//no source---- -1;
	list<clusterNodeSet>::iterator clusterit = G.mClusterBounaryList.begin();

	int clustersize=G.mClusterBounaryList.size();
	//reset
	//	graph.CombineBounaryList.clear();
	//

//	::printClusterBounaryListBasic(graph);
//	printClusterBounaryList_generalV2(graph);
	unsigned char ClusterID;//is source=1,is no source=0
	int numminushead=0;
	unsigned char uc;

	keystring.reserve(50);

	int NumberofHead=1;


	//set head
	for (; clusterit != G.mClusterBounaryList.end(); clusterit++) {

		set<int>::iterator sit = clusterit->clusterSet.begin();
        ////////////////////////////


		for (; sit != clusterit->clusterSet.end(); sit++) {

			if (realhead == -1) {
				realhead = *sit;
			    string numStr = to_string(realhead);
			    NumberofHead=numStr.length();
			    uc=(unsigned char)NumberofHead;

			    G.clusterMinhead=realhead-1;

			    keystring.push_back(uc);//head digit number
			    keystring+=numStr;
			    break;
			}



		}

		if (realhead != -1) {
			break;
		}





	}

	clusterit = G.mClusterBounaryList.begin();


	for (; clusterit != G.mClusterBounaryList.end(); clusterit++) {

		//add clusterSet
		if (clusterit->isSource == false) {
			ClusterID=(unsigned char)0;
//			ssize+=tempsize;
		} else {
			ClusterID=(unsigned char)255;

		}
		keystring.push_back(ClusterID);

		set<int>::iterator sit = clusterit->clusterSet.begin();
        ////////////////////////////


		for (; sit != clusterit->clusterSet.end(); sit++) {
			nodeindex++;
			numminushead=*sit-G.clusterMinhead;
			uc=(unsigned char)numminushead;
			keystring.push_back(uc);

		}



	}


	if(clustersize>0){
//		cout<<"SSSzie-----------"<<ssize<<endl;
//		clustersize=::mergeinteger_int(clustersize,ssize)%100;
//		auxKey=::mergeinteger_int(clustersize, nodeindex)%100;
//		auxKey=::mergeinteger_int(ssize, nodeindex)%100;

//		auxKey=::mergeinteger_int(nodeindex, ssize)%100;
//		auxKey=::mergeinteger_int(nodeindex, ssize);
//		auxKey=::mergeinteger_int(auxKey, maxsize);
//		auxKey*=clustersize;
////		auxKey*=(nodeindex-ssize);
//		auxKey*=maxsize;
		auxKey=nodeindex%cols;
//		auxKey=clustersize;
	}









//	cout<<"auxkey:"<<auxKey<<"--";



	keystring.shrink_to_fit();







}


void UpdateGraphNode(Graph &G){




    int down_startIndex=0;


    while(G.node_degree[down_startIndex]==0){


    	down_startIndex++;

//       	if(graph.node_degree[up_startIndex]==-2){
//       		if(::isNodeTrueFailed(graph, G.minBoundaryNodeID+up_startIndex)==true){
//       			graph.node_degree[up_startIndex]=0;
//       		}
//       	}


       }

    int upperID=G.node_degree.size()-1;

    while(G.node_degree[upperID]==G.node_degreeArray[upperID+G.minBoundaryNodeID]){


    	upperID--;

//       	if(graph.node_degree[up_startIndex]==-2){
//       		if(::isNodeTrueFailed(graph, G.minBoundaryNodeID+up_startIndex)==true){
//       			graph.node_degree[up_startIndex]=0;
//       		}
//       	}


       }
    upperID++;

    down_startIndex--;

    if(down_startIndex<0){
    	down_startIndex=0;
    }

    int len=-1;
      if(upperID<down_startIndex){
    	  upperID=down_startIndex;
    	  len=0;
      }else{
    	  len=upperID-down_startIndex;
      }


//


//       int len=150;


//       cout<<"upperID:"<<upperID<<"--";
//
//       cout<<"Len="<<len<<endl;



//      int len=100;
//      G.minBoundaryNodeID=1;


    if(down_startIndex>=0){

    	int new_index=G.minBoundaryNodeID+down_startIndex;
    	::resetNodeDegreeForCompact(G, G.minBoundaryNodeID,new_index,len);
//    	resetNodeDegree(graph,G.minBoundaryNodeID,new_index,len,G);
    	G.minBoundaryNodeID=new_index;

//
//    	 vector<short int>::const_iterator first1 = G.node_degree.begin();
//    	 vector<short int>::const_iterator Newfirst1 =first1 + new_index-G.minBoundaryNodeID;
//    	 vector<short int>::const_iterator last1 =Newfirst1+len;
//
//    	 vector<short int> newVector(Newfirst1,last1);
//
//    	 G.node_degree.swap(newVector);



    }
}




void DecodeClusterBlistFromString(GraphNode &graph, Graph &G,
		const string &keystring) {
//	cout<<"string:decoding ....."<<endl;

	G.mClusterBounaryList.clear();
	int size = keystring.size();
	if (size > 1) {

		//read head
		unsigned char uc;
		uc = (unsigned char) keystring[0];
		int numdigits = int(uc);
//	 cout<<"numdigits:"<<numdigits<<endl;

		int clusterhead = 0;

		string sub_str1 = keystring.substr(1, numdigits);
//    cout<<"min head:"<<sub_str1<<endl;
		clusterhead = stoi(sub_str1);

		int subclusterHead = 0;

		clusterNodeSet m_cluster;
		bool isfinished = false;
		uc = (unsigned char) keystring[numdigits + 1];
		int node = int(uc);

		if (node == 255) {
			m_cluster.isSource = true;

		} else {
			m_cluster.isSource = false;
		}

		for (int index = numdigits + 2; index < size; index++) {
			uc = (unsigned char) keystring[index];
			node = int(uc);
//		cout<<"node="<<node<<endl;
			if (node == 255 or node == 0) {

				//add the m_cluster to list
				G.mClusterBounaryList.push_back(m_cluster);
				m_cluster.clusterSet.clear();
				if (node == 255) {
					m_cluster.isSource = true;
				} else {
					m_cluster.isSource = false;
				}
				subclusterHead = 0;
				continue;
			} else {

				node += G.clusterMinhead;
				if (subclusterHead == 0) {
					subclusterHead = node;
					m_cluster.head = subclusterHead;
				}
				m_cluster.clusterSet.insert(node);
//			cout<<"node:"<<node<<" is inserted"<<endl;
			}

		}
		//add last cluster
		G.mClusterBounaryList.push_back(m_cluster);

	}

//	cout<<"after code.."<<endl;

//	::printClusterBounaryListBasic(G);

//	graph.BoundaryCharVector.clear();
//	graph.BoundaryCharVector.shrink_to_fit();

//    if(graph.KterminalVec.size()==0){
//    	cout<<"NULL"<<endl;
//    }
//	cout<<"graph.KterminalVec size:"<<graph.KterminalVec.size()<<endl;

//    cout<<"finished..."<<endl;

	G.node_degree.clear();
	G.KterminalSet.clear();

}



void expandBoundary(Graph &G){


    int len=300;
    resetNodeDegree(G,G.minBoundaryNodeID,G.minBoundaryNodeID,len);

//    cout<<"after expand:"<<graph.node_degree.size()<<endl;
}


bool isNodeTrueFailed(Graph &G,int targetID){

//	::printClusterBounaryListBasic(graph);

	bool isFailed=false;

   set<int> neiborSet=::NeiborsOfNodeV2(G, targetID);

   set<int>::iterator neibortIt=neiborSet.begin();
   for(;neibortIt!=neiborSet.end();neibortIt++){
	   if(*neibortIt<G.minBoundaryNodeID){
		   isFailed=true;
		   continue;
	   }else{
		   if(G.node_degree[*neibortIt-G.minBoundaryNodeID]==0){
			   isFailed=true;
		   }else{
			   isFailed=false;
			   break;
		   }
	   }

   }

   return isFailed;



}


void PrintNeiborsDegree(Graph &G,int targetID){

//	::printClusterBounaryListBasic(graph);

   set<int> neiborSet=::NeiborsOfNodeV2(G, targetID);

   set<int>::iterator neibortIt=neiborSet.begin();
   for(;neibortIt!=neiborSet.end();neibortIt++){
	   if(*neibortIt<G.minBoundaryNodeID){

		   continue;
	   }else{
		   cout<<"neiborID:"<<*neibortIt<<":degree is "<<G.node_degree[*neibortIt-G.minBoundaryNodeID]<<"--";
	   }


   }
   cout<<endl;




}



void zeroNodeDegreeFromBSDetection(Graph &graph,Graph &G) {



//    cout<<"recording......"<<endl;
//    printClusterBounaryListBasic(graph);

	//source-----    1
	//no source---- -1;
	list<clusterNodeSet>::iterator clusterit = G.mClusterBounaryList.begin();
	//reset

	for (; clusterit != G.mClusterBounaryList.end(); clusterit++) {



		set<int>::iterator sit = clusterit->clusterSet.begin();
		for (; sit != clusterit->clusterSet.end(); sit++) {
//			varray.push_back(*sit);
			if(graph.node_degree[*sit-G.minBoundaryNodeID]==0){
				cout<<"node :"<<*sit<<" degree =0"<<endl;

			    cout<<"reanalysis------------------------------"<<endl;


			    cout<<"maxbounaryID:============================="<<graph.maxImpactNodeID<<endl;
			    cout<<"minboundaryID:============================="<<G.minBoundaryNodeID<<endl;
				::printClusterBounaryList_generalV2(graph);
				cout<<"reanalysis end------------"<<endl;


				break;
			}


		}



	}



	clusterit = G.mClusterBounaryList.begin();
	//reset

	for (; clusterit != G.mClusterBounaryList.end(); clusterit++) {


		if (clusterit->clusterSet.size() == 1
				and clusterit->isSource == false) {
			set<int>::iterator sit = clusterit->clusterSet.begin();
			for (; sit != clusterit->clusterSet.end(); sit++) {
//			varray.push_back(*sit);
				if (graph.node_degree[*sit-G.minBoundaryNodeID] == 1) {
					cout << "node :" << *sit << " degree =1" << endl;
					cout<<"reanalysis------------------------------"<<endl;

					::printClusterBounaryList_generalV2(graph);
					cout<<"reanalysis end------------"<<endl;
					break;
				}

			}

		}

	}


}








void setTargetEdgeByEdgeVector(Graph &G) {
//	cout<<"targetedge index:"<<G.edgeindex<<endl;
	Edge targetEdge = G.vectorEdge[G.edgeindex];
	int startID = targetEdge.StartID;
	int endID = targetEdge.EndID;
	G.targetEdge.StartID = startID;
	G.targetEdge.EndID = endID;
	G.targetEdge.relia = targetEdge.relia;
	//remove the edge
	if (startID != endID) {
		G.node_degree[startID]--;
		G.node_degree[endID]--;
	}
//	deleteEdge(graph,startID, endID);

	//initial  	_startID_clusterSet _endID_clusterSet

	G.edgeindex++;

}



void setNeiborBasedSeries(Graph &G, int seedID) {
//	cout<<"targetedge index:"<<G.edgeindex<<endl;

	Edge targetEdge = G.vectorEdge[G.edgeindex];
	double relia1 = targetEdge.relia;
	//remove the edge
//	if (targetEdge.StartID != targetEdge.EndID) {
//		graph.node_degree[targetEdge.StartID]--;
//		graph.node_degree[targetEdge.EndID]--;
//	}
//	deleteEdge(graph,startID, endID);
//	G.edgeindex++;
//	cout<<"edge index:"<<G.edgeindex;

	//initial  	_startID_clusterSet _endID_clusterSet
	//set startID
	if (targetEdge.EndID == seedID) {
		G.targetEdge.StartID = targetEdge.StartID;
	} else {
		G.targetEdge.StartID = targetEdge.EndID;
	}

	G.edgeindex++;
//	cout<<"edge index:"<<G.edgeindex<<endl;

	targetEdge = G.vectorEdge[G.edgeindex];
	double relia2 = targetEdge.relia;
//	//remove the edge
//	if (targetEdge.StartID != targetEdge.EndID) {
//		graph.node_degree[targetEdge.StartID]--;
//		graph.node_degree[targetEdge.EndID]--;
//	}

//	G.mClusterBounaryList.erase(graph.pHeadClusterVector[0]);
	//	deleteEdge(graph,startID, endID);

	//initial  	_startID_clusterSet _endID_clusterSet
	//set endID
	if (targetEdge.StartID == seedID) {
		G.targetEdge.EndID = targetEdge.EndID;
	} else {
		G.targetEdge.EndID = targetEdge.StartID;
	}

//	cout<<"midlle Node:"<<seedID<<endl;
//
//	cout<<"neibors:"<<G.targetEdge.StartID<<"--->"<<G.targetEdge.EndID<<endl;

	//set the series edge relia
	G.targetEdge.relia = relia1 * relia2;
	//update the edgeindex
	G.edgeindex++;

}

void setNeiborBasedSeriesForTrueNode(Graph &G, int seedID) {
//	cout<<"targetedge index:"<<G.edgeindex<<endl;
	double relia = G.targetEdge.relia;

	Edge targetEdge = G.vectorEdge[G.edgeindex];
	double relia1 = targetEdge.relia;
	//remove the edge
	if (targetEdge.StartID != targetEdge.EndID) {
		G.node_degree[targetEdge.StartID]--;
		G.node_degree[targetEdge.EndID]--;
	}
//	deleteEdge(graph,startID, endID);

	//initial  	_startID_clusterSet _endID_clusterSet
	//set startID
	if (targetEdge.EndID == seedID) {
		G.targetEdge.StartID = targetEdge.StartID;
	} else {
		G.targetEdge.StartID = targetEdge.EndID;
	}

	//set the series edge relia
	G.targetEdge.relia = relia * relia1;
	//update the edgeindex
	G.edgeindex++;

}


bool isFailedEndIDandNoFailedStartIDContinue(Graph &graph, int startID,
		int endID, auxiliaryGraph &auxiliarygraph) {
	bool iscontinue = true;
//	graph.node_degree[startID - graph.minBoundaryNodeID]--;
//					graph.node_degree[startID]--;

	if (auxiliarygraph.startID_clusterSet_it->isSource == false) {

		if (graph.node_degree[startID - graph.minBoundaryNodeID] == 0) {
			if (auxiliarygraph.startID_clusterSet_it->clusterSet.size() == 1) {

//									cout << "endID:" << endID << " will be removed"
//											<< endl;

				iscontinue = true;

				graph.mClusterBounaryList.erase(
						auxiliarygraph.startID_clusterSet_it);

			} else {

				clusterNodeSet new_cluster_set;

				new_cluster_set.isSource = false;
				new_cluster_set.clusterSet =
						auxiliarygraph.startID_clusterSet_it->clusterSet;
				//the branch edge is
				//removed the original
				new_cluster_set.clusterSet.erase(startID);
				graph.mClusterBounaryList.erase(
						auxiliarygraph.startID_clusterSet_it);
				//insert the old
				//					insertNewClusterV3(graph, new_cluster_set);
//										if (new_cluster_set.clusterSet.size()
//												> 0) {
//											insertNewCluster(graph,
//													new_cluster_set);
//										}

				///////////////////
				if (new_cluster_set.clusterSet.size() > 0) {
					bool isInsert = true;
					if (new_cluster_set.clusterSet.size() == 1) {
						set<int>::iterator remainedClusterit =
								new_cluster_set.clusterSet.begin();
						int rmID = *remainedClusterit;
						if (graph.node_degree[rmID - graph.minBoundaryNodeID]
								== 1) {
							graph.node_degree[rmID - graph.minBoundaryNodeID] =
									-1;
							iscontinue = true;
							isInsert = false;

						}

					}

					if (isInsert == true) {

						insertNewCluster(graph, new_cluster_set);
					}
				}

				/////////////

			}

		}

		//rundant edge
		if (graph.node_degree[startID - graph.minBoundaryNodeID] == 1) {

			if (auxiliarygraph.startID_clusterSet_it->clusterSet.size() == 1) {
				graph.node_degree[startID - graph.minBoundaryNodeID] = -1;

				//							cout << "endID:" << endID << " will be removed"
				//									<< endl;

				iscontinue = true;

				graph.mClusterBounaryList.erase(
						auxiliarygraph.startID_clusterSet_it);

			}
		}

	} else {
		//source cluster situation
		if (graph.node_degree[startID - graph.minBoundaryNodeID] == 0) {
			if (auxiliarygraph.startID_clusterSet_it->clusterSet.size() == 1) {

				//							cout << "endID:" << endID << " will be removed"
				//									<< endl;
				//true failed
				iscontinue = false;
				graph.mSClusterSize = -2;

//
//								graph.mClusterBounaryList.erase(
//										auxiliarygraph.startID_clusterSet_it);

			} else {
				iscontinue = true;

				clusterNodeSet new_cluster_set;

				new_cluster_set.isSource = true;
				new_cluster_set.clusterSet =
						auxiliarygraph.startID_clusterSet_it->clusterSet;
				//the branch edge is
				//removed the original
				new_cluster_set.clusterSet.erase(startID);
				graph.mClusterBounaryList.erase(
						auxiliarygraph.startID_clusterSet_it);
				//insert the old
				//					insertNewClusterV3(graph, new_cluster_set);
				if (new_cluster_set.clusterSet.size() > 0) {
					insertNewCluster(graph, new_cluster_set);
				}

			}

		}

	}

	return iscontinue;

}




bool isFailedStartIDAndNoFailedEndIDContinue(Graph &graph, int startID,
		int endID, auxiliaryGraph &auxiliarygraph) {
	bool iscontinue = true;
//	graph.node_degree[endID - graph.minBoundaryNodeID]--;
	//no source cluster
	if (auxiliarygraph.endID_clusterSet_it->isSource == false) {

		if (graph.node_degree[endID - graph.minBoundaryNodeID] == 0) {
			if (auxiliarygraph.endID_clusterSet_it->clusterSet.size() == 1) {

				//							cout << "endID:" << endID << " will be removed"
				//									<< endl;

				iscontinue = true;

				graph.mClusterBounaryList.erase(
						auxiliarygraph.endID_clusterSet_it);

			} else {

				clusterNodeSet new_cluster_set;

				new_cluster_set.isSource = false;
				new_cluster_set.clusterSet =
						auxiliarygraph.endID_clusterSet_it->clusterSet;
				//the branch edge is
				//removed the original
				new_cluster_set.clusterSet.erase(endID);
				graph.mClusterBounaryList.erase(
						auxiliarygraph.endID_clusterSet_it);
				//insert the old
				//					insertNewClusterV3(graph, new_cluster_set);
				if (new_cluster_set.clusterSet.size() > 0) {
					bool isInsert = true;
					if (new_cluster_set.clusterSet.size() == 1) {

						set<int>::iterator remainedClusterit =
								new_cluster_set.clusterSet.begin();

//												cout<<"node:"<<*remainedClusterit<<"may be removed"<<endl;
						int rmID = *remainedClusterit;

//												cout<<"degree:"<<graph.node_degree[rmID-graph.minBoundaryNodeID]<<endl;

						if (graph.node_degree[rmID - graph.minBoundaryNodeID]
								== 1) {
							graph.node_degree[rmID - graph.minBoundaryNodeID] =
									-1;
							iscontinue = true;
							isInsert = false;

						}

					}

					if (isInsert == true) {

						insertNewCluster(graph, new_cluster_set);
					}
				}

			}

		}

		if (graph.node_degree[endID - graph.minBoundaryNodeID] == 1) {

			if (auxiliarygraph.endID_clusterSet_it->clusterSet.size() == 1) {
				graph.node_degree[endID - graph.minBoundaryNodeID] = -1;

				//							cout << "endID:" << endID << " will be removed"
				//									<< endl;

				iscontinue = true;

				graph.mClusterBounaryList.erase(
						auxiliarygraph.endID_clusterSet_it);

//										cout<<"node :"<<endID<<" is removed"<<endl;
//
//									   ::printClusterBounaryList_generalV2(graph);

			}
		}

	} else {
		//source cluster
		if (graph.node_degree[endID - graph.minBoundaryNodeID] == 0) {
			if (auxiliarygraph.endID_clusterSet_it->clusterSet.size() == 1) {

				//true failed
				iscontinue = false;
				graph.mSClusterSize = -2;
				//
//								graph.mClusterBounaryList.erase(
//										auxiliarygraph.endID_clusterSet_it);

			} else {

				iscontinue = true;

				clusterNodeSet new_cluster_set;

				new_cluster_set.isSource = true;
				new_cluster_set.clusterSet =
						auxiliarygraph.endID_clusterSet_it->clusterSet;
				//the branch edge is
				//removed the original
				new_cluster_set.clusterSet.erase(endID);
				graph.mClusterBounaryList.erase(
						auxiliarygraph.endID_clusterSet_it);
				//insert the old
				//					insertNewClusterV3(graph, new_cluster_set);
				if (new_cluster_set.clusterSet.size() > 0) {
					insertNewCluster(graph, new_cluster_set);
				}

			}

		}

	}

	return iscontinue;
}




inline void removed_redundancy_loopGeneralEdge(Graph &graph) {
//	Edge targetEdge=graph.EdgeList.front();
//	graph.EdgeList.pop_front();
//	int startID=targetEdge.StartID;
//	int endID=targetEdge.EndID;
//	graph.targetEdge.StartID=startID;
//	graph.targetEdge.EndID=endID;
//
//	EdgeProperties ep0 =graph.nodeToTargets[startID][endID];
//	graph.targetEdge.edgepro=ep0;
//
//	//remove the edge
//	deleteEdge(graph,startID, endID);
//	deleteEdge(graph,endID, startID);
//	cout<<"-------------------------------in removed_redundancy_loopGeneralEdge"<<endl;
//	printGraph(graph);
//	printSourceSet(graph);

	bool iscontinue = false;
	bool isstarIDRemove=false;
	bool isendIDRemove=false;
	auxiliaryGraph auxiliarygraph;
	bool isfailedEdge=false;

	while (graph.edgeindex < graph.edgesize) {

		isstarIDRemove=false;
		isendIDRemove=false;
		isfailedEdge=false;

//        graph.Start_cluster_set.clusterSet.clear();
//        graph.Start_cluster_set.clusterSet.clear();
		Edge targetEdge = graph.vectorEdge[graph.edgeindex];
//		graph.EdgeList.pop_front();
		int startID = targetEdge.StartID;
		int endID = targetEdge.EndID;
		graph.targetEdge.StartID = startID;
		graph.targetEdge.EndID = endID;
		graph.targetEdge.relia = targetEdge.relia;





////
////
//		cout << "temp edge:" << "startID=" << startID << endl;
//		cout << "temp edge:" << "endID=" << endID << endl;


//		if(graph.node_degree[startID]==0 or graph.node_degree[endID]==0){
//
//			graph.edgeindex++;
//			continue;
//		}

//		printClusterBounaryList_generalV2(graph);


		///-----------------------locate-------------------------------

//		if((startID-graph.minBoundaryNodeID)<0 or (endID-graph.minBoundaryNodeID)<0){
//			cout<<"needing debug again------------------"<<endl;
//		}



		iscontinue = false;
		bool isreinsert = false;
		////

		if (isLocateclusterSet_it19V6(graph, startID, endID,auxiliarygraph) == true) {
			if (startID != endID) {
				if (auxiliarygraph.endID_clusterSet_it
						== auxiliarygraph.startID_clusterSet_it) {
//				cout << "finding rundant edge:" << startID << "--" << endID	<< endl;
//				cout << "edgeindex=" << graph.edgeindex << endl;
//			if(graph.endID_clusterSet_it->isSource==true and graph.startID_clusterSet_it->isSource==true){

					iscontinue = true;
//				if (startID == 21) {
//					graph.DebugSet.erase(endID);
//				}
//				if (endID == 21) {
//					graph.DebugSet.erase(startID);
//				}

					graph.node_degree[startID - graph.minBoundaryNodeID]--;
					graph.node_degree[endID - graph.minBoundaryNodeID]--;

					if (graph.node_degree[startID - graph.minBoundaryNodeID]
							== 0) {
//					isstarIDRemove=true;

						auxiliarygraph.startID_clusterSet_it->clusterSet.erase(
								startID);
//					cout<<"removed id:"<<startID;
						isreinsert = true;

					}

					if (graph.node_degree[endID - graph.minBoundaryNodeID]
							== 0) {

						auxiliarygraph.endID_clusterSet_it->clusterSet.erase(
								endID);
//					cout<<"removed id:"<<endID;
						isreinsert = true;
//					isendIDRemove=true;

					}

//
//               cout<<"before-ordered----"<<endl;
//               printClusterBounaryList(graph);
					//update order

					if (isreinsert == true) {
						bool isRemovedagain = true;
						//randunt it ana
						if (auxiliarygraph.endID_clusterSet_it->isSource
								== false
								and auxiliarygraph.endID_clusterSet_it->clusterSet.size()
										== 1) {
							set<int>::iterator remainedit =
									auxiliarygraph.endID_clusterSet_it->clusterSet.begin();
							if (graph.node_degree[*remainedit
									- graph.minBoundaryNodeID] == 1) {
								//remove it
								graph.node_degree[*remainedit
										- graph.minBoundaryNodeID] = -2;
								graph.mClusterBounaryList.erase(
										auxiliarygraph.endID_clusterSet_it);
								isRemovedagain = false;
							}
						}

						if (isRemovedagain == true) {
							clusterNodeSet new_cluster_set;

							new_cluster_set.isSource =
									auxiliarygraph.endID_clusterSet_it->isSource;
							new_cluster_set.clusterSet =
									auxiliarygraph.endID_clusterSet_it->clusterSet;
							//the branch edge is
							//removed the original
							graph.mClusterBounaryList.erase(
									auxiliarygraph.endID_clusterSet_it);
							//insert the old
//					insertNewClusterV3(graph, new_cluster_set);
							if (new_cluster_set.clusterSet.size() > 0) {
								insertNewCluster(graph, new_cluster_set);
							} else {

								if (new_cluster_set.isSource == true
										and graph.mSClusterSize == 1
										and graph.KterminalSet.size() == 0) {
									cout << "nnnnnnnnnn" << endl;
								}

								if (new_cluster_set.isSource == true) {
									graph.mSClusterSize = -2;
									break;
								}
							}
						}
//				cout<<"after---ordered"<<endl;
//				printClusterBounaryList(graph);
					}

//				graph.node_degree[startID]--;
//				graph.node_degree[endID]--;
					graph.edgeindex++;
					continue;

				}
			}else if(startID==endID){


				iscontinue = true;
				cout<<"hit one---------------"<<endl;

				graph.edgeindex++;
				continue;



			}

		}

		if (iscontinue == false) {


			if (startID == endID) {
				if ((startID - graph.minBoundaryNodeID) >= 0) {

//					cout << "startID: " << startID << "  :degree is:"
//							<< graph.node_degree[startID
//									- graph.minBoundaryNodeID] << endl;
				}

				if ((startID - graph.minBoundaryNodeID) < 0) {
					iscontinue = true;
				} else {

					if (graph.node_degree[startID - graph.minBoundaryNodeID]
							== 0) {
						iscontinue = true;
					}
					//var nodeedge
					if (graph.node_degree[startID - graph.minBoundaryNodeID]
							==-1) {
						iscontinue = true;
						::failed_nodeEdge_Ana(graph, startID, auxiliarygraph);
						graph.node_degree[startID - graph.minBoundaryNodeID]=0;
						if(graph.mSClusterSize == -2){
							break;
						}

					}

					if (graph.node_degree[startID - graph.minBoundaryNodeID]
							== -2) {
						iscontinue = true;
					}


				}

			} else {

				//failed edge situation
//				if (graph.node_degree[startID-graph.minBoundaryNodeID] <= 0
//						and graph.node_degree[endID-graph.minBoundaryNodeID] <= 0) {
//					;
//				}
//				::printClusterBounaryListBasic(graph);
//				cout<<"**************************"<<endl;

//				if((startID - graph.minBoundaryNodeID)>=0){
//
//				cout<<"startID: "<<startID<<"  :degree is:"<<graph.node_degree[startID-graph.minBoundaryNodeID]<<endl;
//				}
//				if((endID - graph.minBoundaryNodeID)>=0){
//
//				cout<<"endID: "<<endID<<"  :degree is:"<<graph.node_degree[endID-graph.minBoundaryNodeID]<<endl;
//				}


				bool isStartIDZero=false;
				bool isStartID_Failed=false;
				bool isStartID_rundant=false;
				bool isEndID_Failed=false;
				bool isEndID_rundant=false;
				bool isEndIDZero=false;

				bool ishandle=false;




				if((startID-graph.minBoundaryNodeID)<0){
					isStartIDZero=true;
				}else{
					if(graph.node_degree[startID-graph.minBoundaryNodeID]==0){
						isStartIDZero=true;

					}else if(graph.node_degree[startID-graph.minBoundaryNodeID]==-1){

						isStartID_rundant=true;
						graph.node_degree[startID	- graph.minBoundaryNodeID]--;

					}else if(graph.node_degree[startID-graph.minBoundaryNodeID]==-2){
						isStartID_Failed=true;
						graph.node_degree[startID	- graph.minBoundaryNodeID]=0;
					}
				}


				if ((endID - graph.minBoundaryNodeID) < 0) {
					isEndIDZero = true;
				} else {
					if (graph.node_degree[endID - graph.minBoundaryNodeID]
							== 0) {
						isEndIDZero = true;
					} else if (graph.node_degree[endID
							- graph.minBoundaryNodeID] == -1) {

						isEndID_rundant = true;
						graph.node_degree[endID	- graph.minBoundaryNodeID]--;



					} else if (graph.node_degree[endID
							- graph.minBoundaryNodeID] == -2) {
						isEndID_Failed = true;
						graph.node_degree[endID	- graph.minBoundaryNodeID]=0;
					}
				}
////ana for startID
				if(isStartIDZero==true){
//					cout<<"case isStartIDZero"<<endl;
					iscontinue=true;

					if(isEndIDZero==true){
						ishandle=true;
					}

					if(isEndID_rundant == true){
						ishandle=true;
					}
					if(isEndID_Failed == true){
						ishandle=true;
						graph.node_degree[endID	- graph.minBoundaryNodeID]=-2;
					}
//					if(isEndID_Failed == false){
//						ishandle=true;
//						graph.node_degree[endID	- graph.minBoundaryNodeID]=-2;
////						if (graph.isEndIDLocate == true) {
////							bool isC =
////									::isFailedStartIDAndNoFailedEndIDContinue(
////											graph, startID, endID,
////											auxiliarygraph);
////							if (isC == false) {
////								break;
////							}
////						}else{
////
////						}
//
//					}

				}


				if(isStartID_rundant==true){

//					cout<<"case isStartID_rundant"<<endl;

					iscontinue=true;
					if(isEndIDZero==true){
						ishandle=true;
					}
					if(isEndID_rundant == true){
						ishandle=true;
						graph.node_degree[endID	- graph.minBoundaryNodeID]=0;
						graph.node_degree[startID	- graph.minBoundaryNodeID]=0;
						cout<<"finded?????????????"<<endl;
					}
					if(isEndID_Failed == true){
						ishandle=true;
					}
					if (ishandle == false) {
						if (isEndID_Failed == false) {

							if (graph.node_degree[endID
									- graph.minBoundaryNodeID] != 0) {

								graph.node_degree[endID
										- graph.minBoundaryNodeID]--;
							}
							if (graph.isEndIDLocate == true) {
								bool isC =
										::isFailedStartIDAndNoFailedEndIDContinue(
												graph, startID, endID,
												auxiliarygraph);
								if (isC == false) {
									break;
								}
							} else {
//							graph.node_degree[endID	- graph.minBoundaryNodeID]--;
							}

						}
					}

				}

				if(isStartID_Failed==true){

//					cout<<"case isStartID_Failed"<<endl;

						iscontinue=true;
						if(isEndIDZero==true){
							ishandle=true;
							graph.node_degree[startID	- graph.minBoundaryNodeID]=-2;
						}
						if(isEndID_rundant == true){
							ishandle=true;
						}
						if(isEndID_Failed == true){
							ishandle=true;
						}

						if (ishandle == false) {


						if (isEndID_Failed == false) {
							ishandle=true;
							if (graph.node_degree[endID
									- graph.minBoundaryNodeID] != 0) {
							graph.node_degree[endID	- graph.minBoundaryNodeID]--;}
							if (graph.isEndIDLocate == true) {
								bool isC =
										::isFailedStartIDAndNoFailedEndIDContinue(
												graph, startID, endID,
												auxiliarygraph);
								if (isC == false) {
									break;
								}
							} else {

							}

						}
					}

					}

				////ana for endID-----------------------------------
				if (isEndIDZero == true) {
//					cout<<"in ---"<<endl;
//
//					if((startID - graph.minBoundaryNodeID)>=0){
//
//						cout<<"startID: "<<startID<<"  :degree is:"<<graph.node_degree[startID-graph.minBoundaryNodeID]<<endl;
//						}
//						if((endID - graph.minBoundaryNodeID)>=0){
//
//						cout<<"endID: "<<endID<<"  :degree is:"<<graph.node_degree[endID-graph.minBoundaryNodeID]<<endl;
//						}

//					cout<<"case isEndIDZero"<<endl;
					iscontinue = true;
					ishandle=true;
					if (isStartID_Failed == false) {

//						if (graph.isStartIDLocate == true) {
//							bool isC =::isFailedEndIDandNoFailedStartIDContinue(graph, startID, endID, auxiliarygraph);
//							if (isC == false) {
//								break;
//							}
//						}else{
//						graph.node_degree[startID - graph.minBoundaryNodeID]--;}
					}

				}

				if (isEndID_rundant == true) {
//					cout<<"case isEndID_rundant"<<endl;
					iscontinue = true;

					if (ishandle == false) {
						ishandle=true;

						if (isStartID_Failed == false) {
							if (graph.node_degree[startID
									- graph.minBoundaryNodeID] != 0) {
								graph.node_degree[startID
										- graph.minBoundaryNodeID]--;
							}
							if (graph.isStartIDLocate == true) {
								bool isC =
										::isFailedEndIDandNoFailedStartIDContinue(
												graph, startID, endID,
												auxiliarygraph);
								if (isC == false) {
									break;
								}
							} else {
								;
							}
						}
					}

				}


				if (isEndID_Failed == true) {
//					cout<<"case isEndID_Failed"<<endl;
					iscontinue = true;


					if (ishandle == false) {
						ishandle=true;
						if (isStartID_Failed == false) {
							iscontinue = true;
							if(graph.node_degree[startID - graph.minBoundaryNodeID]!=0){
							graph.node_degree[startID - graph.minBoundaryNodeID]--;}
							if (graph.isStartIDLocate == true) {
								bool isC =
										::isFailedEndIDandNoFailedStartIDContinue(
												graph, startID, endID,
												auxiliarygraph);
								if (isC == false) {
									break;
								}
							} else {
							}
						}
					}

				}
//				cout<<"after ---"<<endl;
//
//				if((startID - graph.minBoundaryNodeID)>=0){
//
//					cout<<"startID: "<<startID<<"  :degree is:"<<graph.node_degree[startID-graph.minBoundaryNodeID]<<endl;
//					}
//					if((endID - graph.minBoundaryNodeID)>=0){
//
//					cout<<"endID: "<<endID<<"  :degree is:"<<graph.node_degree[endID-graph.minBoundaryNodeID]<<endl;
//					}




			}
		}

		////////////////////

		//////////////////


      if(iscontinue==true){
    	  graph.edgeindex++;
//
//    	  cout<<"failedEdge  is findded"<<endl;
//			if ((startID - graph.minBoundaryNodeID) >= 0) {
//
//				cout << "startID: " << startID << "  :degree is:"
//						<< graph.node_degree[startID - graph.minBoundaryNodeID]
//						<< endl;
//			}
//			if ((endID - graph.minBoundaryNodeID) >= 0) {
//
//				cout << "endID: " << endID << "  :degree is:"
//						<< graph.node_degree[endID - graph.minBoundaryNodeID]
//						<< endl;
//			}
    	  continue;
      }





		if (iscontinue == false) {
			if (graph.isStartIDLocate == true ) {
				if (auxiliarygraph.startID_clusterSet_it->isSource == false
						and auxiliarygraph.startID_clusterSet_it->clusterSet.size()
								== 1) {

					if (graph.node_degree[startID-graph.minBoundaryNodeID] == 1) {
//						cout << "startID:" << startID << " will be removed"
//								<< endl;

						iscontinue = true;
//				isstartID_clusterfind=true;
						//remove the iter
						graph.mClusterBounaryList.erase(auxiliarygraph.startID_clusterSet_it);
						//locate the endit
						isstarIDRemove=true;
						if (graph.isEndIDLocate == false) {
//							::locateClusterNodeSetByTargetNodeIDV2(graph,
//									endID);
							 ::LocateclusterSet_itbyID19V6(graph, endID, false, auxiliarygraph);
						}


					}

				}
			}

			if (iscontinue == false) {

				if (graph.isEndIDLocate == true ) {

					if (auxiliarygraph.endID_clusterSet_it->isSource == false
							and auxiliarygraph.endID_clusterSet_it->clusterSet.size()
									== 1) {

						if (graph.node_degree[endID-graph.minBoundaryNodeID] == 1) {
//							cout << "endID:" << endID << " will be removed"
//									<< endl;

							iscontinue = true;
							isendIDRemove=true;

//				isendID_clusterfind=true;
							graph.mClusterBounaryList.erase(
									auxiliarygraph.endID_clusterSet_it);
                            if(graph.isStartIDLocate==false){
//							::locateClusterNodeSetByTargetNodeIDV2(graph, startID);
                            ::LocateclusterSet_itbyID19V6(graph, startID, true, auxiliarygraph);
                            }

						}

					}
				}
			}
		}

//		cout<<"step4:-------------------------"<<endl;
//		printClusterBounaryList(graph);




		if (iscontinue == true) {
			//no failed edge
			//remove the edge
//			deleteEdge(graph,startID, endID);
			graph.node_degree[startID-graph.minBoundaryNodeID]--;
			graph.node_degree[endID-graph.minBoundaryNodeID]--;
//			graph.edgeindex++;
//			continue;

			if(isstarIDRemove==true and graph.node_degree[endID-graph.minBoundaryNodeID]==0){

				if(auxiliarygraph.endID_clusterSet_it->isSource==true ){
					if(auxiliarygraph.endID_clusterSet_it->clusterSet.size()
							== 1){
						graph.mSClusterSize=-2;
						break;

					}else{

						clusterNodeSet new_cluster_set;

						new_cluster_set.isSource =true;
						new_cluster_set.clusterSet =
								auxiliarygraph.endID_clusterSet_it->clusterSet;
						//the branch edge is
						//removed the original
						new_cluster_set.clusterSet.erase(endID);
						graph.mClusterBounaryList.erase(
								auxiliarygraph.endID_clusterSet_it);
						//insert the old
						//					insertNewClusterV3(graph, new_cluster_set);
						insertNewCluster(graph, new_cluster_set);

					}


				} else {

					clusterNodeSet new_cluster_set;

					new_cluster_set.isSource = false;
					new_cluster_set.clusterSet =
							auxiliarygraph.endID_clusterSet_it->clusterSet;
					//the branch edge is
					//removed the original
					new_cluster_set.clusterSet.erase(endID);
					graph.mClusterBounaryList.erase(
							auxiliarygraph.endID_clusterSet_it);
					//insert the old
					//					insertNewClusterV3(graph, new_cluster_set);
					if (new_cluster_set.clusterSet.size() > 0) {
						insertNewCluster(graph, new_cluster_set);
					}

				}



			}



			if(isendIDRemove==true and graph.node_degree[startID-graph.minBoundaryNodeID]==0){

				if(auxiliarygraph.startID_clusterSet_it->isSource==true ){
					if(auxiliarygraph.startID_clusterSet_it->clusterSet.size()
							== 1){
						graph.mSClusterSize=-2;
						break;

					}else{

						clusterNodeSet new_cluster_set;

						new_cluster_set.isSource =true;
						new_cluster_set.clusterSet =
								auxiliarygraph.startID_clusterSet_it->clusterSet;
						//the branch edge is
						//removed the original
						new_cluster_set.clusterSet.erase(startID);
						graph.mClusterBounaryList.erase(
								auxiliarygraph.startID_clusterSet_it);
						//insert the old
						//					insertNewClusterV3(graph, new_cluster_set);
						insertNewCluster(graph, new_cluster_set);

					}


				} else {

					clusterNodeSet new_cluster_set;

					new_cluster_set.isSource = false;
					new_cluster_set.clusterSet =
							auxiliarygraph.startID_clusterSet_it->clusterSet;
					//the branch edge is
					//removed the original
					new_cluster_set.clusterSet.erase(startID);
					graph.mClusterBounaryList.erase(
							auxiliarygraph.startID_clusterSet_it);
					//insert the old
					//					insertNewClusterV3(graph, new_cluster_set);
					if (new_cluster_set.clusterSet.size() > 0) {
						insertNewCluster(graph, new_cluster_set);
					}

				}



			}


//			cout << "the rundacedEdge removed:" << startID << "----->" << endID
//					<< endl;
//			graph.EdgeList.pop_front();
			graph.edgeindex++;
			continue;

		} else {

			break;
		}

	}

//	cout<<"-------------------------------out removed_redundancy_loopEdgeV2"<<endl;

}


//
//bool removeFailedEndID(Graph &graph,int startID,int endID,auxiliaryGraph&auxiliarygraph,bool isEndID_rundant){
//	bool iscontinue = true;
//	//endID failed
//	if (graph.isStartIDLocate == true) {
//		if (isEndID_rundant==true){
//			graph.node_degree[startID-G.minBoundaryNodeID]--;
//			graph.node_degree[endID-G.minBoundaryNodeID]=0;
//		}
////					graph.node_degree[startID]--;
//
//		if (auxiliarygraph.startID_clusterSet_it->isSource
//				== false) {
//
//			if (graph.node_degree[startID-G.minBoundaryNodeID] == 0) {
//				if (auxiliarygraph.startID_clusterSet_it->clusterSet.size()
//						== 1) {
//
////									cout << "endID:" << endID << " will be removed"
////											<< endl;
//
//
//
//					G.mClusterBounaryList.erase(
//							auxiliarygraph.startID_clusterSet_it);
//
//				} else {
//
//					clusterNodeSet new_cluster_set;
//
//					new_cluster_set.isSource = false;
//					new_cluster_set.clusterSet =
//							auxiliarygraph.startID_clusterSet_it->clusterSet;
//					//the branch edge is
//					//removed the original
//					new_cluster_set.clusterSet.erase(
//							startID);
//					G.mClusterBounaryList.erase(
//							auxiliarygraph.startID_clusterSet_it);
//					//insert the old
//					//					insertNewClusterV3(graph, new_cluster_set);
////										if (new_cluster_set.clusterSet.size()
////												> 0) {
////											insertNewCluster(graph,
////													new_cluster_set);
////										}
//
//					///////////////////
//					if (new_cluster_set.clusterSet.size()
//							> 0) {
//						bool isInsert = true;
//						if (new_cluster_set.clusterSet.size()
//								== 1) {
//							set<int>::iterator remainedClusterit =
//									new_cluster_set.clusterSet.begin();
//							int rmID = *remainedClusterit;
//							if (graph.node_degree[rmID-
//									G.minBoundaryNodeID]
//									== 1) {
//								graph.node_degree[rmID
//										- G.minBoundaryNodeID] =
//										-1;
//
//								isInsert = false;
//
//							}
//
//						}
//
//						if (isInsert == true) {
//
//							insertNewCluster(graph,
//									new_cluster_set);
//						}
//					}
//
//
//
//
//
//					/////////////
//
//
//
//
//
//
//
//
//				}
//
//			}
//
//			//rundant edge
//			if (graph.node_degree[startID-G.minBoundaryNodeID] == 1) {
//
//				if (auxiliarygraph.startID_clusterSet_it->clusterSet.size()
//						== 1) {
//					graph.node_degree[startID-G.minBoundaryNodeID] = -1;
//
//					//							cout << "endID:" << endID << " will be removed"
//					//									<< endl;
//
//
//
//					G.mClusterBounaryList.erase(
//							auxiliarygraph.startID_clusterSet_it);
//
//				}
//			}
//
//		} else {
//			//source cluster situation
//			if (graph.node_degree[startID-G.minBoundaryNodeID] == 0) {
//				if (auxiliarygraph.startID_clusterSet_it->clusterSet.size()
//						== 1) {
//
//					//							cout << "endID:" << endID << " will be removed"
//					//									<< endl;
//					//true failed
//					iscontinue = false;
//					G.mSClusterSize = -2;
//
////
////								G.mClusterBounaryList.erase(
////										auxiliarygraph.startID_clusterSet_it);
//
//				} else {
//
//
//					clusterNodeSet new_cluster_set;
//
//					new_cluster_set.isSource = true;
//					new_cluster_set.clusterSet =
//							auxiliarygraph.startID_clusterSet_it->clusterSet;
//					//the branch edge is
//					//removed the original
//					new_cluster_set.clusterSet.erase(
//							startID);
//					G.mClusterBounaryList.erase(
//							auxiliarygraph.startID_clusterSet_it);
//					//insert the old
//					//					insertNewClusterV3(graph, new_cluster_set);
//					if (new_cluster_set.clusterSet.size()
//							> 0) {
//						insertNewCluster(graph,
//								new_cluster_set);
//					}
//
//				}
//
//			}
//
//		}
//
//	}
//
//	return iscontinue;
//
//}
//
//bool removeFailedStratID(Graph &graph,int startID,int endID,auxiliaryGraph&auxiliarygraph,bool isStartID_rundant){
//	bool iscontinue = true;
//	if (graph.isEndIDLocate == true) {
//		if (isStartID_rundant==true){
//			graph.node_degree[endID-G.minBoundaryNodeID]--;
//			graph.node_degree[startID-G.minBoundaryNodeID]=0;
//		}
//		//no source cluster
//		if (auxiliarygraph.endID_clusterSet_it->isSource
//				== false) {
//
//
//			if (graph.node_degree[endID-G.minBoundaryNodeID] == 0) {
//				if (auxiliarygraph.endID_clusterSet_it->clusterSet.size()
//						== 1) {
//
//					//							cout << "endID:" << endID << " will be removed"
//					//									<< endl;
//
//
//
//					G.mClusterBounaryList.erase(
//							auxiliarygraph.endID_clusterSet_it);
//
//				} else {
//
//					clusterNodeSet new_cluster_set;
//
//					new_cluster_set.isSource = false;
//					new_cluster_set.clusterSet =
//							auxiliarygraph.endID_clusterSet_it->clusterSet;
//					//the branch edge is
//					//removed the original
//					new_cluster_set.clusterSet.erase(endID);
//					G.mClusterBounaryList.erase(
//							auxiliarygraph.endID_clusterSet_it);
//					//insert the old
//					//					insertNewClusterV3(graph, new_cluster_set);
//					if (new_cluster_set.clusterSet.size()
//							> 0) {
//						bool isInsert = true;
//						if (new_cluster_set.clusterSet.size()
//								== 1) {
//
//							set<int>::iterator remainedClusterit =
//									new_cluster_set.clusterSet.begin();
//
////												cout<<"node:"<<*remainedClusterit<<"may be removed"<<endl;
//							int rmID = *remainedClusterit;
//
////												cout<<"degree:"<<graph.node_degree[rmID-G.minBoundaryNodeID]<<endl;
//
//							if (graph.node_degree[rmID -G.minBoundaryNodeID]
//									== 1) {
//								graph.node_degree[rmID
//										- G.minBoundaryNodeID] =
//										-1;
//
//								isInsert = false;
//
//							}
//
//						}
//
//						if (isInsert == true) {
//
//							insertNewCluster(graph,
//									new_cluster_set);
//						}
//					}
//
//				}
//
//			}
//
//			if (graph.node_degree[endID-G.minBoundaryNodeID] == 1) {
//
//				if (auxiliarygraph.endID_clusterSet_it->clusterSet.size()
//						== 1) {
//					graph.node_degree[endID-G.minBoundaryNodeID] = -1;
//
//					//							cout << "endID:" << endID << " will be removed"
//					//									<< endl;
//
//
//
//					G.mClusterBounaryList.erase(
//							auxiliarygraph.endID_clusterSet_it);
//
////										cout<<"node :"<<endID<<" is removed"<<endl;
////
////									   ::printClusterBounaryList_generalV2(graph);
//
//				}
//			}
//
//
//
//		} else {
//			//source cluster
//			if (graph.node_degree[endID-G.minBoundaryNodeID] == 0) {
//				if (auxiliarygraph.endID_clusterSet_it->clusterSet.size()
//						== 1) {
//
//					//true failed
//					iscontinue = false;
//					G.mSClusterSize = -2;
//
////
////								G.mClusterBounaryList.erase(
////										auxiliarygraph.endID_clusterSet_it);
//
//				} else {
//
//
//
//					clusterNodeSet new_cluster_set;
//
//					new_cluster_set.isSource = true;
//					new_cluster_set.clusterSet =
//							auxiliarygraph.endID_clusterSet_it->clusterSet;
//					//the branch edge is
//					//removed the original
//					new_cluster_set.clusterSet.erase(endID);
//					G.mClusterBounaryList.erase(
//							auxiliarygraph.endID_clusterSet_it);
//					//insert the old
//					//					insertNewClusterV3(graph, new_cluster_set);
//					if (new_cluster_set.clusterSet.size()
//							> 0) {
//						insertNewCluster(graph,
//								new_cluster_set);
//					}
//
//				}
//
//			}
//
//		}
//
//	}
//
//	return 	iscontinue;
//}
//

void degree_detection(Graph &graph){
	int degree=-1;
	int detection_degree=-1;
	for(int i=1;i<graph.node_degree.size();i++){
		degree=graph.node_degree[i];
		detection_degree=0;
		set<int> neibors = NeiborsOfNodeV2(graph, i);
//		cout<<"target node:"<<i<<endl;
//		cout<<"neibor size"<<neibors.size()<<endl;

		set<int>::iterator neiborit = neibors.begin();
		int targetneiborID = -1;
		for (; neiborit != neibors.end(); neiborit++) {
			//degree of neiborID minus
			targetneiborID = *neiborit;
//			cout<<"neibor:"<<targetneiborID<<"-->degree is:"<<graph.node_degree[targetneiborID]<<endl;
			if (graph.node_degree[targetneiborID] > 0) {
				detection_degree++;
			}
		}

		if(degree>0 and detection_degree==0){
//			cout<<"detection node:"<<i<<endl;
//			cout<<"confliction finded"<<endl;
			break;
		}


	}




}



bool isLocateclusterSet_it19V6(Graph &G, int startID, int endID,auxiliaryGraph &auxiliarygraph) {
	//locate startID_it
	bool islocate = false;
	G.isStartIDLocate = false;
	G.isEndIDLocate = false;

	list<clusterNodeSet>::iterator lsetit = G.mClusterBounaryList.begin();
	list<clusterNodeSet>::iterator lsetit_start;
	while (lsetit != G.mClusterBounaryList.end()) {

		if (islocate == false) {
			if (G.isStartIDLocate == false) {
				if (isContains(lsetit->clusterSet, startID) == true) {
					auxiliarygraph.startID_clusterSet_it = lsetit;
					G.isStartIDLocate = true;
				}
			}
			if (G.isEndIDLocate == false) {
				if (isContains(lsetit->clusterSet, endID) == true) {
					auxiliarygraph.endID_clusterSet_it = lsetit;
					G.isEndIDLocate = true;

				}
			}
			if (G.isEndIDLocate == true and G.isStartIDLocate == true) {
				if (auxiliarygraph.startID_clusterSet_it == auxiliarygraph.endID_clusterSet_it) {

					islocate = true;
				}
				break;
			}

		}


		++lsetit;
	}

	return islocate;
}




void LocateclusterSet_itbyID19V6(Graph &G, int ID, bool isstartID,
		auxiliaryGraph &auxiliarygraph) {
	//locate startID_it
	bool islocate = false;

	list<clusterNodeSet>::iterator lsetit = G.mClusterBounaryList.begin();
	list<clusterNodeSet>::iterator lsetit_start;

	if (isstartID == true) {
//		graph.isStartIDLocate = false;

		while (lsetit != G.mClusterBounaryList.end()) {

			if (islocate == false) {
				if (isContains(lsetit->clusterSet, ID) == true) {
					auxiliarygraph.startID_clusterSet_it = lsetit;
					islocate = true;
					break;
				}
			}

			++lsetit;
		}

		if (islocate == false) {
//			graph.isStartIDLocate=true;
			clusterNodeSet new_start;

			new_start.isSource = false;
			new_start.clusterSet.insert(ID);
			new_start.head=ID;
			if (::isContains(G.KterminalSet, ID) == true) {
				new_start.isSource = true;
//				graph.isHeadS = true;
				if (G.mSClusterSize == -1) {
					G.mSClusterSize = 1;
				} else {
					G.mSClusterSize++;
				}
				G.KterminalSet.erase(ID);
			}

			bool isinserted = false;



			list<clusterNodeSet>::iterator lit = find(
					G.mClusterBounaryList.begin(),
					G.mClusterBounaryList.end(), new_start);

			if (lit != G.mClusterBounaryList.end()) {
				G.mClusterBounaryList.insert(lit, new_start);
				lit--;
				auxiliarygraph.startID_clusterSet_it=lit;
			} else {

				G.mClusterBounaryList.push_back(new_start);
				lit = G.mClusterBounaryList.end();
				lit--;
				auxiliarygraph.startID_clusterSet_it=lit;
			}

		}

	} else {


		while (lsetit != G.mClusterBounaryList.end()) {

			if (islocate == false) {
				if (isContains(lsetit->clusterSet, ID) == true) {
					auxiliarygraph.endID_clusterSet_it = lsetit;
					islocate = true;
					break;
				}
			}

			++lsetit;
		}

		if (islocate == false) {
//			graph.isEndIDLocate=true;
			clusterNodeSet new_start;

			new_start.isSource = false;
			new_start.clusterSet.insert(ID);
			new_start.head=ID;
			if (::isContains(G.KterminalSet, ID) == true) {
				new_start.isSource = true;
//				graph.isHeadS = true;
				if (G.mSClusterSize == -1) {
					G.mSClusterSize = 1;
				} else {
					G.mSClusterSize++;
				}
				G.KterminalSet.erase(ID);
			}

			bool isinserted = false;

			list<clusterNodeSet>::iterator lit = find(
					G.mClusterBounaryList.begin(),
					G.mClusterBounaryList.end(), new_start);

			if (lit != G.mClusterBounaryList.end()) {
				G.mClusterBounaryList.insert(lit, new_start);
				lit--;
				auxiliarygraph.endID_clusterSet_it = lit;
			} else {

				G.mClusterBounaryList.push_back(new_start);
				lit = G.mClusterBounaryList.end();
				lit--;
				auxiliarygraph.endID_clusterSet_it = lit;
			}

		}

	}
}



bool isLocateclusterSet_itbyTargetID(Graph &G, int ID, auxiliaryGraph &auxiliarygraph)  {
	//locate startID_it
	bool islocate = false;

	list<clusterNodeSet>::iterator lsetit = G.mClusterBounaryList.begin();
	list<clusterNodeSet>::iterator lsetit_start;

//		graph.isStartIDLocate = false;

		while (lsetit != G.mClusterBounaryList.end()) {

			if (islocate == false) {
				if (isContains(lsetit->clusterSet, ID) == true) {
					auxiliarygraph.TargetID_clusterSet_it = lsetit;
					islocate = true;
					break;
				}
			}

			++lsetit;
		}

   return islocate;

}








void locateClusterNodeSetByGeneralEdge(Graph &graph,auxiliaryGraph &auxiliarygraph){

	int startID=graph.targetEdge.StartID;
	int endID=graph.targetEdge.EndID;
	bool isstartID_clusterfind=false;
	bool isendID_clusterfind=false;
//	cout<<"adress:"<<&auxiliarygraph<<endl;

	//locate startID_it

	list<clusterNodeSet>::iterator lsetit = graph.mClusterBounaryList.begin();
	while (lsetit != graph.mClusterBounaryList.end()) {

		if (isstartID_clusterfind == false) {

			if (isContains(lsetit->clusterSet, startID) == true) {

				isstartID_clusterfind = true;
				auxiliarygraph.startID_clusterSet_it=lsetit;

			}
		}

		if (isendID_clusterfind == false) {

			if (isContains(lsetit->clusterSet, endID) == true) {

				isendID_clusterfind = true;
				auxiliarygraph.endID_clusterSet_it=lsetit;
			}
		}

		if(isendID_clusterfind==true and isstartID_clusterfind==true){

			break;
		}

//		lsetit++;
		++lsetit;
	}

	if (startID == endID and isstartID_clusterfind==false) {

		clusterNodeSet new_start;

		new_start.isSource = false;
		new_start.clusterSet.insert(startID);
		new_start.head = startID;
		if (::isContains(graph.KterminalSet, startID) == true) {
			new_start.isSource = true;

			if (graph.mSClusterSize == -1) {
				graph.mSClusterSize = 1;
			} else {
				graph.mSClusterSize++;
			}
			graph.KterminalSet.erase(startID);
		}

		///insert and find

		list<clusterNodeSet>::iterator lit = find(
				graph.mClusterBounaryList.begin(),
				graph.mClusterBounaryList.end(), new_start);

		if (lit != graph.mClusterBounaryList.end()) {
			graph.mClusterBounaryList.insert(lit, new_start);
			lit--;
			auxiliarygraph.startID_clusterSet_it = lit;
			auxiliarygraph.endID_clusterSet_it=lit;
		} else {

			graph.mClusterBounaryList.push_back(new_start);
			lit = graph.mClusterBounaryList.end();
			lit--;
			auxiliarygraph.startID_clusterSet_it = lit;
			auxiliarygraph.endID_clusterSet_it=lit;
		}




	} else {

		if (isstartID_clusterfind == false) {
			clusterNodeSet new_start;

			new_start.isSource = false;
			new_start.clusterSet.insert(startID);
			new_start.head = startID;
			if (::isContains(graph.KterminalSet, startID) == true) {
				new_start.isSource = true;

				if (graph.mSClusterSize == -1) {
					graph.mSClusterSize = 1;
				} else {
					graph.mSClusterSize++;
				}
				graph.KterminalSet.erase(startID);
			}

			///insert and find

			list<clusterNodeSet>::iterator lit = find(
					graph.mClusterBounaryList.begin(),
					graph.mClusterBounaryList.end(), new_start);

			if (lit != graph.mClusterBounaryList.end()) {
				graph.mClusterBounaryList.insert(lit, new_start);
				lit--;
				auxiliarygraph.startID_clusterSet_it = lit;
			} else {

				graph.mClusterBounaryList.push_back(new_start);
				lit = graph.mClusterBounaryList.end();
				lit--;
				auxiliarygraph.startID_clusterSet_it = lit;
			}

		}

		if (isendID_clusterfind == false) {

			clusterNodeSet new_End;
			new_End.isSource = false;
			new_End.clusterSet.insert(endID);
			new_End.head = endID;

			if (::isContains(graph.KterminalSet, endID) == true) {
				new_End.isSource = true;

				if (graph.mSClusterSize == -1) {
					graph.mSClusterSize = 1;
				} else {
					graph.mSClusterSize++;
				}
				graph.KterminalSet.erase(endID);
			}

			list<clusterNodeSet>::iterator lit = find(
					graph.mClusterBounaryList.begin(),
					graph.mClusterBounaryList.end(), new_End);

			if (lit != graph.mClusterBounaryList.end()) {
				graph.mClusterBounaryList.insert(lit, new_End);
				lit--;
				auxiliarygraph.endID_clusterSet_it = lit;
			} else {

				graph.mClusterBounaryList.push_back(new_End);
				lit = graph.mClusterBounaryList.end();
				lit--;
				auxiliarygraph.endID_clusterSet_it = lit;
			}

		}
	}

//
//	cout<<"in location func"<<endl;
//	printStartIDCluster(graph);
//	printEndIDCluster(graph);
//	cout<<"-------------------"<<endl;



}




inline void insertNewCluster(Graph &G, clusterNodeSet &new_clusterSet) {

	//insert the new cluster

	set<int>::iterator newit = new_clusterSet.clusterSet.begin();
	new_clusterSet.head = *newit;

	list<clusterNodeSet>::iterator lit = find(G.mClusterBounaryList.begin(),
			G.mClusterBounaryList.end(),new_clusterSet);

	if(lit!=G.mClusterBounaryList.end()){
		G.mClusterBounaryList.insert(lit, new_clusterSet);
	}else{

		G.mClusterBounaryList.push_back(new_clusterSet);
	}

}


bool issingleStoT(Graph &graph, int &sID, int tID) {
//
//	cout<<"in issingleStoT"<<endl;
//	printSourceSet(graph);
//	printGraph(graph);

	bool isStoT = false;

//	if (graph.SourceSet.size() == 1) {
////
////		cout<<"in issingleStoT"<<endl;
//
//		set<int>::iterator sit = graph.SourceSet.begin();
//
//		if (NeiborsOfNode(graph, *sit).size() == 1) {
//
//			vector<int> neibors = NeiborsOfNode(graph, *sit);
//
//			vector<int>::iterator neiborit = neibors.begin();
////			cout<<"*sit"<<*sit<<endl;
////			cout<<"tID"<<tID<<endl;
////			cout<<"neiborit"<<*neiborit<<endl;
//			if (*neiborit == tID) {
//
////					cout << "is to T" << endl;
//
//				isStoT = true;
//				sID = *sit;
//			}
//
//		} else {
//
//			if (NeiborsOfNode(graph, tID).size() == 1) {
//
//				vector<int> neibors = NeiborsOfNode(graph, tID);
//				vector<int>::iterator neiborit = neibors.begin();
//
//				if (*neiborit == *sit) {
//
////					cout << "is to T" << endl;
//
//					isStoT = true;
//					sID = *sit;
//
//				}
//
//			}
//
//		}
//
//	}
//	cout<<"isStoT:  "<<isStoT<<endl;

	return isStoT;

}

bool issingleStoTV2(Graph &graph, int &sID, int tID) {
//
//	cout<<"in issingleStoT"<<endl;
//	printSourceSet(graph);
//	printGraph(graph);

	bool isStoT = false;

//	if (graph.SourceSet.size() == 1) {
////
////		cout<<"in issingleStoT"<<endl;
//
//		set<int>::iterator sit = graph.SourceSet.begin();
//
//		if (NeiborsOfNode(graph, *sit).size() == 1) {
//
//			vector<int> neibors = NeiborsOfNode(graph, *sit);
//
//			vector<int>::iterator neiborit = neibors.begin();
////			cout<<"*sit"<<*sit<<endl;
////			cout<<"tID"<<tID<<endl;
////			cout<<"neiborit"<<*neiborit<<endl;
//			if (*neiborit == tID) {
//
////					cout << "is to T" << endl;
//
//				isStoT = true;
//				sID = *sit;
//			}
//
//		}
//	} else {
//
//		if (NeiborsOfNode(graph, tID).size() == 1) {
//
//			vector<int> neibors = NeiborsOfNode(graph, tID);
//			vector<int>::iterator neiborit = neibors.begin();
//
//			if (isContains(graph.SourceSet, *neiborit) == true) {
//
//				isStoT = true;
//				sID = *neiborit;
//
//			}
//		}
//
//	}
//
////	cout<<"isStoT:  "<<isStoT<<endl;

	return isStoT;

}

bool isBranch(Graph &graph, int tID) {
	return true;
}


inline bool isBranchEdge_general(Graph &graph) {

//	Edge targetEdge=graph.EdgeList.front();
//	graph.EdgeList.pop_front();
//	int startID=targetEdge.StartID;
//	int endID=targetEdge.EndID;
//	graph.targetEdge.StartID=startID;
//	graph.targetEdge.EndID=endID;
//
//	EdgeProperties ep0 =graph.nodeToTargets[startID][endID];
//	graph.targetEdge.edgepro=ep0;
//
//	//remove the edge
//	deleteEdge(graph,startID, endID);
//	deleteEdge(graph,endID, startID);
//	cout<<"-------------in isBranchEdge_general---------"<<endl;

	bool isstartID_clusterfind = false;
	bool isendID_clusterfind = false;
	auxiliaryGraph auxiliarygraph;

	bool isBranch = false;

//        graph.Start_cluster_set.clusterSet.clear();
//        graph.Start_cluster_set.clusterSet.clear();
	Edge targetEdge = graph.vectorEdge[graph.edgeindex];

//		graph.EdgeList.pop_front();
	int startID = targetEdge.StartID;
	int endID = targetEdge.EndID;
	graph.targetEdge.StartID = startID;
	graph.targetEdge.EndID = endID;
	graph.targetEdge.relia = targetEdge.relia;

//		cout<<"temp edge:"<<"startID="<<startID<<endl;
//		cout<<"temp edge:"<<"endID="<<endID<<endl;

	///-----------------------locate-------------------------------
	::locateClusterNodeSetByGeneralEdge(graph, auxiliarygraph);

	//////////////////

//		locateClusterNodeSetByTargetEdge(graph);

	if(startID==endID and auxiliarygraph.startID_clusterSet_it->isSource == true){
		isBranch=true;
//		cout<<"node branch is finded"<<endl;
	}




	if (isBranch == false) {

		if (auxiliarygraph.startID_clusterSet_it->isSource == true
				and auxiliarygraph.startID_clusterSet_it->clusterSet.size()
						== 1) {

			if (graph.node_degree[startID-graph.minBoundaryNodeID] == 1) {

//				cout<<"edge branch finded"<<endl;
				bool istwot = false;

				isBranch = true;
//				isstartID_clusterfind=true;
				//remove the iter
				graph.mClusterBounaryList.erase(
						auxiliarygraph.startID_clusterSet_it);

				//deleteEdge(graph,startID, endID);
				graph.node_degree[startID-graph.minBoundaryNodeID]--;
				graph.node_degree[endID-graph.minBoundaryNodeID]--;
				////
//					graph.node_degree.erase(startID);
				///

				//update endID_clusterSet_it

				if (auxiliarygraph.endID_clusterSet_it->isSource == true) {
					graph.mSClusterSize--;
					istwot = true;
				} else {
					auxiliarygraph.endID_clusterSet_it->isSource = true;
				}

				if (graph.node_degree[endID-graph.minBoundaryNodeID] == 0 ) {
					if (istwot == false) {

						clusterNodeSet new_cluster_set;

						new_cluster_set.isSource =
								auxiliarygraph.endID_clusterSet_it->isSource;
						new_cluster_set.clusterSet =
								auxiliarygraph.endID_clusterSet_it->clusterSet;
						//the branch edge is
						//removed the original
						new_cluster_set.clusterSet.erase(endID);
						graph.mClusterBounaryList.erase(
								auxiliarygraph.endID_clusterSet_it);
						//insert the old
						//					insertNewClusterV3(graph, new_cluster_set);
						if (new_cluster_set.clusterSet.size() > 0) {
							insertNewCluster(graph, new_cluster_set);
						} else {

							graph.mSClusterSize = -2;
						}
					}else{

						if(graph.mSClusterSize==1 and graph.KterminalSet.size()==0){
							;

						}else{
							graph.mSClusterSize = -2;

						}





					}

				}

			}

		}

		if (isBranch == false) {

			if (auxiliarygraph.endID_clusterSet_it->isSource == true
					and auxiliarygraph.endID_clusterSet_it->clusterSet.size()
							== 1) {

				if (graph.node_degree[endID-graph.minBoundaryNodeID] == 1) {
//					cout<<"edge branch finded"<<endl;
					bool istwot = false;
					isBranch = true;
//				isendID_clusterfind=true;
					graph.mClusterBounaryList.erase(
							auxiliarygraph.endID_clusterSet_it);
//						deleteEdge(graph,startID, endID);
					graph.node_degree[startID-graph.minBoundaryNodeID]--;
					graph.node_degree[endID-graph.minBoundaryNodeID]--;
					///

					//update startID_clusterSet_it

					if (auxiliarygraph.startID_clusterSet_it->isSource== true) {
						graph.mSClusterSize--;
						istwot = true;
					} else {
						auxiliarygraph.startID_clusterSet_it->isSource = true;
					}

					if (graph.node_degree[startID-graph.minBoundaryNodeID] == 0 ) {

						if (istwot == false) {

							clusterNodeSet new_cluster_set;

							new_cluster_set.isSource =
									auxiliarygraph.startID_clusterSet_it->isSource;
							new_cluster_set.clusterSet =
									auxiliarygraph.startID_clusterSet_it->clusterSet;
							//the branch edge is
							//removed the original
							new_cluster_set.clusterSet.erase(startID);
							graph.mClusterBounaryList.erase(
									auxiliarygraph.startID_clusterSet_it);
							//insert the old
							//					insertNewClusterV3(graph, new_cluster_set);
							if (new_cluster_set.clusterSet.size() > 0) {
								insertNewCluster(graph, new_cluster_set);
							} else {
								graph.mSClusterSize = -2;
							}
						}else{

							if (graph.mSClusterSize == 1
									and graph.KterminalSet.size() == 0) {
								;

							} else {
								graph.mSClusterSize = -2;

							}



						}

					}

				}

			}
		}
	}

	if (isBranch == true) {

//			graph.EdgeList.pop_front();
		graph.edgeindex++;

	}else{

//		if(graph.SClusterSize!=-2){
//
//			if(startID==endID and graph.isStartIDLocate==false){
//
//				graph.mClusterBounaryList.erase(auxiliarygraph.startID_clusterSet_it);
//				graph.edgeindex--;
//
//			}
//
//			if(startID!=endID){
//
//				if(graph.isStartIDLocate==false){
//
//					graph.mClusterBounaryList.erase(auxiliarygraph.startID_clusterSet_it);
//					graph.edgeindex--;
//
//
//				}
//
//				if (graph.isEndIDLocate == false) {
//
//					graph.mClusterBounaryList.erase(
//							auxiliarygraph.endID_clusterSet_it);
//					graph.edgeindex--;
//
//				}
//
//			}
//
//
//
//		}


	}

	return isBranch;

}





void SourceNodeTrue(Graph &graph) {
	//add endIDcluster to SourceSet
//	int ClusterSumBasedKey = graph.pHeadClusterVector.size();
//
//	if (ClusterSumBasedKey == 1) {
//
//		for (int index = 0; index < graph.pHeadClusterVector.size();
//				index++) {
//			set<int>::iterator sit =
//					graph.pHeadClusterVector[index]->clusterSet.begin();
//			for (; sit != graph.pHeadClusterVector[index]->clusterSet.end();
//					sit++) {
//
//				graph.SourceSet.insert(*sit);
//			}
//
//		}
//	} else {
//
//		clusterNodeSet Newstart_clusterSet;
//		Newstart_clusterSet.isSource = true;
//
//		for (int index = 0; index < graph.pHeadClusterVector.size();
//				index++) {
//			set<int>::iterator sit =
//					graph.pHeadClusterVector[index]->clusterSet.begin();
//			for (; sit != graph.pHeadClusterVector[index]->clusterSet.end();
//					sit++) {
//
//				graph.SourceSet.insert(*sit);
//				Newstart_clusterSet.clusterSet.insert(*sit);
//				//remove it
//
//			}
//			G.mClusterBounaryList.erase(graph.pHeadClusterVector[index]);
//
//		}
//
//		//insert the new
//
//		if (Newstart_clusterSet.clusterSet.size() > 0) {
//
//			insertNewCluster(graph, Newstart_clusterSet,G);
//		}
//
//	}

}

void AddClusterIDtoSourceSet(Graph &graph,
		vector<list<clusterNodeSet>::iterator> pAddClusterVector) {
//
//	for (int index = 0; index < pAddClusterVector.size(); index++) {
//		set<int>::iterator sit = pAddClusterVector[index]->clusterSet.begin();
//		for (; sit != pAddClusterVector[index]->clusterSet.end(); sit++) {
//
//			graph.SourceSet.insert(*sit);
//
//		}
//		//remove it from ClusterBounaryList
//		G.mClusterBounaryList.erase(pAddClusterVector[index]);
//
//	}

}

void VarGeneralEdgeTrueFromSoure(Graph &graph, int startID, int endID) {
	//preconditions are rundant edges are removed

//	graph.SourceSet.insert(endID);
//				//if end isvistied ,add its cluster to SourceSet
//				{

	//if startID from source
//	if (::isContains(graph.SourceSet, startID)) {
//		/*
//		 * startID from source
//		 *
//		 */
//		if (::isContains(graph.VisitedSet, startID)) {
//			//startID is visited
//			if (graph.node_degree[startID] == 0) {
//				/*
//				 * node degree ===0
//				 */
//				//removed it from SourceSet and VisitedSet
//				graph.SourceSet.erase(startID);
//				graph.VisitedSet.erase(startID);
//				//add endID-->SourceSet
//
//			}
//
//			graph.SourceSet.insert(endID);
//
//			//if
//			if (::isContains(graph.VisitedSet, endID) == true) {
//				//add endidCluster to SourceSet
//
//				AddClusterIDtoSourceSet(graph, graph.pTailClusterVector);
//
//			}
//
//		} else {
//			//startID is not visited ,means no percolation
//			//only clusterset [startID,endID] is created
//			clusterNodeSet Newstart_clusterSet;
//			Newstart_clusterSet.isSource = true;
//			Newstart_clusterSet.clusterSet.insert(endID);
//			Newstart_clusterSet.clusterSet.insert(startID);
//			//insert it to ClusterBounaryList
//			insertNewCluster(graph, Newstart_clusterSet,G);
//		}
//
//	} else {
//		/*
//		 * endID from source
//		 *
//		 */
//		if (::isContains(graph.VisitedSet, endID)) {
//			//startID is visited
//			if (graph.node_degree[endID] == 0) {
//				/*
//				 * node degree ===0
//				 */
//				//removed it from SourceSet and VisitedSet
//				graph.SourceSet.erase(endID);
//				graph.VisitedSet.erase(endID);
//				//add endID-->SourceSet
//
//			}
//
//			graph.SourceSet.insert(startID);
//
//			//if
//			if (::isContains(graph.VisitedSet, startID) == true) {
//				//add endidCluster to SourceSet
//
//				AddClusterIDtoSourceSet(graph, graph.pHeadClusterVector);
//
//			}
//
//		} else {
//			//startID is not visited ,means no percolation
//			//only clusterset [startID,endID] is created
//			clusterNodeSet Newstart_clusterSet;
//			Newstart_clusterSet.isSource = true;
//			Newstart_clusterSet.clusterSet.insert(endID);
//			Newstart_clusterSet.clusterSet.insert(startID);
//			//insert it to ClusterBounaryList
//			insertNewCluster(graph, Newstart_clusterSet,G);
//		}
//
//	}

}

void VarGeneralEdgeTrue(Graph &graph, int startID, int endID) {
	//preconditions are rundant edges are removed

//	if (::isContains(graph.VisitedSet, startID)) {
//		///  endID--->startID_CLuster
//		//if startID from source
//		if (::isContains(graph.SourceSet, startID)) {
//
//			if (graph.node_degree[startID] == 0) {
//				/*
//				 * node degree ===0
//				 */
//				//removed it from SourceSet and VisitedSet
//				graph.SourceSet.erase(startID);
//				graph.VisitedSet.erase(startID);
//
//			}
//			//add endID-->SourceSet
//			graph.SourceSet.insert(endID);
//
//		} else {
//			//if startID from no source cluster
//
//			if (graph.node_degree[startID] == 0) {
//				/*
//				 * node degree ===0
//				 */
//				graph.VisitedSet.erase(startID);
//				graph.pHeadClusterVector[0]->clusterSet.erase(startID);
//
//			}
//
//			//add merger new
//			clusterNodeSet Newstart_clusterSet;
//			Newstart_clusterSet.clusterSet =
//					graph.pHeadClusterVector[0]->clusterSet;
//			Newstart_clusterSet.clusterSet.insert(endID);
//			//erase the old
//			G.mClusterBounaryList.erase(graph.pHeadClusterVector[0]);
//			//insert it to ClusterBounaryList
//			insertNewCluster(graph, Newstart_clusterSet,G);
//		}
//		//
//
//	}
//
//	if (::isContains(graph.VisitedSet, endID)) {
//		///  startID--->endID_CLuster
//		//if startID from source
//		if (::isContains(graph.SourceSet, endID)) {
//
//			if (graph.node_degree[endID] == 0) {
//				/*
//				 * node degree ===0
//				 */
//				//removed it from SourceSet and VisitedSet
//				graph.SourceSet.erase(endID);
//				graph.VisitedSet.erase(endID);
//
//			}
//			//add startID-->SourceSet
//			graph.SourceSet.insert(startID);
//
//		} else {
//			//if endID from no source cluster
//
//			if (graph.node_degree[endID] == 0) {
//				/*
//				 * node degree ===0
//				 */
//				graph.VisitedSet.erase(endID);
//				graph.pTailClusterVector[0]->clusterSet.erase(endID);
//
//			}
//
//			//add merger new
//
////			graph.pHeadClusterVector
////			graph.pTailClusterVector
//			clusterNodeSet Newstart_clusterSet;
//			Newstart_clusterSet.clusterSet =
//					graph.pTailClusterVector[0]->clusterSet;
//			Newstart_clusterSet.clusterSet.insert(startID);
//			//erase the old
//			G.mClusterBounaryList.erase(graph.pTailClusterVector[0]);
//			//insert it to ClusterBounaryList
//			insertNewCluster(graph, Newstart_clusterSet,G);
//		}
//		//
//
//	}

}
//
//void VarGeneralEdgeTrueV2(Graph &graph, int startID, int endID) {
//	//preconditions are rundant edges are removed
//
//	if (::isContains(graph.VisitedSet, startID)) {
//		///  endID--->startID_CLuster
//		//if startID from source
//		//if startID from no source cluster
//
//		if (graph.node_degree[startID] == 0) {
//			/*
//			 * node degree ===0	 */
//
//			graph.VisitedSet.erase(startID);
//			graph.KterminalSet.erase(startID);
//			graph.pHeadClusterVector[0]->clusterSet.erase(startID);
//
//		}
//
//		//add merger new
//		clusterNodeSet Newstart_clusterSet;
//		Newstart_clusterSet.isSource = graph.pHeadClusterVector[0]->isSource;
//		Newstart_clusterSet.clusterSet =
//				graph.pHeadClusterVector[0]->clusterSet;
//		Newstart_clusterSet.clusterSet.insert(endID);
//		//erase the old
//		G.mClusterBounaryList.erase(graph.pHeadClusterVector[0]);
//		//is endcluster removed
//		bool isendclusterRemoved = false;
//		if (graph.pTailClusterVector.size() == 1
//				and graph.pTailClusterVector[0]->clusterSet.size() == 1) {
//			if (graph.pTailClusterVector[0]->isSource == false) {
//				G.mClusterBounaryList.erase(graph.pTailClusterVector[0]);
//				isendclusterRemoved = true;
//			}
//
//			if (graph.pTailClusterVector[0]->isSource == true
//					and Newstart_clusterSet.isSource == true) {
//				G.mClusterBounaryList.erase(graph.pTailClusterVector[0]);
//				isendclusterRemoved = true;
//			}
//
//		}
//
//		//compare the newcluster to the graph.pTailClusterVector
//		int index = 0;
//		int result = -1;
//		if (isendclusterRemoved == false) {
//			for (; index < graph.pTailClusterVector.size(); index++) {
//				if (Newstart_clusterSet.isSource
//						== graph.pTailClusterVector[0]->isSource) {
//					result = isSetInclusion(Newstart_clusterSet.clusterSet,
//							graph.pTailClusterVector[index]->clusterSet);
//					if (result == 1 or result == 0 or result == 2) {
//						break;
//					}
//				}
//
//			}
//		}
//
//		if (result == 0) {
//			// A==B,no insert Newstart_clusterSet
//			;
//		}
//		if (result == 1) {
//			//A contains B, A inserted and B removed
//			G.mClusterBounaryList.erase(graph.pTailClusterVector[index]);
//			insertNewClusterV3(graph, Newstart_clusterSet);
//
//		}
//
//		if (result == 2) {
//			//B contains A, A no insert
//			;
//
//		}
//
//		if (result == -1) {
//			// A no B
//			//insert A
//			insertNewClusterV3(graph, Newstart_clusterSet);
//
//		}
//		//
//
//	}
//
//}
//
//


void printclusterNodeSet(clusterNodeSet cs){
    cout<<"the new clusterNodeSet is as follows:"<<endl;
	set<int>::iterator cit=cs.clusterSet.begin();
	if(cs.isSource==true){
		cout<<"[*";
	}
	else{
		cout <<"[";
	}
	for(;cit!=cs.clusterSet.end();cit++){
		cout<<"item:"<<*cit<<"--";
	}
	cout<<"]"<<endl;
}





//
//void VarGeneralEdgeTrueV3(Graph &graph, int startID, int endID) {
//	//preconditions are rundant edges are removed
//
//	if (::isContains(graph.VisitedSet, startID)) {
//		///  endID--->startID_CLuster
//		//if startID from source
//		//if startID from no source cluster
//
//		if (graph.node_degree[startID] == 0) {
//			/*
//			 * node degree ===0	 */
//
//			graph.VisitedSet.erase(startID);
//			graph.KterminalSet.erase(startID);
//			graph.pHeadClusterVector[0]->clusterSet.erase(startID);
//			cout << "node is removed:" << startID << endl;
//		}
//
//
//		//add merger new
//		clusterNodeSet Newstart_clusterSet;
//		Newstart_clusterSet.isSource = graph.pHeadClusterVector[0]->isSource;
//		Newstart_clusterSet.clusterSet =
//				graph.pHeadClusterVector[0]->clusterSet;
//		Newstart_clusterSet.clusterSet.insert(endID);
//
//        ::printclusterNodeSet(Newstart_clusterSet);
//		//erase the old
//		G.mClusterBounaryList.erase(graph.pHeadClusterVector[0]);
//		//is endcluster removed
//
////		bool isendclusterRemoved=false;
////
////		if(graph.pTailClusterVector.size()==1 and graph.pTailClusterVector[0]->clusterSet.size()==1
////				){
////			if(graph.pTailClusterVector[0]->isSource==false){
////				G.mClusterBounaryList.erase(graph.pTailClusterVector[0]);
////				isendclusterRemoved=true;
////			}
////
////			if (graph.pTailClusterVector[0]->isSource == true and Newstart_clusterSet.isSource==true) {
////				G.mClusterBounaryList.erase(graph.pTailClusterVector[0]);
////				isendclusterRemoved = true;
////			}
////
////		}
//
//		//compare the newcluster to the graph.pTailClusterVector
//		int index = 0;
//		int result = -1;
////		cout << "Newstart_clusterSet.isSource::" << Newstart_clusterSet.isSource
////				<< endl;
//		if (Newstart_clusterSet.isSource == true) {
//			for (; index < graph.pTailClusterVector.size(); index++) {
//
//				result = isSetInclusion(Newstart_clusterSet.clusterSet,
//						graph.pTailClusterVector[index]->clusterSet);
//				if (result == 1 or result == 0 or result==2) {
//					break;
//				}
//			}
//
//			cout<<"result===="<<result<<endl;
//			if (result == 0 or result == 1 or result==2) {
//				cout << "[index]->isSource :"
//						<< graph.pTailClusterVector[index]->isSource << endl;
//			}
//
//			if (result == 1) {
//
//
//				//removed
//				if (graph.pTailClusterVector[index]->isSource == true) {
//					;
////					G.mSClusterSize--;
////					G.mClusterBounaryList.erase(
////							graph.pTailClusterVector[index]);
//					insertNewClusterV3(graph, Newstart_clusterSet);
//				} else {
//					G.mClusterBounaryList.erase(
//							graph.pTailClusterVector[index]);
//					insertNewClusterV3(graph, Newstart_clusterSet);
//				}
//			}
//
//			if (result == 0) {
//
//				//removed
//				if (graph.pTailClusterVector[index]->isSource == true) {
//					G.mSClusterSize--;
//				} else {
//					G.mClusterBounaryList.erase(
//							graph.pTailClusterVector[index]);
//					insertNewClusterV3(graph, Newstart_clusterSet);
//				}
//			}
//
//			if (result == 2) {
//
//				//removed
//				if (graph.pTailClusterVector[index]->isSource == true) {
//					insertNewClusterV3(graph, Newstart_clusterSet);
//				} else {
//					insertNewClusterV3(graph, Newstart_clusterSet);
//				}
//			}
//
//
//			cout<<"SClusterSize="<<G.mSClusterSize<<endl;
//			printClusterBounaryList_generalV2(graph);
//			if (result == -1) {
//				//removed
//				insertNewClusterV3(graph, Newstart_clusterSet);
//			}
//
//		} else {
//			for (; index < graph.pTailClusterVector.size(); index++) {
//
//				if (graph.pTailClusterVector[index]->isSource == false) {
//					result = isSetInclusion(Newstart_clusterSet.clusterSet,
//							graph.pTailClusterVector[index]->clusterSet);
//					if (result == 1 or result == 0 or result == 2) {
//						break;
//					}
//				} else {
//
//					result = isSetInclusion(Newstart_clusterSet.clusterSet,
//							graph.pTailClusterVector[index]->clusterSet);
//					if (result == 0 or result == 2) {
//						break;
//					}else if(result==1){
//						result=-1;
//						continue;
//					}
//				}
//			}
//
//			if (result == 0) {
//				// A==B,no insert Newstart_clusterSet
//
//				;
//			}
//			if (result == 1) {
//				//A contains B, A inserted and B removed
//				G.mClusterBounaryList.erase(
//						graph.pTailClusterVector[index]);
//				insertNewClusterV3(graph, Newstart_clusterSet);
//
//			}
//
//			if (result == 2) {
//				//B contains A, A no insert
//				;
//
//			}
//
//			if (result == -1) {
//				// A no B
//				//insert A
//				insertNewClusterV3(graph, Newstart_clusterSet);
//
//			}
//			//
//
//		}
//
//	}
//
////	::printClusterBounaryList(graph);
//
//}
//


//
//inline void VarGeneralEdgeTrue_TwoS(Graph &graph, int startID, int endID) {
//	//preconditions are rundant edges are removed
////	cout << "two S combined" << endl;
//	clusterNodeSet Newstart_clusterSet;
//	Newstart_clusterSet.isSource = true;
//	Newstart_clusterSet.clusterSet = graph.startID_clusterSet_it->clusterSet;
//	Newstart_clusterSet.clusterSet.insert(endID);
//
//	if (graph.node_degree[startID] == 0) {
//		/*
//		 * node degree ===0	 */
//
//		Newstart_clusterSet.clusterSet.erase(startID);
//		graph.startID_clusterSet_it->clusterSet.erase(startID);
////		cout << "node is removed:" << startID << endl;
////			G.mSClusterSize--;
//	}
//
//	if (graph.node_degree[endID] == 0) {
//		/*
//		 * node degree ===0	 */
//
//		graph.endID_clusterSet_it->clusterSet.erase(endID);
//		Newstart_clusterSet.clusterSet.erase(endID);
////		cout << "node is removed:" << endID << endl;
////			G.mSClusterSize--;
//	}
//
//	//add merger new
//
//	set<int>::iterator tailIt = graph.endID_clusterSet_it->clusterSet.begin();
//	for (; tailIt != graph.endID_clusterSet_it->clusterSet.end(); tailIt++) {
//		Newstart_clusterSet.clusterSet.insert(*tailIt);
//	}
//
////	::printclusterNodeSet(Newstart_clusterSet);
//	//erase the old
//	G.mClusterBounaryList.erase(graph.startID_clusterSet_it);
//	G.mClusterBounaryList.erase(graph.endID_clusterSet_it);
//	//insert the new
//	if (Newstart_clusterSet.clusterSet.size() > 0) {
//		insertNewCluster(graph, Newstart_clusterSet,G);
//	}
//	G.mSClusterSize--;
//
////	::printClusterBounaryList(graph);
//
//}
//

inline void VarGeneralEdgeTrue_TwoS19V6(Graph &graph, int startID, int endID,auxiliaryGraph &auxiliarygraph) {
	//preconditions are rundant edges are removed
//	cout << "two S combined" << endl;
	clusterNodeSet Newstart_clusterSet;
	Newstart_clusterSet.isSource = true;
	Newstart_clusterSet.clusterSet = auxiliarygraph.startID_clusterSet_it->clusterSet;
	Newstart_clusterSet.clusterSet.insert(endID);

	if (graph.node_degree[startID-graph.minBoundaryNodeID] == 0) {
		/*
		 * node degree ===0	 */

		Newstart_clusterSet.clusterSet.erase(startID);
		auxiliarygraph.startID_clusterSet_it->clusterSet.erase(startID);
//		cout << "node is removed:" << startID << endl;
//			graph.SClusterSize--;
	}

	if (graph.node_degree[endID-graph.minBoundaryNodeID] == 0) {
		/*
		 * node degree ===0	 */

		auxiliarygraph.endID_clusterSet_it->clusterSet.erase(endID);
		Newstart_clusterSet.clusterSet.erase(endID);
//		cout << "node is removed:" << endID << endl;
//			graph.SClusterSize--;
	}

	//add merger new

	set<int>::iterator tailIt = auxiliarygraph.endID_clusterSet_it->clusterSet.begin();
	for (; tailIt != auxiliarygraph.endID_clusterSet_it->clusterSet.end(); tailIt++) {
		Newstart_clusterSet.clusterSet.insert(*tailIt);
	}

//	::printclusterNodeSet(Newstart_clusterSet);
	//erase the old
	graph.mClusterBounaryList.erase(auxiliarygraph.startID_clusterSet_it);
	graph.mClusterBounaryList.erase(auxiliarygraph.endID_clusterSet_it);
	//insert the new
	if (Newstart_clusterSet.clusterSet.size() > 0) {
		insertNewCluster(graph, Newstart_clusterSet);
	}
	graph.mSClusterSize--;

//	::printClusterBounaryList(graph);

}



//
//
//void VarGeneralEdgeTrue_OneS(Graph &graph, int startID, int endID) {
//	//preconditions are rundant edges are removed
//    cout<<"VarGeneralEdgeTrue_OneS"<<endl;
//
//	if(graph.isHeadS==true){
//
//        cout<<"heads is:"<<startID<<endl;
//		if (graph.node_degree[startID] == 0) {
//			/*
//			 * node degree ===0	 */
//
//			graph.VisitedSet.erase(startID);
//			graph.KterminalSet.erase(startID);
//			graph.pHeadClusterVector[0]->clusterSet.erase(startID);
//			cout << "node is removed:" << startID << endl;
//		}
//
//
//		//add merger new
//		clusterNodeSet Newstart_clusterSet;
//		Newstart_clusterSet.isSource =true;
//		Newstart_clusterSet.clusterSet =
//				graph.pHeadClusterVector[0]->clusterSet;
//		Newstart_clusterSet.clusterSet.insert(endID);
//
//        ::printclusterNodeSet(Newstart_clusterSet);
//		//erase the old
//        cout<<"removed the pHeadClusterVector  "<<endl;
//        cout<<"size of graph.pHeadClusterVector:"<<graph.pHeadClusterVector.size()<<endl;
//		G.mClusterBounaryList.erase(graph.pHeadClusterVector[0]);
//		//is endcluster removed
//		cout<<"after removed the pHeadClusterVector"<<endl;
//		printClusterBounaryList_generalV2(graph);
//
//		//compare the newcluster to the graph.pTailClusterVector
//		int index = 0;
//		int result = -1;
////		cout << "Newstart_clusterSet.isSource::" << Newstart_clusterSet.isSource
////				<< endl;
//		if (Newstart_clusterSet.isSource == true) {
//			for (; index < graph.pTailClusterVector.size(); index++) {
//
//				result = isSetInclusion(Newstart_clusterSet.clusterSet,
//						graph.pTailClusterVector[index]->clusterSet);
//				if (result == 1 or result == 0 or result==2) {
//					break;
//				}
//			}
//
//			cout<<"result===="<<result<<endl;
//			if (result == 0 or result == 1 or result==2) {
//				cout << "[index]->isSource :"
//						<< graph.pTailClusterVector[index]->isSource << endl;
//			}
//
//			if (result == 1) {
//
//				//removed
//				if (graph.pTailClusterVector[index]->isSource == false) {
//					G.mClusterBounaryList.erase(
//							graph.pTailClusterVector[index]);
//				}
//				insertNewClusterV3(graph, Newstart_clusterSet);
//
//			}
//
//			if (result == 0) {
//
//				//removed
//				if (graph.pTailClusterVector[index]->isSource == false) {
//					graph.pTailClusterVector[index]->isSource=true;
////					G.mClusterBounaryList.erase(
////							graph.pTailClusterVector[index]);
//				}else{
//					G.mSClusterSize--;
//				}
////				insertNewClusterV3(graph, Newstart_clusterSet);
//
//			}
//
//			if (result == 2) {
//
//				//removed
//
//				insertNewClusterV3(graph, Newstart_clusterSet);
//
//			}
//
//
//			cout<<"SClusterSize="<<G.mSClusterSize<<endl;
//			printClusterBounaryList_generalV2(graph);
//			if (result == -1) {
//				//removed
//				insertNewClusterV3(graph, Newstart_clusterSet);
//			}
//
//		}
//
//
//	}
//
//	if (graph.isTailS == true) {
//		cout<<"TTTTTAIIIItails is:"<<endID<<endl;
//
//		if (graph.node_degree[endID] == 0) {
//			/*
//			 * node degree ===0	 */
//
//			graph.VisitedSet.erase(endID);
//			graph.KterminalSet.erase(endID);
//			graph.pTailClusterVector[0]->clusterSet.erase(endID);
//			cout << "node is removed:" << endID << endl;
//		}
//
//
//
//		//add merger new
//		clusterNodeSet Newstart_clusterSet;
//		Newstart_clusterSet.isSource = true;
//		Newstart_clusterSet.clusterSet =
//				graph.pTailClusterVector[0]->clusterSet;
//
//		Newstart_clusterSet.clusterSet.insert(startID);
//
//		if (::isContains(graph.VisitedSet, startID) == true) {
//			set<int>::iterator headit =
//					graph.pHeadClusterVector[0]->clusterSet.begin();
//			for (; headit != graph.pHeadClusterVector[0]->clusterSet.end();
//					headit++) {
//				Newstart_clusterSet.clusterSet.insert(*headit);
//
//			}
//
//		}
//
//
//
//
//		if (graph.node_degree[startID] == 0) {
//			Newstart_clusterSet.clusterSet.erase(startID);
//			graph.VisitedSet.erase(startID);
//			cout << "node is removed:" << startID << endl;
//
//		}
//
//
//
//		::printclusterNodeSet(Newstart_clusterSet);
//		//erase the old
//		G.mClusterBounaryList.erase(graph.pTailClusterVector[0]);
//		//is endcluster removed
//
//		//		bool isendclusterRemoved=false;
//		//
//		//		if(graph.pTailClusterVector.size()==1 and graph.pTailClusterVector[0]->clusterSet.size()==1
//		//				){
//		//			if(graph.pTailClusterVector[0]->isSource==false){
//		//				G.mClusterBounaryList.erase(graph.pTailClusterVector[0]);
//		//				isendclusterRemoved=true;
//		//			}
//		//
//		//			if (graph.pTailClusterVector[0]->isSource == true and Newstart_clusterSet.isSource==true) {
//		//				G.mClusterBounaryList.erase(graph.pTailClusterVector[0]);
//		//				isendclusterRemoved = true;
//		//			}
//		//
//		//		}
//
//		//compare the newcluster to the graph.pTailClusterVector
//		int index = 0;
//		int result = -1;
//		//		cout << "Newstart_clusterSet.isSource::" << Newstart_clusterSet.isSource
//		//				<< endl;
//		if (Newstart_clusterSet.isSource == true) {
//			for (; index < graph.pHeadClusterVector.size(); index++) {
//
//				result = isSetInclusion(Newstart_clusterSet.clusterSet,
//						graph.pHeadClusterVector[index]->clusterSet);
//				if (result == 1 or result == 0 or result == 2) {
//					break;
//				}
//			}
//
//			cout << "result====" << result << endl;
//			if (result == 0 or result == 1 or result == 2) {
//				cout << "[index]->isSource :"
//						<< graph.pHeadClusterVector[index]->isSource << endl;
//			}
//
//			if (result == 1) {
//
//				//removed
//				if (graph.pHeadClusterVector[index]->isSource == false) {
//					G.mClusterBounaryList.erase(
//							graph.pHeadClusterVector[index]);
//				}
//				insertNewClusterV3(graph, Newstart_clusterSet);
//
//			}
//
//			if (result == 0) {
//
//				//removed
//				if (graph.pHeadClusterVector[index]->isSource == false) {
//					graph.pHeadClusterVector[index]->isSource = true;
//				}else{
//					G.mSClusterSize--;
//				}
//
////				G.mClusterBounaryList.erase(graph.pHeadClusterVector[index]);
////				insertNewClusterV3(graph, Newstart_clusterSet);
//
//			}
//
//			if (result == 2) {
//
//				//removed
//
//				insertNewClusterV3(graph, Newstart_clusterSet);
//
//			}
//
//			cout << "SClusterSize=" << G.mSClusterSize << endl;
//			printClusterBounaryList_generalV2(graph);
//			if (result == -1) {
//				//removed
//				insertNewClusterV3(graph, Newstart_clusterSet);
//			}
//
//		}
//
//	}
//
//
//
//
////	::printClusterBounaryList(graph);
//
//}
//
//
//
//
//inline void VarGeneralEdgeTrue_OneSV2(Graph &graph, int startID, int endID) {
//	//preconditions are rundant edges are removed
////	cout << "VarGeneralEdgeTrue_OneS" << endl;
//
//	if (graph.node_degree[startID] == 0) {
//		/*
//		 * node degree ===0	 */
//
//		graph.startID_clusterSet_it->clusterSet.erase(startID);
////		cout << "node is removed:" << startID << endl;
//	}
//
//	if (graph.node_degree[endID] == 0) {
//		/*
//		 * node degree ===0	 */
//
//
//		graph.endID_clusterSet_it->clusterSet.erase(endID);
////		cout << "node is removed:" << startID << endl;
//	}
//
//	//add merger new
//	clusterNodeSet Newstart_clusterSet;
//	Newstart_clusterSet.isSource = true;
//	Newstart_clusterSet.clusterSet = graph.startID_clusterSet_it->clusterSet;
//
//	//add tail element
//	set<int>::iterator tailit = graph.endID_clusterSet_it->clusterSet.begin();
//	for (; tailit != graph.endID_clusterSet_it->clusterSet.end(); tailit++) {
//		Newstart_clusterSet.clusterSet.insert(*tailit);
//
//	}
//
////	::printclusterNodeSet(Newstart_clusterSet);
//
//	//erase the old
//	G.mClusterBounaryList.erase(graph.startID_clusterSet_it);
//	G.mClusterBounaryList.erase(graph.endID_clusterSet_it);
//	//is endcluster removed
////	cout << "after removed the old cluster" << endl;
////	printClusterBounaryList_generalV2(graph);
//
//	//compare the newcluster to the graph.pTailClusterVector
//	insertNewCluster(graph, Newstart_clusterSet,G);
////	cout << "after insert the merged cluster" << endl;
////	cout << "SClusterSize=" << G.mSClusterSize << endl;
////	printClusterBounaryList_generalV2(graph);
//
//}
//




inline void VarGeneralEdgeTrue_OneS19V6(Graph &graph, int startID, int endID,auxiliaryGraph &auxiliarygraph) {
	//preconditions are rundant edges are removed
//	cout << "VarGeneralEdgeTrue_OneS" << endl;

	if (graph.node_degree[startID-graph.minBoundaryNodeID] == 0) {
		/*
		 * node degree ===0	 */

		auxiliarygraph.startID_clusterSet_it->clusterSet.erase(startID);
//		cout << "node is removed:" << startID << endl;
	}

	if (graph.node_degree[endID-graph.minBoundaryNodeID] == 0) {
		/*
		 * node degree ===0	 */


		auxiliarygraph.endID_clusterSet_it->clusterSet.erase(endID);
//		cout << "node is removed:" << startID << endl;
	}

	//add merger new
	clusterNodeSet Newstart_clusterSet;
	Newstart_clusterSet.isSource = true;
	Newstart_clusterSet.clusterSet = auxiliarygraph.startID_clusterSet_it->clusterSet;

	//add tail element
	set<int>::iterator tailit = auxiliarygraph.endID_clusterSet_it->clusterSet.begin();
	for (; tailit != auxiliarygraph.endID_clusterSet_it->clusterSet.end(); tailit++) {
		Newstart_clusterSet.clusterSet.insert(*tailit);

	}

//	::printclusterNodeSet(Newstart_clusterSet);

	//erase the old
	graph.mClusterBounaryList.erase(auxiliarygraph.startID_clusterSet_it);
	graph.mClusterBounaryList.erase(auxiliarygraph.endID_clusterSet_it);
	//is endcluster removed
//	cout << "after removed the old cluster" << endl;
//	printClusterBounaryList_generalV2(graph);

	//compare the newcluster to the graph.pTailClusterVector
	insertNewCluster(graph, Newstart_clusterSet);
//	cout << "after insert the merged cluster" << endl;
//	cout << "SClusterSize=" << graph.SClusterSize << endl;
//	printClusterBounaryList_generalV2(graph);

}





//
//inline void VarGeneralEdgeTrue_ZeroSV2(Graph &graph, int startID, int endID) {
//	//preconditions are rundant edges are removed
//
////	cout << "VarGeneralEdgeTrue_ZeroS" << endl;
//
//	if (graph.node_degree[startID] == 0) {
//		/*
//		 * node degree ===0	 */
//
//		graph.startID_clusterSet_it->clusterSet.erase(startID);
////		cout << "node is removed:" << startID << endl;
//	}
//
//	if (graph.node_degree[endID] == 0) {
//		/*
//		 * node degree ===0	 */
//
//		graph.endID_clusterSet_it->clusterSet.erase(endID);
////		cout << "node is removed:" << startID << endl;
//	}
//
//	//add merger new
//	clusterNodeSet Newstart_clusterSet;
//	Newstart_clusterSet.isSource = false;
//	Newstart_clusterSet.clusterSet = graph.startID_clusterSet_it->clusterSet;
//
//	//add tail element
//	set<int>::iterator tailit = graph.endID_clusterSet_it->clusterSet.begin();
//	for (; tailit != graph.endID_clusterSet_it->clusterSet.end(); tailit++) {
//		Newstart_clusterSet.clusterSet.insert(*tailit);
//
//	}
//
////	::printclusterNodeSet(Newstart_clusterSet);
//
//	//erase the old
//	G.mClusterBounaryList.erase(graph.startID_clusterSet_it);
//	G.mClusterBounaryList.erase(graph.endID_clusterSet_it);
//	//is endcluster removed
////	cout << "after removed the old cluster" << endl;
////	printClusterBounaryList_generalV2(graph);
//
//	//compare the newcluster to the graph.pTailClusterVector
//	insertNewCluster(graph, Newstart_clusterSet,G);
////	cout << "after insert the merged cluster" << endl;
////	cout << "SClusterSize=" << G.mSClusterSize << endl;
////	printClusterBounaryList_generalV2(graph);
//
//}
//



inline void VarGeneralEdgeTrue_ZeroS19V6(Graph &graph, int startID, int endID,
		auxiliaryGraph &auxiliarygraph) {
	//preconditions are rundant edges are removed

//	cout << "VarGeneralEdgeTrue_ZeroS" << endl;

	if (graph.node_degree[startID-graph.minBoundaryNodeID] == 0) {
		/*
		 * node degree ===0	 */

		auxiliarygraph.startID_clusterSet_it->clusterSet.erase(startID);
//		cout << "node is removed:" << startID << endl;
	}

	if (graph.node_degree[endID-graph.minBoundaryNodeID] == 0) {
		/*
		 * node degree ===0	 */

		auxiliarygraph.endID_clusterSet_it->clusterSet.erase(endID);
//		cout << "node is removed:" << startID << endl;
	}

	//add merger new
	clusterNodeSet Newstart_clusterSet;
	Newstart_clusterSet.isSource = false;
	Newstart_clusterSet.clusterSet = auxiliarygraph.startID_clusterSet_it->clusterSet;

	//add tail element
	set<int>::iterator tailit = auxiliarygraph.endID_clusterSet_it->clusterSet.begin();
	for (; tailit != auxiliarygraph.endID_clusterSet_it->clusterSet.end(); tailit++) {
		Newstart_clusterSet.clusterSet.insert(*tailit);

	}

//	::printclusterNodeSet(Newstart_clusterSet);

	//erase the old
	graph.mClusterBounaryList.erase(auxiliarygraph.startID_clusterSet_it);
	graph.mClusterBounaryList.erase(auxiliarygraph.endID_clusterSet_it);
	//is endcluster removed
//	cout << "after removed the old cluster" << endl;
//	printClusterBounaryList_generalV2(graph);

	//compare the newcluster to the graph.pTailClusterVector
	insertNewCluster(graph, Newstart_clusterSet);
//	cout << "after insert the merged cluster" << endl;
//	cout << "SClusterSize=" << graph.SClusterSize << endl;
//	printClusterBounaryList_generalV2(graph);

}



//
//bool isBranchEdgeTrue(Graph &graph,int startID) {
//	//preconditions are rundant edges are removed
//    bool is_branch=false;
//    Edge nextedge = graph.vectorEdge[G.edgeindex];
//    if(nextedge.StartID !=startID){
//    	return is_branch;
//    }
//	if (graph.startID_clusterSet_it->isSource == true
//			and graph.startID_clusterSet_it->clusterSet.size() == 1
//			and graph.node_degree[startID]==1) {
//
//		is_branch=true;
//
//		 Edge targetEdge = graph.vectorEdge[G.edgeindex];
//		 cout<<"the branch edge is :"<<"startID:"<<targetEdge.StartID;
//		 cout<<"--->endID:"<<targetEdge.EndID<<endl;
//
//		//		graph.EdgeList.pop_front();
//		int startID = targetEdge.StartID;
//		int endID = targetEdge.EndID;
//		G.targetEdge.StartID = startID;
//		G.targetEdge.EndID = endID;
//		G.targetEdge.relia = targetEdge.relia;
//
//		graph.node_degree[startID]--;
//		graph.node_degree[endID]--;
//
//		G.edgeindex++;
//
//
//		//erase the old
//		G.mClusterBounaryList.erase(graph.startID_clusterSet_it);
//
//		//is endcluster removed
//		//insert the new
//		locateSourceIDClusterV2(graph,endID);
//
//	}
//	 return is_branch;
//
//}
//


//
//
//void mergeCombineEdgeTrueV2(Graph &graph, int startID, int endID) {
//	//preconditions are rundant edges are removed
//
////	if (graph.node_degree[startID] == 0) {
////		/*
////		 * node degree ===0
////		 */
////		graph.SourceSet.erase(startID);
////		graph.VisitedSet.erase(startID);
////		graph.KterminalSet.erase(startID);
////		graph.pHeadClusterVector[0]->clusterSet.erase(startID);
////
////	}
//
//
//
//	//add merger new
//	clusterNodeSet Newstart_clusterSet;
//	Newstart_clusterSet.isSource = graph.pHeadClusterVector[0]->isSource;
//	Newstart_clusterSet.clusterSet.insert(endID);
//
//	if (graph.pHeadClusterVector.size() == 1
//			and graph.pHeadClusterVector[0]->isSource == false
//			and graph.pHeadClusterVector[0]->clusterSet.size() == 1) {
//		G.mClusterBounaryList.erase(graph.pHeadClusterVector[0]);
//	}
//
//	if (graph.pTailClusterVector.size() == 1
//			and graph.pTailClusterVector[0]->isSource == false
//			and graph.pTailClusterVector[0]->clusterSet.size() == 1) {
//		G.mClusterBounaryList.erase(graph.pTailClusterVector[0]);
//	}
//
//	//add new cluster
//	insertNewClusterV3(graph, Newstart_clusterSet);
//
////
////
////	//compare pendcluster
////	bool isinserted=false;
////	int index = 0;
////	int result = -1;
////	for (; index < graph.pTailClusterVector.size(); index++) {
////		result = isSetInclusion(Newstart_clusterSet.clusterSet,
////				graph.pTailClusterVector[index]->clusterSet);
////		if (result == 1 or result == 0 or result == 2) {
////			break;
////		}
////
////	}
////
////	if (result == 0) {
////		isinserted=true;
////		// A==B,no insert Newstart_clusterSet
////		;
////	}
////	if (result == 1) {
////		//A contains B, A inserted and B removed
////		isinserted=true;
////		G.mClusterBounaryList.erase(graph.pTailClusterVector[index]);
////		insertNewClusterV3(graph, Newstart_clusterSet);
////
////	}
////
////	if (result == 2) {
////		isinserted=true;
////		//B contains A, A no insert
////		;
////
////	}
////
////	if (result == -1) {
////		// A no B
////		//insert A
////		isinserted=true;
////		insertNewClusterV3(graph, Newstart_clusterSet);
////
////	}
////	//
////
////	if (graph.node_degree[endID] == 0) {
////		/*
////		 * node degree ===0
////		 */
////		graph.SourceSet.erase(endID);
////		graph.VisitedSet.erase(endID);
////		graph.pTailClusterVector[0]->clusterSet.erase(endID);
////
////	}
////
////	index = 0;
////	result = -1;
////	for (; index < graph.pHeadClusterVector.size(); index++) {
////		result = isSetInclusion(Newstart_clusterSet.clusterSet,
////				graph.pHeadClusterVector[index]->clusterSet);
////		if (result == 1 or result == 0 or result == 2) {
////			break;
////		}
////
////	}
////
////	if (result == 0) {
////		// A==B,no insert Newstart_clusterSet
////		;
////	}
////	if (result == 1) {
////		//A contains B, A inserted and B removed
////		G.mClusterBounaryList.erase(graph.pHeadClusterVector[index]);
////		//insertNewClusterV3(graph, Newstart_clusterSet);
////
////	}
////
////	if (result == 2) {
////		//B contains A, A no insert
////		;
////
////	}
////
////	if (result == -1) {
////		// A no B
////		//insert A
////		;//insertNewClusterV3(graph, Newstart_clusterSet);
////
////	}
////	//
//
//}
//

int isSetInclusion(set<int> &A, set<int> &B) {
	// A contains B, returns 1
	// B contains A,returns  2
	// A equals B  ,returns 0
	// A no B  ,returns -1
	int result = -1;

	if (A.size() == B.size()) {

		set<int>::iterator nodeit = A.begin();
		for (; nodeit != A.end(); nodeit++) {
			if (::isContains(B, *nodeit) == true) {
				result = 0;
				continue;
			} else {
				result = -1;
				break;
			}
		}

	}

	if (result == -1) {

		if (A.size() > B.size()) {

			set<int>::iterator nodeit = B.begin();
			for (; nodeit != B.end(); nodeit++) {
				if (::isContains(A, *nodeit) == true) {
					result = 1;
					continue;
				} else {
					result = -1;
					break;
				}
			}

		}
	}
	if (result == -1) {

		if (A.size() < B.size()) {

			set<int>::iterator nodeit = A.begin();
			for (; nodeit != A.end(); nodeit++) {
				if (::isContains(B, *nodeit) == true) {
					result = 2;
					continue;
				} else {
					result = -1;
					break;
				}
			}

		}
	}

	//if

	return result;

}



void printSet(set<int> S) {

	cout << "set items as follows:" << endl;
	set<int>::iterator sit = S.begin();
	for (; sit != S.end(); sit++) {
		cout << ":" << *sit << " ";
	}
	cout << endl;

}

int isFirstSetLarger(set<int> &A, set<int> &B) {
	// A larger B ,rerurns 1
	// A smaller B, returns 0
//   cout<<"in isFirstSetLarger"<<endl;
//
//	printSet(A);
//	printSet(B);

	int result = -1;
	int ASetsize = A.size();
	int BSetsize = B.size();

	if (ASetsize == BSetsize) {

		set<int>::iterator Anodeit = A.begin();
		set<int>::iterator Bnodeit = B.begin();

		int index = 1;
		while (index <= ASetsize) {

			if (*Anodeit > *Bnodeit) {

				result = 1;
				break;
			} else if (*Anodeit < *Bnodeit) {
				result = 0;
				break;

			} else {

				Bnodeit++;
				Anodeit++;
				index++;

				continue;
			}
		}
	}

	if (ASetsize < BSetsize) {

		set<int>::iterator Anodeit = A.begin();
		set<int>::iterator Bnodeit = B.begin();

		int index = 1;
		while (index <= ASetsize) {

			if (*Anodeit > *Bnodeit) {

				result = 1;
				break;
			} else if (*Anodeit < *Bnodeit) {
				result = 0;
				break;

			} else {

				Bnodeit++;
				Anodeit++;
				index++;

				continue;
			}
		}
	}

	if (ASetsize > BSetsize) {

		set<int>::iterator Anodeit = A.begin();
		set<int>::iterator Bnodeit = B.begin();

		int index = 1;
		while (index <= BSetsize) {

			if (*Anodeit > *Bnodeit) {

				result = 1;
				break;
			} else if (*Anodeit < *Bnodeit) {
				result = 0;
				break;

			} else {

				Bnodeit++;
				Anodeit++;
				index++;

				continue;
			}
		}
	}

	return result;

}
//
//void VarGeneralEdgeTrueFromNoSoure(Graph &graph, int startID, int endID) {
//
//	if (::isContains(graph.VisitedSet, startID)) {
//		//startID is visited
//		// endID of cluster --->startID cluster
//		if (::isContains(graph.VisitedSet, endID)) {
//
//			clusterNodeSet Newstart_clusterSet;
//			Newstart_clusterSet.isSource = true;
//			Newstart_clusterSet.clusterSet =
//					graph.pHeadClusterVector[0]->clusterSet;
//			set<int>::iterator endidit =
//					graph.pTailClusterVector[0]->clusterSet.begin();
//			//insert
//			for (; endidit != graph.pTailClusterVector[0]->clusterSet.end();
//					endidit++) {
//
//				Newstart_clusterSet.clusterSet.insert(*endidit);
//			}
//			//remove orginal from list
//
//			G.mClusterBounaryList.erase(graph.pHeadClusterVector[0]);
//			G.mClusterBounaryList.erase(graph.pTailClusterVector[0]);
//			//
//			if (graph.node_degree[startID] == 0) {
//
//				Newstart_clusterSet.clusterSet.erase(startID);
//				graph.VisitedSet.erase(startID);
//			}
//			if (graph.node_degree[endID] == 0) {
//
//				Newstart_clusterSet.clusterSet.erase(endID);
//				graph.VisitedSet.erase(endID);
//			}
//			//insert the new
//			::insertNewCluster(graph, Newstart_clusterSet,G);
//
//		} else {		//::isContains(graph.VisitedSet, endID)==false
//
//			//endID--->start_cluster
//			clusterNodeSet Newstart_clusterSet;
//			Newstart_clusterSet.isSource = true;
//			Newstart_clusterSet.clusterSet =
//					graph.pHeadClusterVector[0]->clusterSet;
//			Newstart_clusterSet.clusterSet.insert(endID);
//
//			if (graph.node_degree[startID] == 0) {
//
//				Newstart_clusterSet.clusterSet.erase(startID);
//				graph.VisitedSet.erase(startID);
//			}
//			//remove orginal from list
//
//			G.mClusterBounaryList.erase(graph.pHeadClusterVector[0]);
//			//insert the new
//			::insertNewCluster(graph, Newstart_clusterSet,G);
//
//		}
//	} else {		//isContains(graph.VisitedSet, startID)==false
//
//		if (::isContains(graph.VisitedSet, endID)) {
//			//startID-->endIDCluster
//
//			clusterNodeSet Newstart_clusterSet;
//			Newstart_clusterSet.isSource = true;
//			Newstart_clusterSet.clusterSet =
//					graph.pTailClusterVector[0]->clusterSet;
//			//addd
//			Newstart_clusterSet.clusterSet.insert(startID);
//			//
//			if (graph.node_degree[endID] == 0) {
//
//				Newstart_clusterSet.clusterSet.erase(endID);
//				graph.VisitedSet.erase(endID);
//			}
//			//remove orginal from list
//
//			G.mClusterBounaryList.erase(graph.pTailClusterVector[0]);
//			//insert the new
//			::insertNewCluster(graph, Newstart_clusterSet,G);
//		} else {
//			//new clusterset [startID,endID] //generated
//			clusterNodeSet Newstart_clusterSet;
//			Newstart_clusterSet.isSource = true;
//			Newstart_clusterSet.clusterSet.insert(startID);
//			Newstart_clusterSet.clusterSet.insert(endID);
//			//insert the new
//			::insertNewCluster(graph, Newstart_clusterSet,G);
//
//		}
//
//	}
//
//}

void VarNodeEdgeTrue(Graph &graph, bool isfromSource) {
//	//add endIDcluster to SourceSet
//	int ClusterSumBasedKey = graph.pHeadClusterVector.size();
//
//	if (ClusterSumBasedKey == 1) {
//
//		for (int index = 0; index < graph.pHeadClusterVector.size();
//				index++) {
//			set<int>::iterator sit =
//					graph.pHeadClusterVector[index]->clusterSet.begin();
//			if (isfromSource == true) {
//				for (;
//						sit
//								!= graph.pHeadClusterVector[index]->clusterSet.end();
//						sit++) {
//
//					graph.SourceSet.insert(*sit);
//
//				}
////				G.mClusterBounaryList.erase(graph.pHeadClusterVector[0]);
//			}
//
//		}
//	} else {
//
//		clusterNodeSet Newstart_clusterSet;
//
//		for (int index = 0; index < graph.pHeadClusterVector.size();
//				index++) {
//			set<int>::iterator sit =
//					graph.pHeadClusterVector[index]->clusterSet.begin();
//			for (; sit != graph.pHeadClusterVector[index]->clusterSet.end();
//					sit++) {
//				if (isfromSource == true) {
//
//					graph.SourceSet.insert(*sit);
////					Newstart_clusterSet.isSource=true;
//				}
//				Newstart_clusterSet.clusterSet.insert(*sit);
//
//				//remove it
//
//			}
//			G.mClusterBounaryList.erase(graph.pHeadClusterVector[index]);
//
//		}
//
//		//insert the new
//
//		if (Newstart_clusterSet.clusterSet.size() > 0
//				and isfromSource == false) {
//
//			insertNewCluster(graph, Newstart_clusterSet,G);
//		}
//
//	}

}



//
//
//void VarNodeEdgeTrueV2(Graph &graph, bool isfromSource) {
////	cout<<" start of VarNodeEdgeTrueV2"<<endl;
//	//add endIDcluster to SourceSet
////	cout<<"in VarNodeEdgeTrueV2"<<endl;
//	int ClusterSumBasedKey = graph.pHeadClusterVector.size();
//
////	if (ClusterSumBasedKey >= 2 and isfromSource==true) {
//////		cout << "the visited Node as follows:=>";
//////		set<int>::iterator it = graph.VisitedSet.begin();
//////		for (; it != graph.VisitedSet.end(); ++it)
//////			cout << ' ' << *it;
//////		cout << endl;
////
//////		::printSourceSet(graph);
//////		cout << "ClusterSumBasedKey:" << ClusterSumBasedKey << endl;
////
////		for (int index = 0; index < ClusterSumBasedKey; index++) {
////			set<int>::iterator sit =
////					graph.pHeadClusterVector[index]->clusterSet.begin();
////			cout << "node:";
////			for (; sit != graph.pHeadClusterVector[index]->clusterSet.end();
////					sit++) {
////				cout << *sit << "  ";
////
////			}
////			cout << endl;
////
////		}
////
////		cout << "debug for rundant=================>" << endl;
////
////	}
//
//	clusterNodeSet Newstart_clusterSet;
//	Newstart_clusterSet.isSource = isfromSource;
//	//collapse to center node startID
//
//	for (int index = 0; index < ClusterSumBasedKey; index++) {
//		set<int>::iterator sit =
//				graph.pHeadClusterVector[index]->clusterSet.begin();
//		for (; sit != graph.pHeadClusterVector[index]->clusterSet.end();
//				sit++) {
////			cout<<"cluster node:"<<*sit<<endl;
//			Newstart_clusterSet.clusterSet.insert(*sit);
//
//			//remove it
//
//		}
//		G.mClusterBounaryList.erase(graph.pHeadClusterVector[index]);
//
//	}
//
//	//compare
//
//	int result = -1;
//
//	list<clusterNodeSet>::iterator clusterit = G.mClusterBounaryList.begin();
//
//	if (Newstart_clusterSet.isSource == true) {
//
//		for (; clusterit != G.mClusterBounaryList.end(); clusterit++) {
//			if (clusterit->isSource == true) {
//				result = isSetInclusion(Newstart_clusterSet.clusterSet,
//						clusterit->clusterSet);
//				if (result == 1 or result == 0 or result == 2) {
//					break;
//				}
//			} else {
//
//				if (clusterit->clusterSet.size()
//						< Newstart_clusterSet.clusterSet.size()) {
//
//					result = isSetInclusion(Newstart_clusterSet.clusterSet,
//							clusterit->clusterSet);
//					if (result == 1) {
//						break;
//					}
//
//				}
//			}
//
//		}
//
//		if (result == 0) {
//			// A==B,no insert Newstart_clusterSet
//			;
//		}
//		if (result == 1) {
//			//A contains B, A inserted and B removed
//			G.mClusterBounaryList.erase(clusterit);
//			insertNewClusterV3(graph, Newstart_clusterSet);
//
//		}
//
//		if (result == 2) {
//			//B contains A, A no insert
//			;
//
//		}
//
//		if (result == -1) {
//			// A no B
//			//insert A
//			insertNewClusterV3(graph, Newstart_clusterSet);
//
//		}
//
//	}
//
//	if (Newstart_clusterSet.isSource == false) {
//
////		cout<<"size of node true ID:"<<Newstart_clusterSet.clusterSet.size()<<endl;
////        cout<<"----"<<endl;
////		printClusterBounaryList(graph);
//
//		for (; clusterit != G.mClusterBounaryList.end(); clusterit++) {
//			if (clusterit->isSource == true) {
//				result = isSetInclusion(Newstart_clusterSet.clusterSet,
//						clusterit->clusterSet);
//				if (result == 0 or result == 2) {
//					break;
//				}else if(result==1){
//					result=-1;
//					continue;
//				}
//			} else {
//
//				result = isSetInclusion(Newstart_clusterSet.clusterSet,
//						clusterit->clusterSet);
//				if (result == 1 or result == 0 or result == 2) {
//					break;
//				}
//
//			}
//
//		}
//
////		cout<<"result="<<result<<endl;
//
//		if (result == 0) {
//			// A==B,no insert Newstart_clusterSet
//			;
//		}
//		if (result == 1) {
//			//A contains B, A inserted and B removed
////			cout<<"erase:lusterit"<<endl;
//
////			cout<<"clusterit->clusterSet size()="<<clusterit->clusterSet.size()<<endl;
//			G.mClusterBounaryList.erase(clusterit);
////			cout<<"insert :Newstart_clusterSet"<<endl;
//			insertNewClusterV3(graph, Newstart_clusterSet);
//
//		}
//
//		if (result == 2) {
//			//B contains A, A no insert
//			;
//
//		}
//
//		if (result == -1) {
//			// A no B
//			//insert A
//			insertNewClusterV3(graph, Newstart_clusterSet);
//
//		}
//
//	}
//
////	cout<<" end of VarNodeEdgeTrueV2"<<endl;
//
//	//
//
//	//insert the new
////
////	if (Newstart_clusterSet.clusterSet.size() > 0) {
////
////		insertNewCluster(graph, Newstart_clusterSet,G);
////	}
//
//}
//
//
//
//
//void VarNodeEdgeTrueFromNoSource(Graph &graph) {
////	cout<<" start of VarNodeEdgeTrueV2"<<endl;
//	//add endIDcluster to SourceSet
////	cout<<"in VarNodeEdgeTrueV2"<<endl;
//	int ClusterSumBasedKey = graph.pHeadClusterVector.size();
//
////	if (ClusterSumBasedKey >= 2 and isfromSource==true) {
//////		cout << "the visited Node as follows:=>";
//////		set<int>::iterator it = graph.VisitedSet.begin();
//////		for (; it != graph.VisitedSet.end(); ++it)
//////			cout << ' ' << *it;
//////		cout << endl;
////
//////		::printSourceSet(graph);
//////		cout << "ClusterSumBasedKey:" << ClusterSumBasedKey << endl;
////
////		for (int index = 0; index < ClusterSumBasedKey; index++) {
////			set<int>::iterator sit =
////					graph.pHeadClusterVector[index]->clusterSet.begin();
////			cout << "node:";
////			for (; sit != graph.pHeadClusterVector[index]->clusterSet.end();
////					sit++) {
////				cout << *sit << "  ";
////
////			}
////			cout << endl;
////
////		}
////
////		cout << "debug for rundant=================>" << endl;
////
////	}
//
//	clusterNodeSet Newstart_clusterSet;
//	Newstart_clusterSet.isSource = false;
//	//collapse to center node startID
//
//	for (int index = 0; index < ClusterSumBasedKey; index++) {
//		set<int>::iterator sit =
//				graph.pHeadClusterVector[index]->clusterSet.begin();
//		for (; sit != graph.pHeadClusterVector[index]->clusterSet.end();
//				sit++) {
////			cout<<"cluster node:"<<*sit<<endl;
//			Newstart_clusterSet.clusterSet.insert(*sit);
//
//			//remove it
//
//		}
//		G.mClusterBounaryList.erase(graph.pHeadClusterVector[index]);
//
//	}
//
//	//compare
//
//	int result = -1;
//
//	list<clusterNodeSet>::iterator clusterit = G.mClusterBounaryList.begin();
//
//	if (Newstart_clusterSet.isSource == false) {
//
////		cout<<"size of node true ID:"<<Newstart_clusterSet.clusterSet.size()<<endl;
////        cout<<"----"<<endl;
////		printClusterBounaryList(graph);
//
//		for (; clusterit != G.mClusterBounaryList.end(); clusterit++) {
//			if (clusterit->isSource == true) {
//				result = isSetInclusion(Newstart_clusterSet.clusterSet,
//						clusterit->clusterSet);
//				if (result == 0 or result == 2) {
//					break;
//				}else if(result==1){
//					result=-1;
//					continue;
//				}
//			} else {
//
//				result = isSetInclusion(Newstart_clusterSet.clusterSet,
//						clusterit->clusterSet);
//				if (result == 1 or result == 0 or result == 2) {
//					break;
//				}
//
//			}
//
//		}
//
////		cout<<"result="<<result<<endl;
//
//		if (result == 0) {
//			// A==B,no insert Newstart_clusterSet
//			;
//		}
//		if (result == 1) {
//			//A contains B, A inserted and B removed
////			cout<<"erase:lusterit"<<endl;
//
////			cout<<"clusterit->clusterSet size()="<<clusterit->clusterSet.size()<<endl;
//			G.mClusterBounaryList.erase(clusterit);
////			cout<<"insert :Newstart_clusterSet"<<endl;
//			insertNewClusterV3(graph, Newstart_clusterSet);
//
//		}
//
//		if (result == 2) {
//			//B contains A, A no insert
//			;
//
//		}
//
//		if (result == -1) {
//			// A no B
//			//insert A
//			insertNewClusterV3(graph, Newstart_clusterSet);
//
//		}
//
//	}
//
////	cout<<" end of VarNodeEdgeTrueV2"<<endl;
//
//	//
//
//	//insert the new
////
////	if (Newstart_clusterSet.clusterSet.size() > 0) {
////
////		insertNewCluster(graph, Newstart_clusterSet,G);
////	}
//
//}
//
//
//
//
//void VarNodeEdgeTrueFromSource(Graph &graph) {
////	cout<<" start of VarNodeEdgeTrueV2"<<endl;
//	printClusterBounaryList_generalV2(graph);
//	//add endIDcluster to SourceSet
////	cout<<"in VarNodeEdgeTrueV2"<<endl;
//	clusterNodeSet Newstart_clusterSet;
//	Newstart_clusterSet.isSource = true;
//	int clusterSsize = graph.pSourceClusterVector.size();
//
////	cout<<"cluster*S*size"<<clusterSsize<<endl;
//
//	for (int index = 0; index < clusterSsize; index++) {
//		set<int>::iterator sit =
//				graph.pSourceClusterVector[index]->clusterSet.begin();
////		cout<<"sit----"<<*sit<<endl;
//		for (; sit != graph.pSourceClusterVector[index]->clusterSet.end();
//				sit++) {
////		  cout<<"cluster node is inserted to Newstart_clusterSet:"<<*sit<<endl;
//			Newstart_clusterSet.clusterSet.insert(*sit);
//
//			//remove it
//
//		}
//		G.mClusterBounaryList.erase(graph.pSourceClusterVector[index]);
//
//
//	}
//
//	//update SClusterSize
//
//	G.mSClusterSize=G.mSClusterSize-clusterSsize+1;
//	///
//
//
//
//	int clusterNSsize = graph.pHeadClusterVector.size();
//
////	cout<<"cluster NS size"<<clusterNSsize<<endl;
//
//	for (int index = 0; index < clusterNSsize; index++) {
//		set<int>::iterator sit =
//				graph.pHeadClusterVector[index]->clusterSet.begin();
//		for (; sit != graph.pHeadClusterVector[index]->clusterSet.end();
//				sit++) {
//			//			cout<<"cluster node:"<<*sit<<endl;
//			Newstart_clusterSet.clusterSet.insert(*sit);
//
//			//remove it
//
//		}
//		G.mClusterBounaryList.erase(graph.pHeadClusterVector[index]);
//
//	}
//
//	//compare
//
//	int result = -1;
//
//	list<clusterNodeSet>::iterator clusterit = G.mClusterBounaryList.begin();
//
//		for (; clusterit != G.mClusterBounaryList.end(); clusterit++) {
//			if (clusterit->isSource == true) {
//				result = isSetInclusion(Newstart_clusterSet.clusterSet,
//						clusterit->clusterSet);
//				if (result == 1 or result == 0 or result == 2) {
//					break;
//				}
//			} else {
//
//				if (clusterit->clusterSet.size()
//						< Newstart_clusterSet.clusterSet.size()) {
//
//					result = isSetInclusion(Newstart_clusterSet.clusterSet,
//							clusterit->clusterSet);
//					if (result == 1) {
//						break;
//					}
//
//				}
//			}
//
//		}
//
//		if (result == 0) {
//			// A==B,no insert Newstart_clusterSet
//			;
//		}
//	if (result == 1) {
//		//A contains B, A inserted and B removed
//		if (clusterit->isSource == true) {
//			insertNewClusterV3(graph, Newstart_clusterSet);
//		} else {
//			G.mClusterBounaryList.erase(clusterit);
//			insertNewClusterV3(graph, Newstart_clusterSet);
//		}
//
//	}
//
//		if (result == 2) {
//		if (clusterit->isSource == true) {
//			insertNewClusterV3(graph, Newstart_clusterSet);
//		} else {
//			//G.mClusterBounaryList.erase(clusterit);
//			insertNewClusterV3(graph, Newstart_clusterSet);
//		}
//
//		}
//
//		if (result == -1) {
//			// A no B
//			//insert A
//			insertNewClusterV3(graph, Newstart_clusterSet);
//
//		}
//
//
////		cout<<" end of VarNodeEdgeTrueV2"<<endl;
////		printClusterBounaryList_generalV2(graph);
//
//}
//
//




bool isBranch_general(Graph &graph, int tID) {

//	Edge targetEdge=graph.EdgeList.front();
//	graph.EdgeList.pop_front();
//	int startID=targetEdge.StartID;
//	int endID=targetEdge.EndID;
//	G.targetEdge.StartID=startID;
//	G.targetEdge.EndID=endID;
//
//	EdgeProperties ep0 =graph.nodeToTargets[startID][endID];
//	G.targetEdge.edgepro=ep0;
//
//	//remove the edge
//	deleteEdge(graph,startID, endID);
//	deleteEdge(graph,endID, startID);
//	cout<<"-----------------------------in removed the failed edge-------------------------------------"<<endl;

	bool isHeadID_clusterfind = false;

	bool isBranch = false;
//
//	if (graph.SourceSet.size() == 1) {
//
////        graph.Start_cluster_set.clusterSet.clear();
////        graph.Start_cluster_set.clusterSet.clear();
//		Edge targetEdge = graph.vectorEdge[G.edgeindex];
//
////		graph.EdgeList.pop_front();
//		int startID = targetEdge.StartID;
//		int endID = targetEdge.EndID;
//		G.targetEdge.StartID = startID;
//		G.targetEdge.EndID = endID;
//		G.targetEdge.relia = targetEdge.relia;
//
////		cout<<"temp edge:"<<"startID="<<startID<<endl;
////		cout<<"temp edge:"<<"endID="<<endID<<endl;
//
//		///-----------------------locate-------------------------------
//		isHeadID_clusterfind = false;
//
//		if (startID == endID) {
//
//			if (isContains(graph.SourceSet, startID) == true
//					and graph.SourceSet.size() == 1) {
//
//				isBranch = true;
//
//				//locate the startIDit
//				locateClusterNodeSetByTargetEdge_general(graph);
//
//				//update
//
//				VarNodeEdgeTrue(graph, true);
//				//update
//				graph.node_degree[startID]--;
//				graph.node_degree[endID]--;
//				G.edgeindex++;
//			}
//
//		}
//
//	}

	return isBranch;

}

bool isBranch_generalV2(Graph &graph, int tID) {
//	cout<<"============================isBranch_generalV2"<<endl;

//	Edge targetEdge=graph.EdgeList.front();
//	graph.EdgeList.pop_front();
//	int startID=targetEdge.StartID;
//	int endID=targetEdge.EndID;
//	G.targetEdge.StartID=startID;
//	G.targetEdge.EndID=endID;
//
//	EdgeProperties ep0 =graph.nodeToTargets[startID][endID];
//	G.targetEdge.edgepro=ep0;
//
//	//remove the edge
//	deleteEdge(graph,startID, endID);
//	deleteEdge(graph,endID, startID);
//	cout<<"-----------------------------in removed the failed edge-------------------------------------"<<endl;

	bool isHeadID_clusterfind = false;

	bool isBranch = false;

//	if (graph.SourceSet.size() == 1) {
//
////        graph.Start_cluster_set.clusterSet.clear();
////        graph.Start_cluster_set.clusterSet.clear();
//		Edge targetEdge = graph.vectorEdge[G.edgeindex];
//
////		graph.EdgeList.pop_front();
//		int startID = targetEdge.StartID;
//		int endID = targetEdge.EndID;
//		G.targetEdge.StartID = startID;
//		G.targetEdge.EndID = endID;
//		G.targetEdge.relia = targetEdge.relia;
//
////		cout<<"temp edge:"<<"startID="<<startID<<endl;
////		cout<<"temp edge:"<<"endID="<<endID<<endl;
//
//		///-----------------------locate-------------------------------
//		isHeadID_clusterfind = false;
//
//		if (startID == endID) {
//
//			if (isContains(graph.SourceSet, startID) == true
//					and graph.SourceSet.size() == 1) {
//
//				graph.VisitedSet.insert(startID);
//
//				isBranch = true;
//				//set targetedge
//				setTargetEdgeByEdgeVector(graph);
//
////				cout<<"startID:"<<G.targetEdge.StartID<<endl;
////				cout<<"endID:"<<G.targetEdge.EndID<<endl;
//
//				//locate the startIDit
//				locateClusterNodeSetByTargetEdge_general(graph);
//
////				//update
////				cout << "=================after locateClusterNodeSetByTargetEdge_general=============" << endl;
////				printClusterBounaryList(graph);
////				printSourceSet(graph);
//
//				VarNodeEdgeTrueV2(graph, true);
//				//update
////				graph.node_degree[startID]--;
////				graph.node_degree[endID]--;
////				G.edgeindex++;
//			}
//
//		}
//
//	}

	return isBranch;
//	cout<<"----------->isBranch_generalV2"<<endl;

}

bool isEdgeBranch_general(Graph &graph) {
////	cout<<"============================isBranch_generalV2"<<endl;
//
////	Edge targetEdge=graph.EdgeList.front();
////	graph.EdgeList.pop_front();
////	int startID=targetEdge.StartID;
////	int endID=targetEdge.EndID;
////	G.targetEdge.StartID=startID;
////	G.targetEdge.EndID=endID;
////
////	EdgeProperties ep0 =graph.nodeToTargets[startID][endID];
////	G.targetEdge.edgepro=ep0;
////
////	//remove the edge
////	deleteEdge(graph,startID, endID);
////	deleteEdge(graph,endID, startID);
////	cout<<"-----------------------------in removed the failed edge-------------------------------------"<<endl;
//
//	bool isHeadID_clusterfind = false;
//
//	bool isBranch = false;
//	double relia = 0.0;
//
//	if (graph.SourceSet.size() == 1) {
//
////        graph.Start_cluster_set.clusterSet.clear();
////        graph.Start_cluster_set.clusterSet.clear();
//		Edge targetEdge = graph.vectorEdge[G.edgeindex];
//
////		graph.EdgeList.pop_front();
//		int startID = targetEdge.StartID;
//		int endID = targetEdge.EndID;
//		G.targetEdge.StartID = startID;
//		G.targetEdge.EndID = endID;
//		G.targetEdge.relia = targetEdge.relia;
//
////		cout<<"temp edge:"<<"startID="<<startID<<endl;
////		cout<<"temp edge:"<<"endID="<<endID<<endl;
//
//		///-----------------------locate-------------------------------
//		isHeadID_clusterfind = false;
//
//		if (startID == endID
//				and ::isContains(graph.SourceSet, startID) == true) {
//
//			if (graph.node_degree[startID] >= 1) {
//
//				graph.VisitedSet.insert(startID);
//				//removed it from K
//				graph.KterminalSet.erase(startID);
//
//				isBranch = true;
//				//set targetedge
//				setTargetEdgeByEdgeVector(graph);
//				relia = G.targetEdge.relia;
//
////				cout<<"startID:"<<G.targetEdge.StartID<<endl;
////				cout<<"endID:"<<G.targetEdge.EndID<<endl;
//
//				//locate the startIDit
//				locateClusterNodeSetByTargetEdge_general(graph);
//
////				//update
////				cout << "=================after locateClusterNodeSetByTargetEdge_general=============" << endl;
////				printClusterBounaryList(graph);
////				printSourceSet(graph);
//
//				VarNodeEdgeTrueV2(graph, true);
//				//update
////				graph.node_degree[startID]--;
////				graph.node_degree[endID]--;
////				G.edgeindex++;
//			}
//			if (graph.node_degree[startID] == 0
//					and graph.KterminalSet.size() > 0) {
//				//edge branch
//				if (graph.pHeadClusterVector.size() == 1) {
//					//rundant edge
//					if (graph.pHeadClusterVector[0]->clusterSet.size()
//							== 1) {
//						cout << "successive branch is finding====" << endl;
//						//set targetEdge
//						setTargetEdgeByEdgeVector(graph);
//						relia *= G.targetEdge.relia;
//						G.targetEdge.relia = relia;
//						//locate endDIt
//						int endID = G.targetEdge.EndID;
//						locateClusterNodeSetByTargetNodeID_general(graph,
//								G.targetEdge.EndID);
//						//
//						G.mClusterBounaryList.erase(
//								graph.pHeadClusterVector[0]);
//						graph.VisitedSet.erase(startID);
//						graph.KterminalSet.erase(startID);
//						cout << "node:" << startID << "is removed" << endl;
//						graph.SourceSet.erase(startID);
//						graph.KterminalSet.erase(startID);
//						graph.VisitedSet.erase(startID);
//
//						graph.SourceSet.insert(endID);
//
//					}
//
//				}
//
//			}
//
//		}
//
////	cout<<"----------->isBranch_generalV2"<<endl;
//
//	}
	return true;

}
//
//bool isEdgeBranch_generalV2(Graph &graph) {
////	cout<<"============================isBranch_generalV2"<<endl;
//
////	Edge targetEdge=graph.EdgeList.front();
////	graph.EdgeList.pop_front();
////	int startID=targetEdge.StartID;
////	int endID=targetEdge.EndID;
////	G.targetEdge.StartID=startID;
////	G.targetEdge.EndID=endID;
////
////	EdgeProperties ep0 =graph.nodeToTargets[startID][endID];
////	G.targetEdge.edgepro=ep0;
////
////	//remove the edge
////	deleteEdge(graph,startID, endID);
////	deleteEdge(graph,endID, startID);
////	cout<<"-----------------------------in removed the failed edge-------------------------------------"<<endl;
//
//	bool isHeadID_clusterfind = false;
//
//	bool isBranch = false;
//	double relia = 0.0;
//
//	if (G.mSClusterSize == 1) {
//
////        graph.Start_cluster_set.clusterSet.clear();
////        graph.Start_cluster_set.clusterSet.clear();
//		Edge targetEdge = graph.vectorEdge[G.edgeindex];
//
////		graph.EdgeList.pop_front();
//		int startID = targetEdge.StartID;
//		int endID = targetEdge.EndID;
//		G.targetEdge.StartID = startID;
//		G.targetEdge.EndID = endID;
//		G.targetEdge.relia = targetEdge.relia;
//
////		cout<<"temp edge:"<<"startID="<<startID<<endl;
////		cout<<"temp edge:"<<"endID="<<endID<<endl;
//
//		///-----------------------locate-------------------------------
//		isHeadID_clusterfind = false;
//
//		if (startID == endID) {
//
//			//locate the startIDit
//			locateClusterNodeSetByTargetEdge_general(graph);
//
//			if (graph.KterminalSet.size() > 0
//					and graph.node_degree[startID] == 1
//					and graph.pHeadClusterVector.size() == 1
//					and graph.pHeadClusterVector[0]->clusterSet.size() == 1
//					and graph.isHeadS == true) {
//				//edge branch
//				//rundant edge
//				cout << "successive branch is finding====" << endl;
//				//set node edge targetEdge
//				relia = G.targetEdge.relia;
//				cout << "relia=" << relia << endl;
//				G.edgeindex++;
//
//				cout << "G.edgeindex=" << G.edgeindex << endl;
//				//next edge
//				//G.edgeindex++;
//				setTargetEdgeByEdgeVector(graph);
//
//				relia *= G.targetEdge.relia;
//				cout << "G.edgeindex=" << G.edgeindex << endl;
//				cout << "relia=" << relia << endl;
//
//				//remove startIDcluster
//				G.mClusterBounaryList.erase(graph.pHeadClusterVector[0]);
//
//				//upate
//				G.targetEdge.relia = relia;
//				//locate endDIt
//				locateClusterNodeSetByTargetNodeID_general(graph,
//						G.targetEdge.EndID);
//				//
//
//				graph.VisitedSet.erase(startID);
//				graph.KterminalSet.erase(startID);
//				cout << "node:" << startID << "is removed" << endl;
//
//			} else {
//
//				graph.VisitedSet.insert(startID);
//				//removed it from K
//				graph.KterminalSet.erase(startID);
//
//				//set targetedge
//				setTargetEdgeByEdgeVector(graph);
//				relia = G.targetEdge.relia;
//
//				cout << "startID:" << G.targetEdge.StartID << endl;
//				cout << "endID:" << G.targetEdge.EndID << endl;
//
////				//update
////				cout << "=================after locateClusterNodeSetByTargetEdge_general=============" << endl;
////				printClusterBounaryList(graph);
////				printSourceSet(graph);
//
//				VarNodeEdgeTrueV2(graph, true);
//				//update
////				graph.node_degree[startID]--;
////				graph.node_degree[endID]--;
////				G.edgeindex++;
//			}
//
////	cout<<"----------->isBranch_generalV2"<<endl;
//		}
//
//	}
//
//	return isBranch;
//
//}
//
//bool isEdgeBranch_generalV3(Graph &graph) {
////	cout<<"============================isBranch_generalV2"<<endl;
//
////	Edge targetEdge=graph.EdgeList.front();
////	graph.EdgeList.pop_front();
////	int startID=targetEdge.StartID;
////	int endID=targetEdge.EndID;
////	G.targetEdge.StartID=startID;
////	G.targetEdge.EndID=endID;
////
////	EdgeProperties ep0 =graph.nodeToTargets[startID][endID];
////	G.targetEdge.edgepro=ep0;
////
////	//remove the edge
////	deleteEdge(graph,startID, endID);
////	deleteEdge(graph,endID, startID);
////	cout<<"-----------------------------in removed the failed edge-------------------------------------"<<endl;
//
//	bool isHeadID_clusterfind = false;
//
//	bool isBranch = false;
//	double relia = 0.0;
//
//	Edge targetEdge = graph.vectorEdge[G.edgeindex];
//
////		graph.EdgeList.pop_front();
//	int startID = targetEdge.StartID;
//	int endID = targetEdge.EndID;
//	G.targetEdge.StartID = startID;
//	G.targetEdge.EndID = endID;
//	G.targetEdge.relia = targetEdge.relia;
//
////		cout<<"temp edge:"<<"startID="<<startID<<endl;
////		cout<<"temp edge:"<<"endID="<<endID<<endl;
//
//	///-----------------------locate-------------------------------
//	isHeadID_clusterfind = false;
//
//	if (startID == endID) {
//
//		//locate the startIDit
//		locateClusterNodeSetByTargetEdge_general(graph);
//
////		cout<<"after locate"<<endl;
////		printClusterBounaryList_generalV2(graph);
//
//		if (graph.pSourceClusterVector.size() == 1
//				and graph.pSourceClusterVector[0]->clusterSet.size() == 1
//				and graph.isHeadS == true) {
//
//			isBranch = true;
//
//			cout<<"the true Node is:"<<startID<<endl;
//
//			graph.VisitedSet.insert(startID);
//			//removed it from K
//			graph.KterminalSet.erase(startID);
//
//			//set targetedge
//			setTargetEdgeByEdgeVector(graph);
//
////				relia = G.targetEdge.relia;
////
////				cout<<"startID:"<<G.targetEdge.StartID<<endl;
////				cout<<"endID:"<<G.targetEdge.EndID<<endl;
//
////				//update
////				cout << "=================after locateClusterNodeSetByTargetEdge_general=============" << endl;
////				printClusterBounaryList(graph);
////				printSourceSet(graph);
//
////				VarNodeEdgeTrueV2(graph, true);
//				VarNodeEdgeTrueFromSource(graph);
//			//update
////				graph.node_degree[startID]--;
////				graph.node_degree[endID]--;
////				G.edgeindex++;
//		}
//
////	cout<<"----------->isBranch_generalV2"<<endl;
//	}
//
//	return isBranch;
//
//}

void combinedthecluster(Graph &graph) {

//	clusterNodeSet newClusterNodeset;
//	newClusterNodeset.isSource = false;
//
//	if (graph.startID_clusterSet_it->isSource == true
//			or graph.endID_clusterSet_it->isSource == true) {
//
//		newClusterNodeset.isSource = true;
//	}
//
//	int minnodeid = -1;
//	set<int>::iterator endit = graph.endID_clusterSet_it->clusterSet.begin();
//	for (; endit != graph.endID_clusterSet_it->clusterSet.end(); ++endit) {
//		if (minnodeid == -1) {
//
//			minnodeid = *endit;
//
//		} else {
//			if (minnodeid > *endit) {
//				minnodeid = *endit;
//
//			}
//
//		}
//
//		newClusterNodeset.clusterSet.insert(*endit);
//
//	}
//
//	set<int>::iterator startit =
//			graph.startID_clusterSet_it->clusterSet.begin();
//
//	//
//	for (; startit != graph.startID_clusterSet_it->clusterSet.end();
//			++startit) {
//
//		if (minnodeid == -1) {
//
//			minnodeid = *startit;
//
//		} else {
//			if (minnodeid > *startit) {
//				minnodeid = *startit;
//
//			}
//
//		}
//
//		newClusterNodeset.clusterSet.insert(*startit);
//
//	}
//
//	//update sourceSet
//	if (newClusterNodeset.isSource == true) {
//
//		graph.SourceSet = newClusterNodeset.clusterSet;
//
//	}
//
//	//removed the old cluster_s and cluster_e
//	G.mClusterBounaryList.erase(graph.endID_clusterSet_it);
//	G.mClusterBounaryList.erase(graph.startID_clusterSet_it);
//
//	//insert the new cluster
//
//	bool isinserted = false;
//
//	list<clusterNodeSet>::iterator clusterSet_it =
//			G.mClusterBounaryList.begin();
//	for (; clusterSet_it != G.mClusterBounaryList.end(); ++clusterSet_it) {
//		set<int>::iterator clusterheadIt = clusterSet_it->clusterSet.begin();
//
//		if (minnodeid < *clusterheadIt) {
//			G.mClusterBounaryList.insert(clusterSet_it, newClusterNodeset);
//			isinserted = true;
//			break;
//
//		} else {
//			continue;
//
//		}
//
//	}
//
//	if (isinserted == false) {
//
//		G.mClusterBounaryList.push_back(newClusterNodeset);
//	}

}
//
//void combinedthecluster_general(Graph &graph) {
//
//	clusterNodeSet newClusterNodeset;
//	newClusterNodeset.isSource = false;
//
//	if (graph.pHeadClusterVector[0]->isSource == true
//			or graph.pTailClusterVector[0]->isSource == true) {
//
//		newClusterNodeset.isSource = true;
//
//		if (graph.pHeadClusterVector[0]->isSource == true) {
//			newClusterNodeset.clusterSet =
//					graph.pHeadClusterVector[0]->clusterSet;
//
//		}
//
//	}
//
//	int minnodeid = -1;
//	set<int>::iterator endit = graph.endID_clusterSet_it->clusterSet.begin();
//	for (; endit != graph.endID_clusterSet_it->clusterSet.end(); ++endit) {
//		if (minnodeid == -1) {
//
//			minnodeid = *endit;
//
//		} else {
//			if (minnodeid > *endit) {
//				minnodeid = *endit;
//
//			}
//
//		}
//
//		newClusterNodeset.clusterSet.insert(*endit);
//
//	}
//
//	set<int>::iterator startit =
//			graph.startID_clusterSet_it->clusterSet.begin();
//
//	//
//	for (; startit != graph.startID_clusterSet_it->clusterSet.end();
//			++startit) {
//
//		if (minnodeid == -1) {
//
//			minnodeid = *startit;
//
//		} else {
//			if (minnodeid > *startit) {
//				minnodeid = *startit;
//
//			}
//
//		}
//
//		newClusterNodeset.clusterSet.insert(*startit);
//
//	}
////
////	//update sourceSet
////	if (newClusterNodeset.isSource == true) {
////
////		graph.SourceSet=newClusterNodeset.clusterSet;
////
////	}
//
//	//removed the old cluster_s and cluster_e
//	G.mClusterBounaryList.erase(graph.endID_clusterSet_it);
//	G.mClusterBounaryList.erase(graph.startID_clusterSet_it);
//
//	//insert the new cluster
//
//	bool isinserted = false;
//
//	list<clusterNodeSet>::iterator clusterSet_it =
//			G.mClusterBounaryList.begin();
//	for (; clusterSet_it != G.mClusterBounaryList.end(); ++clusterSet_it) {
//		set<int>::iterator clusterheadIt = clusterSet_it->clusterSet.begin();
//
//		if (minnodeid < *clusterheadIt) {
//			G.mClusterBounaryList.insert(clusterSet_it, newClusterNodeset);
//			isinserted = true;
//			break;
//
//		} else {
//			continue;
//
//		}
//
//	}
//
//	if (isinserted == false) {
//
//		G.mClusterBounaryList.push_back(newClusterNodeset);
//	}
//
//}

//
//void combinedthecluster_generalV2(Graph &graph, int startID, int endID) {
//
//	clusterNodeSet newClusterNodeset;
//	newClusterNodeset.isSource = false;
//	cout << "(graph.pHeadClusterVector[0]->isSource==> "
//			<< graph.pHeadClusterVector[0]->isSource << endl;
//	cout << "(graph.pTailClusterVector[0]->isSource==> "
//			<< graph.pTailClusterVector[0]->isSource << endl;
//
//	if (graph.pHeadClusterVector[0]->isSource == true
//			or graph.pTailClusterVector[0]->isSource == true) {
//
//		newClusterNodeset.isSource = true;
//
//		if (graph.pHeadClusterVector[0]->isSource == true) {
//			newClusterNodeset.clusterSet =
//					graph.pHeadClusterVector[0]->clusterSet;
//			newClusterNodeset.clusterSet.insert(endID);
//			cout << "endID add:" << endID;
//
//			//remove the pHeadClusterVector from clusterlist
//			G.mClusterBounaryList.erase(graph.pHeadClusterVector[0]);
//
//			if (::isContains(graph.VisitedSet, endID)
//					or graph.pTailClusterVector[0]->clusterSet.size() == 1) {
//
//				set<int>::iterator endit =
//						graph.pTailClusterVector[0]->clusterSet.begin();
//				for (; endit != graph.pTailClusterVector[0]->clusterSet.end();
//						endit++) {
//
//					newClusterNodeset.clusterSet.insert(*endit);
//				}
//
//				//remove the pTailClusterVector from clusterlist
//				cout << "endID cluster removed" << endl;
//				G.mClusterBounaryList.erase(graph.pTailClusterVector[0]);
//
//			}
//		} else {
//
//			newClusterNodeset.clusterSet =
//					graph.pTailClusterVector[0]->clusterSet;
//			newClusterNodeset.clusterSet.insert(startID);
//
//			//remove the pHeadClusterVector from clusterlist
//			G.mClusterBounaryList.erase(graph.pTailClusterVector[0]);
//
//			if (::isContains(graph.VisitedSet, startID)
//					or graph.pHeadClusterVector[0]->clusterSet.size() == 1) {
//
//				set<int>::iterator endit =
//						graph.pHeadClusterVector[0]->clusterSet.begin();
//				for (;
//						endit
//								!= graph.pHeadClusterVector[0]->clusterSet.end();
//						endit++) {
//
//					newClusterNodeset.clusterSet.insert(*endit);
//				}
//
//				//remove the pTailClusterVector from clusterlist
//				G.mClusterBounaryList.erase(graph.pHeadClusterVector[0]);
//
//			}
//
//		}
//
//		//insert
//		insertNewCluster(graph, newClusterNodeset);
//
//	} else {
//		set<int>::iterator endit =
//				graph.pTailClusterVector[0]->clusterSet.begin();
//		for (; endit != graph.pTailClusterVector[0]->clusterSet.end();
//				++endit) {
//
//			newClusterNodeset.clusterSet.insert(*endit);
//
//		}
//
//		set<int>::iterator startit =
//				graph.pHeadClusterVector[0]->clusterSet.begin();
//
//		//
//		for (; startit != graph.pHeadClusterVector[0]->clusterSet.end();
//				++startit) {
//
//			newClusterNodeset.clusterSet.insert(*startit);
//
//		}
//
//		//removed the old cluster_s and cluster_e
//		G.mClusterBounaryList.erase(graph.pTailClusterVector[0]);
//		G.mClusterBounaryList.erase(graph.pHeadClusterVector[0]);
//		//
//
//		//insert
//		insertNewCluster(graph, newClusterNodeset);
//
//	}
//
//}

bool isContains(set<int> &A, int id) {
	bool bcontains = false;
	set<int>::iterator it;
	it = A.find(id);
	if (it != A.end()) {
		bcontains = true;
	}

	return bcontains;
}

bool isHavePathFromStoT(Graph &graph, int tID) {

	bool istrue = false;

////	graph.printGraph();
////	graph.printSet();
//
//	set<int>::iterator sit2 = graph.SourceSet.begin();
//	for (; sit2 != graph.SourceSet.end(); sit2++) {
////		if (graph.has_path(*sit2, tID) == true) {
//
//		if (has_path(graph, *sit2, tID) == true) {
//
//			//
//			istrue = true;
//		}
//
////		cout<<"from "<<*sit2<<" to"<< tID<< istrue<<endl;
//
//	}

	return istrue;

}




list<int>* bfs(Graph &graph, int startID) {
	    list<int> *plistVertex = new list<int>;
		list<int> Stack;
		Stack.push_back(startID);
		int layerIndex=1;
//		graph.node_layer[startID]=layerIndex;
//		graph.node_layer[startID]=1;
//		layerIndex++;
		int mapindex=1;

		//bool s[graph.nVerts]={0};    // 鍒ゆ柇鏄惁宸插瓨鍏ヨ鐐瑰埌S闆嗗悎涓�

		bool* s = new bool [graph.nVerts];
		for (int i = 0; i < graph.nVerts; i++) {
			s[i] = false;
		}


		while (!Stack.empty())	//鍙闃熷垪涓嶇┖锛岄亶鍘嗗氨娌℃湁缁撴潫
		{
			int t = Stack.front();
			plistVertex->push_back(t);
			graph.node_map[t]=mapindex;
			mapindex++;
			s[t] = true;
			//鍙栧嚭瀵瑰ご鍏冪礌
			Stack.pop_front();

			//褰撳墠鑺傜偣鐨勯偦灞呰妭鐐�
			set<int> neibors = NeiborsOfNodeV1(graph, t);
			bool isNew=false;

			set<int>::iterator neiborit;
			for (neiborit = neibors.begin(); neiborit != neibors.end();
					neiborit++) {
				int newID = *neiborit;
				if (s[newID] == false) {
					s[newID] = true;
					Stack.push_back(newID);

					//
//					graph.node_layer[newID]=layerIndex;
//					graph.node_layer[newID]=graph.node_layer[t]+1;
					isNew=true;
				}

			}

			if(isNew==true){
				layerIndex++;
			}

		}

		delete []s;

		return plistVertex;


}





list<int>* bfslayer(Graph &graph, int startID) {
	    list<int> *plistVertex = new list<int>;
		list<int> Stack;
		Stack.push_back(startID);
		int layerIndex=1;
		graph.node_layer[startID]=layerIndex;
//		graph.node_layer[startID]=1;
		layerIndex++;
		int mapindex=1;

		//bool s[graph.nVerts]={0};    // 鍒ゆ柇鏄惁宸插瓨鍏ヨ鐐瑰埌S闆嗗悎涓�

		bool* s = new bool [graph.nVerts];
		for (int i = 0; i < graph.nVerts; i++) {
			s[i] = false;
		}


		while (!Stack.empty())	//鍙闃熷垪涓嶇┖锛岄亶鍘嗗氨娌℃湁缁撴潫
		{
			int t = Stack.front();
			plistVertex->push_back(t);
			graph.node_map[t]=mapindex;
			mapindex++;
			s[t] = true;
			//鍙栧嚭瀵瑰ご鍏冪礌
			Stack.pop_front();

			//褰撳墠鑺傜偣鐨勯偦灞呰妭鐐�
			set<int> neibors = NeiborsOfNodeV1(graph, t);
			bool isNew=false;

			set<int>::iterator neiborit;
			for (neiborit = neibors.begin(); neiborit != neibors.end();
					neiborit++) {
				int newID = *neiborit;
				if (s[newID] == false) {
					s[newID] = true;
					Stack.push_back(newID);

					//
//					graph.node_layer[newID]=layerIndex;
					graph.node_layer[newID]=graph.node_layer[t]+1;
					isNew=true;
				}

			}

			if(isNew==true){
				layerIndex++;
			}

		}

		delete []s;

		return plistVertex;


}






list<int>* classicbfs(Graph &graph, int startID) {
	    list<int> *plistVertex = new list<int>;
		list<int> Stack;
		Stack.push_back(startID);
		int layerIndex=1;
		graph.node_layer[startID]=layerIndex;
//		graph.node_layer[startID]=1;
		layerIndex++;
				//bool s[graph.nVerts]={0};    // 鍒ゆ柇鏄惁宸插瓨鍏ヨ鐐瑰埌S闆嗗悎涓�

		bool* s = new bool [graph.nVerts];
		for (int i = 0; i < graph.nVerts; i++) {
			s[i] = false;
		}


		while (!Stack.empty())	//鍙闃熷垪涓嶇┖锛岄亶鍘嗗氨娌℃湁缁撴潫
		{
			int t = Stack.front();
			plistVertex->push_back(t);

			s[t] = true;
			//鍙栧嚭瀵瑰ご鍏冪礌
			Stack.pop_front();

			//褰撳墠鑺傜偣鐨勯偦灞呰妭鐐�
			set<int> neibors = NeiborsOfNodeV1(graph, t);
			bool isNew=false;

			set<int>::iterator neiborit;
			for (neiborit = neibors.begin(); neiborit != neibors.end();
					neiborit++) {
				int newID = *neiborit;
				if (s[newID] == false) {
					s[newID] = true;
					Stack.push_back(newID);

					//
					graph.node_layer[newID]=layerIndex;
//					graph.node_layer[newID]=graph.node_layer[t]+1;
					isNew=true;
				}

			}

			if(isNew==true){
				layerIndex++;
			}

		}

		delete []s;

		return plistVertex;


}




void bfs_layer(Graph &graph, int startID, map<int,set<int>>& layermap){
	cout<<"layer by node============"<<startID<<endl;
	    list<int> listVertex;
		list<int> Stack;
		Stack.push_back(startID);
		int layerIndex=1;
		graph.node_layer.clear();
		graph.node_layer[startID]=layerIndex;
		layermap[layerIndex].insert(startID);

//		graph.node_layer[startID]=1;
//		layerIndex++;
		int mapindex=1;

		//bool s[graph.nVerts]={0};    // 鍒ゆ柇鏄惁宸插瓨鍏ヨ鐐瑰埌S闆嗗悎涓�

		bool* s = new bool [graph.nVerts];
		for (int i = 0; i < graph.nVerts; i++) {
			s[i] = false;
		}


		while (!Stack.empty())	//鍙闃熷垪涓嶇┖锛岄亶鍘嗗氨娌℃湁缁撴潫
		{
			int t = Stack.front();
			listVertex.push_back(t);

			s[t] = true;
			//鍙栧嚭瀵瑰ご鍏冪礌
			Stack.pop_front();

			//褰撳墠鑺傜偣鐨勯偦灞呰妭鐐�
			set<int> neibors = NeiborsOfNodeV1(graph, t);
			bool isNew=false;

			set<int>::iterator neiborit;
			for (neiborit = neibors.begin(); neiborit != neibors.end();
					neiborit++) {
				int newID = *neiborit;
				if (s[newID] == false) {
					s[newID] = true;
					Stack.push_back(newID);

					//
//					graph.node_layer[newID]=layerIndex;
					graph.node_layer[newID]=graph.node_layer[t]+1;


					layermap[graph.node_layer[t]+1].insert(newID);

					isNew=true;
				}

			}

//			if(isNew==true){
//				layerIndex++;
//			}

		}

		delete [] s;


		 map<int,set<int>>::iterator layerit=layermap.begin();
		 for(;layerit!=layermap.end();layerit++){
			 cout<<"layer="<<layerit->first<<endl;
			 set<int>::iterator intemIt=layerit->second.begin();
			 for(;intemIt!=layerit->second.end();intemIt++){
				 cout<<" "<<*intemIt;

			 }
			 cout<<endl;
		 }
//
//
//		cout<<"order nodelist as follows:"<<endl;
//
//		list<int>::iterator nodeit= listVertex.begin();
//		for(;nodeit!= listVertex.end();nodeit++){
//			cout<<" "<<*nodeit;
//		}
//		cout<<endl;



}




void bfs_layerFromID(Graph &graph, int startID,map<int,int>&BFSLayermap){
	    list<int> *plistVertex = new list<int>;
		list<int> Stack;
		Stack.push_back(startID);
		int layerIndex=1;
		BFSLayermap.clear();
		BFSLayermap[startID]=layerIndex;

//		graph.node_layer[startID]=1;
//		layerIndex++;
		int mapindex=1;

		//bool s[graph.nVerts]={0};    // 鍒ゆ柇鏄惁宸插瓨鍏ヨ鐐瑰埌S闆嗗悎涓�

		bool* s = new bool [graph.nVerts];
		for (int i = 0; i < graph.nVerts; i++) {
			s[i] = false;
		}


		while (!Stack.empty())	//鍙闃熷垪涓嶇┖锛岄亶鍘嗗氨娌℃湁缁撴潫
		{
			int t = Stack.front();
			plistVertex->push_back(t);

			s[t] = true;
			//鍙栧嚭瀵瑰ご鍏冪礌
			Stack.pop_front();

			//褰撳墠鑺傜偣鐨勯偦灞呰妭鐐�
			set<int> neibors = NeiborsOfNodeV1(graph, t);
			bool isNew=false;

			set<int>::iterator neiborit;
			for (neiborit = neibors.begin(); neiborit != neibors.end();
					neiborit++) {
				int newID = *neiborit;
				if (s[newID] == false) {
					s[newID] = true;
					Stack.push_back(newID);

					//
//					graph.node_layer[newID]=layerIndex;
					graph.node_layer[newID]=graph.node_layer[t]+1;
					BFSLayermap[newID]=BFSLayermap[t]+1;

					isNew=true;
				}

			}

//			if(isNew==true){
//				layerIndex++;
//			}

		}

		delete []s;





}







list<int>* bfs_degreeLarge(Graph &graph, int startID) {
	set<int> VisitedSet;

	list<int> *plistVertex = new list<int>;
	list<int> Stack;
	Stack.push_back(startID);
	int mapindex = 1;

	//bool s[graph.nVerts]={0};    // 鍒ゆ柇鏄惁宸插瓨鍏ヨ鐐瑰埌S闆嗗悎涓�

	bool *s = new bool[graph.nVerts];
	for (int i = 0; i < graph.nVerts; i++) {
		s[i] = false;
	}

	while (!Stack.empty())	//鍙闃熷垪涓嶇┖锛岄亶鍘嗗氨娌℃湁缁撴潫
	{
		int t = Stack.front();
		plistVertex->push_back(t);
		//
		VisitedSet.insert(t);
		//
		graph.node_map[t] = mapindex;
		mapindex++;
		s[t] = true;
		//鍙栧嚭瀵瑰ご鍏冪礌
		Stack.pop_front();

		//褰撳墠鑺傜偣鐨勯偦灞呰妭鐐�

//		list<int> sortedneibors = sortedNeibors(graph, t);
		list<int> sortedneibors = sortedNeibors_byVisitedSet(graph, t,VisitedSet);

		list<int>::iterator neiborit = sortedneibors.begin();

		for (; neiborit != sortedneibors.end(); neiborit++) {
			int newID = *neiborit;
			if (s[newID] == false) {
				s[newID] = true;
				Stack.push_back(newID);
			}

		}

//			set<int> neibors = NeiborsOfNodeV1(graph, t);
//			set<int>::iterator neiborit;
//			for (neiborit = neibors.begin(); neiborit != neibors.end();
//					neiborit++) {
//				int newID = *neiborit;
//				if (s[newID] == false) {
//					s[newID] = true;
//					Stack.push_back(newID);
//				}
//
//			}

	}

	delete s;

	return plistVertex;

}



list<int>* bfs_improveV1(Graph &graph, int startID) {
	set<int> VisitedSet;
	set<int> BoundarySet;

	list<int> *plistVertex = new list<int>;
	list<int> Stack;
	Stack.push_back(startID);
	int mapindex = 1;

	//bool s[graph.nVerts]={0};    // 鍒ゆ柇鏄惁宸插瓨鍏ヨ鐐瑰埌S闆嗗悎涓�

	bool *s = new bool[graph.nVerts];
	for (int i = 0; i < graph.nVerts; i++) {
		s[i] = false;
	}

	while (!Stack.empty())	//鍙闃熷垪涓嶇┖锛岄亶鍘嗗氨娌℃湁缁撴潫
	{
		int t = Stack.front();
		plistVertex->push_back(t);
		//
		VisitedSet.insert(t);
		BoundarySet.insert(t);
		//
		graph.node_map[t] = mapindex;
		mapindex++;
		s[t] = true;
		//鍙栧嚭瀵瑰ご鍏冪礌
		Stack.pop_front();

		//褰撳墠鑺傜偣鐨勯偦灞呰妭鐐�

//		list<int> sortedneibors = sortedNeibors(graph, t);
		list<int> sortedneibors = sortedNeibors_byVisitedSet(graph, t,VisitedSet);

		list<int>::iterator neiborit = sortedneibors.begin();

		for (; neiborit != sortedneibors.end(); neiborit++) {
			int newID = *neiborit;
			if (s[newID] == false) {
				s[newID] = true;
				Stack.push_back(newID);
			}

		}

//			set<int> neibors = NeiborsOfNodeV1(graph, t);
//			set<int>::iterator neiborit;
//			for (neiborit = neibors.begin(); neiborit != neibors.end();
//					neiborit++) {
//				int newID = *neiborit;
//				if (s[newID] == false) {
//					s[newID] = true;
//					Stack.push_back(newID);
//				}
//
//			}

	}

	delete []s;

	return plistVertex;

}



list<int>* bfs_degreeLess(Graph &graph, int startID) {
	    list<int> *plistVertex = new list<int>;
		list<int> Stack;
		Stack.push_back(startID);
		int mapindex=1;

		//bool s[graph.nVerts]={0};    // 鍒ゆ柇鏄惁宸插瓨鍏ヨ鐐瑰埌S闆嗗悎涓�

		bool* s = new bool [graph.nVerts];
		for (int i = 0; i < graph.nVerts; i++) {
			s[i] = false;
		}


		while (!Stack.empty())	//鍙闃熷垪涓嶇┖锛岄亶鍘嗗氨娌℃湁缁撴潫
		{
			int t = Stack.front();
			plistVertex->push_back(t);
			graph.node_map[t]=mapindex;
			mapindex++;
			s[t] = true;
			//鍙栧嚭瀵瑰ご鍏冪礌
			Stack.pop_front();

			//褰撳墠鑺傜偣鐨勯偦灞呰妭鐐�


			list<int> sortedneibors = sortedNeibors_less(graph, t);
		list<int>::iterator neiborit = sortedneibors.begin();

		for (; neiborit != sortedneibors.end(); neiborit++) {
			int newID = *neiborit;
			if (s[newID] == false) {
				s[newID] = true;
				Stack.push_back(newID);
			}

		}




//			set<int> neibors = NeiborsOfNodeV1(graph, t);
//			set<int>::iterator neiborit;
//			for (neiborit = neibors.begin(); neiborit != neibors.end();
//					neiborit++) {
//				int newID = *neiborit;
//				if (s[newID] == false) {
//					s[newID] = true;
//					Stack.push_back(newID);
//				}
//
//			}

		}

		delete []s;

		return plistVertex;


}



list<int>* bfsV2(Graph &graph, int startID) {
	    list<int> *plistVertex = new list<int>;
		list<int> Stack;
		Stack.push_back(startID);
		int mapindex=1;

		//bool s[graph.nVerts]={0};    // 鍒ゆ柇鏄惁宸插瓨鍏ヨ鐐瑰埌S闆嗗悎涓�
		int* s = new int[graph.nVerts];
		for (int i = 0; i < graph.nVerts; i++) {
			s[i] = false;
		}

		while (!Stack.empty())	//鍙闃熷垪涓嶇┖锛岄亶鍘嗗氨娌℃湁缁撴潫
		{
			int t = Stack.front();
			plistVertex->push_back(t);
//			graph.node_map[t]=mapindex;
			mapindex++;
			s[t] = true;
			//鍙栧嚭瀵瑰ご鍏冪礌
			Stack.pop_front();

			//褰撳墠鑺傜偣鐨勯偦灞呰妭鐐�
			set<int> neibors = NeiborsOfNodeV2(graph, t);

			set<int>::iterator neiborit;
			for (neiborit = neibors.begin(); neiborit != neibors.end();
					neiborit++) {
				int newID = *neiborit;
				if (s[newID] == false) {
					s[newID] = true;
					Stack.push_back(newID);
				}

			}

		}

		delete []s;

		return plistVertex;


}




set<int> NeiborsOfNode_bfs(Graph &graph,int nodeID){
	set<int> neibor_V;
	map<int,double>::iterator neiborit =graph.adj_relia[nodeID].begin();
	for(;neiborit!=graph.adj_relia[nodeID].end();++neiborit){
		neibor_V.insert(neiborit->first);
	}

   return neibor_V;
}




bool isTargetNodeVisitedCompleted(Graph& graph,int targetID,set<int>&VisitedSet){

	bool iscompleted=false;
	set<int> neiborSet=::NeiborsOfNodeV1(graph, targetID);

	set<int>::iterator neiborit=neiborSet.begin();
	for(;neiborit!=neiborSet.begin();neiborit++){
		if(::isContains(VisitedSet, *neiborit)==false){

			break;
		}else{
			iscompleted=true;
			continue;
		}
	}

	if(neiborSet.size()==0){
		iscompleted=true;
	}

   return iscompleted;

}

void KeyNodeLayerInfoCompute(map<int, vector<int>> &NodeKeyInfomap,
		Graph &graph, int &scanlinelen) {
	cout << "graph nodes size:" << graph.adj_relia.size() << endl;
	map<int, set<int>> layermap;
	bfs_layer(graph, graph.sID, layermap);
	printLayerInfo(layermap, graph.sID);

	map<int, set<int>>::iterator layerit = layermap.begin();
	int nodessize = 0;
	int onetimes=0;
	int maxLen=0;
	for (; layerit != layermap.end(); layerit++) {

		nodessize += layerit->second.size();
		if(layerit->second.size()==1){
			onetimes++;
		}
		if(maxLen<layerit->second.size()){
			maxLen=layerit->second.size();
		}

	}

	scanlinelen = nodessize/ (layermap.size()-onetimes) + 1;
	if(scanlinelen>=maxLen){
		scanlinelen=maxLen-2;
	}



	//remove the

	for (int layerindex = 1; layerindex <= layermap.size(); layerindex++) {
//			cout << "expanded again for the layer:"<<layerindex<<endl;
		set<int>::iterator layerItemIt = layermap[layerindex].begin();
		map<int, set<int>> layertempmap;
		for (; layerItemIt != layermap[layerindex].end(); layerItemIt++) {
//				 cout<<"********start of node *****"<<*layerItemIt<<endl;
			bfs_layer(graph, *layerItemIt, layertempmap);
//				printLayerInfo(layertempmap,*layerItemIt);

			::recordKeyInfoForNode(NodeKeyInfomap, layertempmap, *layerItemIt);

//	            cout<<"*************************end of node *****"<<*layerItemIt<<endl;
			layertempmap.clear();

		}

	}

	map<int, vector<int>>::iterator nodeKeyInfoIit = NodeKeyInfomap.begin();
	for (; nodeKeyInfoIit != NodeKeyInfomap.end(); nodeKeyInfoIit++) {
		cout << "node ID:" << nodeKeyInfoIit->first;
		cout << "largest layer size:" << nodeKeyInfoIit->second[0];
		cout << "   #times:" << nodeKeyInfoIit->second[1];
		cout << "   #layerLen:" << nodeKeyInfoIit->second[2] << endl;
	}

	bfs_layer(graph, graph.sID, layermap);
	printLayerInfo(layermap, graph.sID);

}


void KeyNodeLayerInfoComForTargetSet(map<int, vector<int>>&AbsorptionInfomap,map<int,int>& BreathInfomap,
		Graph &graph, set<int>&targetSet, set<int> &BoundarySet) {
	map<int, set<int>> layermap;
//	cout << "graph nodes size:" << graph.adj_relia.size() << endl;
	set<int>::iterator targetIDit = targetSet.begin();
	for (; targetIDit != targetSet.end(); targetIDit++) {
//		cout << "expanded again for the nodeID:" << *targetIDit << endl;
		map<int, set<int>> layertempmap;

		bfs_layer(graph, *targetIDit, layertempmap);
//		printLayerInfo(layertempmap, *targetIDit);
//		cout<<"layertempmap size:=================="<<layertempmap.size()<<endl;
//		printLayerInfo(layertempmap,*targetIDit);

//		::recordKeyInfoForNodeByBoundarySet(AbsorptionInfomap, layermap, *targetIDit, BoundarySet);
//		cout<<"before record A"<<endl;
		::recordAbsorptionInfoForNodeByBoundarySet(AbsorptionInfomap, layertempmap, *targetIDit, BoundarySet);
//		cout<<"finish record A"<<endl;

		::recordBreadthInfoForNode(BreathInfomap, layertempmap, *targetIDit);
//		cout<<"finish record B"<<endl;

//		cout << "*************************end of node *****" << *targetIDit
//				<< endl;
		layertempmap.clear();

	}

//	map<int, vector<int>>::iterator nodeKeyInfoIit = NodeKeyInfomap.begin();
//	for (; nodeKeyInfoIit != NodeKeyInfomap.end(); nodeKeyInfoIit++) {
//		cout << "neibor ID:" << nodeKeyInfoIit->first;
//		int size = nodeKeyInfoIit->second.size();
//
//		cout << "------------------layer items Interset by Boundary size as follows:" << endl;
//		for (int index = 0; index < size; index++) {
//			cout << "layer:" << index;
//			cout << " intersize:" << nodeKeyInfoIit->second[index] << endl;
//
//		}
//	}

}


void KeyNodeBreathInfoForTargetSet(map<int, vector<int>> &NodeKeyInfomap,
		Graph &graph, set<int> targetSet, set<int> &BoundarySet) {
	map<int, set<int>> layermap;
//	cout << "graph nodes size:" << graph.adj_relia.size() << endl;
	set<int>::iterator targetIDit = targetSet.begin();
	for (; targetIDit != targetSet.end(); targetIDit++) {
//		cout << "expanded again for the nodeID:" << *targetIDit << endl;
		map<int, set<int>> layertempmap;

		bfs_layer(graph, *targetIDit, layertempmap);
//		printLayerInfo(layertempmap, *targetIDit);
//		cout<<"layertempmap size:=================="<<layertempmap.size()<<endl;
//		printLayerInfo(layertempmap,*targetIDit);



		::recordKeyInfoForNodeByBoundarySet(NodeKeyInfomap, layertempmap,
				*targetIDit, BoundarySet);

//		cout << "*************************end of node *****" << *targetIDit
//				<< endl;
		layertempmap.clear();

	}

//	map<int, vector<int>>::iterator nodeKeyInfoIit = NodeKeyInfomap.begin();
//	for (; nodeKeyInfoIit != NodeKeyInfomap.end(); nodeKeyInfoIit++) {
//		cout << "neibor ID:" << nodeKeyInfoIit->first;
//		int size = nodeKeyInfoIit->second.size();
//
//		cout << "------------------layer items Interset by Boundary size as follows:" << endl;
//		for (int index = 0; index < size; index++) {
//			cout << "layer:" << index;
//			cout << " intersize:" << nodeKeyInfoIit->second[index] << endl;
//
//		}
//	}

}



void printNodeMap(Graph& graph){
	cout<<"node map as follows:"<<endl;
	map<int, int>::iterator nodemapit = graph.node_map.begin();
	for (; nodemapit != graph.node_map.end(); nodemapit++) {
		cout << "from ID:" << nodemapit->first << "  to ID:"
				<< nodemapit->second << endl;

	}
	cout<<"_____________________"<<endl;
}



void SelectedEdgebyScanline(Graph &graph, set<int> &visitedSet,
		set<int> &BoundarySet, int &nodemapindex) {
	map<int, vector<int>> NodeKeyInfomap;
	////	Graph InfoGraph = graph;
	////	InfoGraph.adj_relia=graph.adj_relia;
	////	InfoGraph.sID=graph.sID;
	Edge next_selectedEdge;
	Edge nodeEdge;
	int targetID = graph.sID;
	int averaygeNodePerLayer = 0;
	KeyNodeLayerInfoCompute(NodeKeyInfomap, graph,averaygeNodePerLayer);
	int maxLayer = 0;
	int maxsize=0;

	map<int, vector<int>>::iterator nodeKeyInfoIit = NodeKeyInfomap.begin();
	for (; nodeKeyInfoIit != NodeKeyInfomap.end(); nodeKeyInfoIit++) {
		//		cout << "node ID:" << nodeKeyInfoIit->first;
		//		cout << "largest layer size:" << nodeKeyInfoIit->second[0];
		//		cout << "   #times:" << nodeKeyInfoIit->second[1];
		//		cout << "   #layerLen:" << nodeKeyInfoIit->second[2] << endl;
		if (maxLayer < nodeKeyInfoIit->second[2]) {
			maxLayer = nodeKeyInfoIit->second[2];
		}

		if(maxsize<nodeKeyInfoIit->second[0]){
			maxsize=nodeKeyInfoIit->second[0];
		}
	}

	averaygeNodePerLayer = graph.adj_relia.size() / maxLayer + 4;
	averaygeNodePerLayer =maxsize;


	cout << "averaygeNodePerLayer:=================" << averaygeNodePerLayer << endl;
	for (int index = 2; index <= averaygeNodePerLayer; index++) {
		cout << "index..." << index;

		if (selecteEdge_byNodesNebor(graph, visitedSet, targetID,
				NodeKeyInfomap, next_selectedEdge, index, 0) == true) {
			cout << "visited edge:=========" << next_selectedEdge.StartID
					<< "---->" << next_selectedEdge.EndID << endl;

			if (::isContains(visitedSet, next_selectedEdge.EndID) == false) {
				nodeEdge.StartID = next_selectedEdge.EndID;
				nodeEdge.EndID = next_selectedEdge.EndID;
				nodeEdge.relia = 0.9;
				graph.vectorEdge.push_back(nodeEdge);
				//	//nodemap
				graph.node_map[next_selectedEdge.EndID] = nodemapindex;
				nodemapindex++;

				//				cout<<"update nodemap:"<<endl;

				//				tempNodelayermap[next_selectedEdge.EndID]=tempNodelayermap[next_selectedEdge.StartID]+1;
				targetID = next_selectedEdge.EndID;
			}

			//
			//
			//
			//			cout<<"new  edge----::::"<<graph.node_map[next_selectedEdge.StartID]<<
			//					"======================>"<<graph.node_map[next_selectedEdge.EndID]<<endl;

			//push general edge
			graph.vectorEdge.push_back(next_selectedEdge);

			//update visitedSet and BoundarySet
			visitedSet.insert(next_selectedEdge.EndID);

			////removed the seleted edge
			::deleteEdge(graph, next_selectedEdge.StartID,
					next_selectedEdge.EndID);

			if (::isTargetNodeVisitedCompleted(graph, next_selectedEdge.StartID,
					visitedSet) == true) {
				//				cout<<"node"<<next_selectedEdge.StartID<<"remove node from BoundarySet"<<endl;
				BoundarySet.erase(next_selectedEdge.StartID);
			} else {
				BoundarySet.insert(next_selectedEdge.StartID);
			}

			if (::isTargetNodeVisitedCompleted(graph, next_selectedEdge.EndID,
					visitedSet) == true) {
				//				cout<<"node"<<next_selectedEdge.EndID<<"remove node from BoundarySet"<<endl;
				BoundarySet.erase(next_selectedEdge.EndID);
			} else {
				BoundarySet.insert(next_selectedEdge.EndID);
			}

		}

	}

	cout << "finished scanline*********************************************"
			<< endl;
}


void setOrderedEdgelist(Graph &graph,set<int>& visitedSet,set<int>& BoundarySet){


	//generate layer
	map<int, set<int>> layermap;
	map<int, vector<int>> BreadthInfomap;

	graph.adj_relia.size();

	int seed=-1;

	map<int,map<int,double> >::iterator nodeit=graph.adj_relia.begin();
    for(; nodeit!=graph.adj_relia.end();nodeit++){
    	layermap.clear();
    	bfs_layer(graph,nodeit->first, layermap);
    	::recordBandTimeInfoForNode(BreadthInfomap, layermap,nodeit->first);
    }

    nodeit = graph.adj_relia.begin();
	for (; nodeit != graph.adj_relia.end(); nodeit++) {
		cout<<"node :"<<nodeit->first<<" breath:"<<BreadthInfomap[nodeit->first][0]<<"times:"<<BreadthInfomap[nodeit->first][1]<<endl;
		if(seed==-1){
			seed=nodeit->first;
		}else{

			if(BreadthInfomap[seed][0]>BreadthInfomap[nodeit->first][0]){
				seed=nodeit->first;
			}else if(BreadthInfomap[seed][0]==BreadthInfomap[nodeit->first][0]){
				if(BreadthInfomap[seed][1]>BreadthInfomap[nodeit->first][1]){
					seed=nodeit->first;
				}
			}


		}
	}

	seed=graph.sID;

	layermap.clear();
	 bfs_layer(graph,seed, layermap);



	//starting from sID
	visitedSet.insert(seed);
	BoundarySet.insert(seed);
	//initial varEdgelist
	Edge edge;
	edge.StartID = seed;
	edge.EndID = seed;
	edge.relia = 0.9;
	graph.vectorEdge.push_back(edge);

	int nodemapindex = 1;

	//node map
	graph.node_map.clear();
	graph.node_map[seed] = nodemapindex;
	//layer
	nodemapindex++;









	Edge next_selectedEdge;
	Edge nodeEdge;
	bool iscontinue=true;

	map<int,int> nodeLayerBySID;

	int times=0;

	bool seedNodefinished=false;


	while (iscontinue) {

//		cout<<"the edge finded********************************************* "<<times <<"----------------------------"<<endl;
		if(seedNodefinished==true){
		seed=::findSeedWithMinLayerandDegree(graph, BoundarySet);}

		if(seed==0){
			break;
		}

		if (::selecteEdge_byLayer(graph,seed, BoundarySet, next_selectedEdge, layermap)== true) {
//			if(times<=6){
//			::WriteToEdgelistForDecom(graph, times,BoundarySet);
//			}

			times++;

//
//
//			 cout<<"aftter selecteEdge_byPreSetsByLayerInfo"<<endl;
//
//			  printNodeMap(graph);

//			cout<<"==================visited edge:========="<<next_selectedEdge.StartID<<"---->"<<next_selectedEdge.EndID<<endl;

			//generate node edge
			//push node edge
			if (::isContains(visitedSet, next_selectedEdge.EndID) == false) {
				nodeEdge.StartID = next_selectedEdge.EndID;
				nodeEdge.EndID = next_selectedEdge.EndID;
				nodeEdge.relia = 0.9;
				graph.vectorEdge.push_back(nodeEdge);
				//	//nodemap
				graph.node_map[next_selectedEdge.EndID] = nodemapindex;
				nodemapindex++;

//				cout<<"update nodemap:"<<endl;

//				tempNodelayermap[next_selectedEdge.EndID]=tempNodelayermap[next_selectedEdge.StartID]+1;
			}

//
//
//
//			cout<<"==============map  edge----::::"<<graph.node_map[next_selectedEdge.StartID]<<
//					"======================>"<<graph.node_map[next_selectedEdge.EndID]<<endl;

			//push general edge
			graph.vectorEdge.push_back(next_selectedEdge);

			//update visitedSet and BoundarySet
			visitedSet.insert(next_selectedEdge.EndID);

			////removed the seleted edge
			::deleteEdge(graph, next_selectedEdge.StartID,
					next_selectedEdge.EndID);

			if (::isTargetNodeVisitedCompleted(graph, next_selectedEdge.StartID,
					visitedSet) == true) {
//				cout<<"node"<<next_selectedEdge.StartID<<"remove node from BoundarySet"<<endl;
				BoundarySet.erase(next_selectedEdge.StartID);
			} else {
				BoundarySet.insert(next_selectedEdge.StartID);
			}

			if (::isTargetNodeVisitedCompleted(graph, next_selectedEdge.EndID,
					visitedSet) == true) {
//				cout<<"node"<<next_selectedEdge.EndID<<"remove node from BoundarySet"<<endl;
				BoundarySet.erase(next_selectedEdge.EndID);
			} else {
				BoundarySet.insert(next_selectedEdge.EndID);
			}

			if (::isTargetNodeVisitedCompleted(graph, seed,
								visitedSet) == true){
				seedNodefinished=true;
				seed=0;
			}


			//print boundary set
//			cout<<"Boundary set as follows:"<<endl;
//			set<int>::iterator bit=BoundarySet.begin();
//			for(;bit!=BoundarySet.end();bit++){
//				cout<<"node:"<<*bit<<"---";
//			}
//			cout<<endl;
			//iscontinue


			if (BoundarySet.empty() == false) {

				iscontinue = true;
			} else {
				iscontinue = false;
				break;
			}

		}

//		//print boundary set
//		cout << "Visited set as follows:" << endl;
//		set<int>::iterator vit = visitedSet.begin();
//		for (; vit != visitedSet.end(); vit++) {
//			cout << "node:" << *vit << "---";
//		}
//		cout<<endl;

	}
}

void Initialization_VarEdgeList(Graph &graph){

//	list<int> *bfsnodeslist = new list<int>;
//	cout<<"before bfs"<<endl;
////	bfsnodeslist = bfs(graph, startID);
//	bfsnodeslist = bfs_degreeLarge(graph, startID);
////	bfsnodeslist = bfs_degreeLess(graph, startID);

	set<int> visitedSet;
	set<int> BoundarySet;

	map<int,map<int,double> >original_adj_relia=graph.adj_relia;

	//starting from sID
	visitedSet.insert(graph.sID);
	BoundarySet.insert(graph.sID);
    //initial varEdgelist
	Edge edge;
	edge.StartID=graph.sID;
	edge.EndID=graph.sID;
	edge.relia=0.9;
	graph.vectorEdge.push_back(edge);

	int nodemapindex=1;

	//node map

	map<int,int> tempNodelayermap;

   graph.node_map.clear();

	graph.node_map[graph.sID]=nodemapindex;
	//layer
	tempNodelayermap[graph.sID]=1;
	graph.node_layer[graph.sID]=1;
	nodemapindex++;

	//loop
	bool iscontinue=true;
	//next_selectedEdge
	Edge next_selectedEdge;
	//node edge
	Edge nodeEdge;
	int debugtimes=0;

	int Fmax=0;

	while(iscontinue){
//		debugtimes++;
//		cout<<"nodes detials in network***********************"<<endl;
//
//		map<int, map<int, double> >::iterator edgeit = graph.adj_relia.begin();
//		for (; edgeit != graph.adj_relia.end(); edgeit++) {
//			cout<<"nodeID:"<<edgeit->first<<"----degree="<<graph.adj_relia[edgeit->first].size()<<endl;
//			cout<<"    neibor as follows:"<<endl;
//			map<int, double>::iterator neiborit=graph.adj_relia[edgeit->first].begin();
//			for(;neiborit!=graph.adj_relia[edgeit->first].end();neiborit++){
//				cout<<"    :"<<neiborit->first<<"---";
//			}
//			cout<<endl;
//
//
//		}
//		cout<<"*****************************************"<<endl;


		if(::selecteEdge_byPreSets(graph, visitedSet, BoundarySet, next_selectedEdge)==true){
//			cout<<"visited edge:======================================*************************"<<next_selectedEdge.StartID<<"---->"<<next_selectedEdge.EndID<<endl;

			//generate node edge
			//push node edge
			if (::isContains(visitedSet, next_selectedEdge.EndID) == false) {
				nodeEdge.StartID = next_selectedEdge.EndID;
				nodeEdge.EndID = next_selectedEdge.EndID;
				nodeEdge.relia = 0.9;
				graph.vectorEdge.push_back(nodeEdge);
				//	//nodemap
				graph.node_map[next_selectedEdge.EndID]=nodemapindex;
				nodemapindex++;

//				tempNodelayermap[next_selectedEdge.EndID]=tempNodelayermap[next_selectedEdge.StartID]+1;
			}

			//push general edge
			graph.vectorEdge.push_back(next_selectedEdge);



			//update visitedSet and BoundarySet
			visitedSet.insert(next_selectedEdge.EndID);

			////removed the seleted edge
			::deleteEdge(graph,  next_selectedEdge.StartID, next_selectedEdge.EndID);


			if (::isTargetNodeVisitedCompleted(graph, next_selectedEdge.StartID,
					visitedSet) == true) {
//				cout<<"node"<<next_selectedEdge.StartID<<"remove node from BoundarySet"<<endl;
				BoundarySet.erase(next_selectedEdge.StartID);
			} else {
				BoundarySet.insert(next_selectedEdge.StartID);
			}

			if(::isTargetNodeVisitedCompleted(graph, next_selectedEdge.EndID, visitedSet)==true){
//				cout<<"node"<<next_selectedEdge.EndID<<"remove node from BoundarySet"<<endl;
					BoundarySet.erase(next_selectedEdge.EndID);
			}else{
				BoundarySet.insert(next_selectedEdge.EndID);
			}







			//print boundary set
//			cout<<"Boundary set as follows:"<<endl;
//			set<int>::iterator bit=BoundarySet.begin();
//			for(;bit!=BoundarySet.end();bit++){
//				cout<<"node:"<<*bit<<"---";
//			}
//			cout<<endl;
			//iscontinue

			if(BoundarySet.empty()==false){
				iscontinue=true;
			}else{
				iscontinue=false;
				break;
			}

			if(Fmax<BoundarySet.size()){
				Fmax=BoundarySet.size();
			}


		}


//		//print boundary set
//		cout << "Visited set as follows:" << endl;
//		set<int>::iterator vit = visitedSet.begin();
//		for (; vit != visitedSet.end(); vit++) {
//			cout << "node:" << *vit << "---";
//		}
//		cout<<endl;


	}




//	cout<<"final original to new map as follows:"<<endl;
//	map<int,int>::iterator mapotit=graph.node_map.begin();
//	for(;mapotit!=graph.node_map.end();mapotit++){
////		graph.node_layer[mapotit->second]=tempNodelayermap[mapotit->first];
//		cout<<" orinial ID,"<<mapotit->first<<"new ID,"<<mapotit->second<<",layer,"<<graph.node_layer[mapotit->first]<<endl;
//
//	}



//	adj_reliaV2 initialization
//	cout<<"initial adj_reliaV2"<<endl;
	map<int,map<int,double> >::iterator mapit=original_adj_relia.begin();
	for(;mapit!=original_adj_relia.end();mapit++){
		int nodemap1=graph.node_map[mapit->first];
		map<int,double>::iterator neiborit=mapit->second.begin();
		for(;neiborit!=mapit->second.end();neiborit++){
			int nodemap2=graph.node_map[neiborit->first];
			graph.adj_reliaV2[nodemap1][nodemap2]=neiborit->second;

//			cout<<nodemap1<<"--->"<<nodemap2<<":"<<neiborit->second<<endl;

		}



	}

//    cout<<"reset K"<<endl;
	set<int> K=graph.KterminalSet;
	graph.KterminalSet.clear();
	graph.VisitedSet.clear();
	set<int>::iterator kit=K.begin();
	for(; kit!=K.end();kit++){
		graph.KterminalSet.insert(graph.node_map[*kit]);
		graph.VisitedSet.insert(graph.node_map[*kit]);


	}

//	kit=graph.KterminalSet.begin();
//	for(; kit!=graph.KterminalSet.end();kit++){
//		cout<<"node K:"<<*kit<<endl;
//
//	}
//	cout<<"reset node_degreee"<<endl;
	graph.node_degree.clear();
//	graph.node_degree= vector<int>(graph.adj_reliaV2.size()+1);
//
//	map<int, map<int, double> >::iterator edgeit = graph.adj_reliaV2.begin();
//	for (; edgeit != graph.adj_reliaV2.end(); edgeit++) {
//        int size=graph.adj_reliaV2[edgeit->first].size();
//		graph.node_degree[edgeit->first]=size;
//
//	}


	graph.node_degreeArray= vector<int>(graph.adj_reliaV2.size()+1);

	//	cout<<"node_degreeArray len======"<<graph.node_degreeArray.size()<<endl;



	map<int, map<int, double> >::iterator edgeit = graph.adj_reliaV2.begin();
	for (; edgeit != graph.adj_reliaV2.end(); edgeit++) {
		int size = graph.adj_reliaV2[edgeit->first].size();
		graph.node_degreeArray[edgeit->first] = size;
	}

//	cout << "node_degreee" << endl;
//
//	for (int i = 1; i < graph.node_degreeArray.size(); i++) {
//		cout << "nodeID:" << i << "  degree is:" << graph.node_degreeArray[i]<<endl;
////				<< endl;
////		cout<<"======layer is:"<<graph.node_layer[i]<<endl;
//	}


	//update edgeslist

	int edgesize=graph.vectorEdge.size();
//	cout << "--edgesize--" <<edgesize << endl;
	for (int index=0; index<edgesize; index++) {

		//update

		graph.vectorEdge[index].StartID=graph.node_map[graph.vectorEdge[index].StartID];
		graph.vectorEdge[index].EndID=graph.node_map[graph.vectorEdge[index].EndID];

	}




   cout<<"Fmax of "<<graph.networkName<<" is:"<<Fmax<<endl;


}




void InitialBoundaryDegree(GraphNode &graphNode, int len,Graph& G) {
	G.minBoundaryNodeID = 1;

	if (len > G.node_degreeArray.size()) {
		len = G.node_degreeArray.size() - 1;
	}
	G.node_degree = vector<short int>(len);
	int index = G.minBoundaryNodeID;
	int i = 0;
//	cout << "node len:" << len << endl;
	for (; index <=len; index++) {
		G.node_degree[i] = G.node_degreeArray[index];
		i++;
	}
	G.node_degree.shrink_to_fit();
//	cout << " initialization degree finished" << endl;


}



void initial_nodeDegreeV2(Graph &graph){

	graph.node_degree.clear();
	graph.node_degree = vector<short int>(graph.nVerts);

	map<int, map<int, double> >::iterator edgeit = graph.adj_reliaV2.begin();
	for (; edgeit != graph.adj_reliaV2.end(); edgeit++) {
        int size=graph.adj_reliaV2[edgeit->first].size();
		graph.node_degree[edgeit->first]=size;

	}

}



list<int>* bfs_selectedFromK(Graph &graph) {

	int size=graph.KterminalSet.size();
    int a=1;
	int startIDindex=(rand() % (size-a+1))+ a-1;
	cout<<"startIDindex:"<<startIDindex<<endl;
    int startID=-1;
	set<int>::iterator nodeit=graph.KterminalSet.begin();
	int index=0;
	for(;nodeit!=graph.KterminalSet.end();nodeit++){
		if(index==startIDindex){
			startID=*nodeit;
			break;
		}
		index++;


	}




	list<int> *plistVertex = new list<int>;
	list<int> Stack;
	Stack.push_back(startID);

	int* s = new int[graph.nVerts];
	for (int i = 0; i < graph.nVerts; i++) {
		s[i] = false;
	}

	while (!Stack.empty())	//只要队列不空，遍历就没有结束
	{
		int t = Stack.front();
		plistVertex->push_back(t);
		s[t] = true;
		//取出对头元素
		Stack.pop_front();

		//当前节点的邻居节点
		vector<int> neibors = NeiborsOfNode(graph, t);

		vector<int>::iterator neiborit;
		for (neiborit = neibors.begin(); neiborit != neibors.end();
				neiborit++) {
			int newID = *neiborit;
			if (s[newID] == false) {
				s[newID] = true;
				cout << "node is pushed:" << newID << endl;
				Stack.push_back(newID);
			}

		}

	}

	delete[]s;

	return plistVertex;

}



list<int>* bfs_Kset(Graph &graph) {
	list<int> *plistVertex = new list<int>;
	list<int> Stack;
	set<int>::iterator knodeit=graph.KterminalSet.begin();
	for(;knodeit!=graph.KterminalSet.end();knodeit++){
		Stack.push_back(*knodeit);
	}

	bool *s =new bool[graph.nVerts]();    // 判断是否已存入该点到S集合中

	while (!Stack.empty())	//只要队列不空，遍历就没有结束
	{
		int t = Stack.front();
		plistVertex->push_back(t);
		s[t] = true;
		//取出对头元素
		Stack.pop_front();

		//当前节点的邻居节点
		vector<int> neibors = NeiborsOfNode(graph, t);

		vector<int>::iterator neiborit;
		for (neiborit = neibors.begin(); neiborit != neibors.end();
				neiborit++) {
			int newID = *neiborit;
			if (s[newID] == false) {
				s[newID] = true;
				cout << "node is pushed:" << newID << endl;
				Stack.push_back(newID);
			}

		}

	}

	delete [] s;

	return plistVertex;

}




list<int>* layerBFS(Graph &graph) {
	list<int> *plistVertex = new list<int>;
	map<int,map<int,double> >::iterator nodeit=graph.adj_relia.begin();
	for(;nodeit!=graph.adj_relia.end();nodeit++){
		plistVertex->push_back(nodeit->first);
	}

	return plistVertex;

}



list<int>* general_bfs(Graph &graph, int startID) {
	list<int> *plistVertex = new list<int>;
	list<int> Stack;
	Stack.push_back(startID);

	int* s = new int[graph.nVerts];
	for (int i = 0; i < graph.nVerts; i++) {
		s[i] = false;
	}

	while (!Stack.empty())	//只要队列不空，遍历就没有结束
	{
		int t = Stack.front();
		plistVertex->push_back(t);
		s[t] = true;
		//取出对头元素
		Stack.pop_front();

		//当前节点的邻居节点
		vector<int> neibors = NeiborsOfNode(graph, t);

		vector<int>::iterator neiborit;
		for (neiborit = neibors.begin(); neiborit != neibors.end();
				neiborit++) {
			int newID = *neiborit;
			if (s[newID] == false) {
				s[newID] = true;
				Stack.push_back(newID);
			}

		}

	}

	delete[]s;

	return plistVertex;

}

list<int>* bfs_degree(Graph &graph, int startID) {
	list<int> *plistVertex = new list<int>;
	list<int> Stack;
	Stack.push_back(startID);

	int* s = new int[graph.nVerts];
	for (int i = 0; i < graph.nVerts; i++) {
		s[i] = false;
	}

	while (!Stack.empty())	//只要队列不空，遍历就没有结束
	{
		int t = Stack.front();
		plistVertex->push_back(t);
		s[t] = true;
		//取出对头元素
		Stack.pop_front();

		//当前节点的邻居节点
//		vector<int> neibors = NeiborsOfNode(graph, t);

//		vector<int>::iterator neiborit=neibors.begin();

		list<int> sortedneibors = sortedNeibors(graph, t);
		list<int>::iterator neiborit = sortedneibors.begin();

		for (; neiborit != sortedneibors.end(); neiborit++) {
			int newID = *neiborit;
			if (s[newID] == false) {
				s[newID] = true;
				Stack.push_back(newID);
			}

		}

	}
	delete[]s;
	return plistVertex;

}




list<int> sortedNeibors(Graph &graph, int nodeID) {

	list<int> neiborlist;
	map<int, double>::iterator neiborit = graph.adj_relia[nodeID].begin();
	for (; neiborit != graph.adj_relia[nodeID].end(); neiborit++) {
		if (neiborlist.empty() == true) {
			neiborlist.push_back(neiborit->first);
		} else {
			int degreeofneiborNode = graph.adj_relia[neiborit->first].size();
			list<int>::iterator listit = neiborlist.begin();
			bool isinserted = false;
			for (; listit != neiborlist.end(); listit++) {
				if (graph.adj_relia[*listit].size() < degreeofneiborNode) {

					neiborlist.insert(listit, neiborit->first);
					isinserted = true;
					break;
				}

			}
			if (isinserted == false) {
				neiborlist.push_back(neiborit->first);
			}

		}

	}

	return neiborlist;

}


list<int> sortedNeibors_byVisitedSet(Graph &graph, int nodeID,
		set<int> &visitedSet) {

	list<int> neiborlist;
	map<int, double>::iterator neiborit = graph.adj_relia[nodeID].begin();
	for (; neiborit != graph.adj_relia[nodeID].end(); neiborit++) {
		if (::isContains(visitedSet, neiborit->first) == false) {
			if (neiborlist.empty() == true) {
				neiborlist.push_back(neiborit->first);
			} else {
				int importantofTargetNeiborID =::importanceOfTargetID(graph, neiborit->first, visitedSet);
				list<int>::iterator listit = neiborlist.begin();
				bool isinserted = false;
				for (; listit != neiborlist.end(); listit++) {
					int importIndexofExistID = ::importanceOfTargetID(graph,
							*listit, visitedSet);
					if (importIndexofExistID < importantofTargetNeiborID) {
						neiborlist.insert(listit, neiborit->first);
						isinserted = true;
						break;

					}

				}
				if (isinserted == false) {
					neiborlist.push_back(neiborit->first);
				}

			}

		}
	}

	return neiborlist;

}



void importindexbasedBounarySet(Graph&graph,set<int>& BoundarySet,int nodeID,int &importanctDegreeIndex,int &importanctBoundaryIndex){

	importanctBoundaryIndex=0;
	//get the neibors of nodeID
	set<int> neiborset=::NeiborsOfNodeV1(graph, nodeID);
	set<int>::iterator neiborIt=neiborset.begin();
	for(; neiborIt!=neiborset.end();neiborIt++){
		if(::isContains(BoundarySet, *neiborIt)==true){
			importanctBoundaryIndex++;
		}
	}


	importanctDegreeIndex=neiborset.size()*-1;



}


//

int  compareBoundaryIndex(vector<int>& oldVec,vector<int>& newVec){
    int isequal=0;
    /*
     * euqual 0, less -1, larger 1
     */

//    cout<<"original:"<<endl;
//    ::PrintVectorInfo(oldVec);
//
//    cout<<"new:"<<endl;
//     ::PrintVectorInfo(newVec);

    int sizeold=oldVec.size();
    int sizeNew=newVec.size();

    int size=(sizeold>sizeNew)?sizeNew:sizeold;

    if(size>2){
    	size=2;
    }

    for(int index=0;index<size;index++){
    	if(oldVec[index]>newVec[index]){
    		isequal=1;
    		cout<<"large"<<endl;
    		break;
    	}else if(oldVec[index]<newVec[index]){
    		isequal=-1;
    		cout<<"less"<<endl;
    		 break;

    	}


    }





	return isequal;



}


int  compareBoundaryIndexByzeroIndex(vector<int>& oldVec,vector<int>& newVec){
    int isequal=0;
    /*
     * euqual 0, less -1, larger 1
     */

//    cout<<"original:"<<endl;
//    ::PrintVectorInfo(oldVec);
//
//    cout<<"new:"<<endl;
//     ::PrintVectorInfo(newVec);



	if (oldVec[0] > newVec[0]) {
		isequal = 1;
		cout << "large" << endl;

	} else if (oldVec[0] < newVec[0]) {
		isequal = -1;
		cout << "less" << endl;


	}








	return isequal;



}


int  compareBoundaryIndexByoneIndex(vector<int>& oldVec,vector<int>& newVec){
    int isequal=0;
    /*
     * euqual 0, less -1, larger 1
     */

//    cout<<"original:"<<endl;
//    ::PrintVectorInfo(oldVec);
//
//    cout<<"new:"<<endl;
//     ::PrintVectorInfo(newVec);



	if (oldVec[1] > newVec[1]) {
		isequal = 1;
		cout << "large" << endl;

	} else if (oldVec[1] < newVec[1]) {
		isequal = -1;
		cout << "less" << endl;


	}








	return isequal;



}


void GetNeiborSetofBS(Graph &graph, set<int>& BoundarySet,set<int>& NeiborsSet){
	set<int>::iterator sourcenodeIt = BoundarySet.begin();
	for (sourcenodeIt = BoundarySet.begin(); sourcenodeIt != BoundarySet.end();
				sourcenodeIt++){
		set<int> neibors=::NeiborsOfNodeV1(graph, *sourcenodeIt);

		set<int>::iterator nit=neibors.begin();
		for(;nit!=neibors.end();nit++){
			NeiborsSet.insert(*nit);
		}
	}


}



void PrintAbsorptionInfo(map<int, vector<int>>& ABsorptionMap,int nodeID){
	cout<<" "<<nodeID<<" :AbsorptionVector is:";
	vector<int>v =ABsorptionMap[nodeID];
	int size=v.size();
	for(int i=0;i<size;i++){
		cout<<" "<<v[i];
	}
	cout<<endl;

}

void PrintSelectedEdgeInfo(Edge & edge){
	cout<<"edge update:("<<edge.StartID<<"-"<<edge.EndID<<")"<<endl;

}

void PrintLayerIndex(map<int,int>&BFSLayermap,int nodeID){

	cout<<nodeID<<":layer is:"<<BFSLayermap[nodeID]<<endl;

}


void PrintNodeDegreeInfo(Graph &graph,int nodeid){

	cout<<nodeid<<" :Degree is:"<<graph.adj_relia[nodeid].size()<<endl;
}

void PrintNodeDegreeInfoFromSet(Graph &G,set<int>& TargetSet){

	set<int>::iterator sit=TargetSet.begin();
	for (; sit != TargetSet.end(); sit++) {

		cout <<*sit<< " :Degree is:" << G.node_degree[*sit-G.minBoundaryNodeID]
				<< endl;
	}
}



void PrintNodeOrderInfo(Graph &graph,int nodeid){

	cout<<nodeid<<" :Visited Order is:"<<graph.node_map[nodeid]<<endl;
}

void PrintBreathInfo(map<int,int>& BreathInfomap,int id){
	cout<<" "<<id<<" :B metric:"<<BreathInfomap[id]<<endl;


}


void PrintBreathInfoFromSet(map<int,int>& BreathInfomap,set<int>& TargetSet){
	   set<int>::iterator sit=TargetSet.begin();
		for (; sit != TargetSet.end(); sit++) {
			PrintBreathInfo(BreathInfomap,*sit);
		}


}


void GetMinDegreeSetFromTarSetSet(Graph& graph,set<int>& TargetSet,set<int>&SetofMinDegree){
	set<int>::iterator sIt=TargetSet.begin();
	int mindegree=0;
	for (;sIt!=TargetSet.end(); sIt++){
		if(mindegree==0){
			mindegree=graph.adj_relia[*sIt].size();
		}else if(mindegree>graph.adj_relia[*sIt].size()){
			mindegree=graph.adj_relia[*sIt].size();
		}

	}

	for (sIt=TargetSet.begin();sIt!=TargetSet.end(); sIt++){
		if(mindegree==graph.adj_relia[*sIt].size()){
			SetofMinDegree.insert(*sIt);
		}

	}


}


void GetMaxAbsorptionSetFromTargetSet(map<int, vector<int>> AbsorptionInfomap,set<int>&TargetSet, set<int>& MaxAbsorpSet,int zeroIndex){
	int maxBreath=0;
	if (zeroIndex == 0) {
		set<int>::iterator sit = TargetSet.begin();
		for (; sit != TargetSet.end(); sit++) {
			if (maxBreath == 0) {
				maxBreath = AbsorptionInfomap[*sit][zeroIndex];
			} else if (maxBreath < AbsorptionInfomap[*sit][zeroIndex]) {
				maxBreath = AbsorptionInfomap[*sit][zeroIndex];
			}

		}

		for (sit = TargetSet.begin(); sit != TargetSet.end(); sit++) {
			if (maxBreath == AbsorptionInfomap[*sit][zeroIndex]) {
				MaxAbsorpSet.insert(*sit);
			}

		}
	}else{

		vector<int>MaxVectorValue;
		int isInitial=0;

		set<int>::iterator sit = TargetSet.begin();
		for (; sit != TargetSet.end(); sit++) {
			if (isInitial == 0) {
				MaxVectorValue = AbsorptionInfomap[*sit];
				isInitial=1;
			} else{

				int result =::compareBoundaryIndex(MaxVectorValue,  AbsorptionInfomap[*sit]);
				if(result<0){
					MaxVectorValue = AbsorptionInfomap[*sit];
				}
			}

		}

		for (sit = TargetSet.begin(); sit != TargetSet.end(); sit++) {
			int result =::compareBoundaryIndex(MaxVectorValue,  AbsorptionInfomap[*sit]);
			if (result ==0) {
				MaxAbsorpSet.insert(*sit);
			}

		}


	}



}




void GetMinBreathSetFromTargetSet(map<int,int> &BreathInfomap,set<int>&TargetSet, set<int>& MinBset){
	int minBreath=0;
	set<int>::iterator sit = TargetSet.begin();
	for (; sit != TargetSet.end(); sit++) {
		if (minBreath == 0) {
			minBreath = BreathInfomap[*sit];
		} else if (minBreath > BreathInfomap[*sit]) {
			minBreath = BreathInfomap[*sit];
		}


	}

	for ( sit = TargetSet.begin(); sit != TargetSet.end(); sit++) {
		if (minBreath==BreathInfomap[*sit]) {
			MinBset.insert(*sit);
		}

	}




}



void GetMinLayerSetFromTargetSet(map<int,int> &LayerInfomap,set<int>&TargetSet, set<int>& MinLayerset){
	int minLayer=0;
	set<int>::iterator sit = TargetSet.begin();
	for (; sit != TargetSet.end(); sit++) {
		if (minLayer == 0) {
			minLayer = LayerInfomap[*sit];
		} else if (minLayer > LayerInfomap[*sit]) {
			minLayer = LayerInfomap[*sit];
		}


	}

	for ( sit = TargetSet.begin(); sit != TargetSet.end(); sit++) {
		if (minLayer==LayerInfomap[*sit]) {
			MinLayerset.insert(*sit);
		}

	}




}


int GetNodeByMinVisitedFromSet(Graph &graph,set<int> Candidate_set){
	int nodeID=0;
	set<int>::iterator sit;
	if(Candidate_set.size()>1){

		for (sit = Candidate_set.begin(); sit != Candidate_set.end();
				sit++) {
		   ::PrintNodeOrderInfo(graph,*sit );
		   if(nodeID==0){
			   nodeID=*sit;

		   }else{
			   if(graph.node_map[nodeID]>graph.node_map[*sit]){
				   nodeID=*sit;
			   }
		   }
		}

	}else{
		sit = Candidate_set.begin();
		nodeID=*sit;
	}

	return nodeID;
}


int GetNodeByMinDFromSet(Graph &graph,set<int>& Candidate_set){

	int nodeID = 0;
	set<int>::iterator sit;
	if (Candidate_set.size() > 1) {

		for (sit = Candidate_set.begin(); sit != Candidate_set.end(); sit++) {
			::PrintNodeDegreeInfo(graph, *sit);
			if (nodeID == 0) {
				nodeID = *sit;

			} else if (graph.adj_relia[nodeID] > graph.adj_relia[*sit]) {
				nodeID = *sit;
			}
		}

	} else {
		sit = Candidate_set.begin();
		nodeID = *sit;
	}

	return nodeID;

}


void GetcandiEdgebySVisitedOrder(Graph &graph,vector<Edge>& candi_edgeVector, Edge & selectedEdge){
	int size=candi_edgeVector.size();
	int locateIndex=0;

	for(int index=1;index<size;index++){

      if(graph.node_map[candi_edgeVector[locateIndex].StartID]
						>graph.node_map[candi_edgeVector[index].StartID]){
    	  locateIndex=index;
      }

	}

	selectedEdge=candi_edgeVector[locateIndex];

}



void GetCandiEdgeVectorbyEndsSet(Graph &graph,set<int>&BS,set<int>&MAbByMInDSet,vector<Edge>& candi_edgeVector){
	set<int>::iterator endIDit = MAbByMInDSet.begin();
	for (; endIDit != MAbByMInDSet.end(); endIDit++) {
		set<int> neiborOFendID = ::NeiborsOfNodeV1(graph, *endIDit);
		set<int> StartIDSet = intersectionSet(neiborOFendID, BS);
		Edge edge;
		set<int>::iterator sit=StartIDSet.begin();
		for(;sit!=StartIDSet.end();sit++){
			edge.StartID=*sit;
			edge.EndID=*endIDit;
			edge.relia=graph.adj_relia[edge.StartID][edge.EndID];

			candi_edgeVector.push_back(edge);
		}
	}


}

void GetEdgeVectorByMinBFSLayer(	map<int,int>&BFSLayermap,vector<Edge>& candi_edgeVector){
	int edgesize=candi_edgeVector.size();
	int minbfslayer=-1;
	for(int index=0;index<edgesize;index++){

		int startID=candi_edgeVector[index].StartID;

		if(minbfslayer==-1){
			minbfslayer=BFSLayermap[startID];
		}else if(minbfslayer>BFSLayermap[startID]){
			minbfslayer=BFSLayermap[startID];
		}


	}

	vector<Edge> filterEdge;

	for(int index=0;index<edgesize;index++){

		int startID=candi_edgeVector[index].StartID;

		if(minbfslayer==BFSLayermap[startID]){
			filterEdge.push_back(candi_edgeVector[index]);
		}


	}

	candi_edgeVector=filterEdge;


}



void GetEdgeVectorByMaxBFSLayer(	map<int,int>&BFSLayermap,vector<Edge>& candi_edgeVector){
	int edgesize=candi_edgeVector.size();
	int maxbfslayer=-1;
	for(int index=0;index<edgesize;index++){

		int startID=candi_edgeVector[index].StartID;

		if(maxbfslayer==-1){
			maxbfslayer=BFSLayermap[startID];
		}else if(maxbfslayer<BFSLayermap[startID]){
			maxbfslayer=BFSLayermap[startID];
		}


	}

	vector<Edge> filterEdge;

	for(int index=0;index<edgesize;index++){

		int startID=candi_edgeVector[index].StartID;

		if(maxbfslayer==BFSLayermap[startID]){
			filterEdge.push_back(candi_edgeVector[index]);
		}


	}

	candi_edgeVector=filterEdge;


}

int findSeedWithMinLayerandDegree(Graph& graph,set<int>& BoundarySet){

	int minLayer=0;
	int seed=0;

    set<int>::iterator sourcenodeIt=BoundarySet.begin();
    seed=*sourcenodeIt;
    minLayer=graph.node_layer[*sourcenodeIt];

	for( sourcenodeIt=BoundarySet.begin();sourcenodeIt!=BoundarySet.end();sourcenodeIt++){

		if(minLayer>graph.node_layer[*sourcenodeIt]){
			minLayer=graph.node_layer[*sourcenodeIt];
			seed=*sourcenodeIt;
		}
	}

	for( sourcenodeIt=BoundarySet.begin();sourcenodeIt!=BoundarySet.end();sourcenodeIt++){

		if(minLayer==graph.node_layer[*sourcenodeIt]){
			if(graph.adj_relia[seed]>graph.adj_relia[*sourcenodeIt]){
				seed=*sourcenodeIt;
			}
		}
	}

    return seed;

}



bool selecteEdge_byLayer(Graph &graph,int seedNode,set<int>& BoundarySet,Edge &selectedEdge,map<int,set<int>>& layermap) {


//	cout<<"###############selecteEdge_byPreSets..............."<<endl;

	bool is_selectedEdge=false;
	bool isfinded=false;
	int issetting=0;


	set<int>neiborsSetOfSourcenode=::NeiborsOfNodeV1(graph, seedNode);
	set<int>::iterator neibornodeit = neiborsSetOfSourcenode.begin();
	for (; neibornodeit != neiborsSetOfSourcenode.end();
			neibornodeit++) {

		if (::isContains(BoundarySet, *neibornodeit) == true) {
			if (issetting == 0) {

				selectedEdge.StartID =seedNode;
				selectedEdge.EndID = *neibornodeit;
				selectedEdge.relia =
						graph.adj_relia[selectedEdge.StartID][selectedEdge.EndID];
				isfinded = true;
				is_selectedEdge = true;

			} else {

				if (graph.node_layer[selectedEdge.EndID]
						> graph.node_layer[*neibornodeit]) {

					selectedEdge.EndID = *neibornodeit;
					selectedEdge.relia =
							graph.adj_relia[selectedEdge.StartID][selectedEdge.EndID];
					isfinded = true;

				} else if (graph.node_layer[selectedEdge.EndID]
						== graph.node_layer[*neibornodeit]) {

					if(graph.adj_relia[selectedEdge.EndID].size()<graph.adj_relia[*neibornodeit].size())
					selectedEdge.EndID = *neibornodeit;
					selectedEdge.relia =
							graph.adj_relia[selectedEdge.StartID][selectedEdge.EndID];
					isfinded = true;

				}

			}
//				break;
		}
	}


  if(isfinded==false){

		for (neibornodeit = neiborsSetOfSourcenode.begin();
				neibornodeit != neiborsSetOfSourcenode.end(); neibornodeit++) {
			if (issetting == 0) {

					selectedEdge.StartID =seedNode;
					selectedEdge.EndID = *neibornodeit;
					selectedEdge.relia =
							graph.adj_relia[selectedEdge.StartID][selectedEdge.EndID];
					isfinded = true;
					is_selectedEdge = true;

				} else if (graph.node_layer[selectedEdge.EndID]
							> graph.node_layer[*neibornodeit]) {

						selectedEdge.EndID = *neibornodeit;
						selectedEdge.relia =
								graph.adj_relia[selectedEdge.StartID][selectedEdge.EndID];
						isfinded = true;

					} else if (graph.node_layer[selectedEdge.EndID]
							== graph.node_layer[*neibornodeit]) {

						if(graph.adj_relia[selectedEdge.EndID].size()<graph.adj_relia[*neibornodeit].size())
						selectedEdge.EndID = *neibornodeit;
						selectedEdge.relia =
								graph.adj_relia[selectedEdge.StartID][selectedEdge.EndID];
						isfinded = true;

					}
				}

  }
//	cout<<"the finial selected edge:"<<selectedEdge.StartID<<"---->"<<selectedEdge.EndID<<endl;
//	cout<<"#########################end of selecteEdge_byPreSets"<<endl;
	return is_selectedEdge;

}





bool selecteEdge_byPreSetsBFS(Graph &graph, set<int> &visitedSet,set<int>& BoundarySet,Edge &selectedEdge) {


//	cout<<"###############selecteEdge_byPreSets..............."<<endl;

	bool is_selectedEdge=false;

	//selected the optimal nodeID from the Candidate node set
	// Candidate node set is the neibor nodes of BoundarySet,and Meet the conditions for not be Visited

	// get the Candidate node set


//	set<int>::iterator bnodeit=BoundarySet.begin();
//	for(; bnodeit!=BoundarySet.end();bnodeit++){
//
//		set<int> neiborofNodeSet=NeiborsOfNodeV1(graph, *bnodeit);
//		set<int>::iterator neibornodeit=neiborofNodeSet.begin();
//		for(;neibornodeit!=neiborofNodeSet.end();neibornodeit++){
//			if(::isContains(visitedSet, *neibornodeit)==false){
//				CandidateNode_set.insert(*neibornodeit);
//			}
//
//		}
//
//
//	}

	//
	//ends nodes Selection conditions
	/*(1) the ends nodes in BoundarySet
	 *(2) the neibor nodes of  CandidateNode contain more nodes in BoundarySet firstly
	 *(3) the neibor nodes of  CandidateNode contain more nodes  firstly
	 */
    int isinBoundary=0;
	int importanctBoundaryIndex_temp=0;
	int importanctDegreeIndex_temp=0;

	int importanctBoundaryIndex_opti=-1;
	int importanctDegreeIndex_opti=-1;
	int minlayer=0;
	bool isfinded=false;

	set<int>::iterator sourcenodeIt=BoundarySet.begin();
	set<int>neiborsSetOfSourcenode;

	if (BoundarySet.size() >= 11) {
		isfinded = true;
		is_selectedEdge = true;

		//from the first node in BoundarySet
		int firstVisitedID = *sourcenodeIt;
		for (; sourcenodeIt != BoundarySet.end(); sourcenodeIt++) {
			if (graph.node_map[firstVisitedID]
					> graph.node_map[*sourcenodeIt]) {
				firstVisitedID = *sourcenodeIt;
			}

		}

		neiborsSetOfSourcenode = ::NeiborsOfNodeV1(graph, firstVisitedID);
		set<int>::iterator neibornodeit = neiborsSetOfSourcenode.begin();
		for (; neibornodeit != neiborsSetOfSourcenode.end(); neibornodeit++) {

			if (::isContains(BoundarySet, *neibornodeit) == true) {

				selectedEdge.StartID = firstVisitedID;
				selectedEdge.EndID = *neibornodeit;
				selectedEdge.relia =
						graph.adj_relia[selectedEdge.StartID][selectedEdge.EndID];

				break;

			} else {

				::importindexbasedBounarySet(graph, BoundarySet, *neibornodeit,
						importanctDegreeIndex_temp,
						importanctBoundaryIndex_temp);

				if (importanctBoundaryIndex_opti == -1) {

					selectedEdge.StartID = firstVisitedID;
					selectedEdge.EndID = *neibornodeit;
					selectedEdge.relia =
							graph.adj_relia[selectedEdge.StartID][selectedEdge.EndID];
					//

					importanctBoundaryIndex_opti = importanctBoundaryIndex_temp;
					importanctDegreeIndex_opti = importanctDegreeIndex_temp;
					} else {
					//compare

					if (importanctBoundaryIndex_opti
							< importanctBoundaryIndex_temp) {
						selectedEdge.StartID = firstVisitedID;
						selectedEdge.EndID = *neibornodeit;
						selectedEdge.relia =
								graph.adj_relia[selectedEdge.StartID][selectedEdge.EndID];
						//

						importanctBoundaryIndex_opti =
								importanctBoundaryIndex_temp;
						importanctDegreeIndex_opti = importanctDegreeIndex_temp;

					} else if (importanctBoundaryIndex_opti
							== importanctBoundaryIndex_temp) {

						if (importanctDegreeIndex_opti
								< importanctDegreeIndex_temp) {

							selectedEdge.StartID = firstVisitedID;
							selectedEdge.EndID = *neibornodeit;
							selectedEdge.relia =
									graph.adj_relia[selectedEdge.StartID][selectedEdge.EndID];
							//

							importanctBoundaryIndex_opti =
									importanctBoundaryIndex_temp;
							importanctDegreeIndex_opti =
									importanctDegreeIndex_temp;

						}
					}

				}
//				break;
			}

		}

	}






	if (isfinded == false) {

		set<int> CandidateSet;
		sourcenodeIt=BoundarySet.begin();

		int mindofCandidate = 0;
		for (sourcenodeIt = BoundarySet.begin();
				sourcenodeIt != BoundarySet.end(); sourcenodeIt++) {
//		cout<<"sourcenodeID=================="<<*sourcenodeIt<<endl;

			//get the source neibors

			neiborsSetOfSourcenode = ::NeiborsOfNodeV1(graph, *sourcenodeIt);
			set<int>::iterator neibornodeit = neiborsSetOfSourcenode.begin();
			for (; neibornodeit != neiborsSetOfSourcenode.end();
					neibornodeit++) {

				if (::isContains(BoundarySet, *neibornodeit) == true) {
					if (mindofCandidate == 0) {
						mindofCandidate = neiborsSetOfSourcenode.size();
						CandidateSet.insert(*sourcenodeIt);

						//
						selectedEdge.StartID = *sourcenodeIt;
						selectedEdge.EndID = *neibornodeit;
						selectedEdge.relia =
								graph.adj_relia[selectedEdge.StartID][selectedEdge.EndID];
						isfinded = true;
						is_selectedEdge = true;

					} else {

						if (graph.node_map[selectedEdge.StartID]
								> graph.node_map[*sourcenodeIt]) {
							selectedEdge.StartID = *sourcenodeIt;
							selectedEdge.EndID = *neibornodeit;
							selectedEdge.relia =
									graph.adj_relia[selectedEdge.StartID][selectedEdge.EndID];
							isfinded = true;

						}

					}
//				break;
				}
			}

		}

	}


  if(isfinded==false){

    int selectedDgreeofS=0;

	for(sourcenodeIt=BoundarySet.begin(); sourcenodeIt!=BoundarySet.end();sourcenodeIt++){
//		cout<<"sourcenodeID=================="<<*sourcenodeIt<<endl;


		//get the source neibors

		neiborsSetOfSourcenode=::NeiborsOfNodeV1(graph, *sourcenodeIt);
	    set<int>::iterator neibornodeit=neiborsSetOfSourcenode.begin();
		if (isfinded == true) {
			is_selectedEdge = true;
			break;
		}


		for (; neibornodeit != neiborsSetOfSourcenode.end();
				neibornodeit++) {


			if(::isContains(BoundarySet,*neibornodeit)==true){
				//in BSet and selected directly
				selectedEdge.StartID=*sourcenodeIt;
				selectedEdge.EndID=*neibornodeit;
				selectedEdge.relia=graph.adj_relia[selectedEdge.StartID][selectedEdge.EndID];
				isfinded=true;
				break;

			}

			//neibor nodes of the temp neibornode
			::importindexbasedBounarySet(graph, BoundarySet, *neibornodeit, importanctDegreeIndex_temp, importanctBoundaryIndex_temp);
//			cout<<"importanctBoundaryIndex_temp:"<<importanctBoundaryIndex_temp<<endl;
//			cout<<"importanctDegreeIndex_temp:"<<importanctDegreeIndex_temp<<endl;
			if(importanctBoundaryIndex_opti==-1){
				selectedDgreeofS=neiborsSetOfSourcenode.size();
				is_selectedEdge=true;
				selectedEdge.StartID=*sourcenodeIt;
				selectedEdge.EndID=*neibornodeit;
				selectedEdge.relia=graph.adj_relia[selectedEdge.StartID][selectedEdge.EndID];
				//

				importanctBoundaryIndex_opti=importanctBoundaryIndex_temp;
				importanctDegreeIndex_opti=importanctDegreeIndex_temp;
//				minlayer=graph.node_layer[selectedEdge.StartID];

				//seting...frist
//				cout<<"setting ... first"<<endl;
//				cout<<"importanctBoundaryIndex_opti:"<<importanctBoundaryIndex_opti<<endl;
//				cout<<"importanctDegreeIndex_opti:"<<importanctDegreeIndex_opti<<endl;
//				cout<<"the selected edge:"<<selectedEdge.StartID<<"---->"<<selectedEdge.EndID<<endl;
			}else{
				//compare

				if(importanctBoundaryIndex_opti<importanctBoundaryIndex_temp){
					selectedDgreeofS=neiborsSetOfSourcenode.size();
					selectedEdge.StartID=*sourcenodeIt;
					selectedEdge.EndID=*neibornodeit;
					selectedEdge.relia=graph.adj_relia[selectedEdge.StartID][selectedEdge.EndID];
					//

					importanctBoundaryIndex_opti=importanctBoundaryIndex_temp;
					importanctDegreeIndex_opti=importanctDegreeIndex_temp;

					//seting...frist
//					cout<<"setting ... again"<<endl;
//
//
//					cout<<"importanctBoundaryIndex_opti:"<<importanctBoundaryIndex_opti<<endl;
//					cout<<"importanctDegreeIndex_opti:"<<importanctDegreeIndex_opti<<endl;
//
//					cout<<"the new selected edge:"<<selectedEdge.StartID<<"---->"<<selectedEdge.EndID<<endl;


				}else if(importanctBoundaryIndex_opti==importanctBoundaryIndex_temp){









					if(importanctDegreeIndex_opti<importanctDegreeIndex_temp){
						selectedDgreeofS=neiborsSetOfSourcenode.size();
						selectedEdge.StartID=*sourcenodeIt;
						selectedEdge.EndID=*neibornodeit;
						selectedEdge.relia=graph.adj_relia[selectedEdge.StartID][selectedEdge.EndID];
						//

						importanctBoundaryIndex_opti=importanctBoundaryIndex_temp;
						importanctDegreeIndex_opti=importanctDegreeIndex_temp;

						} else if (importanctDegreeIndex_opti
								== importanctDegreeIndex_temp) {
							if (graph.node_map[selectedEdge.StartID]
									> graph.node_map[*sourcenodeIt]) {
								selectedEdge.StartID = *sourcenodeIt;
								selectedEdge.EndID = *neibornodeit;
								selectedEdge.relia =
										graph.adj_relia[selectedEdge.StartID][selectedEdge.EndID];

							}

						}

			}

		}


	}
  }
  }
//	cout<<"the finial selected edge:"<<selectedEdge.StartID<<"---->"<<selectedEdge.EndID<<endl;
//	cout<<"#########################end of selecteEdge_byPreSets"<<endl;
	return is_selectedEdge;

}




bool selecteEdge_byPreSets(Graph &graph, set<int> &visitedSet,set<int>& BoundarySet,Edge &selectedEdge) {


//	cout<<"###############selecteEdge_byPreSets..............."<<endl;

	bool is_selectedEdge=false;

	//selected the optimal nodeID from the Candidate node set
	// Candidate node set is the neibor nodes of BoundarySet,and Meet the conditions for not be Visited

	// get the Candidate node set


//	set<int>::iterator bnodeit=BoundarySet.begin();
//	for(; bnodeit!=BoundarySet.end();bnodeit++){
//
//		set<int> neiborofNodeSet=NeiborsOfNodeV1(graph, *bnodeit);
//		set<int>::iterator neibornodeit=neiborofNodeSet.begin();
//		for(;neibornodeit!=neiborofNodeSet.end();neibornodeit++){
//			if(::isContains(visitedSet, *neibornodeit)==false){
//				CandidateNode_set.insert(*neibornodeit);
//			}
//
//		}
//
//
//	}

	//
	//ends nodes Selection conditions
	/*(1) the ends nodes in BoundarySet
	 *(2) the neibor nodes of  CandidateNode contain more nodes in BoundarySet firstly
	 *(3) the neibor nodes of  CandidateNode contain more nodes  firstly
	 */
    int isinBoundary=0;
	int importanctBoundaryIndex_temp=0;
	int importanctDegreeIndex_temp=0;

	int importanctBoundaryIndex_opti=-1;
	int importanctDegreeIndex_opti=-1;
	int minlayer=0;




	set<int>::iterator sourcenodeIt=BoundarySet.begin();

	set<int>neiborsSetOfSourcenode;

	bool isfinded=false;

	minlayer=graph.node_layer[*sourcenodeIt];

	for(; sourcenodeIt!=BoundarySet.end();sourcenodeIt++){

		if(minlayer>graph.node_layer[*sourcenodeIt]){
			minlayer=graph.node_layer[*sourcenodeIt];
		}

	}

//    set<int> CandidateSet;

    int mindofCandidate=0;
	for (sourcenodeIt = BoundarySet.begin(); sourcenodeIt != BoundarySet.end();
			sourcenodeIt++) {
//		cout<<"sourcenodeID=================="<<*sourcenodeIt<<endl;

		//get the source neibors

		neiborsSetOfSourcenode = ::NeiborsOfNodeV1(graph, *sourcenodeIt);
		set<int>::iterator neibornodeit = neiborsSetOfSourcenode.begin();
		for (; neibornodeit != neiborsSetOfSourcenode.end(); neibornodeit++) {

			if (::isContains(BoundarySet, *neibornodeit) == true) {
				if(mindofCandidate==0){
					mindofCandidate=neiborsSetOfSourcenode.size();
//					CandidateSet.insert(*sourcenodeIt);

					//
					selectedEdge.StartID=*sourcenodeIt;
					selectedEdge.EndID=*neibornodeit;
					selectedEdge.relia=graph.adj_relia[selectedEdge.StartID][selectedEdge.EndID];
					isfinded=true;
					is_selectedEdge=true;

				}else{
					if(mindofCandidate>neiborsSetOfSourcenode.size()){
						mindofCandidate=neiborsSetOfSourcenode.size();
//						CandidateSet.clear();
//						CandidateSet.insert(*sourcenodeIt);
						selectedEdge.StartID=*sourcenodeIt;
						selectedEdge.EndID=*neibornodeit;
						selectedEdge.relia=graph.adj_relia[selectedEdge.StartID][selectedEdge.EndID];
						isfinded=true;

					}
				}
				break;
			}
		}


	}

    if(isfinded==true){
    	int tempID=selectedEdge.StartID;
    	if(graph.node_map[tempID]>graph.node_map[selectedEdge.EndID]){
    		selectedEdge.StartID=selectedEdge.EndID;
    		selectedEdge.EndID=tempID;
    	}
    }




  if(isfinded==false){

    int selectedDgreeofS=0;

	for(sourcenodeIt=BoundarySet.begin(); sourcenodeIt!=BoundarySet.end();sourcenodeIt++){
//		cout<<"sourcenodeID=================="<<*sourcenodeIt<<endl;


		//get the source neibors

		neiborsSetOfSourcenode=::NeiborsOfNodeV1(graph, *sourcenodeIt);
	    set<int>::iterator neibornodeit=neiborsSetOfSourcenode.begin();
		if (isfinded == true) {
			is_selectedEdge = true;
			break;
		}


		for (; neibornodeit != neiborsSetOfSourcenode.end();
				neibornodeit++) {


			if(::isContains(BoundarySet,*neibornodeit)==true){
				//in BSet and selected directly
				selectedEdge.StartID=*sourcenodeIt;
				selectedEdge.EndID=*neibornodeit;
				selectedEdge.relia=graph.adj_relia[selectedEdge.StartID][selectedEdge.EndID];
				isfinded=true;
				break;

			}

			//neibor nodes of the temp neibornode
			::importindexbasedBounarySet(graph, BoundarySet, *neibornodeit, importanctDegreeIndex_temp, importanctBoundaryIndex_temp);
//			cout<<"importanctBoundaryIndex_temp:"<<importanctBoundaryIndex_temp<<endl;
//			cout<<"importanctDegreeIndex_temp:"<<importanctDegreeIndex_temp<<endl;
			if(importanctBoundaryIndex_opti==-1){
				selectedDgreeofS=neiborsSetOfSourcenode.size();
				is_selectedEdge=true;
				selectedEdge.StartID=*sourcenodeIt;
				selectedEdge.EndID=*neibornodeit;
				selectedEdge.relia=graph.adj_relia[selectedEdge.StartID][selectedEdge.EndID];
				//

				importanctBoundaryIndex_opti=importanctBoundaryIndex_temp;
				importanctDegreeIndex_opti=importanctDegreeIndex_temp;
//				minlayer=graph.node_layer[selectedEdge.StartID];

				//seting...frist
//				cout<<"setting ... first"<<endl;
//				cout<<"importanctBoundaryIndex_opti:"<<importanctBoundaryIndex_opti<<endl;
//				cout<<"importanctDegreeIndex_opti:"<<importanctDegreeIndex_opti<<endl;
//				cout<<"the selected edge:"<<selectedEdge.StartID<<"---->"<<selectedEdge.EndID<<endl;
			}else{
				//compare

				if(importanctBoundaryIndex_opti<importanctBoundaryIndex_temp){
					selectedDgreeofS=neiborsSetOfSourcenode.size();
					selectedEdge.StartID=*sourcenodeIt;
					selectedEdge.EndID=*neibornodeit;
					selectedEdge.relia=graph.adj_relia[selectedEdge.StartID][selectedEdge.EndID];
					//

					importanctBoundaryIndex_opti=importanctBoundaryIndex_temp;
					importanctDegreeIndex_opti=importanctDegreeIndex_temp;

					//seting...frist
//					cout<<"setting ... again"<<endl;
//
//
//					cout<<"importanctBoundaryIndex_opti:"<<importanctBoundaryIndex_opti<<endl;
//					cout<<"importanctDegreeIndex_opti:"<<importanctDegreeIndex_opti<<endl;
//
//					cout<<"the new selected edge:"<<selectedEdge.StartID<<"---->"<<selectedEdge.EndID<<endl;


				}else if(importanctBoundaryIndex_opti==importanctBoundaryIndex_temp){

					if(importanctDegreeIndex_opti<importanctDegreeIndex_temp){
						selectedDgreeofS=neiborsSetOfSourcenode.size();
						selectedEdge.StartID=*sourcenodeIt;
						selectedEdge.EndID=*neibornodeit;
						selectedEdge.relia=graph.adj_relia[selectedEdge.StartID][selectedEdge.EndID];
						//

						importanctBoundaryIndex_opti=importanctBoundaryIndex_temp;
						importanctDegreeIndex_opti=importanctDegreeIndex_temp;

					}else {
						if(importanctDegreeIndex_opti==importanctDegreeIndex_temp){

						if(selectedDgreeofS>neiborsSetOfSourcenode.size()){

						selectedDgreeofS=neiborsSetOfSourcenode.size();
						selectedEdge.StartID=*sourcenodeIt;
						selectedEdge.EndID=*neibornodeit;
						selectedEdge.relia=graph.adj_relia[selectedEdge.StartID][selectedEdge.EndID];
						//

						importanctBoundaryIndex_opti=importanctBoundaryIndex_temp;
						importanctDegreeIndex_opti=importanctDegreeIndex_temp;

						//seting...frist
//						cout<<"setting ... again"<<endl;
//						cout<<"importanctBoundaryIndex_opti:"<<importanctBoundaryIndex_opti<<endl;
//						cout<<"importanctDegreeIndex_opti:"<<importanctDegreeIndex_opti<<endl;
//
//						cout<<"the new selected edge again:"<<selectedEdge.StartID<<"---->"<<selectedEdge.EndID<<endl;
						}else {

									if (selectedDgreeofS
											== neiborsSetOfSourcenode.size()) {
										//							int tempStartLayer=graph.node_layer[*sourcenodeIt];
										//							int disLayer=tempStartLayer-minlayer;
										if (BoundarySet.size() > 20) {
											if (graph.node_map[selectedEdge.StartID]
													> graph.node_map[*sourcenodeIt]) {

												selectedEdge.StartID = *sourcenodeIt;
												selectedEdge.EndID = *neibornodeit;
												selectedEdge.relia =
														graph.adj_relia[selectedEdge.StartID][selectedEdge.EndID];

											}

										}else{
											if (graph.node_map[selectedEdge.StartID]
													< graph.node_map[*sourcenodeIt]) {

												selectedEdge.StartID = *sourcenodeIt;
												selectedEdge.EndID = *neibornodeit;
												selectedEdge.relia =
														graph.adj_relia[selectedEdge.StartID][selectedEdge.EndID];

											}


										}
									}







						}


					}}
				}




			}

		}


	}
  }
//	cout<<"the finial selected edge:"<<selectedEdge.StartID<<"---->"<<selectedEdge.EndID<<endl;
//	cout<<"#########################end of selecteEdge_byPreSets"<<endl;
	return is_selectedEdge;

}




bool selecteEdge_byBFSPreSets(Graph &graph, set<int> &visitedSet,
		set<int> &BoundarySet, Edge &selectedEdge) {

//	cout<<"###############selecteEdge_byPreSets..............."<<endl;

	bool is_selectedEdge = false;

	//selected the optimal nodeID from the Candidate node set
	// Candidate node set is the neibor nodes of BoundarySet,and Meet the conditions for not be Visited

	// get the Candidate node set

//	set<int>::iterator bnodeit=BoundarySet.begin();
//	for(; bnodeit!=BoundarySet.end();bnodeit++){
//
//		set<int> neiborofNodeSet=NeiborsOfNodeV1(graph, *bnodeit);
//		set<int>::iterator neibornodeit=neiborofNodeSet.begin();
//		for(;neibornodeit!=neiborofNodeSet.end();neibornodeit++){
//			if(::isContains(visitedSet, *neibornodeit)==false){
//				CandidateNode_set.insert(*neibornodeit);
//			}
//
//		}
//
//
//	}

	//
	//ends nodes Selection conditions
	/*(1) the ends nodes in BoundarySet
	 *(2) the neibor nodes of  CandidateNode contain more nodes in BoundarySet firstly
	 *(3) the neibor nodes of  CandidateNode contain more nodes  firstly
	 */
	int isinBoundary = 0;
	int importanctBoundaryIndex_temp = 0;
	int importanctDegreeIndex_temp = 0;

	int importanctBoundaryIndex_opti = -1;
	int importanctDegreeIndex_opti = -1;
	int minlayer = 0;

	set<int>::iterator sourcenodeIt = BoundarySet.begin();

	set<int> neiborsSetOfSourcenode;

	bool isfinded = false;

	minlayer = graph.node_layer[*sourcenodeIt];

	for (; sourcenodeIt != BoundarySet.end(); sourcenodeIt++) {

		if (minlayer > graph.node_layer[*sourcenodeIt]) {
			minlayer = graph.node_layer[*sourcenodeIt];
		}

	}

//    set<int> CandidateSet;

	int mindofCandidate = 0;
	for (sourcenodeIt = BoundarySet.begin(); sourcenodeIt != BoundarySet.end();
			sourcenodeIt++) {
//		cout<<"sourcenodeID=================="<<*sourcenodeIt<<endl;

		//get the source neibors

		neiborsSetOfSourcenode = ::NeiborsOfNodeV1(graph, *sourcenodeIt);
		set<int>::iterator neibornodeit = neiborsSetOfSourcenode.begin();
		for (; neibornodeit != neiborsSetOfSourcenode.end(); neibornodeit++) {

			if (::isContains(BoundarySet, *neibornodeit) == true) {
				if (mindofCandidate == 0) {
					mindofCandidate = neiborsSetOfSourcenode.size();
//					CandidateSet.insert(*sourcenodeIt);

					//
					selectedEdge.StartID = *sourcenodeIt;
					selectedEdge.EndID = *neibornodeit;
					selectedEdge.relia =
							graph.adj_relia[selectedEdge.StartID][selectedEdge.EndID];
					isfinded = true;
					is_selectedEdge = true;

				} else {
					if (mindofCandidate > neiborsSetOfSourcenode.size()) {
						mindofCandidate = neiborsSetOfSourcenode.size();
//						CandidateSet.clear();
//						CandidateSet.insert(*sourcenodeIt);
						selectedEdge.StartID = *sourcenodeIt;
						selectedEdge.EndID = *neibornodeit;
						selectedEdge.relia =
								graph.adj_relia[selectedEdge.StartID][selectedEdge.EndID];
						isfinded = true;

					}
				}
				break;
			}
		}

	}

	if (isfinded == true) {
		int tempID = selectedEdge.StartID;
		if (graph.node_map[tempID] > graph.node_map[selectedEdge.EndID]) {
			selectedEdge.StartID = selectedEdge.EndID;
			selectedEdge.EndID = tempID;
		}
	}

	if (isfinded == false) {

		int selectedDgreeofS = 0;

		for (sourcenodeIt = BoundarySet.begin();
				sourcenodeIt != BoundarySet.end(); sourcenodeIt++) {
//		cout<<"sourcenodeID=================="<<*sourcenodeIt<<endl;

			//get the source neibors

			neiborsSetOfSourcenode = ::NeiborsOfNodeV1(graph, *sourcenodeIt);
			set<int>::iterator neibornodeit = neiborsSetOfSourcenode.begin();
			if (isfinded == true) {
				is_selectedEdge = true;
				break;
			}

			for (; neibornodeit != neiborsSetOfSourcenode.end();
					neibornodeit++) {

				if (::isContains(BoundarySet, *neibornodeit) == true) {
					//in BSet and selected directly
					selectedEdge.StartID = *sourcenodeIt;
					selectedEdge.EndID = *neibornodeit;
					selectedEdge.relia =
							graph.adj_relia[selectedEdge.StartID][selectedEdge.EndID];
					isfinded = true;
					break;

				}

				//neibor nodes of the temp neibornode
				::importindexbasedBounarySet(graph, BoundarySet, *neibornodeit,
						importanctDegreeIndex_temp,
						importanctBoundaryIndex_temp);
//			cout<<"importanctBoundaryIndex_temp:"<<importanctBoundaryIndex_temp<<endl;
//			cout<<"importanctDegreeIndex_temp:"<<importanctDegreeIndex_temp<<endl;
				if (importanctBoundaryIndex_opti == -1) {
					selectedDgreeofS = neiborsSetOfSourcenode.size();
					is_selectedEdge = true;
					selectedEdge.StartID = *sourcenodeIt;
					selectedEdge.EndID = *neibornodeit;
					selectedEdge.relia =
							graph.adj_relia[selectedEdge.StartID][selectedEdge.EndID];
					//

					importanctBoundaryIndex_opti = importanctBoundaryIndex_temp;
					importanctDegreeIndex_opti = importanctDegreeIndex_temp;
//				minlayer=graph.node_layer[selectedEdge.StartID];

					//seting...frist
//				cout<<"setting ... first"<<endl;
//				cout<<"importanctBoundaryIndex_opti:"<<importanctBoundaryIndex_opti<<endl;
//				cout<<"importanctDegreeIndex_opti:"<<importanctDegreeIndex_opti<<endl;
//				cout<<"the selected edge:"<<selectedEdge.StartID<<"---->"<<selectedEdge.EndID<<endl;
				} else {
					//compare

					if (importanctBoundaryIndex_opti
							< importanctBoundaryIndex_temp) {
						selectedDgreeofS = neiborsSetOfSourcenode.size();
						selectedEdge.StartID = *sourcenodeIt;
						selectedEdge.EndID = *neibornodeit;
						selectedEdge.relia =
								graph.adj_relia[selectedEdge.StartID][selectedEdge.EndID];
						//

						importanctBoundaryIndex_opti =
								importanctBoundaryIndex_temp;
						importanctDegreeIndex_opti = importanctDegreeIndex_temp;

						//seting...frist
//					cout<<"setting ... again"<<endl;
//
//
//					cout<<"importanctBoundaryIndex_opti:"<<importanctBoundaryIndex_opti<<endl;
//					cout<<"importanctDegreeIndex_opti:"<<importanctDegreeIndex_opti<<endl;
//
//					cout<<"the new selected edge:"<<selectedEdge.StartID<<"---->"<<selectedEdge.EndID<<endl;

					} else if (importanctBoundaryIndex_opti
							== importanctBoundaryIndex_temp) {

						if (importanctDegreeIndex_opti
								< importanctDegreeIndex_temp) {
							selectedDgreeofS = neiborsSetOfSourcenode.size();
							selectedEdge.StartID = *sourcenodeIt;
							selectedEdge.EndID = *neibornodeit;
							selectedEdge.relia =
									graph.adj_relia[selectedEdge.StartID][selectedEdge.EndID];
							//

							importanctBoundaryIndex_opti =
									importanctBoundaryIndex_temp;
							importanctDegreeIndex_opti =
									importanctDegreeIndex_temp;

						} else {
							if (importanctDegreeIndex_opti
									== importanctDegreeIndex_temp) {
								if (BoundarySet.size() > 15) {
									if (graph.node_map[selectedEdge.StartID]
											> graph.node_map[*sourcenodeIt]) {

										selectedEdge.StartID = *sourcenodeIt;
										selectedEdge.EndID = *neibornodeit;
										selectedEdge.relia =
												graph.adj_relia[selectedEdge.StartID][selectedEdge.EndID];

									}

								} else {
									if (graph.node_map[selectedEdge.StartID]
											< graph.node_map[*sourcenodeIt]) {

										selectedEdge.StartID = *sourcenodeIt;
										selectedEdge.EndID = *neibornodeit;
										selectedEdge.relia =
												graph.adj_relia[selectedEdge.StartID][selectedEdge.EndID];

									}

								}

							}
						}
					}

				}

			}

		}
	}
//	cout<<"the finial selected edge:"<<selectedEdge.StartID<<"---->"<<selectedEdge.EndID<<endl;
//	cout<<"#########################end of selecteEdge_byPreSets"<<endl;
	return is_selectedEdge;

}






bool selecteEdge_byPreSetsByLayerInfo(Graph &graph, set<int> &visitedSet,
		set<int> &BoundarySet, Edge &selectedEdge) {

	//	cout<<"###############selecteEdge_byPreSets..............."<<endl;


	map<int,int>BFSLayermap;
	::bfs_layerFromID(graph, graph.sID, BFSLayermap);


	bool is_selectedEdge = false;

	//selected the optimal nodeID from the Candidate node set

	set<int>::iterator sourcenodeIt = BoundarySet.begin();


//	set<int> neiborsOfSourceSet;

	bool isfinded = false;

	int isInitial = 0;
	//    cout<<"aftter key nodelayer for boundary set"<<endl;
	//
	//	printNodeMap(graph);


	//get the neibors of BoundarySet all
	set<int> NBS;
	map<int,int>candi_edgemap;
	::GetNeiborSetofBS(graph, BoundarySet, NBS);

	set<int> interSetofNBSbyBS=::intersectionSet(NBS, BoundarySet);

//	cout<<"BS as follows:"<<endl;
//	::PrintSetInfo(BoundarySet);
//	cout<<"NBS as follows:"<<endl;
//	::PrintSetInfo(NBS);


	if(interSetofNBSbyBS.empty()==false){
//		cout<<" Satisfing ################condition 1"<<endl;
//
//		cout<<"Intersection of Set NS and NBS as follows:"<<endl;
//		::PrintSetInfo(interSetofNBSbyBS);
//		::PrintNodeDegreeInfoFromSet(graph, interSetofNBSbyBS);
		set<int> MinDStartIDSet;
		::GetMinDegreeSetFromTarSetSet(graph, interSetofNBSbyBS, MinDStartIDSet);
		int SID=::GetNodeByMinVisitedFromSet(graph, MinDStartIDSet);

		set<int> NeborSetOfSID = ::NeiborsOfNodeV1(graph,SID);
		set<int> candiEndIDSet=::intersectionSet(NeborSetOfSID, interSetofNBSbyBS);
		set<int>MinDEndIDSet;
		::GetMinDegreeSetFromTarSetSet(graph, candiEndIDSet, MinDEndIDSet);
        int EID=::GetNodeByMinVisitedFromSet(graph, MinDEndIDSet);

        selectedEdge.StartID =SID;
        selectedEdge.EndID =EID;
        selectedEdge.relia =   graph.adj_relia[selectedEdge.StartID][selectedEdge.EndID];
        is_selectedEdge = true;
//
//    	cout<<"result  find:"<<endl;
//    	::PrintSelectedEdgeInfo(selectedEdge);



	}else if (interSetofNBSbyBS.empty()==true){
		int selectedEndID=0;
//		cout<<" Satisfing ################condition 2"<<endl;
		map<int, vector<int>> AbsorptionInfomap;
		map<int,int> BreathInfomap;
//        cout<<"before keynodeInfo ..."<<endl;
		::KeyNodeLayerInfoComForTargetSet(AbsorptionInfomap,BreathInfomap, graph,NBS, BoundarySet);
//		 cout<<"after keynodeInfo"<<endl;
		if(BoundarySet.size()==1){
//			cout<<"BS size:"<<BoundarySet.size()<<"    :";
//			cout<<" Satisfing ################condition 2.1"<<endl;
			sourcenodeIt = BoundarySet.begin();
			int SID=*sourcenodeIt;

			set<int>MinBset;
			::GetMinBreathSetFromTargetSet(BreathInfomap, NBS, MinBset);
			int EID=::GetNodeByMinDFromSet(graph, MinBset);
			selectedEdge.StartID = SID;
			selectedEdge.EndID = EID;
			selectedEdge.relia =
					graph.adj_relia[selectedEdge.StartID][selectedEdge.EndID];
			is_selectedEdge = true;
//
//			cout << "result  find:" << endl;
//			::PrintSelectedEdgeInfo(selectedEdge);

		}else{
//			cout<<" Satisfing ################condition 2.2"<<endl;
			is_selectedEdge=true;

			set<int> MaxAbSet;
			::GetMaxAbsorptionSetFromTargetSet(AbsorptionInfomap, NBS, MaxAbSet,
					0);
			//				int EID=::GetMinDegreeSetFromTarSetSet(graph, MaxAbSet, SetofMinDegree)

			vector<Edge>Candi_edgeVector;

			if (MaxAbSet.size() > 1) {

				set<int> MinDSet;
				::GetMinDegreeSetFromTarSetSet(graph, MaxAbSet, MinDSet);
				if(MinDSet.size()>1){
					set<int> MAbByMInDSet;
					::GetMaxAbsorptionSetFromTargetSet(AbsorptionInfomap, MinDSet, MAbByMInDSet,1);
					if(MAbByMInDSet.size()>1){

					}else if(MAbByMInDSet.size()==1){

					}

					::GetCandiEdgeVectorbyEndsSet(graph, BoundarySet, MAbByMInDSet, Candi_edgeVector);
//					::GetEdgeVectorByMinBFSLayer(BFSLayermap, Candi_edgeVector);
					::GetEdgeVectorByMaxBFSLayer(BFSLayermap, Candi_edgeVector);
					::GetcandiEdgebySVisitedOrder(graph, Candi_edgeVector, selectedEdge);
				}else if(MinDSet.size()==1){

					::GetCandiEdgeVectorbyEndsSet(graph, BoundarySet,
							MinDSet, Candi_edgeVector);
//					::GetEdgeVectorByMinBFSLayer(BFSLayermap, Candi_edgeVector);
					::GetEdgeVectorByMaxBFSLayer(BFSLayermap, Candi_edgeVector);
					::GetcandiEdgebySVisitedOrder(graph, Candi_edgeVector,
							selectedEdge);

				}

			} else if (MaxAbSet.size() == 1) {
				::GetCandiEdgeVectorbyEndsSet(graph, BoundarySet, MaxAbSet,
						Candi_edgeVector);
//				::GetEdgeVectorByMinBFSLayer(BFSLayermap, Candi_edgeVector);
				::GetcandiEdgebySVisitedOrder(graph, Candi_edgeVector,
						selectedEdge);

			}
//     section begain
//			int selectedEndID=0;
//							cout<<" Satisfing ################condition 2.2"<<endl;
//							set<int>::iterator sourcenodeIt;
//							for (sourcenodeIt = BoundarySet.begin();
//									sourcenodeIt != BoundarySet.end(); sourcenodeIt++) {
//								cout<<"###############for node:"<<*sourcenodeIt<<" in BS"<<endl;
//								set<int> neiborsOfSourceSet = ::NeiborsOfNodeV1(graph, *sourcenodeIt);
//								set<int>::iterator neibornodeit = neiborsOfSourceSet.begin();
//								for (; neibornodeit != neiborsOfSourceSet.end();neibornodeit++) {
//									cout<<"###############for node:"<<*neibornodeit<<" in NBS"<<endl;
//
//
//									if (selectedEndID == 0) {
//										selectedEdge.StartID = *sourcenodeIt;
//										selectedEdge.EndID = *neibornodeit;
//										selectedEdge.relia =
//												graph.adj_relia[selectedEdge.StartID][selectedEdge.EndID];
//
//										selectedEndID = 1;
//										cout<<"first find"<<endl;
//										::PrintSelectedEdgeInfo(selectedEdge);
//										is_selectedEdge = true;
//
//									} else {
//
//
//										int result =::compareBoundaryIndexByzeroIndex(AbsorptionInfomap[selectedEdge.EndID],
//																		AbsorptionInfomap[*neibornodeit]);
//
//										::PrintAbsorptionInfo(AbsorptionInfomap, selectedEdge.EndID);
//										::PrintAbsorptionInfo(AbsorptionInfomap, *neibornodeit);
//
//
//										cout<<selectedEdge.EndID<<":A value in  firstIndex:"<<AbsorptionInfomap[selectedEdge.EndID][0]<<endl;
//										cout<<*neibornodeit<<":A value in firstIndex:"<<AbsorptionInfomap[*neibornodeit][0]<<endl;
//
//
//										if (result < 0) {
//											cout<<" Satisfing ################condition 2.2.1"<<endl;
//											selectedEdge.StartID = *sourcenodeIt;
//											selectedEdge.EndID = *neibornodeit;
//											selectedEdge.relia =
//													graph.adj_relia[selectedEdge.StartID][selectedEdge.EndID];
//											::PrintSelectedEdgeInfo(selectedEdge);
//
//										} else if (result == 0) {
//
//
//											cout<<" Satisfing ################condition 2.2.2"<<endl;
//
//											::PrintNodeDegreeInfo(graph, selectedEdge.EndID);
//											::PrintNodeDegreeInfo(graph, *neibornodeit);
//
//												if (graph.adj_relia[selectedEdge.EndID].size()
//														> graph.adj_relia[*neibornodeit].size()) {
//													cout<<" Satisfing ################condition 2.2.2.1"<<endl;
//
//
//													selectedEdge.StartID = *sourcenodeIt;
//													selectedEdge.EndID = *neibornodeit;
//													selectedEdge.relia =
//															graph.adj_relia[selectedEdge.StartID][selectedEdge.EndID];
//
//													::PrintSelectedEdgeInfo(selectedEdge);
//
//												} else if (graph.adj_relia[selectedEdge.EndID].size()
//														== graph.adj_relia[*neibornodeit].size()) {
//													cout<<" Satisfing ################condition 2.2.2.1"<<endl;
//												int result = compareBoundaryIndex(
//														AbsorptionInfomap[selectedEdge.EndID],
//														AbsorptionInfomap[*neibornodeit]);
//
//												::PrintAbsorptionInfo(AbsorptionInfomap, selectedEdge.EndID);
//												::PrintAbsorptionInfo(AbsorptionInfomap, *neibornodeit);
//
//
//
//
//												if (result < 0) {
//													cout<<" Satisfing ################condition 2.2.2.1.1"<<endl;
//													selectedEdge.StartID = *sourcenodeIt;
//													selectedEdge.EndID = *neibornodeit;
//													selectedEdge.relia =
//															graph.adj_relia[selectedEdge.StartID][selectedEdge.EndID];
//													::PrintSelectedEdgeInfo(selectedEdge);
//
//												} else if (result == 0) {
//													cout<<" Satisfing ################condition 2.2.2.1.2"<<endl;
//
//													::PrintLayerIndex(BFSLayermap, selectedEdge.StartID);
//													::PrintLayerIndex(BFSLayermap, *sourcenodeIt);
//
//													if (BFSLayermap[selectedEdge.StartID]
//																< BFSLayermap[*sourcenodeIt]) {
//															cout<<" Satisfing ################condition 2.2.2.1.2.1"<<endl;
//															selectedEdge.StartID =
//																	*sourcenodeIt;
//															selectedEdge.EndID = *neibornodeit;
//															selectedEdge.relia =
//																	graph.adj_relia[selectedEdge.StartID][selectedEdge.EndID];
//
//															::PrintSelectedEdgeInfo(selectedEdge);
//
//														} else if (BFSLayermap[selectedEdge.StartID]
//																== BFSLayermap[*sourcenodeIt]) {
//															cout<<" Satisfing ################condition 2.2.2.1.2.2"<<endl;
//															::PrintNodeOrderInfo(graph, selectedEdge.StartID);
//															::PrintNodeOrderInfo(graph, *sourcenodeIt);
//
//															if (graph.node_map[selectedEdge.StartID]
//																	> graph.node_map[*sourcenodeIt]) {
//
//
//																cout<<" Satisfing ################condition 2.2.2.1.2.2.1"<<endl;
//
//																selectedEdge.StartID =
//																		*sourcenodeIt;
//																selectedEdge.EndID =
//																		*neibornodeit;
//																selectedEdge.relia =
//																		graph.adj_relia[selectedEdge.StartID][selectedEdge.EndID];
//																::PrintSelectedEdgeInfo(selectedEdge);
//
//															};
//
//														}
//													}
//
//												}
//											}
//
//										}
//									}
//
//								}
//			section end

			}



	}

	return is_selectedEdge;

}



void PrintSetInfo(set<int>& S){
	set<int>::iterator sit=S.begin();
	for(;sit!=S.end();sit++){
		cout<< " "<<*sit;

	}
	cout<<endl;

}

//void PrintVectorInfo(vector<int>& V){
//	vector <int>::iterator vit=V.begin();
//	for(;vit!=V.end();vit++){
//		cout<< " "<<*vit;
//
//	}
//	cout<<endl;
//
//}



bool selecteEdge_byNodesNebor(Graph &graph, set<int> &visitedSet, int targetID,
		map<int, vector<int>> &NodeKeyInfomap, Edge &selectedEdge,int targetLayer,int rootID) {

//	cout<<"###############selecteEdge_byPreSets..............."<<endl;

	bool is_selectedEdge = false;

	set<int> neiborsSetOfSourcenode;

	int isselected = 0;


	//get the endID connected to selectedEdge.StartID
	neiborsSetOfSourcenode = ::NeiborsOfNodeV1(graph, targetID);

	set<int>neiborsSetOfSourcenode2=neiborsSetOfSourcenode;


	set<int>::iterator neibornodeit = neiborsSetOfSourcenode2.begin();
	for(;neibornodeit != neiborsSetOfSourcenode2.end();neibornodeit++)
	{
		if(::isContains(visitedSet, *neibornodeit)==true){
			neiborsSetOfSourcenode.erase(*neibornodeit);
		}
	}

	for (neibornodeit=neiborsSetOfSourcenode.begin(); neibornodeit != neiborsSetOfSourcenode.end(); neibornodeit++) {

		if(graph.node_layer[*neibornodeit]==targetLayer){
			if (isselected == 0) {
					selectedEdge.StartID = targetID;
					selectedEdge.EndID = *neibornodeit;
					selectedEdge.relia =
							graph.adj_relia[selectedEdge.StartID][selectedEdge.EndID];

					isselected = 1;
					is_selectedEdge = true;

				} else {
					if (NodeKeyInfomap[selectedEdge.EndID][0]
							> NodeKeyInfomap[*neibornodeit][0]) {
						selectedEdge.StartID = targetID;
						selectedEdge.EndID = *neibornodeit;
						selectedEdge.relia =
								graph.adj_relia[selectedEdge.StartID][selectedEdge.EndID];

					}

				}
		}


	}

//	cout<<"the finial selected edge:"<<selectedEdge.StartID<<"---->"<<selectedEdge.EndID<<endl;
//	cout<<"#########################end of selecteEdge_byPreSets"<<endl;
	return is_selectedEdge;

}





list<int> sortedNeibors_less(Graph &graph, int nodeID) {

	list<int> neiborlist;
	map<int, double>::iterator neiborit = graph.adj_relia[nodeID].begin();
	for (; neiborit != graph.adj_relia[nodeID].end(); neiborit++) {
		if (neiborlist.empty() == true) {
			neiborlist.push_back(neiborit->first);
		} else {
			int degreeofneiborNode = graph.adj_relia[neiborit->first].size();
			list<int>::iterator listit = neiborlist.begin();
			bool isinserted = false;
			for (; listit != neiborlist.end(); listit++) {
				if (graph.adj_relia[*listit].size() > degreeofneiborNode) {

					neiborlist.insert(listit, neiborit->first);
					isinserted = true;
					break;
				}

			}
			if (isinserted == false) {
				neiborlist.push_back(neiborit->first);
			}

		}

	}

	return neiborlist;

}




list<int>* dfs(Graph &graph, int startID) {
	list<int> *plistVertex = new list<int>;
	list<int> Stack;
	Stack.push_front(startID);

	int* s = new int[graph.nVerts];
	for (int i = 0; i < graph.nVerts; i++) {
		s[i] = false;
	}

	while (!Stack.empty())	//只要队列不空，遍历就没有结束
	{
		int t = Stack.front();
		plistVertex->push_back(t);
		s[t] = true;
		//取出对头元素
		Stack.pop_front();

		//当前节点的邻居节点
		vector<int> neibors = NeiborsOfNode(graph, t);

		vector<int>::iterator neiborit;
		for (neiborit = neibors.begin(); neiborit != neibors.end();
				neiborit++) {
			int newID = *neiborit;
			if (s[newID] == false) {
				s[newID] = true;
				Stack.push_front(newID);
			}

		}

	}
	delete[]s;
	return plistVertex;

}

bool has_path(Graph &graph, int sID, int tID) {

	bool ishaspath = false;
	list<int> Stack;
	Stack.push_back(sID);

	int* s = new int[graph.nVerts];
	for (int i = 0; i < graph.nVerts; i++) {
		s[i] = false;
	}

	while (!Stack.empty())	//只要队列不空，遍历就没有结束
	{
		int t = Stack.front();
		s[t] = true;
		//取出对头元素
		Stack.pop_front();

		//当前节点的邻居节点
		vector<int> neibors = NeiborsOfNode(graph, t);

		vector<int>::iterator neiborit;
		for (neiborit = neibors.begin(); neiborit != neibors.end();
				neiborit++) {
			if (*neiborit != tID) {
				int newID = *neiborit;

				if (s[newID] == false) {
					s[newID] = true;
					Stack.push_front(newID);
				}
			} else {

				ishaspath = true;
				break;
			}

		}
		if (ishaspath == true) {
			break;
		}

	}
	delete[]s;
	return ishaspath;

}

inline int getSourcekey(Graph &graph) {
	int key = 0;
//	set<int>::iterator it = graph.SourceSet.begin();
//	for (; it != graph.SourceSet.end(); ++it) {
//		key += *it;
//
//	}
//	key+=graph.SourceSet.size();
//
//	key+=graph.CombineBounaryList.size();

	return key;
}

void printSourceSet(Graph &graph) {

////	   cout<<"the relia is:"<<this->relia<<endl;
//
//	cout << "the SourceSet as follows:=>";
//	set<int>::iterator it = graph.SourceSet.begin();
//	for (; it != graph.SourceSet.end(); ++it)
//		cout << ' ' << *it;
//	cout << endl;
//
//	cout << "KKKKKKKKKKKKKKKKKKKKKKKKKKKKKKthe Kset as follows:=>";
//	it = graph.KterminalSet.begin();
//	for (; it != graph.KterminalSet.end(); ++it)
//		cout << ' ' << *it;
//	cout << endl;
//
////	map<int,int>::iterator nodeit= graph.node_degree.begin();
////	for (; nodeit != graph.node_degree.end(); nodeit++) {
////		cout << "node:" << nodeit->first;
////		cout << " degree is:" << nodeit->second << endl;
////
////	}

}

void serial_ana(Graph &graph, int sID, int tID) {

	list<int> Stack;

	map<int, map<int, double> >::iterator nodeidIt = graph.adj_relia.begin();
	for (; nodeidIt != graph.adj_relia.end(); nodeidIt++) {

		if (graph.adj_relia[nodeidIt->first].size() == 2
				and nodeidIt->first != sID and nodeidIt->first != tID) {

			Stack.push_back(nodeidIt->first);
		}
	}

	while (Stack.empty() == false) {

		int nodeId = Stack.front();
		Stack.pop_front();
		vector<int> neibors = NeiborsOfNode(graph, nodeId);
		vector<int>::iterator neiborit = neibors.begin();
		int neibor0 = *neiborit;
		neiborit++;
		int neibor1 = *neiborit;

		double newedge_relia = graph.adj_relia[nodeId][neibor0]
				* graph.adj_relia[nodeId][neibor1];

		addEdge(graph,neibor0, neibor1, newedge_relia);

		//del edge
		deleteEdge(graph,nodeId, neibor0);
		deleteEdge(graph,nodeId, neibor1);
		//erase node
		graph.adj_relia.erase(nodeId);

	}

	map<int, map<int, double> >::iterator edgeit = graph.adj_relia.begin();
	for (; edgeit != graph.adj_relia.end(); edgeit++) {
		int size = graph.adj_relia[edgeit->first].size();
		graph.node_degree[edgeit->first] = size;

	}

}

void serial_ana_general(Graph &graph, int sID, int tID) {

	list<int> Stack;

	map<int, map<int, double> >::iterator nodeidIt = graph.adj_relia.begin();
	for (; nodeidIt != graph.adj_relia.end(); nodeidIt++) {

		if (graph.adj_relia[nodeidIt->first].size() == 2
				and nodeidIt->first != sID and nodeidIt->first != tID) {

			Stack.push_back(nodeidIt->first);
		}
	}

	while (Stack.empty() == false) {

		int nodeId = Stack.front();
		Stack.pop_front();
		vector<int> neibors = NeiborsOfNode(graph, nodeId);
		vector<int>::iterator neiborit = neibors.begin();
		int neibor0 = *neiborit;
		neiborit++;
		int neibor1 = *neiborit;

		double newedge_relia = graph.adj_relia[nodeId][neibor0]
				* graph.adj_relia[nodeId][neibor1] * 0.9;

		addEdge(graph,neibor0, neibor1, newedge_relia);

		//del edge
		deleteEdge(graph,nodeId, neibor0);
		deleteEdge(graph,nodeId, neibor1);
		//erase node
		graph.adj_relia.erase(nodeId);

	}

	map<int, map<int, double> >::iterator edgeit = graph.adj_relia.begin();
	for (; edgeit != graph.adj_relia.end(); edgeit++) {
		int size = graph.adj_relia[edgeit->first].size();
		graph.node_degree[edgeit->first] = size;

	}

}



void serial_ana_generalV7(Graph &graph) {


	list<int> Stack;

	map<int, map<int, double> >::iterator nodeidIt = graph.adj_relia.begin();
	for (; nodeidIt != graph.adj_relia.end(); nodeidIt++) {

		if (graph.adj_relia[nodeidIt->first].size() <= 2
				and ::isContains(graph.KterminalSet, nodeidIt->first)==false) {
//            cout<<"node is pushed:"<<nodeidIt->first<<endl;
			Stack.push_back(nodeidIt->first);
		}
	}

	while (Stack.empty() == false) {



		int nodeId = Stack.front();
		Stack.pop_front();
		set <int> neibors = NeiborsOfNodeV1(graph, nodeId);
//		cout<<"neibors.size()"<<neibors.size()<<endl;
		if(neibors.size()==0){
//			cout<<"removed node:"<<nodeId<<endl;
			graph.adj_relia.erase(nodeId);
			continue;

		}
		if(neibors.size()==1){
			set<int>::iterator neiborit = neibors.begin();
			int neibor0 = *neiborit;
			deleteEdge(graph, nodeId, neibor0);
			if (graph.adj_relia[neibor0].size() <= 2
					and ::isContains(graph.KterminalSet, neibor0)
							== false) {
//				cout<<"new id pushed:"<<neibor0<<endl;
				Stack.push_back(neibor0);
			}
		} else {
//			cout<<"mid ID:"<<nodeId<<endl;
			set<int>::iterator neiborit = neibors.begin();
			int neibor0 = *neiborit;
			neiborit++;
			int neibor1 = *neiborit;

//			cout<<"neibors:"<<neibor0<<endl;
//			cout<<"neibors1:"<<neibor1<<endl;

			double newedge_relia = graph.adj_relia[nodeId][neibor0]
					* graph.adj_relia[nodeId][neibor1];

			addEdge(graph, neibor0, neibor1, newedge_relia);

			//del edge
			deleteEdge(graph, nodeId, neibor0);
			deleteEdge(graph, nodeId, neibor1);

			if (graph.adj_relia[neibor0].size() <= 2
					and ::isContains(graph.KterminalSet, neibor0) == false) {
//				cout << "new id pushed:" << neibor0 << endl;
				Stack.push_back(neibor0);
			}

			if (graph.adj_relia[neibor1].size() <= 2
					and ::isContains(graph.KterminalSet, neibor1) == false) {
//				cout << "new id pushed:" << neibor1<< endl;
				Stack.push_back(neibor1);
			}




		}
//		graph.nVerts--;
		//erase node
//		cout<<"removed node:"<<nodeId<<endl;

		graph.adj_relia.erase(nodeId);
//		cout<<"Stack size:"<<Stack.size()<<endl;

	}




	map<int, map<int, double> >::iterator edgeit = graph.adj_relia.begin();
	for (; edgeit != graph.adj_relia.end(); edgeit++) {
		int size = graph.adj_relia[edgeit->first].size();
		graph.node_degree[edgeit->first] = size;

	}

}




void serial_ana_generalEdge(Graph &graph) {


	list<int> Stack;

	map<int, map<int, double> >::iterator nodeidIt = graph.adj_relia.begin();
	for (; nodeidIt != graph.adj_relia.end(); nodeidIt++) {

		if (graph.adj_relia[nodeidIt->first].size() <= 2
				and ::isContains(graph.KterminalSet, nodeidIt->first)==false) {
//            cout<<"node is pushed:"<<nodeidIt->first<<endl;
			Stack.push_back(nodeidIt->first);
		}
	}

	while (Stack.empty() == false) {



		int nodeId = Stack.front();
		Stack.pop_front();
		set <int> neibors = NeiborsOfNodeV1(graph, nodeId);
//		cout<<"neibors.size()"<<neibors.size()<<endl;
		if(neibors.size()==0){
//			cout<<"removed node:"<<nodeId<<endl;
			graph.adj_relia.erase(nodeId);
			continue;

		}
		if(neibors.size()==1){
			set<int>::iterator neiborit = neibors.begin();
			int neibor0 = *neiborit;
			deleteEdge(graph, nodeId, neibor0);
			if (graph.adj_relia[neibor0].size() <= 2
					and ::isContains(graph.KterminalSet, neibor0)
							== false) {
//				cout<<"new id pushed:"<<neibor0<<endl;
				Stack.push_back(neibor0);
			}
		} else {
//			cout<<"mid ID:"<<nodeId<<endl;
			set<int>::iterator neiborit = neibors.begin();
			int neibor0 = *neiborit;
			neiborit++;
			int neibor1 = *neiborit;

//			cout<<"neibors:"<<neibor0<<endl;
//			cout<<"neibors1:"<<neibor1<<endl;

			double newedge_relia = graph.adj_relia[nodeId][neibor0]
					* graph.adj_relia[nodeId][neibor1]*0.9;

			addEdge(graph, neibor0, neibor1, newedge_relia);

			//del edge
			deleteEdge(graph, nodeId, neibor0);
			deleteEdge(graph, nodeId, neibor1);

			if (graph.adj_relia[neibor0].size() <= 2
					and ::isContains(graph.KterminalSet, neibor0) == false) {
//				cout << "new id pushed:" << neibor0 << endl;
				Stack.push_back(neibor0);
			}

			if (graph.adj_relia[neibor1].size() <= 2
					and ::isContains(graph.KterminalSet, neibor1) == false) {
//				cout << "new id pushed:" << neibor1<< endl;
				Stack.push_back(neibor1);
			}




		}
//		graph.nVerts--;
		//erase node
//		cout<<"removed node:"<<nodeId<<endl;

		graph.adj_relia.erase(nodeId);
//		cout<<"Stack size:"<<Stack.size()<<endl;

	}




	map<int, map<int, double> >::iterator edgeit = graph.adj_relia.begin();
	for (; edgeit != graph.adj_relia.end(); edgeit++) {
		int size = graph.adj_relia[edgeit->first].size();
		graph.node_degree[edgeit->first] = size;

	}

}


void WriteToEdgelist(Graph &graph){
	 ofstream ofile;               //瀹氫箟杈撳嚭鏂囦欢
	 string parStr= "G:/reliability/StudyNetworkGML/results/compact/";
	 string myfile=parStr+graph.networkName+".edgelist";
	 ofile.open(myfile);
		const int n= graph.nVerts;


	//	int (*s)[len] = new int[len][len]();

	//	auto s = new int[n][n];
	//
		int **s=new int*[n];
		for(int i=0;i< n;i++){
			s[i]=new int[n];
		}


		for (int i = 1; i < graph.nVerts; i++) {
			for (int j = 1; j < graph.nVerts; j++) {

				s[i][j] = false;
			}
		}


		list<int>* bfsnodeslist=new list<int>;
		set<int>::iterator sit=graph.KterminalSet.begin();
		bfsnodeslist= bfs(graph, *sit);

		list<int> Stack=*bfsnodeslist;


		while (!Stack.empty())
		{
			int startID = Stack.front();
			//鍙栧嚭瀵瑰ご鍏冪礌
			Stack.pop_front();

			set<int> neibors = NeiborsOfNodeV1(graph, startID);

			set<int>::iterator neiborit = neibors.begin();
			for (; neiborit != neibors.end(); neiborit++) {
	//			cout<<"arc"<<*neiborit<<"-->"<<startID<<s[*neiborit][startID]<<endl;
				if (s[*neiborit][startID] == false) {
					s[*neiborit][startID] = true;
					s[startID][*neiborit] = true;

					int endID = *neiborit;
					Edge edge;
					edge.StartID = startID;
					edge.EndID = endID;
					edge.relia = graph.adj_relia[startID][endID];
					//add to the pedgelist
					ofile << edge.StartID << " " <<edge.EndID<< " "
							<< "{'relia':" <<" "<< edge.relia << "}" << endl;
				}

			}


		}

		cout<<"s bool array is released"<<endl;
		for (int i = 0; i < n; i++) {
			delete s[i];
		}
		delete []s;

	ofile.close();                //鍏抽棴鏂囦欢

}




void GraphReset_BasedBFS(Graph &graph,int startID){

	list<int> *bfsnodeslist = new list<int>;

	bfsnodeslist = bfs(graph, startID);


	map<int,map<int,double> >::iterator mapit=graph.adj_relia.begin();
	for(;mapit!=graph.adj_relia.end();mapit++){
		int nodemap1=graph.node_map[mapit->first];
		map<int,double>::iterator neiborit=mapit->second.begin();
		for(;neiborit!=mapit->second.end();neiborit++){
			int nodemap2=graph.node_map[neiborit->first];
			graph.adj_reliaV2[nodemap1][nodemap2]=neiborit->second;

		}



	}

//    cout<<"reset K"<<endl;
	set<int> K=graph.KterminalSet;
	graph.KterminalSet.clear();
	graph.VisitedSet.clear();
	set<int>::iterator kit=K.begin();
	for(; kit!=K.end();kit++){
		graph.KterminalSet.insert(graph.node_map[*kit]);
		graph.VisitedSet.insert(graph.node_map[*kit]);


	}

//	kit=graph.KterminalSet.begin();
//	for(; kit!=graph.KterminalSet.end();kit++){
//		cout<<"node K:"<<*kit<<endl;
//
//	}
//	cout<<"reset node_degreee"<<endl;
	graph.node_degree.clear();
	graph.node_degree= vector<short int>(graph.adj_reliaV2.size()+1);

	map<int, map<int, double> >::iterator edgeit = graph.adj_reliaV2.begin();
	for (; edgeit != graph.adj_reliaV2.end(); edgeit++) {
        int size=graph.adj_reliaV2[edgeit->first].size();
		graph.node_degree[edgeit->first]=size;

	}


//    cout<<"node_degreee"<<endl;
//
//	for(int i=1;i<graph.node_degree.size();i++){
//		cout<<"nodeID:"<<i<<"  degree is:"<<graph.node_degree[i]<<endl;
//	}


//	graph.adj_relia=graph.adj_reliaV2;
//
	graph.adj_relia=graph.adj_reliaV2;
	graph.adj_reliaV2.clear();

	graph.sID=1;


}





void serial_ana_generalEdge(Graph &graph,double noderelia) {


	list<int> Stack;

	map<int, map<int, double> >::iterator nodeidIt = graph.adj_relia.begin();
	for (; nodeidIt != graph.adj_relia.end(); nodeidIt++) {

		if (graph.adj_relia[nodeidIt->first].size() <= 2
				and ::isContains(graph.KterminalSet, nodeidIt->first)==false) {
//            cout<<"node is pushed:"<<nodeidIt->first<<endl;
			Stack.push_back(nodeidIt->first);
		}
	}

	while (Stack.empty() == false) {



		int nodeId = Stack.front();
		Stack.pop_front();
		set <int> neibors = NeiborsOfNodeV1(graph, nodeId);
//		cout<<"neibors.size()"<<neibors.size()<<endl;
		if(neibors.size()==0){
//			cout<<"removed node:"<<nodeId<<endl;
			graph.adj_relia.erase(nodeId);
			continue;

		}
		if(neibors.size()==1){
			set<int>::iterator neiborit = neibors.begin();
			int neibor0 = *neiborit;
			deleteEdge(graph, nodeId, neibor0);
			if (graph.adj_relia[neibor0].size() <= 2
					and ::isContains(graph.KterminalSet, neibor0)
							== false) {
//				cout<<"new id pushed:"<<neibor0<<endl;
				Stack.push_back(neibor0);
			}
		} else {
//			cout<<"mid ID:"<<nodeId<<endl;
			set<int>::iterator neiborit = neibors.begin();
			int neibor0 = *neiborit;
			neiborit++;
			int neibor1 = *neiborit;

//			cout<<"neibors:"<<neibor0<<endl;
//			cout<<"neibors1:"<<neibor1<<endl;

			double newedge_relia = graph.adj_relia[nodeId][neibor0]
					* graph.adj_relia[nodeId][neibor1]*noderelia;

			addEdge(graph, neibor0, neibor1, newedge_relia);

			//del edge
			deleteEdge(graph, nodeId, neibor0);
			deleteEdge(graph, nodeId, neibor1);

			if (graph.adj_relia[neibor0].size() <= 2
					and ::isContains(graph.KterminalSet, neibor0) == false) {
//				cout << "new id pushed:" << neibor0 << endl;
				Stack.push_back(neibor0);
			}

			if (graph.adj_relia[neibor1].size() <= 2
					and ::isContains(graph.KterminalSet, neibor1) == false) {
//				cout << "new id pushed:" << neibor1<< endl;
				Stack.push_back(neibor1);
			}




		}
//		graph.nVerts--;
		//erase node
//		cout<<"removed node:"<<nodeId<<endl;

		graph.adj_relia.erase(nodeId);
//		cout<<"Stack size:"<<Stack.size()<<endl;

	}




	map<int, map<int, double> >::iterator edgeit = graph.adj_relia.begin();
	for (; edgeit != graph.adj_relia.end(); edgeit++) {
		int size = graph.adj_relia[edgeit->first].size();
		graph.node_degree[edgeit->first] = size;

	}

}





void removedRundantEdge(Graph &graph) {


	list<int> Stack;

	map<int, map<int, double> >::iterator nodeidIt = graph.adj_relia.begin();
	for (; nodeidIt != graph.adj_relia.end(); nodeidIt++) {
		graph.adj_relia[nodeidIt->first].erase(nodeidIt->first);

		if (graph.adj_relia[nodeidIt->first].size() ==1
				and ::isContains(graph.KterminalSet, nodeidIt->first)==false) {
//            cout<<"node is pushed:"<<nodeidIt->first<<endl;
			Stack.push_back(nodeidIt->first);
		}


	}

	while (Stack.empty() == false) {



		int nodeId = Stack.front();
		Stack.pop_front();
		set <int> neibors = NeiborsOfNodeV1(graph, nodeId);
//		cout<<"neibors.size()"<<neibors.size()<<endl;
		if(neibors.size()==0){
//			cout<<"removed node:"<<nodeId<<endl;
			graph.adj_relia.erase(nodeId);
			continue;

		}
		if(neibors.size()==1){
			set<int>::iterator neiborit = neibors.begin();
			int neibor0 = *neiborit;
			deleteEdge(graph, nodeId, neibor0);
			if (graph.adj_relia[neibor0].size() <=1
					and ::isContains(graph.KterminalSet, neibor0)
							== false) {
//				cout<<"new id pushed:"<<neibor0<<endl;
				Stack.push_back(neibor0);
			}
		}
//		graph.nVerts--;
		//erase node
//		cout<<"removed node:"<<nodeId<<endl;

		graph.adj_relia.erase(nodeId);
//		cout<<"Stack size:"<<Stack.size()<<endl;

	}




	map<int, map<int, double> >::iterator edgeit = graph.adj_relia.begin();
	for (; edgeit != graph.adj_relia.end(); edgeit++) {
		int size = graph.adj_relia[edgeit->first].size();
		graph.node_degree[edgeit->first] = size;

	}

}




void serial_ana_generalV8(Graph &graph) {

	list<int> Stack;

	map<int, map<int, double> >::iterator nodeidIt = graph.adj_relia.begin();
	for (; nodeidIt != graph.adj_relia.end(); nodeidIt++) {

		if (graph.adj_relia[nodeidIt->first].size() == 2
				and ::isContains(graph.KterminalSet, nodeidIt->first)==false) {

			Stack.push_back(nodeidIt->first);
		}
	}

	while (Stack.empty() == false) {

		int nodeId = Stack.front();
		Stack.pop_front();
		set <int> neibors = NeiborsOfNodeV1(graph, nodeId);
		set<int>::iterator neiborit = neibors.begin();
		int neibor0 = *neiborit;
		neiborit++;
		int neibor1 = *neiborit;

		double newedge_relia = graph.adj_relia[nodeId][neibor0]
				* graph.adj_relia[nodeId][neibor1];

		addEdge(graph,neibor0, neibor1, newedge_relia);

		//del edge
		deleteEdge(graph,nodeId, neibor0);
		deleteEdge(graph,nodeId, neibor1);
//		graph.nVerts--;
		//erase node
		graph.adj_relia.erase(nodeId);

	}

}







void BridgeNetWork(Graph &graph, double edgefuncsp) {

	graph.nVerts = 5;	//0---4
	//	this->vertexList = new int[nVerts];
	//	for (int i = 1; i < this->nVerts; ++i) {
	//
	//		this->vertexList[i] = i;
	//	}

	graph.node_degree = vector<short int>(graph.nVerts);
	addEdge(graph,1, 2, edgefuncsp);
	addEdge(graph,2, 1, edgefuncsp);
	addEdge(graph,1, 3, edgefuncsp);
	addEdge(graph,3, 1, edgefuncsp);
	addEdge(graph,2, 3, edgefuncsp);
	addEdge(graph,3, 2, edgefuncsp);
	addEdge(graph,3, 4, edgefuncsp);
	addEdge(graph,4, 3, edgefuncsp);
	addEdge(graph,2, 4, edgefuncsp);
	addEdge(graph,4, 2, edgefuncsp);

	map<int, map<int, double> >::iterator edgeit = graph.adj_relia.begin();
	for (; edgeit != graph.adj_relia.end(); edgeit++) {
		int size = graph.adj_relia[edgeit->first].size();
		graph.node_degree[edgeit->first] = size;

	}

	//add Kset
	int sID = 1;
	int tID = 4;
	graph.KterminalSet.insert(sID);
	graph.KterminalSet.insert(tID);
	graph.VisitedSet.insert(sID);
	graph.VisitedSet.insert(tID);
	graph.sID=1;

	graph.mSClusterSize = -1;

}









void Network_ring(Graph &graph, double varFunctionsp) {


	graph.edgesize = 0;
//	graph.ClusterSumBasedKey = 0;
	graph.edgeindex = 0;
//	graph.relia = 0.0;

	graph.nVerts =7;
	graph.node_degree = vector<short int>(graph.nVerts);

    for(int i=1;i<6;i++){
    	graph.adj_relia[i][i+1]=varFunctionsp;
    	graph.adj_relia[i+1][i]=varFunctionsp;
    }

    graph.adj_relia[1][6]=varFunctionsp;
    graph.adj_relia[6][1]=varFunctionsp;



	map<int, map<int, double> >::iterator edgeit = graph.adj_relia.begin();
	for (; edgeit != graph.adj_relia.end(); edgeit++) {
		int size = graph.adj_relia[edgeit->first].size();
		graph.node_degree[edgeit->first] = size;

	}



	//add Kset
    int sID=1;
    int tID=3;
	graph.KterminalSet.insert(sID);
    graph.KterminalSet.insert(tID);
	graph.VisitedSet.insert(sID);
    graph.VisitedSet.insert(tID);





    graph.mSClusterSize=-1;


}






void Network1(Graph &graph, double varFunctionsp) {
	graph.networkName="Network1 K=2";
    int nodesize=6;
    InitialCompleteGraph(graph,nodesize, varFunctionsp);
	//add Kset
    int sID=1;
    int tID=nodesize;
	graph.KterminalSet.insert(sID);
    graph.KterminalSet.insert(tID);
	graph.VisitedSet.insert(sID);
    graph.VisitedSet.insert(tID);
    graph.mSClusterSize=-1;

   graph.sID=1;
}



void Network_completeDim10(Graph &graph, double varFunctionsp) {
	graph.networkName="Network_completeDim10 K=2";
    int nodesize=10;
    InitialCompleteGraph(graph,nodesize, varFunctionsp);
	//add Kset
    int sID=1;
    int tID=nodesize;
	graph.KterminalSet.insert(sID);
    graph.KterminalSet.insert(tID);
	graph.VisitedSet.insert(sID);
    graph.VisitedSet.insert(tID);
    graph.mSClusterSize=-1;

   graph.sID=1;
}


void Network_completeDim12(Graph &graph, double varFunctionsp) {
	graph.networkName="Network_completeDim12 K=2";
    int nodesize=12;
    InitialCompleteGraph(graph,nodesize, varFunctionsp);
	//add Kset
    int sID=1;
    int tID=nodesize;
	graph.KterminalSet.insert(sID);
    graph.KterminalSet.insert(tID);
	graph.VisitedSet.insert(sID);
    graph.VisitedSet.insert(tID);
    graph.mSClusterSize=-1;

   graph.sID=1;
}

void Network_completeDim14(Graph &graph, double varFunctionsp) {
	graph.networkName="Network_completeDim14 K=2";
    int nodesize=14;
    InitialCompleteGraph(graph,nodesize, varFunctionsp);
	//add Kset
    int sID=1;
    int tID=nodesize;
	graph.KterminalSet.insert(sID);
    graph.KterminalSet.insert(tID);
	graph.VisitedSet.insert(sID);
    graph.VisitedSet.insert(tID);
    graph.mSClusterSize=-1;

   graph.sID=1;
}




void Network11(Graph &graph, double varFunctionsp) {
	graph.networkName="Network11 K>2";
	int nodesize = 6;
	InitialCompleteGraph(graph,nodesize, varFunctionsp);
	//add Kset
	int sID = 1;
	int tID = nodesize;
	graph.KterminalSet.insert(sID);
	graph.KterminalSet.insert(tID);
	graph.VisitedSet.insert(sID);
	graph.VisitedSet.insert(tID);
    int knode=0;
    for (int i = 1; i <= 2; i++) {
		knode = 2 + (i - 1) * 1;
		graph.KterminalSet.insert(knode);
		graph.VisitedSet.insert(knode);
	}

    graph.mSClusterSize=-1;
    graph.sID=1;

}




void Network2(Graph &graph, double varFunctionsp) {

	graph.networkName="Network2";
	graph.edgesize = 0;
//	graph.ClusterSumBasedKey = 0;
	graph.edgeindex = 0;
//	graph.relia = 0.0;

	graph.nVerts =21;
	graph.node_degree = vector<short int>(graph.nVerts);

    for(int i=1;i<10;i++){
    	graph.adj_relia[i][i+1]=varFunctionsp;
    	graph.adj_relia[i+1][i]=varFunctionsp;
    }

    graph.adj_relia[1][10]=varFunctionsp;
    graph.adj_relia[10][1]=varFunctionsp;

    for (int i = 1; i <=10; i++) {
		graph.adj_relia[i][i + 10] = varFunctionsp;
		graph.adj_relia[i + 10][i] = varFunctionsp;
	}




    for (int i = 11; i < 20; i++) {
		graph.adj_relia[i][i + 1] = varFunctionsp;
		graph.adj_relia[i + 1][i] = varFunctionsp;
	}

    graph.adj_relia[11][20]=varFunctionsp;
    graph.adj_relia[20][11]=varFunctionsp;


	map<int, map<int, double> >::iterator edgeit = graph.adj_relia.begin();
	for (; edgeit != graph.adj_relia.end(); edgeit++) {
		int size = graph.adj_relia[edgeit->first].size();
		graph.node_degree[edgeit->first] = size;

	}



	//add Kset
    int sID=1;
    int tID=16;
	graph.KterminalSet.insert(sID);
    graph.KterminalSet.insert(tID);
	graph.VisitedSet.insert(sID);
    graph.VisitedSet.insert(tID);
    graph.sID=1;





    graph.mSClusterSize=-1;


}



void RingNet(Graph &graph, int m,int len,double varFunctionsp) {

	graph.networkName="RingNet"+::to_string(m)+"X"+::to_string(len);
	graph.edgesize = 0;
//	graph.ClusterSumBasedKey = 0;
	graph.edgeindex = 0;
//	graph.relia = 0.0;

	graph.nVerts =m*len+1;
	graph.node_degree = vector<short int>(graph.nVerts);


    for (int l = 1; l <=len; l++) {

    	for(int i=1;i<m;i++){
    		int j=i+(l-1)*m;
    		graph.adj_relia[j][j+1]=varFunctionsp;
    	    graph.adj_relia[j+1][j]=varFunctionsp;

    	}
    	int ii=1+(l-1)*m;
    	int jj=(l-1)*m+m;
    	 graph.adj_relia[ii][jj]=varFunctionsp;
    	 graph.adj_relia[jj][ii]=varFunctionsp;

	}


    for (int l = 1; l <len; l++) {

    	for(int i=1;i<=m;i++){
    		int j=i+(l-1)*m;
    		int t=i+l*m;
    		graph.adj_relia[j][t]=varFunctionsp;
    	    graph.adj_relia[t][j]=varFunctionsp;

    	}


	}


	map<int, map<int, double> >::iterator edgeit = graph.adj_relia.begin();
	for (; edgeit != graph.adj_relia.end(); edgeit++) {
		int size = graph.adj_relia[edgeit->first].size();
		graph.node_degree[edgeit->first] = size;

	}



	//add Kset
    int sID=1;
    int tID=m*(len-1)+m/2+1;
	graph.KterminalSet.insert(sID);
    graph.KterminalSet.insert(tID);
	graph.VisitedSet.insert(sID);
    graph.VisitedSet.insert(tID);
    graph.sID=1;

    graph.mSClusterSize=-1;


}




void NetworkV2(Graph &graph, double varFunctionsp) {

	graph.networkName="Network2 K=2";
	graph.edgesize = 0;
//	graph.ClusterSumBasedKey = 0;
	graph.edgeindex = 0;
//	graph.relia = 0.0;

	graph.nVerts =21;
	graph.node_degree = vector<short int>(graph.nVerts);

    for(int i=1;i<10;i++){
    	graph.adj_relia[i][i+1]=varFunctionsp;
    	graph.adj_relia[i+1][i]=varFunctionsp;
    }

    graph.adj_relia[1][10]=varFunctionsp;
    graph.adj_relia[10][1]=varFunctionsp;

    for (int i = 1; i <=10; i++) {
		graph.adj_relia[i][i + 14] = varFunctionsp;
		graph.adj_relia[i + 14][i] = varFunctionsp;
	}




    for (int i = 11; i < 20; i++) {
		graph.adj_relia[i][i + 1] = varFunctionsp;
		graph.adj_relia[i + 1][i] = varFunctionsp;
	}

    graph.adj_relia[11][20]=varFunctionsp;
    graph.adj_relia[20][11]=varFunctionsp;


	map<int, map<int, double> >::iterator edgeit = graph.adj_relia.begin();
	for (; edgeit != graph.adj_relia.end(); edgeit++) {
		int size = graph.adj_relia[edgeit->first].size();
		graph.node_degree[edgeit->first] = size;

	}



	//add Kset
    int sID=1;
    int tID=16;
	graph.KterminalSet.insert(sID);
    graph.KterminalSet.insert(tID);
	graph.VisitedSet.insert(sID);
    graph.VisitedSet.insert(tID);
    graph.sID=16;





    graph.mSClusterSize=-1;


}




void Network12(Graph &graph, double varFunctionsp) {

	graph.networkName="Network12 K>2";
	graph.edgesize = 0;
//	graph.ClusterSumBasedKey = 0;
	graph.edgeindex = 0;
//	graph.relia = 0.0;

	graph.nVerts =21;
	graph.node_degree = vector<short int>(graph.nVerts);

    for(int i=1;i<10;i++){
    	graph.adj_relia[i][i+1]=varFunctionsp;
    	graph.adj_relia[i+1][i]=varFunctionsp;
    }

    graph.adj_relia[1][10]=varFunctionsp;
    graph.adj_relia[10][1]=varFunctionsp;

    for (int i = 1; i <=10; i++) {
		graph.adj_relia[i][i + 10] = varFunctionsp;
		graph.adj_relia[i + 10][i] = varFunctionsp;
	}




    for (int i = 11; i < 20; i++) {
		graph.adj_relia[i][i + 1] = varFunctionsp;
		graph.adj_relia[i + 1][i] = varFunctionsp;
	}

    graph.adj_relia[11][20]=varFunctionsp;
    graph.adj_relia[20][11]=varFunctionsp;


	map<int, map<int, double> >::iterator edgeit = graph.adj_relia.begin();
	for (; edgeit != graph.adj_relia.end(); edgeit++) {
		int size = graph.adj_relia[edgeit->first].size();
		graph.node_degree[edgeit->first] = size;

	}



	//add Kset
    int sID=1;
    int tID=16;






	graph.KterminalSet.insert(sID);
    graph.KterminalSet.insert(tID);
	graph.VisitedSet.insert(sID);
    graph.VisitedSet.insert(tID);



    for (int i = 1; i <= 5; i++) {
		graph.KterminalSet.insert(i);
		graph.VisitedSet.insert(i);
	}

    for (int i = 11; i <15; i++) {
		graph.KterminalSet.insert(i);
		graph.VisitedSet.insert(i);
	}




    graph.sID=1;

    graph.mSClusterSize=-1;


}




void Network3(Graph &graph, double varFunctionsp) {

	graph.networkName="Network3 K=2";
	graph.edgesize = 0;
//	graph.ClusterSumBasedKey = 0;
	graph.edgeindex = 0;
//	graph.relia = 0.0;

	graph.nVerts =17;
	graph.node_degree = vector<short int>(graph.nVerts);

    for(int i=1;i<8;i++){
    	graph.adj_relia[i][i+1]=varFunctionsp;
    	graph.adj_relia[i+1][i]=varFunctionsp;
    }

    graph.adj_relia[1][8]=varFunctionsp;
    graph.adj_relia[8][1]=varFunctionsp;

    for (int i = 1; i <=8; i++) {
		graph.adj_relia[i][i + 8] = varFunctionsp;
		graph.adj_relia[i +8][i] = varFunctionsp;
	}




    for (int i = 9; i < 16; i++) {
		graph.adj_relia[i][i + 1] = varFunctionsp;
		graph.adj_relia[i + 1][i] = varFunctionsp;
	}

    graph.adj_relia[9][16]=varFunctionsp;
    graph.adj_relia[16][9]=varFunctionsp;


	map<int, map<int, double> >::iterator edgeit = graph.adj_relia.begin();
	for (; edgeit != graph.adj_relia.end(); edgeit++) {
		int size = graph.adj_relia[edgeit->first].size();
		graph.node_degree[edgeit->first] = size;

	}



	//add Kset
    int sID=1;
    int tID=14;
	graph.KterminalSet.insert(sID);
    graph.KterminalSet.insert(tID);
	graph.VisitedSet.insert(sID);
    graph.VisitedSet.insert(tID);



    graph.sID=14;

    graph.mSClusterSize=-1;


}



void Network13(Graph &graph, double varFunctionsp) {

	graph.networkName="Network13 K=N/2";
	graph.edgesize = 0;
//	graph.ClusterSumBasedKey = 0;
	graph.edgeindex = 0;
//	graph.relia = 0.0;

	graph.nVerts =17;
	graph.node_degree = vector<short int>(graph.nVerts);

    for(int i=1;i<8;i++){
    	graph.adj_relia[i][i+1]=varFunctionsp;
    	graph.adj_relia[i+1][i]=varFunctionsp;
    }

    graph.adj_relia[1][8]=varFunctionsp;
    graph.adj_relia[8][1]=varFunctionsp;

    for (int i = 1; i <=8; i++) {
		graph.adj_relia[i][i + 8] = varFunctionsp;
		graph.adj_relia[i +8][i] = varFunctionsp;
	}




    for (int i = 9; i < 16; i++) {
		graph.adj_relia[i][i + 1] = varFunctionsp;
		graph.adj_relia[i + 1][i] = varFunctionsp;
	}

    graph.adj_relia[9][16]=varFunctionsp;
    graph.adj_relia[16][9]=varFunctionsp;


	map<int, map<int, double> >::iterator edgeit = graph.adj_relia.begin();
	for (; edgeit != graph.adj_relia.end(); edgeit++) {
		int size = graph.adj_relia[edgeit->first].size();
		graph.node_degree[edgeit->first] = size;

	}



	//add Kset
    int sID=1;
    int tID=14;
	graph.KterminalSet.insert(sID);
    graph.KterminalSet.insert(tID);
	graph.VisitedSet.insert(sID);
    graph.VisitedSet.insert(tID);


    for (int i = 6; i <=9; i++) {
   		graph.KterminalSet.insert(i);
   		graph.VisitedSet.insert(i);
   	}

       for (int i = 14; i <=16; i++) {
   		graph.KterminalSet.insert(i);
   		graph.VisitedSet.insert(i);
   	}


   graph.sID=1;

    graph.mSClusterSize=-1;


}


void Network4(Graph &graph, double varFunctionsp) {

    graph.networkName="Network4 k=2";
	graph.edgesize = 0;
//	graph.ClusterSumBasedKey = 0;
	graph.edgeindex = 0;
//	graph.relia = 0.0;

	graph.nVerts =15;
	graph.node_degree = vector<short int>(graph.nVerts);

    //from 1  ---2,3,4
    set<int> neibors_set_1;
    neibors_set_1.insert(2);
    neibors_set_1.insert(3);
    neibors_set_1.insert(4);

   set<int>::iterator nit=neibors_set_1.begin();
   for(; nit!=neibors_set_1.end();nit++){
	   graph.adj_relia[1][*nit] = varFunctionsp;
	   graph.adj_relia[*nit][1] = varFunctionsp;
   }

   //from 2 ----4,5

	neibors_set_1.clear();
	neibors_set_1.insert(5);
	neibors_set_1.insert(4);
	nit = neibors_set_1.begin();
	for (; nit != neibors_set_1.end(); nit++) {
		graph.adj_relia[2][*nit] = varFunctionsp;
		graph.adj_relia[*nit][2] = varFunctionsp;
	}

	   //from 3 ----4,5,6

	neibors_set_1.clear();
	neibors_set_1.insert(5);
	neibors_set_1.insert(4);
	neibors_set_1.insert(6);
	nit = neibors_set_1.begin();
	for (; nit != neibors_set_1.end(); nit++) {
		graph.adj_relia[3][*nit] = varFunctionsp;
		graph.adj_relia[*nit][3] = varFunctionsp;
	}

	//from 4 ---7,8
	neibors_set_1.clear();
	neibors_set_1.insert(7);
	neibors_set_1.insert(8);
	nit = neibors_set_1.begin();
	for (; nit != neibors_set_1.end(); nit++) {
		graph.adj_relia[4][*nit] = varFunctionsp;
		graph.adj_relia[*nit][4] = varFunctionsp;
	}

	//from 5 ---7
	neibors_set_1.clear();
	neibors_set_1.insert(7);
	nit = neibors_set_1.begin();
	for (; nit != neibors_set_1.end(); nit++) {
		graph.adj_relia[5][*nit] = varFunctionsp;
		graph.adj_relia[*nit][5] = varFunctionsp;
	}

	//from 6 ---8,9
	neibors_set_1.clear();
	neibors_set_1.insert(8);
	neibors_set_1.insert(9);
	nit = neibors_set_1.begin();
	for (; nit != neibors_set_1.end(); nit++) {
		graph.adj_relia[6][*nit] = varFunctionsp;
		graph.adj_relia[*nit][6] = varFunctionsp;
	}

	//from 7 ---10,11
	neibors_set_1.clear();
	neibors_set_1.insert(10);
	neibors_set_1.insert(11);
	nit = neibors_set_1.begin();
	for (; nit != neibors_set_1.end(); nit++) {
		graph.adj_relia[7][*nit] = varFunctionsp;
		graph.adj_relia[*nit][7] = varFunctionsp;
	}

	//from 8 ---9,12
	neibors_set_1.clear();
	neibors_set_1.insert(9);
	neibors_set_1.insert(12);
	nit = neibors_set_1.begin();
	for (; nit != neibors_set_1.end(); nit++) {
		graph.adj_relia[8][*nit] = varFunctionsp;
		graph.adj_relia[*nit][8] = varFunctionsp;
	}


	//from 9---11,13
	neibors_set_1.clear();
	neibors_set_1.insert(11);
	neibors_set_1.insert(13);
	nit = neibors_set_1.begin();
	for (; nit != neibors_set_1.end(); nit++) {
		graph.adj_relia[9][*nit] = varFunctionsp;
		graph.adj_relia[*nit][9] = varFunctionsp;
	}

	//from 10---11,14
	neibors_set_1.clear();
	neibors_set_1.insert(11);
	neibors_set_1.insert(14);
	nit = neibors_set_1.begin();
	for (; nit != neibors_set_1.end(); nit++) {
		graph.adj_relia[10][*nit] = varFunctionsp;
		graph.adj_relia[*nit][10] = varFunctionsp;
	}

	//from 12---14
	neibors_set_1.clear();
	neibors_set_1.insert(14);
	nit = neibors_set_1.begin();
	for (; nit != neibors_set_1.end(); nit++) {
		graph.adj_relia[12][*nit] = varFunctionsp;
		graph.adj_relia[*nit][12] = varFunctionsp;
	}

	//from 13---14
	neibors_set_1.clear();
	neibors_set_1.insert(14);
	nit = neibors_set_1.begin();
	for (; nit != neibors_set_1.end(); nit++) {
		graph.adj_relia[13][*nit] = varFunctionsp;
		graph.adj_relia[*nit][13] = varFunctionsp;
	}








	map<int, map<int, double> >::iterator edgeit = graph.adj_relia.begin();
	for (; edgeit != graph.adj_relia.end(); edgeit++) {
		int size = graph.adj_relia[edgeit->first].size();
		graph.node_degree[edgeit->first] = size;

	}



	//add Kset
    graph.sID=1;
	graph.KterminalSet.insert(1);
//	 graph.KterminalSet.insert(2);
//	 graph.KterminalSet.insert(3);
//	 graph.KterminalSet.insert(4);
//	 graph.KterminalSet.insert(5);
    graph.KterminalSet.insert(12);


    graph.mSClusterSize=-1;


}


void Network14(Graph &graph, double varFunctionsp) {

    graph.networkName="Network14 k=N/2";
	graph.edgesize = 0;
//	graph.ClusterSumBasedKey = 0;
	graph.edgeindex = 0;
//	graph.relia = 0.0;

	graph.nVerts =15;
	graph.node_degree = vector<short int>(graph.nVerts);

    //from 1  ---2,3,4
    set<int> neibors_set_1;
    neibors_set_1.insert(2);
    neibors_set_1.insert(3);
    neibors_set_1.insert(4);

   set<int>::iterator nit=neibors_set_1.begin();
   for(; nit!=neibors_set_1.end();nit++){
	   graph.adj_relia[1][*nit] = varFunctionsp;
	   graph.adj_relia[*nit][1] = varFunctionsp;
   }

   //from 2 ----4,5

	neibors_set_1.clear();
	neibors_set_1.insert(5);
	neibors_set_1.insert(4);
	nit = neibors_set_1.begin();
	for (; nit != neibors_set_1.end(); nit++) {
		graph.adj_relia[2][*nit] = varFunctionsp;
		graph.adj_relia[*nit][2] = varFunctionsp;
	}

	   //from 3 ----4,5,6

	neibors_set_1.clear();
	neibors_set_1.insert(5);
	neibors_set_1.insert(4);
	neibors_set_1.insert(6);
	nit = neibors_set_1.begin();
	for (; nit != neibors_set_1.end(); nit++) {
		graph.adj_relia[3][*nit] = varFunctionsp;
		graph.adj_relia[*nit][3] = varFunctionsp;
	}

	//from 4 ---7,8
	neibors_set_1.clear();
	neibors_set_1.insert(7);
	neibors_set_1.insert(8);
	nit = neibors_set_1.begin();
	for (; nit != neibors_set_1.end(); nit++) {
		graph.adj_relia[4][*nit] = varFunctionsp;
		graph.adj_relia[*nit][4] = varFunctionsp;
	}

	//from 5 ---7
	neibors_set_1.clear();
	neibors_set_1.insert(7);
	nit = neibors_set_1.begin();
	for (; nit != neibors_set_1.end(); nit++) {
		graph.adj_relia[5][*nit] = varFunctionsp;
		graph.adj_relia[*nit][5] = varFunctionsp;
	}

	//from 6 ---8,9
	neibors_set_1.clear();
	neibors_set_1.insert(8);
	neibors_set_1.insert(9);
	nit = neibors_set_1.begin();
	for (; nit != neibors_set_1.end(); nit++) {
		graph.adj_relia[6][*nit] = varFunctionsp;
		graph.adj_relia[*nit][6] = varFunctionsp;
	}

	//from 7 ---10,11
	neibors_set_1.clear();
	neibors_set_1.insert(10);
	neibors_set_1.insert(11);
	nit = neibors_set_1.begin();
	for (; nit != neibors_set_1.end(); nit++) {
		graph.adj_relia[7][*nit] = varFunctionsp;
		graph.adj_relia[*nit][7] = varFunctionsp;
	}

	//from 8 ---9,12
	neibors_set_1.clear();
	neibors_set_1.insert(9);
	neibors_set_1.insert(12);
	nit = neibors_set_1.begin();
	for (; nit != neibors_set_1.end(); nit++) {
		graph.adj_relia[8][*nit] = varFunctionsp;
		graph.adj_relia[*nit][8] = varFunctionsp;
	}


	//from 9---11,13
	neibors_set_1.clear();
	neibors_set_1.insert(11);
	neibors_set_1.insert(13);
	nit = neibors_set_1.begin();
	for (; nit != neibors_set_1.end(); nit++) {
		graph.adj_relia[9][*nit] = varFunctionsp;
		graph.adj_relia[*nit][9] = varFunctionsp;
	}

	//from 10---11,14
	neibors_set_1.clear();
	neibors_set_1.insert(11);
	neibors_set_1.insert(14);
	nit = neibors_set_1.begin();
	for (; nit != neibors_set_1.end(); nit++) {
		graph.adj_relia[10][*nit] = varFunctionsp;
		graph.adj_relia[*nit][10] = varFunctionsp;
	}

	//from 12---14
	neibors_set_1.clear();
	neibors_set_1.insert(14);
	nit = neibors_set_1.begin();
	for (; nit != neibors_set_1.end(); nit++) {
		graph.adj_relia[12][*nit] = varFunctionsp;
		graph.adj_relia[*nit][12] = varFunctionsp;
	}

	//from 13---14
	neibors_set_1.clear();
	neibors_set_1.insert(14);
	nit = neibors_set_1.begin();
	for (; nit != neibors_set_1.end(); nit++) {
		graph.adj_relia[13][*nit] = varFunctionsp;
		graph.adj_relia[*nit][13] = varFunctionsp;
	}








	map<int, map<int, double> >::iterator edgeit = graph.adj_relia.begin();
	for (; edgeit != graph.adj_relia.end(); edgeit++) {
		int size = graph.adj_relia[edgeit->first].size();
		graph.node_degree[edgeit->first] = size;

	}



	//add Kset
    graph.sID=12;
	graph.KterminalSet.insert(1);
	 graph.KterminalSet.insert(2);
	 graph.KterminalSet.insert(3);
	 graph.KterminalSet.insert(4);
	 graph.KterminalSet.insert(5);
    graph.KterminalSet.insert(12);


    graph.mSClusterSize=-1;


}




vector< string> split(string str, string pattern)
{
    vector<string> ret;
    if (pattern.empty()) return ret;
    size_t start = 0, index = str.find_first_of(pattern, 0);
    while (index != str.npos)
    {
        if (start != index)
            ret.push_back(str.substr(start, index - start));
        start = index + 1;
        index = str.find_first_of(pattern, start);
    }
    if (!str.substr(start).empty())
        ret.push_back(str.substr(start));
    return ret;
}







void Network_Bridgedemo(Graph &graph, double varFunctionsp) {

	graph.networkName="brideNet";

	graph.edgesize = 0;
//	graph.ClusterSumBasedKey = 0;
	graph.edgeindex = 0;
//	graph.relia = 0.0;

	graph.nVerts =5;
	graph.node_degree = vector<short int>(graph.nVerts);


	//from 1  ---2,3
    int seedID=1;
    set<int> neibors_set_1;
    neibors_set_1.insert(2);
    neibors_set_1.insert(3);

   set<int>::iterator nit=neibors_set_1.begin();
   for(; nit!=neibors_set_1.end();nit++){
	   graph.adj_relia[seedID][*nit] = varFunctionsp;
	   graph.adj_relia[*nit][seedID] = varFunctionsp;
   }

    seedID=2;
	//from 2 ---3,4
   //clear
	neibors_set_1.clear();
	//update
	neibors_set_1.insert(3);
	neibors_set_1.insert(4);
	nit = neibors_set_1.begin();
	for (; nit != neibors_set_1.end(); nit++) {
		graph.adj_relia[seedID][*nit] = varFunctionsp;
		graph.adj_relia[*nit][seedID] = varFunctionsp;
	}


	//from 3 ---4
	seedID = 3;
	//clear
	neibors_set_1.clear();
	//update
	neibors_set_1.insert(4);

	nit = neibors_set_1.begin();
	for (; nit != neibors_set_1.end(); nit++) {
		graph.adj_relia[seedID][*nit] = varFunctionsp;
		graph.adj_relia[*nit][seedID] = varFunctionsp;
	}




	map<int, map<int, double> >::iterator edgeit = graph.adj_relia.begin();
	for (; edgeit != graph.adj_relia.end(); edgeit++) {
		int size = graph.adj_relia[edgeit->first].size();
		graph.node_degree[edgeit->first] = size;

	}



	//add Kset
    int sID=1;
    int tID=4;
	graph.KterminalSet.insert(sID);
    graph.KterminalSet.insert(tID);
	graph.VisitedSet.insert(sID);
    graph.VisitedSet.insert(tID);

    graph.sID=1;
    graph.mSClusterSize=-1;


}



void Network_mesh3_3_2T(Graph &graph, double varFunctionsp) {
    int row=3;
    int col=3;
    InitialMeshGraph(graph,row, col, varFunctionsp);
	//add Kset
    int sID=1;
    int tID=row*col;
	graph.KterminalSet.insert(sID);
    graph.KterminalSet.insert(tID);
	graph.VisitedSet.insert(sID);
    graph.VisitedSet.insert(tID);





    graph.mSClusterSize=-1;
    graph.sID=1;


}

void Network_mesh2_3_2T(Graph &graph, double varFunctionsp) {
    int row=2;
    int col=3;
    InitialMeshGraph(graph,row, col, varFunctionsp);
	//add Kset
    int sID=1;
    int tID=row*col;
	graph.KterminalSet.insert(sID);
    graph.KterminalSet.insert(tID);






    graph.mSClusterSize=-1;


}




void Network5(Graph &graph, double varFunctionsp) {

	graph.networkName="Network5";
	graph.edgesize = 0;
//	graph.ClusterSumBasedKey = 0;
	graph.edgeindex = 0;
//	graph.relia = 0.0;

	graph.nVerts =21;
	graph.node_degree = vector<short int>(graph.nVerts);

    for(int i=1;i<5;i++){
    	graph.adj_relia[i][i+1]=varFunctionsp;
    	graph.adj_relia[i+1][i]=varFunctionsp;
    }

    graph.adj_relia[1][5]=varFunctionsp;
    graph.adj_relia[5][1]=varFunctionsp;

    for (int i = 6; i <15; i++) {
    	graph.adj_relia[i][i+1]=varFunctionsp;
    	graph.adj_relia[i+1][i]=varFunctionsp;
	}

    graph.adj_relia[6][15]=varFunctionsp;
    graph.adj_relia[15][6]=varFunctionsp;


    for (int i = 16; i <20; i++) {
      	graph.adj_relia[i][i+1]=varFunctionsp;
      	graph.adj_relia[i+1][i]=varFunctionsp;
  	}

    graph.adj_relia[16][20]=varFunctionsp;
     graph.adj_relia[20][16]=varFunctionsp;

     int index=6;
     for (int i = 1; i <= 5; i++) {
		graph.adj_relia[i][index] = varFunctionsp;
		graph.adj_relia[index][i] = varFunctionsp;
		index+=2;
	}

	index = 7;
	for (int i = 16; i <= 20; i++) {
		graph.adj_relia[i][index] = varFunctionsp;
		graph.adj_relia[index][i] = varFunctionsp;
		index += 2;
	}





	map<int, map<int, double> >::iterator edgeit = graph.adj_relia.begin();
	for (; edgeit != graph.adj_relia.end(); edgeit++) {
		int size = graph.adj_relia[edgeit->first].size();
		graph.node_degree[edgeit->first] = size;

	}



	//add Kset
    int sID=1;
    int tID=18;
	graph.KterminalSet.insert(sID);
    graph.KterminalSet.insert(tID);
	graph.VisitedSet.insert(sID);
    graph.VisitedSet.insert(tID);


    graph.sID=1;


    graph.mSClusterSize=-1;


}



void Network15(Graph &graph, double varFunctionsp) {

	graph.networkName="Network15 K>2";
	graph.edgesize = 0;
//	graph.ClusterSumBasedKey = 0;
	graph.edgeindex = 0;
//	graph.relia = 0.0;

	graph.nVerts =21;
	graph.node_degree = vector<short int>(graph.nVerts);

    for(int i=1;i<5;i++){
    	graph.adj_relia[i][i+1]=varFunctionsp;
    	graph.adj_relia[i+1][i]=varFunctionsp;
    }

    graph.adj_relia[1][5]=varFunctionsp;
    graph.adj_relia[5][1]=varFunctionsp;

    for (int i = 6; i <15; i++) {
    	graph.adj_relia[i][i+1]=varFunctionsp;
    	graph.adj_relia[i+1][i]=varFunctionsp;
	}

    graph.adj_relia[6][15]=varFunctionsp;
    graph.adj_relia[15][6]=varFunctionsp;


    for (int i = 16; i <20; i++) {
      	graph.adj_relia[i][i+1]=varFunctionsp;
      	graph.adj_relia[i+1][i]=varFunctionsp;
  	}

    graph.adj_relia[16][20]=varFunctionsp;
     graph.adj_relia[20][16]=varFunctionsp;

     int index=6;
     for (int i = 1; i <= 5; i++) {
		graph.adj_relia[i][index] = varFunctionsp;
		graph.adj_relia[index][i] = varFunctionsp;
		index+=2;
	}

	index = 7;
	for (int i = 16; i <= 20; i++) {
		graph.adj_relia[i][index] = varFunctionsp;
		graph.adj_relia[index][i] = varFunctionsp;
		index += 2;
	}





	map<int, map<int, double> >::iterator edgeit = graph.adj_relia.begin();
	for (; edgeit != graph.adj_relia.end(); edgeit++) {
		int size = graph.adj_relia[edgeit->first].size();
		graph.node_degree[edgeit->first] = size;

	}



	//add Kset
    int sID=1;
    int tID=18;

	graph.KterminalSet.insert(sID);
    graph.KterminalSet.insert(tID);
	graph.VisitedSet.insert(sID);
    graph.VisitedSet.insert(tID);


    for (int i = 17; i <20; i++) {
   		graph.KterminalSet.insert(i);
   		graph.VisitedSet.insert(i);
   	}

    for (int i = 9; i <14; i++) {
   		graph.KterminalSet.insert(i);
   		graph.VisitedSet.insert(i);
   	}




    graph.sID=1;
    graph.mSClusterSize=-1;




}






void Network6(Graph &graph, double varFunctionsp) {
    int row=10;
    int col=3;
    InitialMeshGraph(graph,row, col, varFunctionsp);
	//add Kset
    int sID=1;
    int tID=row*col;
	graph.KterminalSet.insert(sID);
    graph.KterminalSet.insert(tID);
	graph.VisitedSet.insert(sID);
    graph.VisitedSet.insert(tID);





    graph.mSClusterSize=-1;
    graph.sID=1;


}



void Network16(Graph &graph, double varFunctionsp) {
    int row=10;
    int col=3;
    InitialMeshGraph(graph,row, col, varFunctionsp);
	//add Kset
    int sID=1;
    int tID=row*col;

    int knode=0;
    for(int i=1;i<=8;i++){
    	knode=1+(i-1)*3;
    	 graph.KterminalSet.insert(knode);
    	 graph.VisitedSet.insert(knode);
    }
    knode=0;
    for(int i=1;i<=6;i++){
    	knode=2+(i-1)*3;
    	 graph.KterminalSet.insert(knode);
    	 graph.VisitedSet.insert(knode);
    }


    graph.KterminalSet.insert(tID);
    graph.VisitedSet.insert(tID);
    graph.mSClusterSize=-1;

    graph.sID=1;


}



void Network7(Graph &graph, double varFunctionsp) {
	graph.networkName="Network7 mesh12X3 K=2";
    int row=12;
    int col=3;
    InitialMeshGraph(graph,row, col, varFunctionsp);
	//add Kset
    int sID=1;
    int tID=row*col;
	graph.KterminalSet.insert(sID);
    graph.KterminalSet.insert(tID);
	graph.VisitedSet.insert(sID);
    graph.VisitedSet.insert(tID);
    graph.mSClusterSize=-1;
    graph.sID=1;


}




void Network17(Graph &graph, double edgefuncsp) {
	graph.networkName="Network7 mesh12X3 K=N/2";
    int row=12;
    int col=3;
    InitialMeshGraph(graph,row, col, edgefuncsp);
	//add Kset
    int sID=1;
    int tID=row*col;

    int knode=0;
    for(int i=1;i<=10;i++){
    	knode=1+(i-1)*3;
    	 graph.KterminalSet.insert(knode);
    	 graph.VisitedSet.insert(knode);
    }
    knode=0;
    for(int i=1;i<=7;i++){
    	knode=2+(i-1)*3;
    	 graph.KterminalSet.insert(knode);
    	 graph.VisitedSet.insert(knode);
    }


    graph.KterminalSet.insert(tID);
    graph.VisitedSet.insert(tID);
    graph.mSClusterSize=-1;
    graph.sID=1;


}



void Network8(Graph &graph, double varFunctionsp) {
	graph.networkName="Network8";
    int row=5;
    int col=5;
    InitialMeshGraph(graph,row, col, varFunctionsp);
	//add Kset
    int sID=1;
    int tID=row*col;
	graph.KterminalSet.insert(sID);
    graph.KterminalSet.insert(tID);
	graph.VisitedSet.insert(sID);
    graph.VisitedSet.insert(tID);
    graph.mSClusterSize=-1;
    graph.sID=1;


}





void Network18(Graph &graph, double edgefuncsp) {
	graph.networkName="Network18 mesh5X5 K>2";
    int row=5;
    int col=5;
    InitialMeshGraph(graph,row, col, edgefuncsp);
	//add Kset
    int sID=1;
    int tID=row*col;

    int knode=0;
    for(int i=1;i<=5;i++){
    	knode=1+(i-1)*5;
    	 graph.KterminalSet.insert(knode);
    	 graph.VisitedSet.insert(knode);
    }

    for(int i=1;i<=5;i++){
    	knode=2+(i-1)*5;
    	 graph.KterminalSet.insert(knode);
    	 graph.VisitedSet.insert(knode);
    }

    knode=0;
    for(int i=1;i<=1;i++){
    	knode=3+(i-1)*5;
    	 graph.KterminalSet.insert(knode);
    	 graph.VisitedSet.insert(knode);
    }

    set<int>::iterator kit=graph.KterminalSet.begin();
    for(;kit!=graph.KterminalSet.end();kit++){
    	cout<<"K:="<<*kit<<" --";

    }
    cout<<endl;

    graph.sID=1;
    graph.KterminalSet.insert(tID);
    graph.VisitedSet.insert(tID);
    graph.mSClusterSize=-1;


}




void Network9(Graph &graph, double varFunctionsp) {
	graph.networkName="Network19 mesh20X2 K=2";
    int row=20;
    int col=2;
    InitialMeshGraph(graph,row, col, varFunctionsp);
	//add Kset
    int sID=1;
    int tID=row*col;
	graph.KterminalSet.insert(sID);
    graph.KterminalSet.insert(tID);
	graph.VisitedSet.insert(sID);
    graph.VisitedSet.insert(tID);
    graph.mSClusterSize=-1;
    graph.sID=1;


}




void Network19(Graph &graph, double edgefuncsp) {
	graph.networkName="Network19 mesh20X2 K=N/2";
    int row=20;
    int col=2;
    InitialMeshGraph(graph,row, col, edgefuncsp);
	//add Kset
    int sID=1;
    int tID=row*col;

    int knode=0;
    for(int i=1;i<=10;i++){
    	knode=1+(i-1)*2;
    	 graph.KterminalSet.insert(knode);
    	 graph.VisitedSet.insert(knode);
    }

    for(int i=1;i<=9;i++){
    	knode=2+(i-1)*2;
    	 graph.KterminalSet.insert(knode);
    	 graph.VisitedSet.insert(knode);
    }




    graph.KterminalSet.insert(tID);
    graph.VisitedSet.insert(tID);
    graph.mSClusterSize=-1;
    graph.sID=1;


}




void Network10(Graph &graph, double varFunctionsp) {
    int row=100;
    int col=2;
    InitialMeshGraph(graph,row, col, varFunctionsp);
	//add Kset
    int sID=1;
    int tID=row*col;
	graph.KterminalSet.insert(sID);
    graph.KterminalSet.insert(tID);
	graph.VisitedSet.insert(sID);
    graph.VisitedSet.insert(tID);
    graph.mSClusterSize=-1;
    graph.sID=1;


}




void Network20(Graph &graph, double edgefuncsp) {
    int row=100;
    int col=2;
    InitialMeshGraph(graph,row, col, edgefuncsp);
	//add Kset
    int sID=1;
    int tID=row*col;

    int knode=0;
    for(int i=1;i<=50;i++){
    	knode=1+(i-1)*2;
    	 graph.KterminalSet.insert(knode);
    	 graph.VisitedSet.insert(knode);
    }

    for(int i=1;i<=49;i++){
    	knode=2+(i-1)*2;
    	 graph.KterminalSet.insert(knode);
    	 graph.VisitedSet.insert(knode);
    }




    graph.KterminalSet.insert(tID);
    graph.VisitedSet.insert(tID);
    graph.mSClusterSize=-1;
    graph.sID=1;


}




void initial_nodeDegree(Graph &graph) {

	map<int, map<int, double> >::iterator edgeit = graph.adj_relia.begin();
	for (; edgeit != graph.adj_relia.end(); edgeit++) {
		int size = graph.adj_relia[edgeit->first].size();
		graph.node_degree[edgeit->first] = size;

	}

}

//void initial_nodeDegreeVector(Graph &graph){
//
//	map<int, map<int, double> >::iterator edgeit = graph.adj_relia.begin();
//	for (; edgeit != graph.adj_relia.end(); edgeit++) {
//
//		graph.node_degree[edgeit->first]=graph.adj_relia[edgeit->first].size();
//
//	}
//
//}

void CreateLatticeGraph(int row, int col, Graph &graph) {

//	graph.relia = 0.0;
	graph.nVerts = row * col + 1;
	//	this->vertexList = new int[nVerts];
	//	for (int i = 1; i < this->nVerts; ++i) {
	//
	//		this->vertexList[i] = i;
	//	}

	cout << "add row" << endl;

	//增加row

	for (int r = 1; r <= row; r++) {
		for (int c = 1; c <= col - 1; c++) {

			int start = (r - 1) * col + c;
			int end = start + 1;
			graph.adj_relia[start][end] = 0.9;
			graph.adj_relia[end][start] = 0.9;

		}

	}

	cout << "add col" << endl;

	//增加col
	for (int r = 1; r < row; r++) {
		for (int c = 1; c <= col; c++) {

			int sID = (r - 1) * col + c;
			int tID = sID + col;
			graph.adj_relia[sID][tID] = 0.9;
			graph.adj_relia[tID][sID] = 0.9;

		}

	}

	//	this->addEdge(0, 1);
	//	this->addEdge(1,2);
	//	this->addEdge(2,3);

}

void initial_VhashMap(vector<HashGraphmap> &Vhashmap, int row, int col) {
	int edgessize = (row - 1) * col + row * (col - 1) + 1;

	for (int i = 1; i <= edgessize; i++) {
		HashGraphmap hashGraphmap;
		Vhashmap.push_back(hashGraphmap);
	}
}





inline void removeZerodegreeOfEndIDFromEdge19V6(Graph &graph, int endID,
		auxiliaryGraph &auxiliarygraph) {
	auxiliarygraph.endID_clusterSet_it->clusterSet.erase(endID);
	bool isremoveAgain = true;

	if (auxiliarygraph.endID_clusterSet_it->clusterSet.size() > 0) {

		//
		set<int>::iterator remainedIt =
				auxiliarygraph.endID_clusterSet_it->clusterSet.begin();

		if (graph.node_degree[*remainedIt-graph.minBoundaryNodeID] == 1) {
			if (auxiliarygraph.endID_clusterSet_it->clusterSet.size() == 1
					and auxiliarygraph.endID_clusterSet_it->isSource == false) {
				//reduntbranch

				graph.mClusterBounaryList.erase(
						auxiliarygraph.endID_clusterSet_it);
				graph.node_degree[*remainedIt-graph.minBoundaryNodeID] = -2;
				isremoveAgain = false;
			}

		}

		//

		if (isremoveAgain == true) {

			clusterNodeSet Newend_clusterSet;
			Newend_clusterSet.isSource =
					auxiliarygraph.endID_clusterSet_it->isSource;
			Newend_clusterSet.clusterSet =
					auxiliarygraph.endID_clusterSet_it->clusterSet;

			//remove it first
			graph.mClusterBounaryList.erase(auxiliarygraph.endID_clusterSet_it);
			//insert the new

			if (Newend_clusterSet.clusterSet.size() > 0) {

				insertNewCluster(graph, Newend_clusterSet);
			}
		}
	}else{
		graph.mClusterBounaryList.erase(
							auxiliarygraph.endID_clusterSet_it);
	}

}



inline void removeZerodegreeOfStartIDFromEdge19V6(Graph &graph, int startID,
		auxiliaryGraph &auxiliarygraph) {

	auxiliarygraph.startID_clusterSet_it->clusterSet.erase(startID);
	bool isremoveAgain=true;

	if (auxiliarygraph.startID_clusterSet_it->clusterSet.size() > 0) {
		set<int>::iterator remainedIt =
				auxiliarygraph.startID_clusterSet_it->clusterSet.begin();
		bool isremoveAgain=true;
		if (graph.node_degree[*remainedIt-graph.minBoundaryNodeID] == 1) {
			if (auxiliarygraph.startID_clusterSet_it->clusterSet.size() == 1
					and auxiliarygraph.startID_clusterSet_it->isSource
							== false) {
				//reduntbranch

				graph.mClusterBounaryList.erase(auxiliarygraph.startID_clusterSet_it);
				graph.node_degree[*remainedIt-graph.minBoundaryNodeID]=-2;
				isremoveAgain=false;
			}

		}

		if (isremoveAgain == true) {

			clusterNodeSet Newstart_clusterSet;
			Newstart_clusterSet.isSource =
					auxiliarygraph.startID_clusterSet_it->isSource;
			Newstart_clusterSet.clusterSet =
					auxiliarygraph.startID_clusterSet_it->clusterSet;

			//remove it first
			graph.mClusterBounaryList.erase(
					auxiliarygraph.startID_clusterSet_it);
			//insert the new

			if (Newstart_clusterSet.clusterSet.size() > 0) {

				insertNewCluster(graph, Newstart_clusterSet);
			}
		}
	}else{
		graph.mClusterBounaryList.erase(auxiliarygraph.startID_clusterSet_it);


	}

}




inline bool Right_decom_finished2D(GraphNode* pRightGraphNode,GraphNode &graph, vector<vector<HashGraphmap>> &phashgraphmap2DArray,Graph &G){
//
//	cout
//			<< "T"
//			<< endl;

//	if(graph.DebugSet.size()==0 and graph.node_degree[21-G.minBoundaryNodeID>0]){
//		cout<<"need debug"<<endl;
//	}


//	::DetectFailedNodeInClusterBounaryListBasic(graph);
//	cout << "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@level-----" << G.edgeindex + 1
//			<< endl;
//////
////////

	::expandBoundary(G);
//	cout<<"before locate....."<<endl;
//	::printClusterBounaryListBasic(G);
//	zeroNodeDegreeFromBSDetection(graph);
//	::DecodeClusterBlistFromString(graph, G.mClusterBounaryList);

//	::printClusterBounaryListBasic(graph);

	//::setTargetEdgeLoop(graph);$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
	auxiliaryGraph auxiliarygraph;
	bool iss=::setTargetEdge_general(G,auxiliarygraph);




	if(iss==false){
		pRightGraphNode->isfinished = true;
//		pRightGraphNode->relia = 0.0;
		return true;


	}

//    cout<<"after locate....."<<endl;
//    ::printClusterBounaryListBasic(G);
//
	double preRelia = G.targetEdge.relia;
//////
//	cout << "target egdge:=============>" << G.targetEdge.StartID
//				<< "---------->" << G.targetEdge.EndID << endl;
//
//
//	cout<<"starID: degree"<<graph.node_degree[G.targetEdge.StartID-G.minBoundaryNodeID]<<endl;
//	cout<<"endID: degree"<<graph.node_degree[G.targetEdge.EndID-G.minBoundaryNodeID]<<endl;
////
//
//	cout<<"G.mSClusterSize"<<G.mSClusterSize<<endl;

	if (G.KterminalSet.size() == 0 and G.mSClusterSize == 1) {
//		graph.NetRelia += preEdgeRelia * preRelia;

		G.NetRelia += preRelia* pRightGraphNode->PreVarRelia;



//		cout << "all visited" << endl;
//		cout << "relia:" << preRelia << " is returned" << endl;

		pRightGraphNode->isfinished = true;
    	pRightGraphNode->PreVarRelia*=preRelia;
		return true;

	}


	int startID = G.targetEdge.StartID;
	int endID = G.targetEdge.EndID;
	///////////////////

//		VarGeneralEdgeTrue(graph,startID,endID);
//		cout<<"before VarGeneralEdgeTrueV2"<<endl;
//			printClusterBounaryList(graph);
//		//	printGraphEdges(graph);
//			printSourceSet(graph);

//		VarGeneralEdgeTrueV3(graph, startID, endID);
	if (startID == endID) {
		;
	} else {
		if (auxiliarygraph.startID_clusterSet_it->isSource == true
				and auxiliarygraph.endID_clusterSet_it->isSource == true) {
			if (auxiliarygraph.startID_clusterSet_it
					!= auxiliarygraph.endID_clusterSet_it) {
//			cout << "two S combined" << endl;
				::VarGeneralEdgeTrue_TwoS19V6(G, startID, endID,
						auxiliarygraph);
			}
		} else if (auxiliarygraph.startID_clusterSet_it->isSource == false
				and auxiliarygraph.endID_clusterSet_it->isSource == false) {
			if (auxiliarygraph.startID_clusterSet_it
					!= auxiliarygraph.endID_clusterSet_it) {
//			cout << "No S combined" << endl;
				::VarGeneralEdgeTrue_ZeroS19V6(G, startID, endID,
						auxiliarygraph);

			}

		} else {
//		cout << "one S combined" << endl;
			::VarGeneralEdgeTrue_OneS19V6(G, startID, endID,
					auxiliarygraph);
		}
	}

//	cout << "=================after combined=============" << endl;
////	//	printClusterBounaryList(graph);
//	printClusterBounaryList_generalV2(graph);

//	cout << "graph.KterminalSet.size()=" << graph.KterminalSet.size() << endl;
//	cout << "G.mSClusterSize:" << G.mSClusterSize << endl;

	if (G.KterminalSet.size() == 0 and G.mSClusterSize == 1) {

		G.NetRelia += preRelia* pRightGraphNode->PreVarRelia;
//		graph.NetRelia += preEdgeRelia * preRelia;
//		cout << "all visited" << endl;
//		cout << "relia:" << preRelia << " is returned" << endl;

		pRightGraphNode->isfinished = true;
//		pRightGraphNode->relia = preRelia;
//		pRightGraphNode->prelia=1.0;
//		cout<<"----1.0-----returned"<<endl;
		pRightGraphNode->PreVarRelia*=preRelia;
		return true;

	}

//
//    cout<<"after merge....."<<endl;
//    ::printClusterBounaryListBasic(G);
//	int targetID=21-G.minBoundaryNodeID;
//
//	if(graph.node_degree[targetID]==1){
//		cout<<21<< "++++++++++++degree is"<<1<<endl;
//		::PrintDegreeInfo(graph);
//	}

//	::DetectFailedNodeInClusterBounaryListBasic(graph);


	//banch ana
	bool isRecursiveAna = true;

	while (isRecursiveAna == true) {
//		::removed_redundancy_loopEdge19V7(graph);
		::removed_redundancy_loopGeneralEdge(G);
		if (G.mSClusterSize == -2) {
			break;
		}
		//		 ::removed_redundancy_loopEdge_generalV4(graph, tID);
//		cout << "after removed removed_redundancy_loopEdge" << endl;
//		//		//	printClusterBounaryList(graph);
//		printClusterBounaryList_generalV2(graph);
		isRecursiveAna = false;
//		cout << "before isBranchEdge ana" << endl;
//		printClusterBounaryList_generalV2(graph);

		if(G.edgeindex==G.edgesize){
			break;
		}

		if (::isBranchEdge_general(G) == true) {
			if (G.mSClusterSize == -2) {
				break;
			}

			preRelia *= G.targetEdge.relia;

//			preEdgeRelia*=preRelia;

			isRecursiveAna = true;
			if (G.KterminalSet.size() == 0 and G.mSClusterSize == 1) {
				isRecursiveAna = false;
//				if (graph.KterminalSet.size() == 0
//						and G.mSClusterSize == 1) {
//					graph.NetRelia += preEdgeRelia * preRelia;
//
//					//		cout << "all visited" << endl;
//					//		cout << "relia:" << preRelia << " is returned" << endl;
//
//					return preRelia;
//
//				}



				break;

			} else {

				continue;
			}

		} else {
			isRecursiveAna = false;
			break;
		}

//		cout << "after isBranchEdge ana" << endl;
//		printClusterBounaryList_generalV2(graph);
	}

//	cout<<"-----------after recur rundant"<<endl;
////	G.edgeindex--;
////	cout<<"G.edgeindex"<<G.edgeindex<<endl;
//	printClusterBounaryList_generalV2(graph);


	if (G.mSClusterSize == -2) {

		//
		//		graph.clear();
		pRightGraphNode->isfinished = true;
		return true;

	}


	if (G.mSClusterSize >1 and G.edgeindex==G.edgesize) {

		//
		//		graph.clear();
		//		graph.clear();
		pRightGraphNode->isfinished = true;
//			cout << "---0----returned" << endl;
		return true;

	}



//    cout<<"after isRecursiveAna....."<<endl;
//    ::printClusterBounaryList_generalV2(graph);

	if (G.KterminalSet.size() == 0 and G.mSClusterSize == 1) {
//		graph.NetRelia += preEdgeRelia * preRelia;
//		cout << "all visited" << endl;
//		cout << "relia:" << preRelia << " is returned" << endl;
		G.NetRelia += preRelia* pRightGraphNode->PreVarRelia;

		pRightGraphNode->isfinished = true;
		pRightGraphNode->PreVarRelia*=preRelia;
//		cout<<"----1.0-----returned"<<endl;
		return true;

	}
//	cout<<"-----------after recur rundant"<<endl;
////	cout<<"before locate....."<<endl;
//	::printClusterBounaryListBasic(G);

	string keystring;
	int auxkey=0;
	int cols=20;
	::ClusterBListToCombineString2D(G,keystring,auxkey,cols);

	//		int key2=subgraph.nodesize;
//	int outerkey=G.edgeindex;
	int outerkey=G.edgeindex;


	pRightGraphNode->PreVarRelia*=preRelia;



//			bool ishit = ::findbySubGraph_general19_basic(phashMap, outerkey, primarykey,pRightGraphNode->pSubGrah);
	bool ishit=::find_pGraph_2D(phashgraphmap2DArray[outerkey][auxkey], pRightGraphNode,keystring);
//	cout<<"is hit:"<<ishit<<endl;
//	cout<<"after fined ..."<<endl;
//	printmap_basic19V10(phashMap,graph.edgesize);

//	subGraph.vboundarray.clear();
//    cout<<"after find....."<<endl;
//    ::printClusterBounaryList_generalV2(graph);

//		hashMap.findbyString(key1, key2, graph);
//		hashMap.find2(key1, key2, graph);

	if (ishit == true) {
		//release temp pointer

//		releaseGraph(pRightGraphNode);
		pRightGraphNode->isfinished=true;

		return true;

	}else{

		pRightGraphNode->isfinished = false;
//		cout<<"degree size:"<<pRightGraphNode->node_degree.size()<<endl;
		::UpdateGraphNode(G);

//		::set_DegreeVecV2(phashgraphmap2DArray[outerkey][auxkey], G, graph);
		::record_BSVector(phashgraphmap2DArray[outerkey][auxkey], G, graph);
//		cout<<"after update degree size:"<<pRightGraphNode->node_degree.size()<<endl;




		return false;




	}



}




inline bool Right_decom_finished2DV2(GraphNode* pRightGraphNode,GraphNode &gnode, vector<vector<HashGraphmap>> &phashgraphmap2DArray,Graph &graph){
//
//	cout
//			<< "************************#########*for true branch*************************************************************"
//			<< endl;
//	cout << "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@level-----" << graph.edgeindex + 1
//			<< endl;
//////
////////
    ::expandBoundary(graph);

	//::setTargetEdgeLoop(graph);$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
	auxiliaryGraph auxiliarygraph;
	bool iss=::setTargetEdge_general(graph, auxiliarygraph);
	if(iss==false){
		pRightGraphNode->isfinished = true;
//		pRightGraphNode->relia = 0.0;
		return true;


	}

////    cout<<"after locate....."<<endl;
//    ::printClusterBounaryList_generalV2(graph);
//
	double preRelia = graph.targetEdge.relia;
//////
//	cout << "target egdge:=============>" << graph.targetEdge.StartID
//			<< "---------->" << graph.targetEdge.EndID << endl;
//	cout<<"preRelia:"<<graph.targetEdge.relia<<endl;
//
//
//	cout<<"graph.SClusterSize"<<graph.SClusterSize<<endl;

	if (graph.KterminalSet.size() == 0 and graph.mSClusterSize == 1) {
//		graph.NetRelia += preEdgeRelia * preRelia;

		graph.NetRelia += preRelia* pRightGraphNode->PreVarRelia;



//		cout << "all visited" << endl;
//		cout << "relia:" << preRelia << " is returned" << endl;

		pRightGraphNode->isfinished = true;
//		pRightGraphNode->relia = preRelia;
//		pRightGraphNode->prelia=1.0;
		return true;

	}


	int startID = graph.targetEdge.StartID;
	int endID = graph.targetEdge.EndID;
	///////////////////

//		VarGeneralEdgeTrue(graph,startID,endID);
//		cout<<"before VarGeneralEdgeTrueV2"<<endl;
//			printClusterBounaryList(graph);
//		//	printGraphEdges(graph);
//			printSourceSet(graph);

//		VarGeneralEdgeTrueV3(graph, startID, endID);
	if (startID == endID) {
		;
	} else {
		if (auxiliarygraph.startID_clusterSet_it->isSource == true
				and auxiliarygraph.endID_clusterSet_it->isSource == true) {
			if (auxiliarygraph.startID_clusterSet_it
					!= auxiliarygraph.endID_clusterSet_it) {
//			cout << "two S combined" << endl;
				::VarGeneralEdgeTrue_TwoS19V6(graph, startID, endID,
						auxiliarygraph);
			}
		} else if (auxiliarygraph.startID_clusterSet_it->isSource == false
				and auxiliarygraph.endID_clusterSet_it->isSource == false) {
			if (auxiliarygraph.startID_clusterSet_it
					!= auxiliarygraph.endID_clusterSet_it) {
//			cout << "No S combined" << endl;
				::VarGeneralEdgeTrue_ZeroS19V6(graph, startID, endID,
						auxiliarygraph);

			}

		} else {
//		cout << "one S combined" << endl;
			::VarGeneralEdgeTrue_OneS19V6(graph, startID, endID,
					auxiliarygraph);
		}
	}

//	cout << "=================after combined=============" << endl;
//
//	if(::isNodeTrueFailed(graph, 21)==true){
//			cout<<"need debug--"<<endl;
//		}
//	//	printClusterBounaryList(graph);
//	printClusterBounaryList_generalV2(graph);

//	cout << "graph.KterminalSet.size()=" << graph.KterminalSet.size() << endl;
//	cout << "graph.SClusterSize:" << graph.SClusterSize << endl;

	if (graph.KterminalSet.size() == 0 and graph.mSClusterSize == 1) {

		graph.NetRelia += preRelia* pRightGraphNode->PreVarRelia;
//		graph.NetRelia += preEdgeRelia * preRelia;
//		cout << "all visited" << endl;
//		cout << "relia:" << preRelia << " is returned" << endl;

		pRightGraphNode->isfinished = true;
//		pRightGraphNode->relia = preRelia;
//		pRightGraphNode->prelia=1.0;
//		cout<<"----1.0-----returned"<<endl;
		return true;

	}

//
//    cout<<"after merge....."<<endl;
//    ::printClusterBounaryList_generalV2(graph);


	//banch ana
	bool isRecursiveAna = true;

	while (isRecursiveAna == true) {
//		::removed_redundancy_loopEdge19V7(graph);
		::removed_redundancy_loopGeneralEdge(graph);
		if (graph.mSClusterSize == -2) {
			break;
		}
		//		 ::removed_redundancy_loopEdge_generalV4(graph, tID);
//		cout << "after removed removed_redundancy_loopEdge" << endl;
//		//		//	printClusterBounaryList(graph);
//		printClusterBounaryList_generalV2(graph);
		isRecursiveAna = false;
//		cout << "before isBranchEdge ana" << endl;
//		printClusterBounaryList_generalV2(graph);

		if(graph.edgeindex==graph.edgesize){
			break;
		}

		if (::isBranchEdge_general(graph) == true) {
			if (graph.mSClusterSize == -2) {
				break;
			}

			preRelia *= graph.targetEdge.relia;

//			preEdgeRelia*=preRelia;

			isRecursiveAna = true;
			if (graph.KterminalSet.size() == 0 and graph.mSClusterSize == 1) {
				isRecursiveAna = false;
//				if (graph.KterminalSet.size() == 0
//						and graph.SClusterSize == 1) {
//					graph.NetRelia += preEdgeRelia * preRelia;
//
//					//		cout << "all visited" << endl;
//					//		cout << "relia:" << preRelia << " is returned" << endl;
//
//					return preRelia;
//
//				}



				break;

			} else {

				continue;
			}

		} else {
			isRecursiveAna = false;
			break;
		}

//		cout << "after isBranchEdge ana" << endl;
//		printClusterBounaryList_generalV2(graph);
	}


	if (graph.mSClusterSize == -2) {

		//
		//		graph.clear();
		pRightGraphNode->isfinished = true;

		return true;

	}


	if (graph.mSClusterSize >1 and graph.edgeindex==graph.edgesize) {

		//
		//		graph.clear();
		//		graph.clear();
		pRightGraphNode->isfinished = true;

		return true;

	}



//    cout<<"after isRecursiveAna....."<<endl;
//    ::printClusterBounaryList_generalV2(graph);

	if (graph.KterminalSet.size() == 0 and graph.mSClusterSize == 1) {
//		graph.NetRelia += preEdgeRelia * preRelia;
//		cout << "all visited" << endl;
//		cout << "relia:" << preRelia << " is returned" << endl;
		graph.NetRelia += preRelia* pRightGraphNode->PreVarRelia;

		pRightGraphNode->isfinished = true;
//		pRightGraphNode->relia = preRelia;
//		pRightGraphNode->prelia=1.0;
//		cout<<"----1.0-----returned"<<endl;
		return true;

	}

//	cout<<"-----------after recur rundant"<<endl;
//	::DetectFailedNodeInClusterBounaryListBasic(graph);

	string keystring;
		int auxkey=0;
		int cols=20;
		::ClusterBListToCombineString2D(graph,keystring,auxkey,cols);

		//		int key2=subgraph.nodesize;
	//	int outerkey=G.edgeindex;
		int outerkey=graph.edgeindex;


		pRightGraphNode->PreVarRelia*=preRelia;



	//			bool ishit = ::findbySubGraph_general19_basic(phashMap, outerkey, primarykey,pRightGraphNode->pSubGrah);
		bool ishit=::find_pGraph_2D(phashgraphmap2DArray[outerkey][auxkey], pRightGraphNode,keystring);
	//	cout<<"is hit:"<<ishit<<endl;
	//	cout<<"after fined ..."<<endl;
	//	printmap_basic19V10(phashMap,graph.edgesize);

	//	subGraph.vboundarray.clear();
	//    cout<<"after find....."<<endl;
	//    ::printClusterBounaryList_generalV2(graph);

	//		hashMap.findbyString(key1, key2, graph);
	//		hashMap.find2(key1, key2, graph);

		if (ishit == true) {
			//release temp pointer

	//		releaseGraph(pRightGraphNode);
			pRightGraphNode->isfinished=true;

			return true;

		}else{

			pRightGraphNode->isfinished = false;
	//		cout<<"degree size:"<<pRightGraphNode->node_degree.size()<<endl;
			::UpdateGraphNode(graph);

	//		::set_DegreeVecV2(phashgraphmap2DArray[outerkey][auxkey], G, graph);
			::record_BSVector(phashgraphmap2DArray[outerkey][auxkey], graph, gnode);
	//		cout<<"after update degree size:"<<pRightGraphNode->node_degree.size()<<endl;




			return false;




		}


}





void set_DegreeVec(HashGraphmap &phashmap,Graph &G,GraphNode &Gnode){
	int dsize=phashmap.LayerBSDegreeVec.size();
	int size=G.node_degree.size();
	bool isfind=false;
	int loc=0;
	for(int i=0;i<dsize;i++){
		if(phashmap.LayerBSDegreeVec[i].size()==size){

			for(int j=0;j<size;j++){
				if(G.node_degree[j]==phashmap.LayerBSDegreeVec[i][j]){
					isfind=true;

				}else{
					isfind=false;
					break;
				}

			}

			if(isfind==true){
				loc=i;
				break;
			}

		}
	}

	if(isfind==true){

		Gnode.Dindex=loc;
	}else{
		phashmap.LayerBSDegreeVec.push_back(G.node_degree);
		Gnode.Dindex=dsize;
	}






}



void set_DegreeVecV2(HashGraphmap &phashmap,Graph &G,GraphNode &Gnode){
	int dsize=phashmap.LayerBSDegreeVec.size();
	int size=G.node_degree.size();
	bool isfind=false;
	int loc=0;
	for(int i=0;i<dsize;i++){
		if(phashmap.LayerBSDegreeVec[i].size()==size){

			if(phashmap.LayerBSDegreeVec[i]==G.node_degree){
				isfind=true;
				loc=i;
				break;
			}else{
				continue;
			}

		}
	}

	if(isfind==true){

		Gnode.Dindex=loc;
	}else{
		phashmap.LayerBSDegreeVec.push_back(G.node_degree);
		Gnode.Dindex=dsize;
	}



}




void record_BSVector(HashGraphmap &phashmap,Graph &G,GraphNode &Gnode){
	int dsize=phashmap.LayerBSDegreeVec.size();
	int size=G.node_degree.size();
	bool isfind=false;
	int loc=0;
	for(int i=0;i<dsize;i++){
		if(phashmap.LayerBSDegreeVec[i].size()==size){

			if(phashmap.LayerBSDegreeVec[i]==G.node_degree){
				isfind=true;
				loc=i;
				break;
			}else{
				continue;
			}

		}
	}

	if(isfind==true){

		Gnode.Dindex=loc;
	}else{
		phashmap.LayerBSDegreeVec.push_back(G.node_degree);
		Gnode.Dindex=dsize;
	}

    vector<int> BS;
    BS.reserve(4);
    BS.push_back(G.edgeindex);
    BS.push_back(G.mSClusterSize);
    BS.push_back(G.minBoundaryNodeID);
    BS.push_back(G.clusterMinhead);


    int BSsize=phashmap.LayerBSVec.size();
    int bsloc=0;
    bool isbsfind=false;
    for(int j=0;j<BSsize;j++){
		if (phashmap.LayerBSVec[j] ==BS) {
			isbsfind = true;
			bsloc = j;
			break;
		} else {
			continue;
		}
    }

    if (isbsfind == true) {

		Gnode.BSindex = bsloc;
	} else {
		phashmap.LayerBSVec.push_back(BS);
		Gnode.BSindex = BSsize;
	}

}









void releaseGraph(GraphNode *pgraph){










}


void failed_nodeEdge_Ana(Graph &graph,int faildNode,auxiliaryGraph &auxiliarygraph){
	//ana neibors

   //######################################################
	set<int> neibors = NeiborsOfNodeV2(graph, faildNode);

	set<int>::iterator neiborit = neibors.begin();
	int targetneiborID = -1;
	for (; neiborit != neibors.end(); neiborit++) {
		if((*neiborit-graph.minBoundaryNodeID)<0){
			continue;
		}
		//degree of neiborID minus
		targetneiborID = *neiborit;
////		original as follows:
//		if (graph.node_degree[targetneiborID-graph.minBoundaryNodeID] > 0) {
//			graph.node_degree[targetneiborID-graph.minBoundaryNodeID]--;
//		}
//     // end original
		if (graph.node_degree[targetneiborID - graph.minBoundaryNodeID] >= 1) {
			graph.node_degree[targetneiborID - graph.minBoundaryNodeID]--;



		}

		if (graph.node_degree[targetneiborID - graph.minBoundaryNodeID] == -1) {
			graph.node_degree[targetneiborID - graph.minBoundaryNodeID] = 0;

//			if(::isNodeTrueFailed(graph, targetneiborID)==false){
//				cout<<"targetneiborID"<<targetneiborID<<":::"<<"is set to zero degree"<<endl;
//				::PrintNeiborsDegree(graph, targetneiborID);
//			}
		}

		if (graph.node_degree[targetneiborID - graph.minBoundaryNodeID] == -2) {
			graph.node_degree[targetneiborID - graph.minBoundaryNodeID] = 0;
		}










		//is in bsCluster
		bool islocateTargetID = isLocateclusterSet_itbyTargetID(graph,
				targetneiborID, auxiliarygraph);
		if (islocateTargetID == true) {

			if (auxiliarygraph.TargetID_clusterSet_it->isSource == false) {

				if (graph.node_degree[targetneiborID-graph.minBoundaryNodeID] == 0) {
					if (auxiliarygraph.TargetID_clusterSet_it->clusterSet.size()
							== 1) {

						graph.mClusterBounaryList.erase(
								auxiliarygraph.TargetID_clusterSet_it);

					} else {

						clusterNodeSet new_cluster_set;

						new_cluster_set.isSource = false;
						new_cluster_set.clusterSet =
								auxiliarygraph.TargetID_clusterSet_it->clusterSet;
						//the branch edge is
						//removed the original
						new_cluster_set.clusterSet.erase(targetneiborID);
						bool isinsertagain=true;

						///
						if (new_cluster_set.clusterSet.size() == 1) {

							set<int>::iterator nodeit =
									new_cluster_set.clusterSet.begin();

							if (graph.node_degree[*nodeit-graph.minBoundaryNodeID] == 1) {
								graph.node_degree[*nodeit-graph.minBoundaryNodeID] = -1;
								graph.mClusterBounaryList.erase(
										auxiliarygraph.TargetID_clusterSet_it);
								isinsertagain=false;

							}

						}
						///

						if (isinsertagain == true) {

							graph.mClusterBounaryList.erase(
									auxiliarygraph.TargetID_clusterSet_it);
							//insert the old
							//					insertNewClusterV3(graph, new_cluster_set);

							if (new_cluster_set.clusterSet.size() > 0) {

								insertNewCluster(graph, new_cluster_set);
							}
						}

					}

				}
				//rundant branch edge
				if (graph.node_degree[targetneiborID-graph.minBoundaryNodeID] == 1){

					if (auxiliarygraph.TargetID_clusterSet_it->clusterSet.size()== 1
							and auxiliarygraph.TargetID_clusterSet_it->isSource==false) {
						//rundant branch
						graph.mClusterBounaryList.erase(auxiliarygraph.TargetID_clusterSet_it);
						//set it -1 degree for rundant ends

						graph.node_degree[targetneiborID-graph.minBoundaryNodeID]=-1;
					}




				}

			} else {
				//source situation
				if (graph.node_degree[targetneiborID-graph.minBoundaryNodeID] == 0) {
					if (auxiliarygraph.TargetID_clusterSet_it->clusterSet.size()
							== 1) {

						//							cout << "endID:" << endID << " will be removed"
						//									<< endl;
						//true failed
						graph.mSClusterSize = -2;
						break;

					} else {

						clusterNodeSet new_cluster_set;

						new_cluster_set.isSource = true;
						new_cluster_set.clusterSet =
								auxiliarygraph.TargetID_clusterSet_it->clusterSet;
						//the branch edge is
						//removed the original
						new_cluster_set.clusterSet.erase(targetneiborID);
						graph.mClusterBounaryList.erase(
								auxiliarygraph.TargetID_clusterSet_it);
						//insert the old
						//					insertNewClusterV3(graph, new_cluster_set);
						if (new_cluster_set.clusterSet.size() > 0) {
							insertNewCluster(graph, new_cluster_set);
						}

					}

				}

			}
		}else{
			//no locate
			if (graph.node_degree[targetneiborID-graph.minBoundaryNodeID] == 0) {
				if (::isContains(graph.KterminalSet, targetneiborID)
						== true) {
					//true failed
					graph.mSClusterSize = -2;
					break;
				}
			}

			if (graph.node_degree[targetneiborID-graph.minBoundaryNodeID] == 1) {
				if (::isContains(graph.KterminalSet, targetneiborID)
						== false) {
					//true failed
					//set it -1 degree for rundant ends

					graph.node_degree[targetneiborID-graph.minBoundaryNodeID]=-1;
				}
			}






		}

	}
}



void PrintBoundaryList(Graph& graph,list<clusterNodeSet> &mClusterBounaryList){


//reset
int clusterhead = -1;
set<int> Sset;
list<clusterNodeSet>::iterator clusterit=mClusterBounaryList.begin();
for (;clusterit!= mClusterBounaryList.end(); clusterit++) {
//		cout<<"adress:"<<&*clusterit<<endl;

	if (clusterit->isSource == false) {

		clusterhead = 0;
//			subCluster++;

		cout << "[";
	} else {

		clusterhead = -1;
		cout << "[#";

	}

//		cout << "[";

	set<int>::iterator sit = clusterit->clusterSet.begin();
	for (; sit != clusterit->clusterSet.end(); sit++) {

		cout << "item:"<< *sit;
		if(::isContains(graph.VisitedSet, *sit)==true){
			cout<<"*";
		}
		cout<< "--";
		cout << "degree=" << graph.node_degree[*sit-graph.minBoundaryNodeID] << "||";

	}
	cout << "]----";

}
cout<<endl;

}




bool Left_decom_finished2D(GraphNode* pleftGraphNode,GraphNode &graph,vector<vector<HashGraphmap>> &phashgraphmap2DArray,Graph &G) {

	////
//	cout
//			<< "F"
//			<< endl;
//	cout << "@@@@@@@@@@@@@@@@@@@@@@@@@initial level-----" << G.edgeindex + 1
//			<< endl;
////    cout<<"before locate "<<endl;
////	cout << "SCluster size:=" << G.mSClusterSize << endl;

	//cout<<"detection---"<<endl;
	//degree_detection(graph);


//    ::DetectFailedNodeInClusterBounaryListBasic(graph);
	::expandBoundary(G);
//	zeroNodeDegreeFromBSDetection(graph);

//	::printClusterBounaryListBasic(graph);
//	list<clusterNodeSet> mClusterBounaryList;
//	::DecodeClusterBlistFromString(graph, G.mClusterBounaryList);
//	::printClusterBounaryListBasic(G);







	//::setTargetEdgeLoop(graph);

	//define isterator for startID and endID****************************
	 auxiliaryGraph auxiliarygraph;
//	 cout<<"adress:----iniital"<<&auxiliarygraph<<endl;
	 bool iss=::setTargetEdge_general(G,auxiliarygraph);

//	cout << "target egdge:=============>" << G.targetEdge.StartID
//				<< "---------->" << G.targetEdge.EndID << endl;
//
//
//	cout<<"starID: degree"<<graph.node_degree[G.targetEdge.StartID-G.minBoundaryNodeID]<<endl;
//	cout<<"endID: degree"<<graph.node_degree[G.targetEdge.EndID-G.minBoundaryNodeID]<<endl;



	if(iss==false){
		pleftGraphNode->isfinished = true;
		return true;


	}

	//settargetEdgeLoop as follows-----------------------------------------------------------------:

	//end set targetEdge loop====================================

//    cout<<"after locate "<<endl;
//	::printClusterBounaryListBasic(G);


	int startID = G.targetEdge.StartID;
	int endID = G.targetEdge.EndID;

//	bool maybranchana = false;

	double preRelia = 1 - G.targetEdge.relia;

	////

//	cout << "NetUnRelia unrelia:" << graph.NetUnRelia << endl;
//
//	cout<<"PreVarRelia:"<<pleftGraphNode->PreVarRelia<<endl;
	//
	//	cout<<"is_startID from S:"<<graph.startID_clusterSet_it->isSource<<endl;
	//	cout<<"is_endID from S:"<<graph.endID_clusterSet_it->isSource<<endl;

	//failed situation

	/////




//   cout<<"adress:----in left"<<&auxiliarygraph<<endl;
//
//	cout<<auxiliarygraph.startID_clusterSet_it->clusterSet.size()<<endl;

	//-----------------(1)varnode_False#############################
	if(startID==endID){
		//var_node failed, set degree of it to zero
		G.node_degree[startID-G.minBoundaryNodeID]=0;
		if (auxiliarygraph.startID_clusterSet_it->isSource == true
				and auxiliarygraph.startID_clusterSet_it->clusterSet.size()
						== 1) {

			pleftGraphNode->isfinished = true;
//			cout<<"---0----returned"<<endl;
			return true;

		}
		removeZerodegreeOfStartIDFromEdge19V6(G,startID,auxiliarygraph);
//		cout<<"after removeZerodegreeOfStartIDFromEdge19V6"<<endl;
//		::DetectFailedNodeInClusterBounaryListBasic(graph);
		//ana neibors
//		cout<<"before failed_nodeEdge_Ana"<<endl;
//		::DetectFailedNodeInClusterBounaryListBasic(graph);
		::failed_nodeEdge_Ana(G, startID, auxiliarygraph);
//         cout<<"after failed_nodeEdge_Ana"<<endl;
//		::PrintNeiborsDegree(graph, startID);

	}

	if (G.mSClusterSize == -2) {

		//
		//		graph.clear();
		pleftGraphNode->isfinished = true;


		return true;

	}






	//-----------------(2)varedge_False#############################

	if (startID != endID) {

		if (G.node_degree[startID-G.minBoundaryNodeID] == 0) {

			if (auxiliarygraph.startID_clusterSet_it->isSource == true
					and auxiliarygraph.startID_clusterSet_it->clusterSet.size()
							== 1) {

				pleftGraphNode->isfinished = true;

				return true;

			}

			//		cout << "removeZerodegreeOfStartIDFromEdge:" << startID << endl;
			removeZerodegreeOfStartIDFromEdge19V6(G, startID,
					auxiliarygraph);

		}

		if (G.node_degree[endID-G.minBoundaryNodeID] == 0) {

			if (auxiliarygraph.endID_clusterSet_it->isSource == true
					and auxiliarygraph.endID_clusterSet_it->clusterSet.size()
							== 1) {

				pleftGraphNode->isfinished = true;

				return true;

			}
			//
			//		cout << "removeZerodegreeOfEndIDFromEdge:" << endID << endl;
			removeZerodegreeOfEndIDFromEdge19V6(G, endID, auxiliarygraph);
		}

		///redunant edge ##############################################
		//***********************************************************
		if (G.node_degree[startID-G.minBoundaryNodeID] == 1) {

			if (auxiliarygraph.startID_clusterSet_it->isSource == false
					and auxiliarygraph.startID_clusterSet_it->clusterSet.size()
							== 1) {

				//				isstartID_clusterfind=true;
				//remove the iter
				G.mClusterBounaryList.erase(
						auxiliarygraph.startID_clusterSet_it);
				G.node_degree[startID-G.minBoundaryNodeID]=-2;

			}

		}

		if (G.node_degree[endID-G.minBoundaryNodeID] == 1) {

			if (auxiliarygraph.endID_clusterSet_it->isSource == false
					and auxiliarygraph.endID_clusterSet_it->clusterSet.size()
							== 1) {

				//				isstartID_clusterfind=true;
				//remove the iter
				G.mClusterBounaryList.erase(
						auxiliarygraph.endID_clusterSet_it);
				G.node_degree[endID-G.minBoundaryNodeID] = -2;

			}

		}
		/////////////////////////////////////



	}
//	cout<<"________________after removed var....."<<endl;
//	::printClusterBounaryListBasic(G);
////	int targetID=21-G.minBoundaryNodeID;
//
//	if(graph.node_degree[targetID]==1){
//		cout<<21<< "++++++++++++degree is"<<1<<endl;
//		::PrintDegreeInfo(graph);
//	}



//	::DetectFailedNodeInClusterBounaryListBasic(graph);
//	::printClusterBounaryList_generalV2(graph);

//	cout<<"G.mSClusterSize:"<<G.mSClusterSize<<endl;

	if (G.mSClusterSize == 0) {

		//
		//		graph.clear();
		pleftGraphNode->isfinished = true;

		return true;

	} else {

//		cout<<"next decom..."<<endl;
		//next decom
			//banch ana
		bool isRecursiveAna = true;

//		cout<<"recur----"<<endl;

		while (isRecursiveAna == true) {
			::removed_redundancy_loopGeneralEdge(G);
			if(G.mSClusterSize==-2){
				break;
			}
			//		 ::removed_redundancy_loopEdge_generalV4(graph, tID);
//						cout << "after removed removed_redundancy_loopEdge" << endl;
			//		//	printClusterBounaryList(graph);
			//			printClusterBounaryList_generalV2(graph);
			isRecursiveAna = false;
			//			cout << "before isBranchEdge ana" << endl;
			//			printClusterBounaryList_generalV2(graph);
//			cout<<"branch  finding.."<<endl;
			if(G.edgeindex<G.edgesize){
			if (::isBranchEdge_general(G) == true) {

//				cout << "branch is finded:" << endl;

//				printClusterBounaryList_generalV2(graph);

				if (G.mSClusterSize == -2) {
					break;
				}


				preRelia *= G.targetEdge.relia;

				//				preEdgeRelia*=preRelia;

				isRecursiveAna = true;
				if (G.KterminalSet.size() == 0
						and G.mSClusterSize == 1) {
					isRecursiveAna = false;


					break;

				} else {

					continue;
				}

			} else {



				isRecursiveAna = false;
				break;
			}
			}

		}



		if (G.mSClusterSize == -2) {

			//
			//		graph.clear();
			//		graph.clear();
			pleftGraphNode->isfinished = true;
			return true;

		}

		if (G.mSClusterSize >1 and G.edgeindex==G.edgesize) {


			pleftGraphNode->isfinished = true;

			return true;

		}





		if (G.KterminalSet.size() == 0 and G.mSClusterSize == 1) {

			pleftGraphNode->isfinished = true;

			G.NetRelia += preRelia* pleftGraphNode->PreVarRelia;
//			cout<<"return"<<preRelia<<endl;
//			cout<<"complete-----"<<endl;
			return true;



		}


//		cout<<"-----------after recur rundant"<<endl;
//		printBSVectorInfor(G);
//		int targetID=21-G.minBoundaryNodeID;
//
//		if(graph.node_degree[targetID]==1){
//			cout<<21<< "++++++++++++degree is"<<1<<endl;
//			::PrintDegreeInfo(graph);
//		}

//		::DetectFailedNodeInClusterBounaryListBasic(graph);

//		cout<<"before hit"<<endl;

		////

//		cout<<" to CombineBounaryVector..."<<endl;



//		::ClusterBListToCombineBounaryVectorV1(graph);
		string keystring;
		int auxkey=10;
		int cols=20;
		::ClusterBListToCombineString2D(G,keystring,auxkey,cols);


		int outerkey=G.edgeindex;


		 pleftGraphNode->PreVarRelia*=preRelia;

		bool ishit=::find_pGraph_2D(phashgraphmap2DArray[outerkey][auxkey], pleftGraphNode,keystring);


		if (ishit == true) {

//			cout << "===============hit for f============" << endl;


			pleftGraphNode->isfinished = true;

			return true;

//
//			return preRelia;
		}else{
			pleftGraphNode->isfinished = false;

			::UpdateGraphNode(G);
			::record_BSVector(phashgraphmap2DArray[outerkey][auxkey], G, graph);

		}

//		cout<<"after hit rutine"<<endl;



		return false;

	}
}




bool Left_decom_finished2DV2(GraphNode* pleftGraphNode,GraphNode &gnode,vector<vector<HashGraphmap>> &phashgraphmap2DArray,Graph &graph) {

	////
//	cout
//			<< "**********************###############False branch*****************************************************************************"
//			<< endl;
//	cout << "@@@@@@@@@@@@@@@@@@@@@@@@@initial level-----" << graph.edgeindex + 1
//			<< endl;
////    cout<<"before locate "<<endl;
////	cout << "SCluster size:=" << graph.SClusterSize << endl;
//	printClusterBounaryList_generalV2(graph);

	//cout<<"detection---"<<endl;
	//degree_detection(graph);
//	zeroNodeDegreeFromBSDetection(graph);

	//::setTargetEdgeLoop(graph);
	::expandBoundary(graph);

	//define isterator for startID and endID****************************
	 auxiliaryGraph auxiliarygraph;
//	 cout<<"adress:----iniital"<<&auxiliarygraph<<endl;
	bool iss=setTargetEdge_general(graph, auxiliarygraph);
	if(iss==false){
		pleftGraphNode->isfinished = true;
//		pleftGraphNode->PreVarRelia = 0.0;


		return true;


	}

	//settargetEdgeLoop as follows-----------------------------------------------------------------:

	//end set targetEdge loop====================================

//    cout<<"after locate "<<endl;
////	cout << "SCluster size:=" << graph.SClusterSize << endl;
//	printClusterBounaryList_generalV2(graph);


	int startID = graph.targetEdge.StartID;
	int endID = graph.targetEdge.EndID;

//	bool maybranchana = false;

	double preRelia = 1 - graph.targetEdge.relia;

	////
//	cout << "target egdge:=============>" << graph.targetEdge.StartID
//				<< "---------->" << graph.targetEdge.EndID << endl;
//	cout << "NetUnRelia unrelia:" << graph.NetUnRelia << endl;
//
//	cout<<"PreVarRelia:"<<pleftGraphNode->PreVarRelia<<endl;
	//
	//	cout<<"is_startID from S:"<<graph.startID_clusterSet_it->isSource<<endl;
	//	cout<<"is_endID from S:"<<graph.endID_clusterSet_it->isSource<<endl;

	//failed situation

	/////




//   cout<<"adress:----in left"<<&auxiliarygraph<<endl;
//
//	cout<<auxiliarygraph.startID_clusterSet_it->clusterSet.size()<<endl;

	//-----------------(1)varnode_False#############################
	if(startID==endID){
		//var_node failed, set degree of it to zero
		graph.node_degree[startID-graph.minBoundaryNodeID]=0;
		if (auxiliarygraph.startID_clusterSet_it->isSource == true
				and auxiliarygraph.startID_clusterSet_it->clusterSet.size()
						== 1) {

			//////////////////
			pleftGraphNode->isfinished = true;
//			pleftGraphNode->relia = 0.0;
//			cout<<"---0----returned"<<endl;
			return true;

		}
		removeZerodegreeOfStartIDFromEdge19V6(graph,startID,auxiliarygraph);
		//ana neibors
		::failed_nodeEdge_Ana(graph, startID, auxiliarygraph);
	}

	if (graph.mSClusterSize == -2) {

		//
		//		graph.clear();
		pleftGraphNode->isfinished = true;

		return true;

	}






	//-----------------(2)varedge_False#############################

	if (startID != endID) {

		if (graph.node_degree[startID-graph.minBoundaryNodeID] == 0) {

			if (auxiliarygraph.startID_clusterSet_it->isSource == true
					and auxiliarygraph.startID_clusterSet_it->clusterSet.size()
							== 1) {

				pleftGraphNode->isfinished = true;

				return true;

			}

			//		cout << "removeZerodegreeOfStartIDFromEdge:" << startID << endl;
			removeZerodegreeOfStartIDFromEdge19V6(graph, startID,
					auxiliarygraph);

		}

		if (graph.node_degree[endID-graph.minBoundaryNodeID] == 0) {

			if (auxiliarygraph.endID_clusterSet_it->isSource == true
					and auxiliarygraph.endID_clusterSet_it->clusterSet.size()
							== 1) {

				pleftGraphNode->isfinished = true;

				return true;

			}
			//
			//		cout << "removeZerodegreeOfEndIDFromEdge:" << endID << endl;
			removeZerodegreeOfEndIDFromEdge19V6(graph, endID, auxiliarygraph);
		}

		///redunant edge ##############################################
		//***********************************************************
		if (graph.node_degree[startID-graph.minBoundaryNodeID] == 1) {

			if (auxiliarygraph.startID_clusterSet_it->isSource == false
					and auxiliarygraph.startID_clusterSet_it->clusterSet.size()
							== 1) {

				//				isstartID_clusterfind=true;
				//remove the iter
				graph.mClusterBounaryList.erase(
						auxiliarygraph.startID_clusterSet_it);
				graph.node_degree[startID-graph.minBoundaryNodeID]=-2;

			}

		}

		if (graph.node_degree[endID-graph.minBoundaryNodeID] == 1) {

			if (auxiliarygraph.endID_clusterSet_it->isSource == false
					and auxiliarygraph.endID_clusterSet_it->clusterSet.size()
							== 1) {

				//				isstartID_clusterfind=true;
				//remove the iter
				graph.mClusterBounaryList.erase(
						auxiliarygraph.endID_clusterSet_it);
				graph.node_degree[endID-graph.minBoundaryNodeID] = -2;

			}

		}
		/////////////////////////////////////



	}



//	cout<<"________________after removed var....."<<endl;
//	if(::isNodeTrueFailed(graph, 21)==true){
//			cout<<"need debug--"<<endl;
//		}
//
//	::DetectFailedNodeInClusterBounaryListBasic(graph);
//	::printClusterBounaryList_generalV2(graph);

//	cout<<"graph.SClusterSize:"<<graph.SClusterSize<<endl;

	if (graph.mSClusterSize == 0) {

		//
		//		graph.clear();
		pleftGraphNode->isfinished = true;

		return true;

	} else {

//		cout<<"next decom..."<<endl;
		//next decom
			//banch ana
		bool isRecursiveAna = true;

//		cout<<"recur----"<<endl;

		while (isRecursiveAna == true) {
			::removed_redundancy_loopGeneralEdge(graph);
			if(graph.mSClusterSize==-2){
				break;
			}

			isRecursiveAna = false;

			if(graph.edgeindex<graph.edgesize){
			if (::isBranchEdge_general(graph) == true) {



				if (graph.mSClusterSize == -2) {
					break;
				}


				preRelia *= graph.targetEdge.relia;

						isRecursiveAna = true;
				if (graph.KterminalSet.size() == 0
						and graph.mSClusterSize == 1) {
					isRecursiveAna = false;


					break;

				} else {

					continue;
				}

			} else {



				isRecursiveAna = false;
				break;
			}
			}

		}


		if (graph.mSClusterSize == -2) {

			pleftGraphNode->isfinished = true;

			return true;

		}

		if (graph.mSClusterSize >1 and graph.edgeindex==graph.edgesize) {

			pleftGraphNode->isfinished = true;


			return true;

		}





		if (graph.KterminalSet.size() == 0 and graph.mSClusterSize == 1) {

			pleftGraphNode->isfinished = true;

			graph.NetRelia += preRelia* pleftGraphNode->PreVarRelia;

			return true;



		}

		string keystring;
				int auxkey=10;
				int cols=20;
				::ClusterBListToCombineString2D(graph,keystring,auxkey,cols);


				int outerkey=graph.edgeindex;

				 pleftGraphNode->PreVarRelia*=preRelia;

				bool ishit=::find_pGraph_2D(phashgraphmap2DArray[outerkey][auxkey], pleftGraphNode,keystring);

				if (ishit == true) {

		//			cout << "===============hit for f============" << endl;


					pleftGraphNode->isfinished = true;

					return true;

		//
		//			return preRelia;
				}else{
					pleftGraphNode->isfinished = false;

					::UpdateGraphNode(graph);

					::record_BSVector(phashgraphmap2DArray[outerkey][auxkey], graph, gnode);

				}




		return false;

	}
}



//
//
//inline void setTargetEdgeLoop19V6(Graph& graph,auxiliaryGraph &auxiliarygraph){
//	bool isfinished = false;
//	bool isserial=false;
//		while (isfinished == false) {
//			isserial=false;
//			Edge targetEdge = graph.vectorEdge[G.edgeindex];
//			int startID = targetEdge.StartID;
//			int endID = targetEdge.EndID;
//			G.targetEdge.StartID = startID;
//			G.targetEdge.EndID = endID;
//			G.targetEdge.relia = targetEdge.relia;
////
////			    cout<<"Intial edge"<<" startID:"<<startID<<" --->"<<"endID:"<<endID<<endl;
//			//	//
//			//	//locate===================================
//
//
//			::locateClusterNodeSetByTargetEdge19V6(graph, auxiliarygraph);
////			cout<<auxiliarygraph.startID_clusterSet_it->clusterSet.size()<<endl;
////			cout<<"after locate "<<endl;
//	           //after locate ===============================
//
//			int tempStartID=graph.vectorEdge[G.edgeindex+1].StartID;
//			if(startID==tempStartID){
//				isserial=true;
//			}
//
//
//			if (isserial==true
//					and auxiliarygraph.startID_clusterSet_it->clusterSet.size() == 1
//					and auxiliarygraph.startID_clusterSet_it->isSource == false
//					and graph.node_degree[startID] == 2) {
//				//remains Series
////						cout<<"merge operation..."<<endl;
//				//get neibors
//				setNeiborBasedSeries(graph, startID);
//				//remove the startID cluster
//				//		cout<<"removed midit="<<&*graph.startID_clusterSet_it<<endl;
//				//		G.mClusterBounaryList.erase(graph.startID_clusterSet_it);
////				cout<<"removed startID_clusterSet_it..."<<endl;
//				G.mClusterBounaryList.erase(auxiliarygraph.startID_clusterSet_it);
////						cout<<"after removed..."<<endl;
//
//
//				//locate again
//				::locateClusterNodeSetByTargetEdge19V6(graph, auxiliarygraph);
//				//
////				cout<<"locate again"<<endl;
//
//
//				if(auxiliarygraph.startID_clusterSet_it==auxiliarygraph.endID_clusterSet_it){
////					cout<<"s and e it eaqual"<<endl;
//					isfinished=false;
//					graph.node_degree[G.targetEdge.StartID]--;
//					graph.node_degree[G.targetEdge.EndID]--;
//	//				cout<<"findidng rundancy edge in setedge state"<<endl;
//	//				::clean_redundancy_loopEdgeV1(graph);
////					::clean_redundancy_loopEdge19V6(graph);
//					removed_redundancy_loopEdge19V7(graph);
//					continue;
//
//				}else{
//
////					cout<<"s and e it no eaqual"<<endl;
//
//	//				cout<<"graph.startID_clusterSet_it->isSource:"<<graph.startID_clusterSet_it->isSource<<endl;
//	//				cout<<"graph.startID_clusterSet_it->clusterSet.size():"<<graph.startID_clusterSet_it->clusterSet.size()<<endl;
//	//				cout<<graph.node_degree[G.targetEdge.StartID]<<endl;
//					if (auxiliarygraph.startID_clusterSet_it->isSource == false
//							and auxiliarygraph.startID_clusterSet_it->clusterSet.size()== 1) {
//						if (graph.node_degree[G.targetEdge.StartID] == 1) {
//	//						cout<<"clean branch:..."<<G.targetEdge.StartID<<"--->"<<G.targetEdge.EndID<<endl;
//							graph.node_degree[G.targetEdge.StartID]--;
//							graph.node_degree[G.targetEdge.EndID]--;
//							isfinished=false;
//							::clean_redundancy_loopEdge19V6(graph);
//							continue;
//						}
//					}
//					isfinished=true;
//					graph.node_degree[G.targetEdge.StartID]--;
//					graph.node_degree[G.targetEdge.EndID]--;
//					break;
//				}
//
//
//			} else {
//				isfinished=true;
//
//				graph.node_degree[startID]--;
//				graph.node_degree[endID]--;
//				G.edgeindex++;
//	//			cout<<"finished"<<endl;
//
//			}
//
//		}
////		cout<<"adress:"<<&auxiliarygraph<<endl;
////		cout<<auxiliarygraph.startID_clusterSet_it->clusterSet.size()<<endl;
////
////	    cout<<"after locate loop"<<endl;
//}
//
//





//
//
//inline bool setTargetEdgeLoop19V8(Graph& graph,auxiliaryGraph &auxiliarygraph){
//	bool isfinished = false;
//	bool isserial=false;
//		while (isfinished == false) {
//			isserial=false;
//			Edge targetEdge = graph.vectorEdge[G.edgeindex];
//			int startID = targetEdge.StartID;
//			int endID = targetEdge.EndID;
//			G.targetEdge.StartID = startID;
//			G.targetEdge.EndID = endID;
//			G.targetEdge.relia = targetEdge.relia;
////
////			    cout<<"Intial edge"<<" startID:"<<startID<<" --->"<<"endID:"<<endID<<endl;
//			//	//
//			//	//locate===================================
//
//
//			::locateClusterNodeSetByTargetEdge19V6(graph, auxiliarygraph);
////			cout<<auxiliarygraph.startID_clusterSet_it->clusterSet.size()<<endl;
////			cout<<"after locate "<<endl;
//	           //after locate ===============================
//
//			int tempStartID=graph.vectorEdge[G.edgeindex+1].StartID;
//			if(startID==tempStartID){
//				isserial=true;
//			}
//
//
//			if (isserial==true
//					and auxiliarygraph.startID_clusterSet_it->clusterSet.size() == 1
//					and auxiliarygraph.startID_clusterSet_it->isSource == false
//					and graph.node_degree[startID] == 2) {
//				//remains Series
////						cout<<"merge operation..."<<endl;
//				//get neibors
//				setNeiborBasedSeries(graph, startID);
//				//remove the startID cluster
//				//		cout<<"removed midit="<<&*graph.startID_clusterSet_it<<endl;
//				//		G.mClusterBounaryList.erase(graph.startID_clusterSet_it);
////				cout<<"removed startID_clusterSet_it..."<<endl;
//				G.mClusterBounaryList.erase(auxiliarygraph.startID_clusterSet_it);
////						cout<<"after removed..."<<endl;
//
//
//				//locate again
//				::locateClusterNodeSetByTargetEdge19V6(graph, auxiliarygraph);
//				//
////				cout<<"locate again"<<endl;
//
//
//				if (auxiliarygraph.startID_clusterSet_it== auxiliarygraph.endID_clusterSet_it) {
////					cout<<"s and e it eaqual"<<endl;
//				isfinished = false;
//				bool isreinsert = false;
//				graph.node_degree[G.targetEdge.StartID]--;
//				graph.node_degree[G.targetEdge.EndID]--;
//
//				if (graph.node_degree[G.targetEdge.StartID] == 0) {
//					//					isstarIDRemove=true;
//
//					auxiliarygraph.startID_clusterSet_it->clusterSet.erase(
//							G.targetEdge.StartID);
//					isreinsert = true;
//
//				}
//				if (graph.node_degree[G.targetEdge.EndID] == 0) {
//
//					auxiliarygraph.endID_clusterSet_it->clusterSet.erase(
//							G.targetEdge.EndID);
//					isreinsert = true;
//					//					isendIDRemove=true;
//
//				}
//
//				//
//				//               cout<<"before-ordered----"<<endl;
//				//               printClusterBounaryList(graph);
//				//update order
//
//				if (isreinsert == true) {
//
//					clusterNodeSet new_cluster_set;
//
//					new_cluster_set.isSource =
//							auxiliarygraph.endID_clusterSet_it->isSource;
//					new_cluster_set.clusterSet =
//							auxiliarygraph.endID_clusterSet_it->clusterSet;
//					//the branch edge is
//					//removed the original
//					G.mClusterBounaryList.erase(
//							auxiliarygraph.endID_clusterSet_it);
//					//insert the old
//					//					insertNewClusterV3(graph, new_cluster_set);
//					if (new_cluster_set.clusterSet.size() > 0) {
//						insertNewCluster(graph, new_cluster_set,G);
//					} else {
//						if (new_cluster_set.isSource == true) {
//							G.mSClusterSize = -2;
//							isfinished=false;
//							break;
//						}
//					}
//
//				}
//
//
//				removed_redundancy_loopEdge19V7(graph);
//				continue;
//				if (G.mSClusterSize == -2) {
//					isfinished = false;
//					break;
//				}
//
//
//
//	//				cout<<"findidng rundancy edge in setedge state"<<endl;
//	//				::clean_redundancy_loopEdgeV1(graph);
////					::clean_redundancy_loopEdge19V6(graph);
//
//
//				}else{
//
//					bool iscontinue=removedBranch_finished(graph,auxiliarygraph);
//
//					if(G.mSClusterSize==-2){
//						isfinished=false;
//						break;
//					}
//
//					if(iscontinue==true){
//						removed_redundancy_loopEdge19V7(graph);
//					if (G.mSClusterSize == -2) {
//						isfinished = false;
//						break;
//					}
//						continue;
//
//
//					}else{
//					isfinished = true;
//					graph.node_degree[G.targetEdge.StartID]--;
//					graph.node_degree[G.targetEdge.EndID]--;
//					break;
//
//					}
//
//
//				}
//
//
//			} else {
//				isfinished=true;
//
//				graph.node_degree[startID]--;
//				graph.node_degree[endID]--;
//				G.edgeindex++;
//	//			cout<<"finished"<<endl;
//
//			}
//
//		}
//
//		return isfinished;
////		cout<<"adress:"<<&auxiliarygraph<<endl;
////		cout<<auxiliarygraph.startID_clusterSet_it->clusterSet.size()<<endl;
////
////	    cout<<"after locate loop"<<endl;
//}
//
//



inline bool setTargetEdge_general(Graph &graph,
		auxiliaryGraph &auxiliarygraph) {
	bool isfinished = true;
//	graph.edgeindex++;
	Edge targetEdge = graph.vectorEdge[graph.edgeindex];
	int startID = targetEdge.StartID;
	int endID = targetEdge.EndID;
	graph.targetEdge.StartID = startID;
	graph.targetEdge.EndID = endID;
	graph.targetEdge.relia = targetEdge.relia;
//
//			    cout<<"Intial edge"<<" startID:"<<startID<<" --->"<<"endID:"<<endID<<endl;
	//	//
	//	//locate===================================

	::locateClusterNodeSetByGeneralEdge(graph, auxiliarygraph);
//			cout<<auxiliarygraph.startID_clusterSet_it->clusterSet.size()<<endl;
//			cout<<"after locate "<<endl;
	//after locate ===============================

	if (startID != endID) {

		graph.node_degree[startID-graph.minBoundaryNodeID]--;
		graph.node_degree[endID-graph.minBoundaryNodeID]--;
	}
	graph.edgeindex++;
	//			cout<<"finished"<<endl;

	return isfinished;
//		cout<<"adress:"<<&auxiliarygraph<<endl;
//		cout<<auxiliarygraph.startID_clusterSet_it->clusterSet.size()<<endl;
//
//	    cout<<"after locate loop"<<endl;
}




void printGraphEdges(Graph &graph) {

//	cout<<"graph.adj_relia size:"<<graph.adj_relia.size()<<endl;
	map<int, map<int, double> >::iterator edgeit = graph.adj_relia.begin();
	for (; edgeit != graph.adj_relia.end(); edgeit++) {
		cout << "arc from:  " << edgeit->first << endl;
		map<int, double>::iterator neiborit = edgeit->second.begin();

		for (; neiborit != edgeit->second.end(); neiborit++) {

			cout << "arc to:  " << neiborit->first << ":relia:"
					<< neiborit->second << endl;
		}

	}

//	cout << "edges as follows:" << endl;
//	list<Edge>::iterator edgeit2 = graph.EdgeList.begin();
//	for (; edgeit2 != graph.EdgeList.end(); edgeit2++) {
//		cout << "edge:" << edgeit2->StartID << "--->" << edgeit2->EndID << endl;
//
//	}
}


void printGraphEdgesV2(Graph &graph) {


//	cout<<"graph.adj_relia size:"<<graph.adj_relia.size()<<endl;
	map<int, map<int, double> >::iterator edgeit = graph.adj_reliaV2.begin();
	for (; edgeit != graph.adj_reliaV2.end(); edgeit++) {
		cout << "arc from:  " << edgeit->first << endl;
		map<int, double>::iterator neiborit = edgeit->second.begin();

		for (; neiborit != edgeit->second.end(); neiborit++) {

			cout << "arc to:  " << neiborit->first << ":relia:"
					<< neiborit->second << endl;
		}

	}

//	cout << "edges as follows:" << endl;
//	list<Edge>::iterator edgeit2 = graph.EdgeList.begin();
//	for (; edgeit2 != graph.EdgeList.end(); edgeit2++) {
//		cout << "edge:" << edgeit2->StartID << "--->" << edgeit2->EndID << endl;
//
//	}
}



void printClusterBounaryList(Graph &graph) {
	cout << "---------------------------ClusterBounary as follows" << endl;

	//source-----    1
	//no source---- -1;
	list<clusterNodeSet>::iterator clusterit = graph.mClusterBounaryList.begin();
	//reset
	int clusterhead = -1;
	for (; clusterit != graph.mClusterBounaryList.end(); clusterit++) {

		if (clusterit->isSource == false) {

			clusterhead = 0;
			cout << "[";
//			subCluster++;
		} else {

			clusterhead = -1;
			cout << "[#";

		}

		set<int>::iterator sit = clusterit->clusterSet.begin();
		//cout << "item:";
		for (; sit != clusterit->clusterSet.end(); sit++) {

			cout << *sit;
			//cout << "degree=" << graph.node_degree[*sit] << "||";
			if (::isContains(graph.VisitedSet, *sit)) {
				cout << "*";
			}
			cout << "--";

		}
		cout << "]" << endl;

	}

//
//	cout<<"after combinged........................."<<endl;
//	printClusterBounaryList(graph);

}

void DiamondNet(Graph &graph, double edgefuncsp) {

	graph.nVerts = 6;	//0---5
	//	this->vertexList = new int[nVerts];
	//	for (int i = 1; i < this->nVerts; ++i) {
	//
	//		this->vertexList[i] = i;
	//	}

	graph.node_degree = vector<short int>(graph.nVerts);
	addEdge(graph,1, 2, edgefuncsp);
	addEdge(graph,2, 1, edgefuncsp);

	addEdge(graph,1, 3, edgefuncsp);
	addEdge(graph,3, 1, edgefuncsp);

	addEdge(graph,1, 4, edgefuncsp);
	addEdge(graph,4, 1, edgefuncsp);

	addEdge(graph,2, 3, edgefuncsp);
	addEdge(graph,3, 2, edgefuncsp);

	addEdge(graph,2, 5, edgefuncsp);
	addEdge(graph,5, 2, edgefuncsp);

	addEdge(graph,3, 4, edgefuncsp);
	addEdge(graph,4, 3, edgefuncsp);

	addEdge(graph,3, 5, edgefuncsp);
	addEdge(graph,5, 3, edgefuncsp);

	addEdge(graph,4, 5, edgefuncsp);
	addEdge(graph,5, 4, edgefuncsp);

	map<int, map<int, double> >::iterator edgeit = graph.adj_relia.begin();
	for (; edgeit != graph.adj_relia.end(); edgeit++) {
		int size = graph.adj_relia[edgeit->first].size();
		graph.node_degree[edgeit->first] = size;

	}

//	for(int nodeindex=1;nodeindex<graph.nVerts;nodeindex++){
//		addEdge(graph,nodeindex, nodeindex, edgefuncsp);
//
//	}

}




void printClusterBounaryList_general(Graph &graph) {
	cout << "---------------------------ClusterBounary as follows" << endl;

	//source-----    1
	//no source---- -1;
	list<clusterNodeSet>::iterator clusterit = graph.mClusterBounaryList.begin();
	//reset
	int clusterhead = -1;
	set<int> Sset;
	for (; clusterit != graph.mClusterBounaryList.end(); clusterit++) {

		if (clusterit->isSource == false) {

			clusterhead = 0;
//			subCluster++;
			cout << "[";
		} else {

			clusterhead = -1;
			cout << "[*";

		}

//		cout << "[";

		set<int>::iterator sit = clusterit->clusterSet.begin();
		for (; sit != clusterit->clusterSet.end(); sit++) {

			cout << "item:" << *sit << "--";
//			cout << "degree=" << graph.node_degree[*sit] << "||";

		}
		cout << "]" << endl;

	}

//
//	cout<<"after combinged........................."<<endl;
//	printClusterBounaryList(graph);

}



void printClusterBounaryList_generalV2(Graph &graph) {
	cout << "Graph level-----" << graph.edgeindex+1<<endl;
	cout<<"graph.mSClusterSize============"<<graph.mSClusterSize<<endl;
	cout<<"cluster size:============="<<graph.mClusterBounaryList.size()<<endl;
	cout << "---------------------------ClusterBounary as follows" << endl;

	//source-----    1
	//no source---- -1;
	list<clusterNodeSet>::iterator clusterit = graph.mClusterBounaryList.begin();
	//reset
	int clusterhead = -1;
	set<int> Sset;
	for (; clusterit != graph.mClusterBounaryList.end(); clusterit++) {
//		cout<<"adress:"<<&*clusterit<<endl;

		if (clusterit->isSource == false) {

			clusterhead = 0;
//			subCluster++;

			cout << "[";
		} else {

			clusterhead = -1;
			cout << "[#";

		}

//		cout << "[";

		set<int>::iterator sit = clusterit->clusterSet.begin();
		for (; sit != clusterit->clusterSet.end(); sit++) {

			cout << "item:"<< *sit;
			if(::isContains(graph.VisitedSet, *sit)==true){
				cout<<"*";
			}
			cout<< "--";
			cout << "degree=" << graph.node_degree[*sit-graph.minBoundaryNodeID] << "||";

		}
		cout << "]----";

	}

	cout << "---------------------------Kset as follows" << endl;

	set<int>::iterator kit = graph.KterminalSet.begin();
	cout << "[*";
	for (; kit !=  graph.KterminalSet.end(); kit++) {

		cout << "item:" << *kit << "--";
					cout << "degree=" << graph.node_degree[*kit] << "||";

	}
	cout<<endl;
	cout << "]" << endl;

//
//	cout<<"after combinged........................."<<endl;
//	printClusterBounaryList(graph);
//	cout << "---------------------------node_degree as follows" << endl;
//
//	for(int i=1;i<graph.node_degree.size();i++){
//		cout<<"nodeID:"<<i<<"  degree is:"<<graph.node_degree[i-G.minBoundaryNodeID]<<endl;
//	}

//	cout<<"detection---"<<endl;
//	degree_detection(graph);

}




void resetNodeDegree(Graph &G,int oldMinID,int newMinID,int len) {


    vector<short int> newVector;
    newVector.reserve(len);

    //asssinOld  from newMinID to last of original node_degree
    int i=newMinID-oldMinID;
    int j=0;//for newVector
    for(;i<G.node_degree.size();i++){
    	newVector.push_back(G.node_degree[i]);
    	j++;

    }

    int extend=newMinID+len-1;
    if(extend> G.node_degreeArray.size()){
    	extend=G.node_degreeArray.size()-1;
    }

    //assign new
	if (extend == newMinID + len - 1) {
		for (; j < len; j++) {
			newVector.push_back(G.node_degreeArray[newMinID + j]);
		}
	}else{
		len=extend-newMinID+1;
		for (; j < len; j++) {
			newVector.push_back(G.node_degreeArray[newMinID + j]);
		}



	}

	G.node_degree.swap(newVector);
	G.node_degree.shrink_to_fit();

}




void resetNodeDegreeForCompact(Graph &G,int oldMinID,int newMinID,int len) {


    vector<short int> newVector;
    newVector.reserve(len+G.KterminalSet.size()+1);

    newVector.push_back(len);

    //asssinOld  from newMinID to last of original node_degree
    int i=newMinID-oldMinID;
    int j=0;//for newVector
    int orginalsize=G.node_degree.size();

    int actuallen=0;

 	if (len + i <= orginalsize) {
// 		cout<<"less eaqual--"<<"Origianl size:"<<orginalsize;
// 		cout<<"len +i"<<len + i<<endl;
		actuallen=len + i;
		for (; i < actuallen; i++) {
			newVector.push_back(G.node_degree[i]);
			j++;

		}

	}else{

		for (; i < orginalsize; i++) {
				newVector.push_back(G.node_degree[i]);
				j++;

		}

		  int extend=newMinID+len-1;
		    if(extend> G.node_degreeArray.size()){
		    	extend=G.node_degreeArray.size()-1;
		    }

		    //assign new
			if (extend == newMinID + len - 1) {
				for (; j < len; j++) {
					newVector.push_back(G.node_degreeArray[newMinID + j]);

				}
			}else{
				len=extend-newMinID+1;
				for (; j < len; j++) {
					newVector.push_back(G.node_degreeArray[newMinID + j]);
				}



			}



	}




	set<int>::iterator sit=G.KterminalSet.begin();

	for(;sit!=G.KterminalSet.end();sit++){

		newVector.push_back(*sit);
//		cout<<"K pushed:"<<*sit;

	}


	G.node_degree.swap(newVector);
	G.node_degree.shrink_to_fit();

//	int size=graph.node_degree.size();
//	cout<<"minID-"<<G.minBoundaryNodeID<<"--"<<"maxID"<<"--"<<graph.maxImpactNodeID;
//
//	cout<<"-"<<size;







}




void TruncationNodeDegree(Graph &graph,int oldMinID,int len) {


    int up_startIndex=0;
    int down_startIndex=0;
    bool isNeedReset=false;


    while(graph.node_degree[up_startIndex]==0){


    	up_startIndex++;


    }
    up_startIndex--;
    down_startIndex=up_startIndex;


//    cout<<"up_startIndex:"<<up_startIndex<<endl;

    if(down_startIndex==-1){
    	down_startIndex=0;
    }

    graph.minBoundaryNodeID=graph.minBoundaryNodeID+down_startIndex;

	int trimlen=graph.maxImpactNodeID-graph.minBoundaryNodeID+10;

    vector<short int> newVector;
    newVector.reserve(trimlen+1);

    //asssinOld  from newMinID to last of original node_degree

    for(int i=down_startIndex;i<=trimlen;i++){
    	newVector.push_back(graph.node_degree[i]);
//    	cout<<"new size:"<<newVector.size()<<" capacity:"<<newVector.capacity()<<endl;
    }

	graph.node_degree.swap(newVector);
//	cout<<"maxID:"<<graph.maxImpactNodeID<<" minID:"<<G.minBoundaryNodeID<<endl;
//
//	cout<<"reserve size:"<<trimlen+1;
//	cout<<"new size:"<<newVector.size()<<" capacity:"<<newVector.capacity()<<endl;
//	cout<<"actual size:"<<graph.node_degree.capacity()<<endl;
	newVector.clear();

}





void printClusterBounaryListBasic(Graph &graph) {
	if( graph.mClusterBounaryList.size()>0){
	cout << "Graph level-----" << graph.edgeindex+1<<endl;
		cout<<"graph.mSClusterSize============"<<graph.mSClusterSize<<endl;
		cout<<"cluster size:============="<<graph.mClusterBounaryList.size()<<endl;
		cout << "---------------------------ClusterBounary as follows" << endl;

		//source-----    1
		//no source---- -1;
		list<clusterNodeSet>::iterator clusterit = graph.mClusterBounaryList.begin();
		//reset
		int clusterhead = -1;
		set<int> Sset;
		for (; clusterit != graph.mClusterBounaryList.end(); clusterit++) {
	//		cout<<"adress:"<<&*clusterit<<endl;

			if (clusterit->isSource == false) {

				clusterhead = 0;
	//			subCluster++;

				cout << "[";
			} else {

				clusterhead = -1;
				cout << "[#";

			}

	//		cout << "[";

			set<int>::iterator sit = clusterit->clusterSet.begin();
			for (; sit != clusterit->clusterSet.end(); sit++) {

				cout << "item:"<< *sit;
				if(::isContains(graph.VisitedSet, *sit)==true){
					cout<<"*";
				}
				cout<< "--";
				cout << "degree=" << graph.node_degree[*sit-graph.minBoundaryNodeID] << "||";

			}
			cout << "]----";

		}}
		cout<<endl;


}

void PrintDegreeInfo(Graph &graph){

	for(int index=0;index<graph.node_degree.size();index++){
		cout<<" "<<graph.minBoundaryNodeID+index<<"D="<<graph.node_degree[index];
	}
	cout<<endl;

}




void DetectFailedNodeInClusterBounaryListBasic(Graph &graph) {
		//source-----    1
		//no source---- -1;
		list<clusterNodeSet>::iterator clusterit = graph.mClusterBounaryList.begin();
		//reset
		int clusterhead = -1;
		set<int> Sset;
		bool isfinded=false;
		for (; clusterit != graph.mClusterBounaryList.end(); clusterit++) {
	//		cout<<"adress:"<<&*clusterit<<endl;
			if(isfinded==true){
								break;
			}
	//		cout << "[";

			set<int>::iterator sit = clusterit->clusterSet.begin();
			for (; sit != clusterit->clusterSet.end(); sit++) {

				if(::isNodeTrueFailed(graph, *sit)==true){
					isfinded=true;
					cout<<"failed node is finded------------->"<<*sit<<endl;
					break;
				}

//				cout << "item:"<< *sit;
//				if(::isContains(graph.VisitedSet, *sit)==true){
//					cout<<"*";
//				}
//				cout<< "--";
//				cout << "degree=" << graph.node_degree[*sit-G.minBoundaryNodeID] << "||";

			}
//			cout << "]----";

		}

		if(isfinded==true){
			::printClusterBounaryListBasic(graph);

		}



}



inline unsigned long  mergeinteger1(unsigned long a, unsigned long b)
{
	unsigned long  sum = 0;
	//if (b >= 0)
	unsigned long mid = b;
	sum += a * 10;
	/*while (mid /= 10)
	{
		sum *= 10;
	}*/
	for (;mid/=10;sum*=10);
	sum += b;
	return sum;
}



inline long long mergeinteger_longlong(long long a, long long b)
{
	long long sum = 0;
	//if (b >= 0)
	long long mid = b;
	sum += a * 10;
	/*while (mid /= 10)
	{
		sum *= 10;
	}*/
	for (;mid/=10;sum*=10);
	sum += b;
	return sum;
}


void CreateGraph(Graph &graph,int row, int col, double p) {
	graph.edgesize = 0;
//	graph.ClusterSumBasedKey = 0;
	graph.edgeindex = 0;
//	graph.relia = 0.0;

	graph.nVerts = row * col + 1;
//	graph.vertexList = new int[nVerts];
//	for (int i = 1; i < this->nVerts; ++i) {
//
//		graph.vertexList[i] = i;
//	}

	graph.node_degree = vector<short int>(graph.nVerts);

//	for (int i = 1; i < graph.nVerts; i++) {
//
//		graph.node_degree.push_back(i);
//
//	}

	cout << "add row" << endl;

	//增加row

	for (int r = 1; r <= row; r++) {
		for (int c = 1; c <= col - 1; c++) {

			int start = (r - 1) * col + c;
			int end = start + 1;
			graph.adj_relia[start][end] = p;
			graph.adj_relia[end][start] = p;

		}

	}

	cout << "add col" << endl;

	//增加col
	for (int r = 1; r < row; r++) {
		for (int c = 1; c <= col; c++) {

			int sID = (r - 1) * col + c;
			int tID = sID + col;
			graph.adj_relia[sID][tID] = p;
			graph.adj_relia[tID][sID] = p;

		}

	}

//	addEdge(graph,0, 1);
//	addEdge(graph,1,2);
//	addEdge(graph,2,3);
	map<int, map<int, double> >::iterator edgeit = graph.adj_relia.begin();
	for (; edgeit != graph.adj_relia.end(); edgeit++) {
		int size = graph.adj_relia[edgeit->first].size();
		graph.node_degree[edgeit->first] = size;

	}

}



void InitialMeshGraph(Graph &graph,int row, int col, double varfunctionp) {




	graph.edgesize = 0;
//	graph.ClusterSumBasedKey = 0;
	graph.edgeindex = 0;
//	graph.relia = 0.0;

	graph.nVerts = row * col + 1;
	//	graph.vertexList = new int[nVerts];
	//	for (int i = 1; i < graph.nVerts; ++i) {
	//
	//		graph.vertexList[i] = i;
	//	}

	graph.node_degree = vector<short int>(graph.nVerts);

	//	for (int i = 1; i < graph.nVerts; i++) {
	//
	//		graph.node_degree.push_back(i);
	//
	//	}

	cout << "add row" << endl;

	//增加row

	for (int r = 1; r <= row; r++) {
		for (int c = 1; c <= col - 1; c++) {

			int start = (r - 1) * col + c;
			int end = start + 1;
			graph.adj_relia[start][end] = varfunctionp;
			graph.adj_relia[end][start] = varfunctionp;

		}

	}

	cout << "add col" << endl;

	//增加col
	for (int r = 1; r < row; r++) {
		for (int c = 1; c <= col; c++) {

			int sID = (r - 1) * col + c;
			int tID = sID + col;
			graph.adj_relia[sID][tID] = varfunctionp;
			graph.adj_relia[tID][sID] = varfunctionp;

		}

	}

	//	addEdge(graph,0, 1);
	//	addEdge(graph,1,2);
	//	addEdge(graph,2,3);
	map<int, map<int, double> >::iterator edgeit = graph.adj_relia.begin();
	for (; edgeit != graph.adj_relia.end(); edgeit++) {
		int size = graph.adj_relia[edgeit->first].size();
		graph.node_degree[edgeit->first] = size;

	}

}




void InitiallatticeGraph3D(Graph &graph,int row, int col,int hei, double varfunctionp) {




	graph.edgesize = 0;
//	graph.ClusterSumBasedKey = 0;
	graph.edgeindex = 0;
//	graph.relia = 0.0;

	graph.nVerts = row * col + 1;
	//	graph.vertexList = new int[nVerts];
	//	for (int i = 1; i < graph.nVerts; ++i) {
	//
	//		graph.vertexList[i] = i;
	//	}

	graph.node_degree = vector<short int>(graph.nVerts);

	//	for (int i = 1; i < graph.nVerts; i++) {
	//
	//		graph.node_degree.push_back(i);
	//
	//	}

	cout << "add row" << endl;

	for(int h=1;h<hei;h++){
		//增加row

		for (int r = 1; r <= row; r++) {
			for (int c = 1; c <= col - 1; c++) {

				int start = (r - 1) * col + c;
				int end = start + 1;
				graph.adj_relia[start][end] = varfunctionp;
				graph.adj_relia[end][start] = varfunctionp;

			}

		}

		cout << "add col" << endl;

		//增加col
		for (int r = 1; r < row; r++) {
			for (int c = 1; c <= col; c++) {

				int sID = (r - 1) * col + c;
				int tID = sID + col;
				graph.adj_relia[sID][tID] = varfunctionp;
				graph.adj_relia[tID][sID] = varfunctionp;

			}

		}




	}



	//	addEdge(graph,0, 1);
	//	addEdge(graph,1,2);
	//	addEdge(graph,2,3);
	map<int, map<int, double> >::iterator edgeit = graph.adj_relia.begin();
	for (; edgeit != graph.adj_relia.end(); edgeit++) {
		int size = graph.adj_relia[edgeit->first].size();
		graph.node_degree[edgeit->first] = size;

	}

}





void InitialCompleteGraph(Graph &graph,int nodesize, double varfunctionp) {
	graph.edgesize = 0;
//	graph.ClusterSumBasedKey = 0;
	graph.edgeindex = 0;
//	graph.relia = 0.0;

	graph.nVerts = nodesize + 1;
	//	graph.vertexList = new int[nVerts];
	//	for (int i = 1; i < graph.nVerts; ++i) {
	//
	//		graph.vertexList[i] = i;
	//	}

	graph.node_degree = vector<short int>(graph.nVerts);

	//	for (int i = 1; i < graph.nVerts; i++) {
	//
	//		graph.node_degree.push_back(i);
	//
	//	}

	cout << "add edge loop" << endl;

	//增加edge

	for (int r = 1; r <= nodesize; r++) {
		for (int c = 1; c <= nodesize; c++) {

			if (r != c) {
				graph.adj_relia[r][c] = varfunctionp;
				graph.adj_relia[c][r] = varfunctionp;
			}

		}

	}


	map<int, map<int, double> >::iterator edgeit = graph.adj_relia.begin();
	for (; edgeit != graph.adj_relia.end(); edgeit++) {
		int size = graph.adj_relia[edgeit->first].size();
		graph.node_degree[edgeit->first] = size;

	}

}


void printMat(Graph& graph) {
	map<int, map<int, double> >::iterator edgeit = graph.adj_relia.begin();
	for (; edgeit != graph.adj_relia.end(); edgeit++) {
		cout << "arc from:  " << edgeit->first << endl;
		map<int, double>::iterator neiborit = edgeit->second.begin();

		for (; neiborit != edgeit->second.end(); neiborit++) {

			cout << "arc to:  " << neiborit->first << ":relia:"
					<< neiborit->second << endl;
		}

	}

}

void addEdge(Graph& graph,int start, int end, double relia) {
//	cout << "arc:" << start << "---" << end << " added" << endl;

	if(graph.adj_relia.find(start)!=graph.adj_relia.end()){
		if(graph.adj_relia[start].find(end)!=graph.adj_relia[start].end()){
//			cout<<"edge existence!! and para ana is needed"<<endl;
			relia=1.0-(1.0-relia)*(1.0-graph.adj_relia[start][end]);
		}
	}



	graph.adj_relia[start][end] = relia;
	graph.adj_relia[end][start] = relia;

}

void deleteEdge(Graph &graph,int start, int end) {
	graph.adj_relia[start].erase(end);
	graph.adj_relia[end].erase(start);

}



void expandStackGraph(list<GraphNode*> &redundantpGraphStack,int &allnodes){
	int initialsize=200000;
	for(int n=1;n<=initialsize;n++){
		GraphNode *pgraph=new GraphNode();
		redundantpGraphStack.push_back(pgraph);
	}

	allnodes+=initialsize;

}


void expandStackGraphV2(vector<GraphNode*> &redundantpGraphStack,int &allnodes){
	int initialsize=200000;
	for(int n=1;n<=initialsize;n++){
		GraphNode *pgraph=new GraphNode();
		redundantpGraphStack.push_back(pgraph);
	}
	allnodes+=initialsize;

}


void printBSVectorInfor(Graph &G){
	cout<<"BS-------------------------"<<endl;
	cout<<"edgeIndex:"<<G.edgeindex<<"--";
	cout<<"mSClusterSize:"<<G.mSClusterSize<<"--";
	cout<<"minBoundaryNodeID:"<<G.minBoundaryNodeID<<"--";
	cout<<"clusterMinhead:"<<G.clusterMinhead<<endl;

}



void RDecomLayerV2(vector<vector<HashGraphmap>> &phashgraphmap2DArray,
		HashGraphmap &phashgraphmap,Graph &G,vector<GraphNode*> &redundantpGraphStack,int &usednodes) {

	GraphNode *leftGraph = NULL;
	GraphNode *rightGraph = NULL;


	int addTimes = 0;
	int minusTimes = 0;

	int Cumulative_minus = 0;
	int Cumulative_recov = 0;




	addTimes = 0;
	minusTimes = 0;
	int maxdegreesize=0;


	unordered_map<string, GraphNode*>::iterator pgraphIt =
			phashgraphmap.mapstring_relia.begin();
//	cout<<"==============>subsize--"<<phashgraphmap.mapstring_relia.size()<<endl;
	for (; pgraphIt != phashgraphmap.mapstring_relia.end();) {


//		string keystring= pgraphIt->first;

//		if(maxdegreesize<pgraphIt->second->node_degree.size()){
//			maxdegreesize=pgraphIt->second->node_degree.size();
//		}




		int dIndex=pgraphIt->second->Dindex;
        int BsIndex=pgraphIt->second->BSindex;




		set<int> Ket;

		int dlen=phashgraphmap.LayerBSDegreeVec[dIndex][0];
		int len=phashgraphmap.LayerBSDegreeVec[dIndex].size();

		 vector<short int>::const_iterator first1 = phashgraphmap.LayerBSDegreeVec[dIndex].begin()+1;
		 vector<short int>::const_iterator last1  = phashgraphmap.LayerBSDegreeVec[dIndex].begin() +dlen+1;
		 vector<short int> Dvector(first1,last1);

//		for(int d=1;d<=dlen;d++){
//			Dvector.push_back(phashgraphmap.LayerBSDegreeVec[dIndex][d]);
//		}

		for (int k = dlen+1; k < len; k++) {
			Ket.insert(phashgraphmap.LayerBSDegreeVec[dIndex][k]);
		}


		G.edgeindex=phashgraphmap.LayerBSVec[BsIndex][0];
		G.mSClusterSize=phashgraphmap.LayerBSVec[BsIndex][1];
		G.minBoundaryNodeID=phashgraphmap.LayerBSVec[BsIndex][2];
		G.clusterMinhead=phashgraphmap.LayerBSVec[BsIndex][3];

		::DecodeClusterBlistFromString(*pgraphIt->second, G, pgraphIt->first);

//		printBSVectorInfor(G);



		G.node_degree=Dvector;
		G.KterminalSet=Ket;

//		set<int>::iterator kit=Ket.begin();
//		for(;kit!=Ket.end();kit++){
//			cout<<"k:"<<*kit<<"--";
//		}
//
//		cout<<"---"<<endl;

		list<clusterNodeSet> mclusterlist = G.mClusterBounaryList;


//		cout<<"after decode..."<<G.mClusterBounaryList.size()<<endl;

//			cout<<"node PreRelia:"<<pgraphIt->second->PreVarRelia<<endl;
		if (redundantpGraphStack.size() > 0) {
			leftGraph = redundantpGraphStack.back();
			redundantpGraphStack.pop_back();
			::CopyGNode(*leftGraph, *pgraphIt->second);
			minusTimes++;
			Cumulative_minus++;

		} else {

			::expandStackGraphV2(redundantpGraphStack, usednodes);

			leftGraph = redundantpGraphStack.back();
			redundantpGraphStack.pop_back();
			::CopyGNode(*leftGraph, *pgraphIt->second);
		}

//			 Assign(leftGraph->pGraphData,cur->pGraphData);

		//Graph graph_T(cur->pGraphData);

		if (redundantpGraphStack.size() > 0) {
			rightGraph = redundantpGraphStack.back();
			redundantpGraphStack.pop_back();

			AssignGNode(*rightGraph, *pgraphIt->second);
			minusTimes++;
			Cumulative_minus++;

		} else {

//				cout<<"redundantpGraphStack size:"<<redundantpGraphStack.size()<<endl;

			::expandStackGraphV2(redundantpGraphStack, usednodes);

			rightGraph = redundantpGraphStack.back();
			redundantpGraphStack.pop_back();
			AssignGNode(*rightGraph, *pgraphIt->second);

//				cout<<"redundantpGraphStack size:"<<redundantpGraphStack.size()<<endl;
		}
//		cout<<"decom ---F---"<<endl;
		bool isleftfinished = Left_decom_finished2DV2(leftGraph, *leftGraph,
				phashgraphmap2DArray, G);
//			cout<<"F---Finished"<<endl;

		G.mClusterBounaryList.swap(mclusterlist);
		G.KterminalSet.swap(Ket);
		G.node_degree.swap(Dvector);

		G.edgeindex=phashgraphmap.LayerBSVec[BsIndex][0];
		G.mSClusterSize=phashgraphmap.LayerBSVec[BsIndex][1];
		G.minBoundaryNodeID=phashgraphmap.LayerBSVec[BsIndex][2];
		G.clusterMinhead=phashgraphmap.LayerBSVec[BsIndex][3];



//		cout<<"decom ---T---"<<endl;

		bool isrightfinished = Right_decom_finished2DV2(rightGraph, *rightGraph,
				phashgraphmap2DArray, G);
//			cout<<"T---Finished"<<endl;

		if (leftGraph->isfinished == true) {
			::releaseGraph(leftGraph);

			redundantpGraphStack.push_back(leftGraph);
			addTimes++;
			Cumulative_recov++;
//				delete leftGraph;
//				leftGraph=NULL;
		}

		if (rightGraph->isfinished == true) {
			::releaseGraph(rightGraph);

//				delete rightGraph;
//				rightGraph=NULL;
			redundantpGraphStack.push_back(rightGraph);
			addTimes++;
			Cumulative_recov++;
		}

//			release temp
//			cout<<pgraphIt->second<<endl;
		releaseGraph(pgraphIt->second);
//			delete pgraphIt->second;
//			pgraphIt->second=NULL;

//			phashgraphmapArray[targetLayerindex].mapstring_relia.erase(pgraphIt++);

		redundantpGraphStack.push_back(pgraphIt->second);
//		addTimes++;
//		Cumulative_recov++;

//			cout<<"innner loop stack nodes:============="<<redundantpGraphStack.size()<<endl;

//			cout<<"erased....."<<endl;

//			phashgraphmapArray[targetLayerindex].mapstring_relia.erase(pgraphIt);

		pgraphIt++;
//			cout<<" after erased....."<<endl;

	}

//	if(maxdegreesize>0){cout<<"MD-"<<maxdegreesize;}




	phashgraphmap.mapstring_relia.clear();

}









void RDecomLayer2D(vector<vector<HashGraphmap>> &phashgraphmap2DArray,
		int Layernumber,int cols, Graph &G) {
	int layerindex=0;
	size_t bddnodesize=0;
	int maxlayersize=0;


//	list<GraphNode*>redundantpGraphStack;

	vector<GraphNode*>redundantpGraphStack;

	int initialsize=4000000;

	redundantpGraphStack.reserve(initialsize+100);
//	for(int n=1;n<=initialsize;n++){
//		GraphNode *pgraph=new GraphNode();
//		redundantpGraphStack.push_back(pgraph);
//	}

	int usednodes=0;

	int stack_nodes=0;
	int allnodesused=initialsize;

	int templayersize=0;
	int subsize=0;

	int tempvectsize=0;

	int bsVsize=0;



	for(;layerindex<Layernumber;layerindex++){
		templayersize=0;
		tempvectsize=0;
		bsVsize=0;
//		cout<<"temp layerIndex:"<<layerindex<<"----"<<Layernumber<<endl;

		stack_nodes=redundantpGraphStack.size();
//		cout<<"surplus stack nodes:--"<<stack_nodes<< "    all nodes:--"<<usednodes<<endl;


		for(int c=0;c<cols;c++ ){




			HashGraphmap & phashgraph=phashgraphmap2DArray[layerindex][c];
//			if(phashgraphmap2DArray[layerindex][c].LayerBSDegreeVec.size()>0){
//			cout<<"degree vec size:--"<<phashgraphmap2DArray[layerindex][c].LayerBSDegreeVec.size()<<endl;}

			subsize=phashgraph.mapstring_relia.size();
//			cout<<subsize<<" ";

			templayersize+=subsize;
			tempvectsize+=phashgraph.LayerBSDegreeVec.size();
			bsVsize+=phashgraph.LayerBSVec.size();
			//
//			if(layerindex==59){
//			cout<<"c==="<<c;
//			cout<<"-"<<subsize<<" ";}

			::RDecomLayerV2(phashgraphmap2DArray, phashgraph, G, redundantpGraphStack,usednodes);




		}
//		cout<<endl;
		cout<<"level:="<<layerindex<<"<<"  "<<templayersize:"<<templayersize<<endl;

		//release
		phashgraphmap2DArray[layerindex].clear();
		phashgraphmap2DArray[layerindex].shrink_to_fit();


		if (maxlayersize < templayersize) {
			maxlayersize = templayersize;
		}

		bddnodesize += templayersize;


	}



	vector<GraphNode*>::iterator pGraphIt=redundantpGraphStack.begin();
	while( pGraphIt!=redundantpGraphStack.end()){

		delete *pGraphIt;

//			pgraphIt=phashgraphmapArray[layerindex].map_relia.erase(pgraphIt);
//        redundantpGraphStack.erase(pGraphIt++);
        pGraphIt++;
	}
	redundantpGraphStack.clear();





	cout<<"nodes size all:================"<<bddnodesize<<endl;
	cout<<"maxLayersize:"<<maxlayersize<<endl;
	printf("the 2 X maxLayersize/all nodes  :%.10f\n",2.0*maxlayersize/bddnodesize);

}




inline void Assign(Graph &G1,Graph& graph){

	// graph to G1
		G1.nVerts = graph.nVerts;
	//	this->ClusterSumBasedKey = graph.ClusterSumBasedKey;
		G1.edgeindex = graph.edgeindex;
		G1.edgesize = graph.edgesize;
	//
	//	this->vertexList = new int[nVerts];
	//
	//	*(this->vertexList)=*graph.vertexList;

	//	this->SourceSet = graph.SourceSet;
//		G1.VisitedSet = graph.VisitedSet;
	//	this->targetEdge=G.targetEdge;
	//	this->EdgeList=graph.EdgeList;
//		G1.relia = graph.relia;

//		G1.ptrClusterBounaryList=new list<clusterNodeSet>;
//		*G1.ptrClusterBounaryList=*graph.ptrClusterBounaryList;
//		G1.mClusterBounaryList.swap(G.mClusterBounaryList);

	//	this->CombineBounaryList=graph.CombineBounaryList;

	//	this->subGraph=graph.subGraph;

		G1.node_degree.swap(graph.node_degree);
//		G1.node_degree=graph.node_degree;
	//	this->adj_relia=graph.adj_relia;
		G1.KterminalSet = graph.KterminalSet;
		G1.mSClusterSize = graph.mSClusterSize;
		G1.minBoundaryNodeID=graph.minBoundaryNodeID;
		G1.PreVarRelia=graph.PreVarRelia;
		G1.maxImpactNodeID=graph.maxImpactNodeID;
		G1.clusterMinhead=graph.clusterMinhead;



}


inline void CopyGraph(Graph &G1,Graph& graph){

	// graph to G1
		G1.nVerts = graph.nVerts;
	//	this->ClusterSumBasedKey = graph.ClusterSumBasedKey;
		G1.edgeindex = graph.edgeindex;
		G1.edgesize = graph.edgesize;
	//
	//	this->vertexList = new int[nVerts];
	//
	//	*(this->vertexList)=*graph.vertexList;

	//	this->SourceSet = graph.SourceSet;
//		G1.VisitedSet = graph.VisitedSet;
	//	this->targetEdge=G.targetEdge;
	//	this->EdgeList=graph.EdgeList;
//		G1.relia = graph.relia;

//		G1.ptrClusterBounaryList=new list<clusterNodeSet>;
//		*G1.ptrClusterBounaryList=*graph.ptrClusterBounaryList;
//		G1.mClusterBounaryList=G.mClusterBounaryList;

	//	this->CombineBounaryList=graph.CombineBounaryList;

	//	this->subGraph=graph.subGraph;

		G1.node_degree=graph.node_degree;
//		G1.node_degree=graph.node_degree;
	//	this->adj_relia=graph.adj_relia;
		G1.KterminalSet = graph.KterminalSet;
		G1.mSClusterSize = graph.mSClusterSize;
		G1.minBoundaryNodeID=graph.minBoundaryNodeID;
		G1.PreVarRelia=graph.PreVarRelia;
		G1.maxImpactNodeID=graph.maxImpactNodeID;
		G1.clusterMinhead=graph.clusterMinhead;


}




inline void AssignGNode(GraphNode &G1,GraphNode& graph){

	// graph to G1

	//	this->ClusterSumBasedKey = graph.ClusterSumBasedKey;


	//
	//	this->vertexList = new int[nVerts];
	//
	//	*(this->vertexList)=*graph.vertexList;

	//	this->SourceSet = graph.SourceSet;
//		G1.VisitedSet = graph.VisitedSet;
	//	this->targetEdge=G.targetEdge;
	//	this->EdgeList=graph.EdgeList;
//		G1.relia = graph.relia;

//		G1.ptrClusterBounaryList=new list<clusterNodeSet>;
//		*G1.ptrClusterBounaryList=*graph.ptrClusterBounaryList;
//		G1.mClusterBounaryList.swap(G.mClusterBounaryList);

	//	this->CombineBounaryList=graph.CombineBounaryList;

	//	this->subGraph=graph.subGraph;


//		G1.node_degree=graph.node_degree;
	//	this->adj_relia=graph.adj_relia;
//		G1.KterminalVec.swap(graph.KterminalVec);

		G1.PreVarRelia=graph.PreVarRelia;




}


inline void CopyGNode(GraphNode &G1,GraphNode& graph){



		G1.PreVarRelia=graph.PreVarRelia;



}




inline void CopyGNodeFromGraph(GraphNode &G1,Graph& G){

	// graph to G1

	//	this->ClusterSumBasedKey = graph.ClusterSumBasedKey;


	//	this->vertexList = new int[nVerts];
	//
	//	*(this->vertexList)=*graph.vertexList;

	//	this->SourceSet = graph.SourceSet;
//		G1.VisitedSet = graph.VisitedSet;
	//	this->targetEdge=G.targetEdge;
	//	this->EdgeList=graph.EdgeList;
//
		G1.PreVarRelia=G.PreVarRelia;


}






void printmap_basicV17(HashGraphmap &hashGraphmap) {

}



void printmap_basic19V10(HashGraphmap *phashGraphmap,int len) {




}


void printmap_basic_CapacityV2(HashGraphmap *phashGraphmap,int len) {
	int nodesize=0;
	int hitsize=0;
	int truesize=0;
	cout << "the outerlayer size:" << len<< endl;
	bool isprint=false;

    for(int i=0;i<len;i++){
    	hitsize+=phashGraphmap[i].hit;
//    	cout<<"level size:"<<phashGraphmap[i].m_graphNodeCluster.mapindex.size()<<endl;
//    	cout<<"----------------edge index-------------------------"<<i<<endl;
//    	cout << "the outer layer size:========================" << phashGraphmap[i].hashsubGraphmap_vectorV17.size();
//        cout<<"====================================================="<<endl;

    	nodesize+=phashGraphmap[i].mapstring_relia.size();
//    	cout<<"level:="<<i<<" bddnodes:"<<phashGraphmap[i].map_relia.size()<<endl;

  //    	for(;reliaMapVectorit!=phashGraphmap[i].map_relia.end();reliaMapVectorit++){
//
//    		truesize+=reliaMapVectorit->second.size();
//    		if(reliaMapVectorit->second.size()>100){
//    			cout<<"sub truesize:"<<reliaMapVectorit->second.size()<<endl;
//    		}
//
//
//
//    	}



    }


	cout << "the graph node size:" <<nodesize << endl;
	cout << "the true graph node size:" <<truesize << endl;
	cout << "the hit times is:" <<hitsize<< endl;

}

void printmap_basicV16(HashGraphmap &hashGraphmap) {


}

void clearV16(HashGraphmap &hashGraphmap) {


}


void clearV17(HashGraphmap &hashGraphmap) {

}



void loadGraphFromEdgelist(Graph &graph, string strFlle) {



  	graph.edgesize = 0;
//	graph.ClusterSumBasedKey = 0;
  	graph.edgeindex = 0;
//	graph.relia = 0.0;




	string pattern = " ";
	string label_file = strFlle;
	std::ifstream labels(label_file.c_str());
	if (!labels.is_open()) {
		cout << "Can't open the label file!" << endl;
		return;
	}
	string line;
	int startID = -1;
	int endID = -1;
	double relia = 0.0;
	while (std::getline(labels, line)) {
//		cout << line << endl;
		replace(line.begin(), line.end(), '{', ' ');
		replace(line.begin(), line.end(), '}', ' ');
//		replace(line.begin(), line.end(), '"', ' ');

		vector<string> result = split(line, pattern);

//		for (int i = 0; i < result.size(); i++) {
//			cout << "item:" << result[i] << "-";
//		}
//		cout << endl;

		startID = atoi(result[0].c_str());
		endID = atoi(result[1].c_str());
//		string str = result[3];
		relia = atof(result[3].c_str());

		graph.adj_relia[startID][endID] = relia;
		graph.adj_relia[endID][startID] = relia;

	}

  ///////////////
	graph.nVerts =graph.adj_relia.size()+1;
	graph.node_degree = vector<short int>(graph.nVerts);

////


	map<int, map<int, double> >::iterator edgeit = graph.adj_relia.begin();
	for (; edgeit != graph.adj_relia.end(); edgeit++) {
		int size = graph.adj_relia[edgeit->first].size();
		graph.node_degree[edgeit->first] = size;

	}



	//add Kset

    graph.mSClusterSize=-1;


}



void loadGraphFromNodeKlist(Graph &graph, string strFlle) {


	string pattern = " ";
	string label_file = strFlle;
	std::ifstream labels(label_file.c_str());
	if (!labels.is_open()) {
		cout << "Can't open the label file!" << endl;
		return;
	}
	string line;
	int nodeID = -1;
	int isK=-1;

	while (std::getline(labels, line)) {
		cout << line << endl;
		replace(line.begin(), line.end(), '{', ' ');
		replace(line.begin(), line.end(), '}', ' ');
//		replace(line.begin(), line.end(), '"', ' ');

		vector<string> result = split(line, pattern);

//		for (int i = 0; i < result.size(); i++) {
//			cout << "item:" << result[i] << "-";
//		}
//		cout << endl;



		nodeID = atoi(result[0].c_str());
		isK = atoi(result[3].c_str());

		cout<<"nodeID"<<nodeID<<endl;
		cout<<"is K"<<isK<<endl;

		if (isK == 1) {
			graph.KterminalSet.insert(nodeID);
		}

	}



}



void WriteToEdgelistForDecom(Graph &graph,int step,set<int>& BS){
	 ofstream ofile;               //定义输出文件
	 string parStr= "G:/reliability/networkDecom/debug";


	 if (0 != access(parStr.c_str(), 0))
	     {
	         // if this folder not exist, create a new one.
	         mkdir(parStr.c_str());   // 返回 0 表示创建成功，-1 表示失败
	         //换成 ::_mkdir  ::_access 也行，不知道什么意思
	     }


	 string myfile=parStr+"/"+
			 graph.networkName+::to_string(step)+".edgelist";
	 ofile.open(myfile);
		const int n= graph.nVerts;


	//	int (*s)[len] = new int[len][len]();

	//	auto s = new int[n][n];
	//
		int **s=new int*[n];
		for(int i=0;i< n;i++){
			s[i]=new int[n];
		}


		for (int i = 1; i < graph.nVerts; i++) {
			for (int j = 1; j < graph.nVerts; j++) {

				s[i][j] = false;
			}
		}


		list<int>* bfsnodeslist=new list<int>;
		set<int>::iterator sit=BS.begin();

		bfsnodeslist= classicbfs(graph, *sit);

		// 判断是否已存入该边到S集合中
		list<int> Stack=*bfsnodeslist;


		while (!Stack.empty())	//只要队列不空，遍历就没有结束
		{
			int startID = Stack.front();
			//取出对头元素
			Stack.pop_front();
//			cout<<"node:"<<startID<<" is popped"<<endl;
			//add the nodeself
	//		if (s[startID][startID] == false) {
	//			s[startID][startID] = true;
	//            if(::isContains(graph.KterminalSet, startID)==false){
	//			Edge edge;
	//			edge.StartID = startID;
	//			edge.EndID = startID;
	//			edge.relia = nodefuncs;
	//			graph.vectorEdge.push_back(edge);}
	//		}

			set<int> neibors = NeiborsOfNodeV1(graph, startID);

			set<int>::iterator neiborit = neibors.begin();
			for (; neiborit != neibors.end(); neiborit++) {
	//			cout<<"arc"<<*neiborit<<"-->"<<startID<<s[*neiborit][startID]<<endl;
				if (s[*neiborit][startID] == false) {
					s[*neiborit][startID] = true;
					s[startID][*neiborit] = true;
	//				cout<<"arc"<<*neiborit<<"--"<<startID<<s[*neiborit][startID]<<endl;
	//				cout<<"arc"<<startID<<"--"<<*neiborit<<s[startID][*neiborit]<<endl;
					int endID = *neiborit;
					Edge edge;
					edge.StartID = startID;
					edge.EndID = endID;
					edge.relia = graph.adj_relia[startID][endID];
					//add to the pedgelist
					ofile << edge.StartID << " " <<edge.EndID<< " "
							<< "{'relia':" <<" "<< edge.relia << "}" << endl;
	////				add to stack
	//				Stack.push_back(endID);
				}

			}


		}

//		cout<<"s bool array is released"<<endl;
		for (int i = 0; i < n; i++) {
			delete s[i];
		}
		delete []s;

	ofile.close();                //关闭文件

}


void WriteToOriginalEdgelist(Graph &graph){
	 ofstream ofile;               //定义输出文件
	 string parStr="G:/reliability/StudyNetworkGML/Results/";
	 string myfile=parStr+graph.networkName+"Original.edgelist";
	 ofile.open(myfile);
		const int n= graph.nVerts;


	//	int (*s)[len] = new int[len][len]();

	//	auto s = new int[n][n];
	//
		int **s=new int*[n];
		for(int i=0;i< n;i++){
			s[i]=new int[n];
		}


		for (int i = 1; i < graph.nVerts; i++) {
			for (int j = 1; j < graph.nVerts; j++) {

				s[i][j] = false;
			}
		}


		list<int>* bfsnodeslist=new list<int>;
		set<int>::iterator sit=graph.KterminalSet.begin();
		bfsnodeslist= bfs(graph, *sit);

		// 判断是否已存入该边到S集合中
		list<int> Stack=*bfsnodeslist;


		while (!Stack.empty())	//只要队列不空，遍历就没有结束
		{
			int startID = Stack.front();
			//取出对头元素
			Stack.pop_front();
//			cout<<"node:"<<startID<<" is popped"<<endl;
			//add the nodeself
	//		if (s[startID][startID] == false) {
	//			s[startID][startID] = true;
	//            if(::isContains(graph.KterminalSet, startID)==false){
	//			Edge edge;
	//			edge.StartID = startID;
	//			edge.EndID = startID;
	//			edge.relia = nodefuncs;
	//			graph.vectorEdge.push_back(edge);}
	//		}

			set<int> neibors = NeiborsOfNodeV1(graph, startID);

			set<int>::iterator neiborit = neibors.begin();
			for (; neiborit != neibors.end(); neiborit++) {
	//			cout<<"arc"<<*neiborit<<"-->"<<startID<<s[*neiborit][startID]<<endl;
				if (s[*neiborit][startID] == false) {
					s[*neiborit][startID] = true;
					s[startID][*neiborit] = true;
	//				cout<<"arc"<<*neiborit<<"--"<<startID<<s[*neiborit][startID]<<endl;
	//				cout<<"arc"<<startID<<"--"<<*neiborit<<s[startID][*neiborit]<<endl;
					int endID = *neiborit;
					Edge edge;
					edge.StartID = startID;
					edge.EndID = endID;
					edge.relia = graph.adj_relia[startID][endID];
					//add to the pedgelist
					ofile << edge.StartID << " " <<edge.EndID<< " "
							<< "{'relia':" <<" "<< edge.relia << "}" << endl;
	////				add to stack
	//				Stack.push_back(endID);
				}

			}


		}

		cout<<"s bool array is released"<<endl;
		for (int i = 0; i < n; i++) {
			delete s[i];
		}
		delete []s;

	ofile.close();                //关闭文件

}



void WriteToEdgelistV2(Graph &graph){
	 ofstream ofile;               //定义输出文件
	 string parStr="G:\\reliability\\StudyNetworkGML\\";
	 string myfile=parStr+graph.networkName+"V2.edgelist";
	 ofile.open(myfile);
	 const int n= graph.nVerts;


	//	int (*s)[len] = new int[len][len]();

	//	auto s = new int[n][n];
	//
		int **s=new int*[n];
		for(int i=0;i< n;i++){
			s[i]=new int[n];
		}


		for (int i = 1; i < graph.nVerts; i++) {
			for (int j = 1; j < graph.nVerts; j++) {

				s[i][j] = false;
			}
		}


		list<int>* bfsnodeslist=new list<int>;
		bfsnodeslist= bfsV2(graph, 1);

		// 判断是否已存入该边到S集合中
		list<int> Stack=*bfsnodeslist;


		while (!Stack.empty())	//只要队列不空，遍历就没有结束
		{
			int startID = Stack.front();
			//取出对头元素
			Stack.pop_front();
			cout<<"node:"<<startID<<" is popped"<<endl;
			//add the nodeself
	//		if (s[startID][startID] == false) {
	//			s[startID][startID] = true;
	//            if(::isContains(graph.KterminalSet, startID)==false){
	//			Edge edge;
	//			edge.StartID = startID;
	//			edge.EndID = startID;
	//			edge.relia = nodefuncs;
	//			graph.vectorEdge.push_back(edge);}
	//		}

			set<int> neibors = NeiborsOfNodeV2(graph, startID);

			set<int>::iterator neiborit = neibors.begin();
			for (; neiborit != neibors.end(); neiborit++) {
	//			cout<<"arc"<<*neiborit<<"-->"<<startID<<s[*neiborit][startID]<<endl;
				if (s[*neiborit][startID] == false) {
					s[*neiborit][startID] = true;
					s[startID][*neiborit] = true;
	//				cout<<"arc"<<*neiborit<<"--"<<startID<<s[*neiborit][startID]<<endl;
	//				cout<<"arc"<<startID<<"--"<<*neiborit<<s[startID][*neiborit]<<endl;
					int endID = *neiborit;
					Edge edge;
					edge.StartID = startID;
					edge.EndID = endID;
					edge.relia = graph.adj_reliaV2[startID][endID];
					//add to the pedgelist
					ofile << edge.StartID << " " <<edge.EndID<< " "
							<< "{'relia':" <<" "<< edge.relia << "}" << endl;
	////				add to stack
	//				Stack.push_back(endID);
				}

			}


		}

		cout<<"s bool array is released"<<endl;
		for (int i = 0; i < n; i++) {
			delete s[i];
		}
		delete []s;

	ofile.close();                //关闭文件





}



void WriteToNodelist(Graph &graph){
	 ofstream ofile;               //定义输出文件
	 string parStr="G:/reliability/StudyNetworkGML/Results/";
	 string myfile=parStr+graph.networkName+".nodelist";
	 ofile.open(myfile);

	 set<int>::iterator sit=graph.KterminalSet.begin();
	 for(;sit!=graph.KterminalSet.end();sit++){

			ofile << *sit<< " " <<"{ is K 1 }" << endl;


	 }

	ofile.close();                //关闭文件





}



void LoadMesh100X3(Graph &graph) {
	string parStr = "G:\\reliability\\StudyNetworkGML\\";
	string myfile = parStr + "Network_mesh100x3.edgelist";

	loadGraphFromEdgelist(graph, myfile);
/////
	graph.sID = 1;
	graph.KterminalSet.insert(1);
	graph.KterminalSet.insert(300);
}


void LoadMesh3X3(Graph &graph) {
	string parStr = "G:\\reliability\\StudyNetworkGML\\";
	string myfile = parStr + "Network_mesh3_3_2T.edgelist";

	loadGraphFromEdgelist(graph, myfile);
/////
	graph.sID = 1;
	graph.KterminalSet.insert(1);
	graph.KterminalSet.insert(9);
}


void LoadNetWork5_2K(Graph &graph) {
	string parStr = "G:\\reliability\\StudyNetworkGML\\";
	string myfile = parStr + "Network5 K_2.edgelist";

	loadGraphFromEdgelist(graph, myfile);
/////
	graph.sID = 20;
	graph.KterminalSet.insert(1);
	graph.KterminalSet.insert(15);
}



void LoadCogentcoNet(Graph &graph) {
	string parStr = "G:\\reliability\\StudyNetworkGML\\";
	string myfile = parStr + "Cogentco.edgelist";

	loadGraphFromEdgelist(graph, myfile);
/////
	graph.networkName="Cogentco K=127 183 ";
	graph.sID = 183;
	graph.KterminalSet.insert(127);
	graph.KterminalSet.insert(183);
}


void LoadMesh12X12(Graph &graph) {
	string parStr = "G:\\reliability\\StudyNetworkGML\\";
	string myfile = parStr + "Network_mesh12x12.edgelist";
	string strFlle= parStr + "Network_mesh12x12.nodelist";

	loadGraphFromEdgelist(graph, myfile);
	::loadGraphFromNodeKlist(graph, strFlle);
/////
	graph.networkName="mesh12X12";
	graph.sID = 1;

}

void LoadMesh10X10(Graph &graph) {
	string parStr = "G:\\reliability\\StudyNetworkGML\\";
	string myfile = parStr + "Network_mesh10x10.edgelist";
	string strFlle= parStr + "Network_mesh10x10.nodelist";

	loadGraphFromEdgelist(graph, myfile);
	::loadGraphFromNodeKlist(graph, strFlle);
/////
	graph.networkName="mesh10X10";
	graph.sID =7;

}


void LoadNetWork15(Graph &graph) {
	string parStr = "G:\\reliability\\StudyNetworkGML\\";
	string myfile = parStr + "Network15.edgelist";
	string strFlle= parStr + "Network15.nodelist";

	loadGraphFromEdgelist(graph, myfile);
	::loadGraphFromNodeKlist(graph, strFlle);
/////
	graph.networkName="Network15";
	graph.sID = 1;

}


void LoadNetWork5(Graph &graph) {
	string parStr = "G:\\reliability\\StudyNetworkGML\\";
	string myfile = parStr + "Network5 K=2.edgelist";
	string strFlle= parStr + "Network5 K=2.nodelist";

	loadGraphFromEdgelist(graph, myfile);
	::loadGraphFromNodeKlist(graph, strFlle);
/////
	graph.networkName="Network5";
	graph.sID = 1;

}




void LoadKDLNet(Graph &graph) {
	string parStr = "G:/reliability/TheInternetTopologyZoo/edgelist/reducedResults/";
	string myfile = parStr + "KdlNetV2.edgelist";

	loadGraphFromEdgelist(graph, myfile);
/////
	/*
	 * 176 nodes and  295 edges
	 * the max path is: 21
['10', '5', '2', '4', '8', '15', '23', '32', '40', '49', '58', '70', '80',
'92', '107', '122', '134', '145', '156', '167', '175']
	 */

	cout<<"the nodesize is:"<<graph.adj_relia.size();
	graph.networkName="KdlNetV2";
	graph.sID = 10;
	graph.KterminalSet.insert(10);
	graph.KterminalSet.insert(175);

	cout<<"176 nodes and  295 edges for KdlNetV2"<<endl;
}



void LoadKDLNetV1(Graph &graph){
	/*

	['11', '10', '272', '534', '311', '16', '718', '717', '720', '719', '6', '308', '515', '516', '513', '199', '511', '512', '242', '240', '505', '506', '411', '408', '201', '613', '614', '42', '18', '487', '24', '490', '49', '52', '230', '705', '231', '227', '234', '233', '640', '641', '642', '434', '400', '399', '71', '665', '520', '533', '532', '538', '531', '541', '140', '56', '55', '562', '12']
	(1)max path is: 11 to  12 len= 59
	['11', '10', '272', '534', '311', '16', '718', '717', '720', '719', '5', '317', '315', '314', '316', '309', '624', '39', '211', '32', '742', '741', '690', '523', '654', '634', '238', '237', '0', '751', '745', '565', '245', '649', '4', '2', '441', '442', '440', '402', '401', '549', '587', '629', '82', '341', '217', '218', '215', '216', '518', '222', '221', '213', '212', '598', '599', '566', '70']
	(2)max path is: 11 to  70 len= 59
	['422', '419', '420', '425', '426', '423', '428', '427', '150', '546', '545', '458', '321', '314', '316', '309', '624', '508', '509', '512', '242', '240', '505', '506', '411', '408', '201', '97', '96', '98', '87', '495', '293', '677', '678', '679', '680', '676', '358', '357', '360', '355', '356', '353', '637', '473', '470', '471', '468', '469', '631', '633', '410', '682', '299', '684', '681', '683', '686']
	(3)max path is: 422 to  686 len= 59
	['421', '708', '738', '753', '752', '737', '428', '427', '150', '546', '545', '458', '321', '314', '316', '309', '624', '508', '509', '512', '242', '240', '505', '506', '411', '408', '201', '97', '96', '98', '87', '495', '293', '677', '678', '679', '680', '676', '358', '357', '360', '355', '356', '353', '637', '473', '470', '471', '468', '469', '631', '633', '410', '682', '299', '684', '681', '683', '686']
	(4)max path is: 421 to  686 len= 59
	['619', '620', '101', '102', '750', '737', '428', '427', '150', '546', '545', '458', '321', '314', '316', '309', '624', '508', '509', '512', '242', '240', '505', '506', '411', '408', '201', '97', '96', '98', '87', '495', '293', '677', '678', '679', '680', '676', '358', '357', '360', '355', '356', '353', '637', '473', '470', '471', '468', '469', '631', '633', '410', '682', '299', '684', '681', '683', '686']
	(5)max path is: 619 to  686 len= 59
	['686', '685', '396', '395', '480', '481', '482', '465', '483', '476', '478', '697', '456', '667', '462', '472', '590', '268', '266', '594', '253', '250', '210', '372', '296', '628', '24', '487', '18', '42', '614', '613', '201', '408', '411', '506', '505', '240', '242', '512', '509', '508', '624', '309', '316', '314', '321', '458', '545', '546', '150', '427', '428', '737', '750', '749', '748', '747', '746']
	(6)max path is: 686 to  746 len= 59
	['686', '685', '396', '395', '480', '481', '482', '465', '483', '476', '478', '697', '456', '667', '462', '472', '590', '268', '266', '594', '253', '250', '210', '372', '296', '628', '24', '487', '18', '42', '614', '613', '201', '408', '411', '506', '505', '240', '242', '512', '509', '508', '624', '309', '316', '314', '321', '458', '545', '546', '150', '427', '428', '737', '750', '749', '748', '747', '746']

	 */

	string parStr = "G:/reliability/StudyNetworkGML/results/";
	parStr = "./KDL/";
	string filename="kdl";
	string suffix=".edgelist";
	string myfile = parStr + filename+suffix;

	loadGraphFromEdgelist(graph, myfile);

	cout<<"the nodesize is:"<<graph.adj_relia.size();
	graph.networkName=filename;
	graph.sID = 11;
//	graph.sID = 87;
//	graph.sID = 174;
	graph.KterminalSet.insert(11);
	graph.KterminalSet.insert(70);




//	::WriteToEdgelist(graph);




}




void LoadKDLNetV2(Graph &graph) {
	string parStr = "G:/reliability/StudyNetworkGML/results/compact/";
	string filename="KdlV2_11_12_1_87";
	filename="KdlV2_11_70_1_87";
//	filename="KdlV2_421_686_1_141";
//	filename="KdlV2_422_686_1_141";
//	filename="KdlV2_619_686_1_141";
//	filename="KdlV2_686_746_1_174";




	string suffix=".edgelist";
	string myfile = parStr + filename+suffix;

	loadGraphFromEdgelist(graph, myfile);
/////
	/*
	 * 176 nodes and  295 edges
	 * the max path is: 21
['10', '5', '2', '4', '8', '15', '23', '32', '40', '49', '58', '70', '80',
'92', '107', '122', '134', '145', '156', '167', '175']
	 */

	cout<<"the nodesize is:"<<graph.adj_relia.size();
	graph.networkName=filename;
	graph.sID = 1;
//	graph.sID = 87;
//	graph.sID = 174;
	graph.KterminalSet.insert(1);
	graph.KterminalSet.insert(87);


}








void LoadDeltacomNet(Graph &graph) {
	string parStr = "G:/reliability/StudyNetworkGML/";
	string myfile = parStr + "Deltacom.edgelist";

	loadGraphFromEdgelist(graph, myfile);
//	{'startID': '108', 'endID': '39', 'length': 23}
//	{'startID': '108', 'endID': '39', 'length': 23}
/////
	graph.networkName="Deltacom K={108,39}";
	graph.sID = 39;
	graph.KterminalSet.insert(108);
	graph.KterminalSet.insert(39);
}

void LoadRedBestel(Graph &graph) {
	string parStr = "G:\\StudyNetworkGML\\";
	string myfile = parStr + "RedBestel.edgelist";

	loadGraphFromEdgelist(graph, myfile);
	/*
	 *

	{'startID': '83', 'endID': '82', 'length': 28}
	{'startID': '83', 'endID': '82', 'length': 28}
	{'startID': '83', 'endID': '82', 'length': 28}
	{'startID': '83', 'endID': '82', 'length': 28}
	 */
/////
	graph.networkName="RedBestel K={82,83} ";
	graph.sID = 82;
	graph.KterminalSet.insert(82);
	graph.KterminalSet.insert(83);
}

void LoadVtlWavenet2008(Graph &graph) {
	string parStr = "G:\\StudyNetworkGML\\";
	string myfile = parStr + "VtlWavenet2008.edgelist";

	loadGraphFromEdgelist(graph, myfile);
	/*
	 *

{'startID': '82', 'endID': '67', 'length': 31}
	 */
/////
	graph.networkName="RedBestel K={82,67} ";
	graph.sID = 82;
	graph.KterminalSet.insert(82);
	graph.KterminalSet.insert(67);
}



void LoadVtlWavenet2011(Graph &graph) {
	string parStr = "G:\\StudyNetworkGML\\";
	string myfile = parStr + "VtlWavenet2011.edgelist";

	loadGraphFromEdgelist(graph, myfile);
	/*
	 *

{'startID': '86', 'endID': '71', 'length': 31}

	 */
/////
	graph.networkName="RedBestel K={86,71} ";
	graph.sID = 86;
	graph.KterminalSet.insert(86);
	graph.KterminalSet.insert(71);
}


void LoadInteroute(Graph &graph) {
	string parStr = "G:\\StudyNetworkGML\\";
	string myfile = parStr + "Interoute.edgelist";

	loadGraphFromEdgelist(graph, myfile);
	/*
	 *

{'startID': '108', 'endID': '60', 'length': 17}

	 */
/////
	graph.networkName="Interoute K={108,60} ";
	graph.sID = 60;
	graph.KterminalSet.insert(108);
	graph.KterminalSet.insert(60);
}

void LoadION(Graph &graph) {
	string parStr = "G:\\StudyNetworkGML\\";
	string myfile = parStr + "ION.edgelist";

	loadGraphFromEdgelist(graph, myfile);
	/*
	 *

    {'startID': '102', 'endID': '79', 'length': 25}

	 */
/////
	graph.networkName="ION K={102,79} ";
	graph.sID = 102;
	graph.KterminalSet.insert(102);
	graph.KterminalSet.insert(79);
	graph.sID = 79;
}

void LoadDialtelecomCz(Graph &graph) {
	string parStr = "G:\\StudyNetworkGML\\";
	string myfile = parStr + "DialtelecomCz.edgelist";

	loadGraphFromEdgelist(graph, myfile);
	/*
	 *

   {'startID': 'n111', 'endID': 'n48', 'length': 30}

	 */
/////
	graph.networkName="DialtelecomCz K={111,48} ";
	graph.sID = 111;
	graph.KterminalSet.insert(111);
	graph.KterminalSet.insert(48);
}


void LoadTataNld(Graph &graph) {
	string parStr = "G:\\StudyNetworkGML\\";
	string myfile = parStr + "TataNld.edgelist";

	loadGraphFromEdgelist(graph, myfile);
	/*
	 *

{'startID': '139', 'endID': '111', 'length': 28}

	 */
/////
	graph.networkName="TataNld K={139,111} ";
	graph.sID = 139;
	graph.KterminalSet.insert(139);
	graph.KterminalSet.insert(111);
//	graph.sID = 111;
}



void ringofring(Graph &graph, double varFunctionsp) {


	graph.edgesize = 0;
//	graph.ClusterSumBasedKey = 0;
	graph.edgeindex = 0;
//	graph.relia = 0.0;

	graph.nVerts =21;
	graph.node_degree = vector<short int>(graph.nVerts);

    for(int i=1;i<5;i++){
    	graph.adj_relia[i][i+1]=varFunctionsp;
    	graph.adj_relia[i+1][i]=varFunctionsp;
    }

    graph.adj_relia[1][5]=varFunctionsp;
    graph.adj_relia[5][1]=varFunctionsp;




    for (int i = 5; i < 20; i++) {
		graph.adj_relia[i][i + 1] = varFunctionsp;
		graph.adj_relia[i + 1][i] = varFunctionsp;
	}

    graph.adj_relia[5][20]=varFunctionsp;
    graph.adj_relia[20][5]=varFunctionsp;


	map<int, map<int, double> >::iterator edgeit = graph.adj_relia.begin();
	for (; edgeit != graph.adj_relia.end(); edgeit++) {
		int size = graph.adj_relia[edgeit->first].size();
		graph.node_degree[edgeit->first] = size;

	}



	//add Kset
    int sID=1;
    int tID=4;
	graph.KterminalSet.insert(sID);
    graph.KterminalSet.insert(tID);
	graph.VisitedSet.insert(sID);
    graph.VisitedSet.insert(tID);



    graph.sID=4;

    graph.mSClusterSize=-1;


}





void LoadUsCarriercomNet(Graph &graph) {
	string parStr = "G:/reliability/StudyNetworkGML/";
	string myfile = parStr + "UsCarrier.edgelist";

	loadGraphFromEdgelist(graph, myfile);
/////
	graph.networkName="UsCarrier Net K={40,147}";

//	max shortest path results.....
//	{'startID': '40', 'endID': '147', 'length': 35}

	graph.sID = 40;
	graph.KterminalSet.insert(40);
	graph.KterminalSet.insert(147);
//	graph.sID=147;
}




void LoadNetwork4comNet(Graph &graph) {
	string parStr = "G:\\reliability\\StudyNetworkGML\\";
	string myfile = parStr + "Network4 k=2.edgelist";

	loadGraphFromEdgelist(graph, myfile);
/////
	graph.networkName="Network4 k=2 ";
	graph.sID =1;
	graph.KterminalSet.insert(1);
	graph.KterminalSet.insert(9);
}




void WriteToEdgelist_final(Graph &graph){
	 ofstream ofile;               //定义输出文件
	 string parStr= "G:\\reliability\\StudyNetworkGML\\PreBest\\";
	 string myfile=parStr+graph.networkName+".edgelist";
	 ofile.open(myfile);
		const int n= graph.nVerts;


	//	int (*s)[len] = new int[len][len]();

	//	auto s = new int[n][n];
	//
		int **s=new int*[n];
		for(int i=0;i< n;i++){
			s[i]=new int[n];
		}


		for (int i = 1; i < graph.nVerts; i++) {
			for (int j = 1; j < graph.nVerts; j++) {

				s[i][j] = false;
			}
		}


		list<int>* bfsnodeslist=new list<int>;
		set<int>::iterator sit=graph.KterminalSet.begin();
		bfsnodeslist= bfs(graph, *sit);

		// 判断是否已存入该边到S集合中
		list<int> Stack=*bfsnodeslist;


		while (!Stack.empty())	//只要队列不空，遍历就没有结束
		{
			int startID = Stack.front();
			//取出对头元素
			Stack.pop_front();
			cout<<"node:"<<startID<<" is popped"<<endl;
			//add the nodeself
	//		if (s[startID][startID] == false) {
	//			s[startID][startID] = true;
	//            if(::isContains(graph.KterminalSet, startID)==false){
	//			Edge edge;
	//			edge.StartID = startID;
	//			edge.EndID = startID;
	//			edge.relia = nodefuncs;
	//			graph.vectorEdge.push_back(edge);}
	//		}

			set<int> neibors = NeiborsOfNodeV1(graph, startID);

			set<int>::iterator neiborit = neibors.begin();
			for (; neiborit != neibors.end(); neiborit++) {
	//			cout<<"arc"<<*neiborit<<"-->"<<startID<<s[*neiborit][startID]<<endl;
				if (s[*neiborit][startID] == false) {
					s[*neiborit][startID] = true;
					s[startID][*neiborit] = true;
	//				cout<<"arc"<<*neiborit<<"--"<<startID<<s[*neiborit][startID]<<endl;
	//				cout<<"arc"<<startID<<"--"<<*neiborit<<s[startID][*neiborit]<<endl;
					int endID = *neiborit;
					Edge edge;
					edge.StartID = startID;
					edge.EndID = endID;
					edge.relia = graph.adj_relia[startID][endID];
					//add to the pedgelist
					ofile << edge.StartID << " " <<edge.EndID<< " "
							<< "{'relia':" <<" "<< edge.relia << "}" << endl;
	////				add to stack
	//				Stack.push_back(endID);
				}

			}


		}

		cout<<"s bool array is released"<<endl;
		for (int i = 0; i < n; i++) {
			delete s[i];
		}
		delete []s;

	ofile.close();                //关闭文件

}

void WriteToNodelist_final(Graph &graph){
	 ofstream ofile;               //定义输出文件
	 string parStr= "G:\\reliability\\StudyNetworkGML\\PreBest\\";
	 string myfile=parStr+graph.networkName+".nodelist";
	 ofile.open(myfile);

	 set<int>::iterator sit=graph.KterminalSet.begin();
	 for(;sit!=graph.KterminalSet.end();sit++){

			ofile << *sit<< " " <<"{ is K 1 }" << endl;


	 }

	ofile.close();                //关闭文件





}




void ReliaCompute(Graph &graph){
	clock_t start, finish;
	cout<<"from /////G_KT_2023_RlayerVPRJ  routine"<<endl;
    cout<<"initialization for var list"<<endl;
    set<int> K=graph.KterminalSet;
    ::Initialization_VarEdgeList(graph);


     graph.adj_relia=graph.adj_reliaV2;




	int edgesize=graph.vectorEdge.size();
//	cout << "var_ list:" << endl;
//	cout << "--var--" <<edgesize << endl;
//	for (int index=0; index<edgesize; index++) {
//
//		cout<<"varindex,"<<index<<","<<graph.vectorEdge[index].StartID << "-->" << graph.vectorEdge[index].EndID << ",relia"
//				<< graph.vectorEdge[index].relia << endl;
//
//	}

    cout<<"running... ..."<<endl;
    graph.edgeindex=0;
	graph.edgesize=edgesize;


	graph.node_degreeArray.size();

	HashGraphmap hashgraphmap;
	int outtersize=graph.edgesize;
	int maplen=outtersize;
	HashGraphmap *phashgraphmapArray=new HashGraphmap[maplen] ;

	int col=20;
	vector<vector<HashGraphmap>> phashgraphmap2DArray(maplen,vector<HashGraphmap>(col));


	start = clock();

	hashgraphmap.hit = 0;
	graph.minBoundaryNodeID=1;
	graph.maxImpactNodeID=1;



	GraphNode *pGraphNode=new GraphNode();


	int boundaryLen=50;
	InitialBoundaryDegree(*pGraphNode,boundaryLen,graph);



//    cout<<"network name:"<<graph.networkName<<endl;
    pGraphNode->isfinished=false;


    pGraphNode->PreVarRelia=1.0;


	phashgraphmapArray[0].existence = true;

	unsigned char headID = (unsigned char)0;
	string head;
	head.push_back(headID);

	phashgraphmap2DArray[0][0].mapstring_relia[head]=pGraphNode;

	int dlen=graph.node_degree.size();

	vector<short int> newDvector;
	newDvector.reserve(dlen+graph.KterminalSet.size()+1);
	newDvector.push_back(dlen);
	for(int d=0;d<dlen;d++){
		newDvector.push_back(graph.node_degree[d]);
	}

	set<int>::iterator kit=graph.KterminalSet.begin();
	for(;kit!=graph.KterminalSet.end();kit++){
		newDvector.push_back(*kit);
	}

	graph.node_degree.swap(newDvector);


	::record_BSVector(phashgraphmap2DArray[0][0], graph, *pGraphNode);



	::RDecomLayer2D(phashgraphmap2DArray, outtersize, col, graph);




	finish = clock();

	cout<<"vertices size:"<<graph.node_degreeArray.size()-1<<"----";
	cout<<"edges size:"<<graph.edgesize-graph.node_degreeArray.size()+1<<endl;


	cout << "/*" << endl;

	double totaltime = (double) (finish - start) / 1000;

	cout
			<< "the target network is as follows:###############"
			<< endl;
	cout<<"the newtork type:"<<graph.networkName<<"---------------"<<endl;

	printf("the relia  :%.10f\n", graph.NetRelia);



	cout<<"general variable size:"<<graph.edgesize<<endl;

	cout<<"BFS from ID:"<<graph.sID<<endl;

    cout<<"K set as follows:"<<endl;
    set<int>::iterator sit=K.begin();
    for(;sit!=K.end();sit++){
    	cout<<" "<<*sit;
    }
    cout<<endl;



	cout << "*/" << endl;

	cout << "the run time is:" << totaltime << "s！" << endl;
}


void printLayerInfo(map<int,set<int>>& layermap,int fromNodeID){
	  cout<<"********************layer by fromNodeID###############"<<fromNodeID<<endl;
		map<int, set<int>>::iterator layerit = layermap.begin();
		for (; layerit != layermap.end(); layerit++) {
			cout << "layer:" << layerit->first << "+++++++++++++++++++++++++++size:"
					<< layerit->second.size() << endl;
			set<int>::iterator layerItemIt = layerit->second.begin();
			for (; layerItemIt != layerit->second.end(); layerItemIt++) {
				cout << " " << *layerItemIt;
			}
			cout << endl;

		}
}




void recordKeyInfoForNode(map<int, vector<int>> &NodeKeyInfomap,
		map<int, set<int>> &layermap, int targetID) {
	int largestLayerSize = 0;
	//sum times for largestLayerSize
	int times = 0;
	int importantIndex=0;
	int layerLen=0;

	if (NodeKeyInfomap.find(targetID) == NodeKeyInfomap.end()) {
		layerLen=layermap.size();
		map<int, set<int>>::iterator layerit = layermap.begin();
		for (; layerit != layermap.end(); layerit++) {
//			cout << "layer:" << layerit->first
//					<< "+++++++++++++++++++++++++++size:"
//					<< layerit->second.size() << endl;

			importantIndex+=layerit->second.size()*layerit->second.size();
			if (largestLayerSize == 0) {
				largestLayerSize = layerit->second.size();
			} else {
				if (largestLayerSize < layerit->second.size()) {
					largestLayerSize = layerit->second.size();
				}
			}

		}

		for (layerit = layermap.begin(); layerit != layermap.end(); layerit++) {
//			cout << "layer:" << layerit->first
//					<< "+++++++++++++++++++++++++++size:"
//					<< layerit->second.size() << endl;
			if (largestLayerSize == layerit->second.size()) {
				times++;
			}

		}

		NodeKeyInfomap[targetID].push_back(largestLayerSize);
//		NodeKeyInfomap[targetID].push_back(importantIndex);
		NodeKeyInfomap[targetID].push_back(times);
		NodeKeyInfomap[targetID].push_back(layerLen);


	}
}


set<int> intersectionSet(set<int>&A,set<int>&B){
//	cout<<"interSection...."<<endl;
	set<int>ISet;
	set<int>::iterator Ait=A.begin();
	for(;Ait!=A.end();Ait++){
		if(::isContains(B,*Ait)==true){
			ISet.insert(*Ait);
		}

	}
//	cout<<"interSection finished"<<endl;
	return ISet;


}


void recordKeyInfoForNodeByBoundarySet(map<int, vector<int>> &NodeKeyInfomap,
		map<int, set<int>> &layermap, int targetID,set<int> BoundarySet) {


//	NodeKeyInfomap  map<int, vector<int>>  struct

/*
 * key===>nodeID
 * vector[i],i Corresponds layer,
 * the value representss the size of the intersection with the boundary set
 *
 *
 *
 */


	int largestLayerSize = 0;
	//sum times for largestLayerSize
	int times = 0;
	int importantIndex=0;
	int layerLen=0;

	if (NodeKeyInfomap.find(targetID) == NodeKeyInfomap.end()) {
		layerLen=layermap.size();
		map<int, set<int>>::iterator layerit = layermap.begin();
		layerit++;
		for (; layerit != layermap.end(); layerit++) {
//			cout << "layer:" << layerit->first
//					<< "+++++++++++++++++++++++++++size:"
//					<< layerit->second.size();

////			importantIndex+=layerit->second.size()*layerit->second.size();
//			if (largestLayerSize == 0) {
//				largestLayerSize = layerit->second.size();
//			} else {
//				if (largestLayerSize < layerit->second.size()) {
//					largestLayerSize = layerit->second.size();
//				}
//			}



			set<int> resultsSet=layerit->second;
            set<int> InterSectionByBoudarySet=::intersectionSet(resultsSet, BoundarySet);
            int size=InterSectionByBoudarySet.size();
//            cout<<"intersection set size:"<<size<< endl;
            NodeKeyInfomap[targetID].push_back(size);
//			if (size > 0) {
//
//				NodeKeyInfomap[targetID].push_back(size);
//			}else{
//				break;
//			}


		}
//		NodeKeyInfomap[targetID].push_back(largestLayerSize);
//
//		for (layerit = layermap.begin(); layerit != layermap.end(); layerit++) {
////			cout << "layer:" << layerit->first
////					<< "+++++++++++++++++++++++++++size:"
////					<< layerit->second.size() << endl;
//			if (largestLayerSize == layerit->second.size()) {
//				times++;
//			}
//
//		}
//
//		NodeKeyInfomap[targetID].push_back(largestLayerSize);
////		NodeKeyInfomap[targetID].push_back(importantIndex);
//		NodeKeyInfomap[targetID].push_back(times);
//		NodeKeyInfomap[targetID].push_back(layerLen);


	}
}




void recordAbsorptionInfoForNodeByBoundarySet(map<int, vector<int>> &NodeKeyInfomap,
		map<int, set<int>> &layermap, int targetID,set<int> BoundarySet) {



//	NodeKeyInfomap  map<int, vector<int>>  struct

/*
 * key===>nodeID
 * vector[i],i Corresponds layer,
 * the value representss the size of the intersection with the boundary set
 *
 *
 *
 */

	int Shit=0;


	if (NodeKeyInfomap.find(targetID) == NodeKeyInfomap.end()) {
//		cout<<"find false"<<endl;

		map<int, set<int>>::iterator layerit = layermap.begin();
		layerit++;
		for (; layerit != layermap.end(); layerit++) {
//			cout << "layer:" << layerit->first
//					<< "+++++++++++++++++++++++++++size:"
//					<< layerit->second.size();

////			importantIndex+=layerit->second.size()*layerit->second.size();
//			if (largestLayerSize == 0) {
//				largestLayerSize = layerit->second.size();
//			} else {
//				if (largestLayerSize < layerit->second.size()) {
//					largestLayerSize = layerit->second.size();
//				}
//			}



			set<int> resultsSet=layerit->second;
            set<int> InterSectionByBoudarySet=::intersectionSet(resultsSet, BoundarySet);
            int size=InterSectionByBoudarySet.size();
//            cout<<"intersection set size:"<<size<< endl;
            NodeKeyInfomap[targetID].push_back(size);
            Shit+=size;
            if(Shit==BoundarySet.size()){
            	break;
            }
//			if (size > 0) {
//
//				NodeKeyInfomap[targetID].push_back(size);
//			}else{
//				break;
//			}


		}
//		NodeKeyInfomap[targetID].push_back(largestLayerSize);
//
//		for (layerit = layermap.begin(); layerit != layermap.end(); layerit++) {
////			cout << "layer:" << layerit->first
////					<< "+++++++++++++++++++++++++++size:"
////					<< layerit->second.size() << endl;
//			if (largestLayerSize == layerit->second.size()) {
//				times++;
//			}
//
//		}
//
//		NodeKeyInfomap[targetID].push_back(largestLayerSize);
////		NodeKeyInfomap[targetID].push_back(importantIndex);
//		NodeKeyInfomap[targetID].push_back(times);
//		NodeKeyInfomap[targetID].push_back(layerLen);


	}
}




void recordBreadthInfoForNode(map<int,int> &BreadthInfomap,map<int, set<int>> &layermap, int targetID) {


//	NodeKeyInfomap  map<int, vector<int>>  struct

/*
 * key===>nodeID
 * vector[i],i Corresponds layer,
 * the value representss the size of the intersection with the boundary set
 *
 *
 *
 */


	int largestLayerSize = 0;
	//sum times for largestLayerSize
	int times = 0;
	int importantIndex=0;
	int layerLen=0;

	if (BreadthInfomap.find(targetID) == BreadthInfomap.end()) {
		layerLen=layermap.size();
		map<int, set<int>>::iterator layerit = layermap.begin();
		layerit++;
		for (; layerit != layermap.end(); layerit++) {
//			cout << "layer:" << layerit->first
//					<< "+++++++++++++++++++++++++++size:"
//					<< layerit->second.size();

////			importantIndex+=layerit->second.size()*layerit->second.size();
			if (largestLayerSize == 0) {
				largestLayerSize = layerit->second.size();
			} else {
				if (largestLayerSize < layerit->second.size()) {
					largestLayerSize = layerit->second.size();
				}
			}


		}
		BreadthInfomap[targetID]=largestLayerSize;
	}
}


void recordBandTimeInfoForNode(map<int,vector<int>> &BandTimesInfomap,map<int, set<int>> &layermap, int targetID) {


//	NodeKeyInfomap  map<int, vector<int>>  struct

/*
 * key===>nodeID
 * vector[i],i Corresponds layer,
 * the value representss the size of the intersection with the boundary set
 *
 *
 *
 */


	int largestLayerSize = 0;
	//sum times for largestLayerSize
	int times = 0;
	int importantIndex=0;
	int layerLen=0;

	if (BandTimesInfomap.find(targetID) == BandTimesInfomap.end()) {
		layerLen=layermap.size();
		map<int, set<int>>::iterator layerit = layermap.begin();
		layerit++;
		for (; layerit != layermap.end(); layerit++) {
//			cout << "layer:" << layerit->first
//					<< "+++++++++++++++++++++++++++size:"
//					<< layerit->second.size();

////			importantIndex+=layerit->second.size()*layerit->second.size();
			if (largestLayerSize == 0) {
				largestLayerSize = layerit->second.size();
			} else {
				if (largestLayerSize < layerit->second.size()) {
					largestLayerSize = layerit->second.size();
				}
			}


		}

	}


	if (BandTimesInfomap.find(targetID) == BandTimesInfomap.end()) {
		int times=0;
		map<int, set<int>>::iterator layerit = layermap.begin();
		layerit++;
		for (; layerit != layermap.end(); layerit++) {
//			cout << "layer:" << layerit->first
//					<< "+++++++++++++++++++++++++++size:"
//					<< layerit->second.size();

////			importantIndex+=layerit->second.size()*layerit->second.size();
			if (largestLayerSize==layerit->second.size()){
				times++;
			}


		}
		BandTimesInfomap[targetID].push_back(largestLayerSize);
		BandTimesInfomap[targetID].push_back(times);
	}
}


