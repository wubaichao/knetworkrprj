/*
 * Graph.h
 *
 *  Created on: 2021年5月1日
 *      Author: Administrator
 */

#ifndef GRAPH_H_
#define GRAPH_H_

#include <vector>
#include <iostream>
#include <set>
#include <list>
#include <map>
#include<map>
#include<unordered_map>
#include <vector>
#include<stack>
#include <array>

using namespace std;

class Vertex{
public:
	Vertex(int lab=1) {
		Label = lab;
		wasVisited = false;
	}
public:
	bool wasVisited;
	int Label;
};

struct Edge {  //边的定义
	int StartID;  //始节点
	int EndID;  //终节点
	double relia;  //可靠性
};









struct clusterNodeSet{

	bool isSource;  //true ----1 false ------0
	set<int> clusterSet;
	int head=0;
	bool operator==(const clusterNodeSet& obj)
	{
		return obj.head<head;
	}

};

class GraphNode
{
public:

	int Dindex=0;
	int BSindex=0;
    double PreVarRelia;

    bool isfinished=false;
};



class Graph
{
public:
	static double NetRelia;		//定义私有类的静态成员变量


    static vector<Edge> vectorEdge;
    static vector<int> node_degreeArray;
    static map<int,map<int,double> >adj_reliaV2;
    static map<int,map<int,double> >adj_relia;
    static string networkName;
	static map<int,int>node_layer;
	static set<int> VisitedSet;
	static map<int,int>node_map;
	static list<clusterNodeSet> mClusterBounaryList;


	int edgeindex;
	int edgesize;
	int sID=-1;
	int outkey=-1;

//    list<Edge> EdgeList;
    Edge targetEdge;

	set<int> KterminalSet;
	bool isStartIDLocate=false;
	bool isEndIDLocate=false;

	int mSClusterSize;

	vector<short int> node_degree;


	int minBoundaryNodeID;
	int maxImpactNodeID;

	Graph();

	~Graph();

    int nVerts;//实际节点数量

    int clusterMinhead=1;
    double PreVarRelia;

    bool isfinished=false;

};



struct auxiliaryGraph{
	list<clusterNodeSet>::iterator startID_clusterSet_it;
	list<clusterNodeSet>::iterator endID_clusterSet_it;

	list<clusterNodeSet>::iterator TargetID_clusterSet_it;
};






struct HashGraphmap
{

      unordered_map<string,GraphNode*>mapstring_relia;

      vector<vector<short int>> LayerBSDegreeVec;
      vector<vector<int>> LayerBSVec;
    bool existence=false;
    int hit=0;
    int size=0;
};


void clear(Graph &graph);

//inline unsigned long mergeinteger1(int a, int b);
inline int mergeinteger_int(int a, int b);
inline long long mergeinteger_longlong(long long a, long long b);
void InitialCompleteGraph(Graph &graph,int nodesize, double varfunctionp);
void InitialMeshGraph(Graph &graph,int row, int col, double varfunctionp);


void BridgeNetWork(Graph &graph,double edgefuncsp);
void DiamondNet(Graph &graph,double edgefuncsp);


void Network_ring(Graph &graph, double varFunctionsp);

void Network1(Graph &graph, double edgefuncsp);
void Network11(Graph &graph, double edgefuncsp);

void Network2(Graph &graph, double varFunctionsp);
void Network12(Graph &graph, double varFunctionsp);

void Network3(Graph &graph, double varFunctionsp);
void Network13(Graph &graph, double varFunctionsp);

void Network4(Graph &graph, double varFunctionsp);
void Network14(Graph &graph, double varFunctionsp);


void Network5(Graph &graph, double varFunctionsp);
void Network15(Graph &graph, double varFunctionsp);

void Network6(Graph &graph, double edgefuncsp);
void Network16(Graph &graph, double edgefuncsp);

void Network7(Graph &graph, double edgefuncsp);
void Network17(Graph &graph, double edgefuncsp);

//void Network8(Graph &graph, double edgefuncsp);
void Network18(Graph &graph, double edgefuncsp);

void Network9(Graph &graph, double edgefuncsp);
void Network19(Graph &graph, double edgefuncsp);

void Network10(Graph &graph, double edgefuncsp);
void Network20(Graph &graph, double edgefuncsp);





void loadGraphFromEdgelist(Graph &graph, string strFlle);
void loadGraphFromNodeKlist(Graph &graph, string strFlle);
void WriteToEdgelist(Graph &graph);
void WriteToNodelist(Graph &graph);

void WriteToEdgelistV2(Graph &graph);

void LoadMesh100X3(Graph &graph);
void LoadDeltacomNet(Graph &graph);
void LoadUsCarriercomNet(Graph &graph);
void LoadNetwork4comNet(Graph &graph);
void LoadKDLNet(Graph &graph);
void LoadKDLNetV1(Graph &graph);
void LoadKDLNetV2(Graph &graph);

void LoadCogentcoNet(Graph &graph);
void LoadNetWork5(Graph &graph);
void LoadNetWork15(Graph &graph);
void LoadMesh3X3(Graph &graph);

void LoadMesh12X12(Graph &graph);
void LoadMesh10X10(Graph &graph);


void LoadNetWork5_2K(Graph &graph);
void removedRundantEdge(Graph &graph);
void LoadRedBestel(Graph &graph);
void LoadVtlWavenet2008(Graph &graph);
void LoadVtlWavenet2011(Graph &graph);
void LoadInteroute(Graph &graph);
void LoadION(Graph &graph);
void LoadDialtelecomCz(Graph &graph);
void LoadTataNld(Graph &graph);

void ringofring(Graph &graph, double varFunctionsp);


//void LoadDodecahedralK2(Graph &graph);
//void LoadIndia35K2(Graph &graph);
//void LoadNewyorkK2(Graph &graph);
//void LoadPioro40K2(Graph &graph);
//void LoadGermany50K2(Graph &graph);
//void LoadTa2K2(Graph &graph);
//void LoadDodecahedralK2(Graph &graph);
//void LoadMesh7X7K2(Graph &graph);

//
//void LoadMesh10X10K2(Graph &graph);

void Network_Complete(Graph &graph,int dim,int Keysize,double varFunctionsp);



void Network_completeDim10(Graph &graph, double varFunctionsp);
void Network_completeDim14(Graph &graph, double varFunctionsp);
void Network_completeDim12(Graph &graph, double varFunctionsp);

void WriteToNodelist_final(Graph &graph);
void WriteToEdgelist_final(Graph &graph);


#endif /* GRAPH_H_ */
