/*
 * Networkdecom.h
 *
 *  Created on: 2022年12月11日
 *      Author: Administrator
 */

#ifndef NETWORKDECOM_H_
#define NETWORKDECOM_H_
#include "Graph.h"




vector <int> NeiborsOfNode(Graph &graph,int NodeID);
set<int> NeiborsOfNodeV1(Graph &graph, int nodeID);
set<int> NeiborsOfNodeV2(Graph &graph, int nodeID);
int importanceOfTargetID(Graph &graph, int nodeID,set<int>&VisitedSet);
list<int> sortedNeibors(Graph &graph,int nodeID);
list<int> sortedNeibors_byVisitedSet(Graph &graph, int nodeID,set<int>& visitedSet);
bool selecteEdge_byPreSets(Graph &graph, set<int> &visitedSet,set<int>& BoundarySet,Edge &selectedEdge);
bool selecteEdge_byBFSPreSets(Graph &graph, set<int> &visitedSet,set<int>& BoundarySet,Edge &selectedEdge);
bool selecteEdge_byPreSetsBFS(Graph &graph, set<int> &visitedSet,set<int>& BoundarySet,Edge &selectedEdge);
bool selecteEdge_byLayer(Graph &graph,int seed,set<int>& BoundarySet,Edge &selectedEdge,map<int,set<int>>& layermap);

int findSeedWithMinLayerandDegree(Graph& graph,set<int>& BoundarySet);

bool selecteEdge_byPreSetsByLayerInfo(Graph &graph, set<int> &visitedSet,set<int> &BoundarySet, Edge &selectedEdge);
void GetNeiborSetofBS(Graph &graph, set<int>& BoundarySet,set<int>& NeiborsSet );
int  compareBoundaryIndex(vector<int>& oldVec,vector<int>& newVec);
int  compareBoundaryIndexByzeroIndex(vector<int>& oldVec,vector<int>& newVec);
int  compareBoundaryIndexByoneIndex(vector<int>& oldVec,vector<int>& newVec);

void GetcandiEdgebySVisitedOrder(Graph &graph,vector<Edge>& candi_edgeVector, Edge & selectedEdge);
void GetCandiEdgeVectorbyEndsSet(Graph &graph,set<int>&BS,set<int>&MAbByMInDSet,vector<Edge>& candi_edgeVector);
void GetEdgeVectorByMinBFSLayer(	map<int,int>&BFSLayermap,vector<Edge>& candi_edgeVector);
void GetEdgeVectorByMaxBFSLayer(	map<int,int>&BFSLayermap,vector<Edge>& candi_edgeVector);

bool selecteEdge_byNodesNebor(Graph &graph, set<int> &visitedSet, int targetID,
		map<int, vector<int>> &NodeKeyInfomap, Edge &selectedEdge,int targetLayer,int rootID);

bool isTargetNodeVisitedCompleted(Graph& graph,int targetID,set<int>&VisitedSet);
void Initialization_VarEdgeList(Graph &graph);
void setOrderedEdgelist(Graph &graph,set<int>& visitedSet,set<int>& BoundarySet);
void printNodeMap(Graph& graph);
void importindexbasedBounarySet(Graph&graph,set<int>& BoundarySet,int nodeID,int &importanctDegreeIndex,int &importanctBoundaryIndex);

list<int> sortedNeibors_less(Graph &graph, int nodeID);
void printNeiborsOfId(Graph &graph,int nodeID);

int dijkstra(Graph &graph, int start_vertex, int end_vertex, vector<int> &path);

list<int>* bfs(Graph &graph,int startID);
list<int>* classicbfs(Graph &graph, int startID);
void bfs_layer(Graph &graph, int startID, map<int,set<int>>& layermap);
void bfs_layerFromID(Graph &graph, int startID,map<int,int>&BFSLayermap);
list<int>* bfs_degreeLarge(Graph &graph, int startID);
list<int>* bfs_degreeLess(Graph &graph, int startID);
list<int>* bfs_improveV1(Graph &graph, int startID);
list<int>* bfsV2(Graph &graph, int startID);
list<int>* bfs_selectedFromK(Graph &graph);
list<int>* bfs_Kset(Graph &graph);
list<int>* general_bfs(Graph &graph, int startID);

list<int>* bfs_degree(Graph &graph, int startID);

list<int>* dfs(Graph &graph,int startID);

void BFSBasedEdgeListPtrV2(Graph &graph,list<int>& bfsNodeslist,list<Edge>& pedgelist);
void BFSBasedEdgeVectorPtrV2(Graph &graph,list<int>& bfsNodeslist,vector<Edge>& vectorEdge);
void BFSBasedEdgeVectorPtrV3(Graph &graph,list<int>& bfsNodeslist);
void BFSBasedEdgeVectorPtr_generalV4(Graph &graph, list<int> &bfsNodeslist,
		double nodefuncs);
void BFSBasedEdgeVectorPtr_generalV1(Graph &graph,list<int>& bfsNodeslist,double nodefuncs);
void BFSBasedEdgeVectorPtr_generalV2(Graph &graph, list<int> &bfsNodeslist,	double nodefuncs);
void BFSBasedEdgeVectorPtr_generalV3(Graph &graph, list<int> &bfsNodeslist,	double nodefuncs);
void DFSBasedEdgeListPtrV2(Graph &graph,list<int>& bfsNodeslist,list<Edge>& pedgelist);
void BFSBasedEdgeListPtr(Graph &graph,list<int>& bfsNodeslist,list<Edge>& pedgelist);

void ClusterBListToCombineBounaryList(Graph &graph);
void ClusterBListToCombineBounaryVector(Graph &graph);

void ClusterBListToSubGraphV2(Graph &graph);

void PrintSetInfo(set<int>& S);
void PrintNodeDegreeInfoFromSet(Graph &graph,set<int>& TargetSet);
void PrintVectorInfo(vector<int>& V);
void PrintSelectedEdgeInfo(Edge & edge);
void PrintNodeDegreeInfo(Graph &graph,int nodeid);
void PrintNodeOrderInfo(Graph &graph,int nodeid);
void PrintBreathInfo(map<int,int>& BreathInfomap,int id);
void PrintBreathInfoFromSet(map<int,int>& BreathInfomap,set<int>& TargetSet);


void PrintLayerIndex(map<int,int>&BFSLayermap,int nodeID);
void PrintAbsorptionInfo(map<int, vector<int>>& ABsorptionMap,int nodeID);

void GetMinDegreeSetFromTarSetSet(Graph& graph,set<int>& TargetSet,set<int>&SetofMinDegree);
void GetMinBreathSetFromTargetSet(map<int,int> &BreathInfomap,set<int>&TargetSet, set<int>& MinBset);
void GetMinLayerSetFromTargetSet(map<int,int> &LayerInfomap,set<int>&TargetSet, set<int>& MinLayerset);

void GetMaxAbsorptionSetFromTargetSet(map<int, vector<int>> AbsorptionInfomap,set<int>&TargetSet, set<int>& MaxAbsorpSet,int zeroIndex);


int GetNodeByMinVisitedFromSet(Graph &graph,set<int> Candidate_set);
int GetNodeByMinDFromSet(Graph &graph,set<int>& Candidate_set);

void locateClusterNodeSetByTargetEdgeV2(Graph &graph);
void locateClusterNodeSetByTargetNodeIDV2(Graph &graph,int targetNodeID,Graph &G);
void locateClusterNodeSetByTargetEdge_general(Graph &graph);
void locateClusterNodeSetByTargetNodeID_general(Graph &graph,int endID);
void locateClusterNodeSetByTargetEdge_generalPart(Graph &graph);
void locateClusterNodeSetByTargetEdgeByVector(Graph &graph);
inline void insertNewCluster(Graph &G, clusterNodeSet &new_clusterSet);
inline void insertNewClusterR(Graph &graph, clusterNodeSet &new_clusterSet);
void insertNewClusterV2(Graph &graph,clusterNodeSet &new_clusterSet);
void insertNewClusterV3(Graph &graph,clusterNodeSet &new_clusterSet);

void insertNewCluster_GeneralV1(Graph &graph,clusterNodeSet &Newstart_clusterSet);


bool issingleStoT(Graph&graph,int &sID,int tID);
bool issingleStoTV2(Graph &graph, int &sID, int tID);
void removed_redundancy_loopEdgeV2(Graph &graph,int tID);
void removed_failed_loopEdge_general(Graph &graph, int failedNodeID);

void removed_redundancy_loopEdge_general(Graph &graph,int tID);
void removed_redundanceBranch_general(Graph &graph, int tID);
void removed_redundancy_loopEdge_generalV2(Graph &graph, int tID);
void removed_redundancy_loopEdge_generalV3(Graph &graph,int tID);

void removed_redundancy_loopEdge_generalV4(Graph &graph,int tID);

bool isLocateclusterSet_it(Graph &graph,int startID,int endID);
bool islocateClusterNodeSetbranchEdge_general(Graph &graph,int startID);

bool isBranch(Graph &graph,int tID);
inline bool isBranchEdge(Graph &graph);
bool isBranch_general(Graph &graph,int tID);
void SourceNodeTrue(Graph& graph);
void VarGeneralEdgeTrue(Graph &graph, int startID, int endID);
void VarGeneralEdgeTrueV2(Graph &graph, int startID, int endID);
void VarGeneralEdgeTrueV3(Graph &graph, int startID, int endID);
void VarNodeEdgeTrue(Graph& graph,bool isfromSource);
void VarNodeEdgeTrueV2(Graph& graph,bool isfromSource);
void VarGeneralEdgeTrueFromNoSoure(Graph& graph,int startID,int endID);
void VarGeneralEdgeTrueFromSoure(Graph& graph,int startID,int endID);
void AddClusterIDtoSourceSet(Graph &graph, vector<list<clusterNodeSet>::iterator> pAddClusterVector);
void RemovedNodeFromCluster(Graph& graph,int removedNodeId,vector<list<clusterNodeSet>::iterator> pClusterVector);
void combinedthecluster(Graph &graph);

bool isContains(set<int> &A,int id);

void setTargetEdgeByEdgelist(Graph &graph);
void setTargetEdgeByEdgeVector(Graph &graph);
void setTargetEdgeByEdgeVectorV2(Graph &graph);
inline void setTargetEdgeLoop(Graph &graph);

inline bool setTargetEdge_general(Graph &G,auxiliaryGraph &auxiliarygraph);
void locateClusterNodeSetByGeneralEdge(Graph &G,auxiliaryGraph &auxiliarygraph);
void releaseGraph(GraphNode *pgraph);

bool isHavePathFromStoT(Graph &graph,int tID);
bool has_path(Graph &graph,int sID,int tID);
int getSourcekey(Graph &graph);

void printGraphEdges(Graph &graph);
void printSourceSet(Graph &graph);

void serial_ana(Graph &graph,int sID,int tID);
void serial_ana_general(Graph &graph, int sID, int tID);

void initial_nodeDegree(Graph &graph);
void initial_VhashMap(vector<HashGraphmap> &Vhashmap,int row,int col);
void CreateLatticeGraph(int row,int col,Graph &graph);


bool find_pGraph_2D(HashGraphmap &phashmap,GraphNode* pgraph,string &keystring);
bool find_pGraph_V1(HashGraphmap *phashmap, int primarykey,
		GraphNode* pgraph);

void VHashMapclear(vector<HashGraphmap>& VHashMap);
void VHashMapPrint(vector<HashGraphmap>& VHashMap);

void printGraphEdges(Graph &graph);
void printmap_details(vector<HashGraphmap> &Vhashmap, int key1, int key2);
void printClusterBounaryList(Graph &graph);
void combinedthecluster_general(Graph &graph);
void combinedthecluster_generalV2(Graph &graph,int startID,int endID);


void BridgeNetWork(Graph &graph,double edgefuncsp);
void DiamondNet(Graph &graph,double edgefuncsp);

int isSetInclusion(set<int>&A,set<int>&B);
int isFirstSetLarger(set<int> &A, set<int> &B);

void printSet(set<int> S);
void setNeiborBasedSeries(Graph &graph,int seedID);
void mergeCombineEdgeTrueV2(Graph &graph, int startID, int endID);
bool isEdgeBranch_general(Graph &graph);
bool isEdgeBranch_generalV2(Graph &graph);
bool isEdgeBranch_generalV3(Graph &graph);

void setNeiborBasedSeriesForTrueNode(Graph &graph,int seedID);

void printClusterBounaryList_general(Graph &graph);
void printClusterBounaryList_generalV2(Graph &graph);
void findingrundantCluster(Graph &graph);
void printGraphEdgesV2(Graph &graph);

void removeNodeFromNoSoureCluster(Graph &graph, int startID);
void removeNodeFromSoureClusterV2(Graph &graph, int startID);
void removeNodeFromSoureCluster(Graph &graph, int startID);

inline void removeZerodegreeOfStartIDFromEdge(Graph &graph,int startID);
inline void removeZerodegreeOfEndIDFromEdge(Graph &graph,int endID);
void VarNodeEdgeTrueFromSource(Graph &graph);
void VarNodeEdgeTrueFromNoSource(Graph &graph);
bool isBranchEdgeTrue(Graph &graph);
void printclusterNodeSet(clusterNodeSet cs);
void locateSourceIDClusterV2(Graph &graph, int sourceID);
inline void VarGeneralEdgeTrue_TwoS(Graph &graph, int startID, int endID);
void VarGeneralEdgeTrue_OneS(Graph &graph, int startID, int endID);
inline void VarGeneralEdgeTrue_OneSV2(Graph &graph, int startID, int endID);
void VarGeneralEdgeTrue_ZeroS(Graph &graph, int startID, int endID);
inline void VarGeneralEdgeTrue_ZeroSV2(Graph &graph, int startID, int endID);
list<int>* layerBFS(Graph &graph);

bool islocaterundantBranchNodeCluster(Graph &graph, int targetID);
void InitialMeshGraph(int row,int col,double varfunctionp);
//void Decomposition_generalV7(Graph &graph, HashGraphmap &VHashMap,double preEdgeRelia, SubGraph *psubGraph);
//double subdecompoForFalse_generalV7(Graph &graph, HashGraphmap &VHashMap, double preEdgeRelia);
//double subdecompoForTrue_generalV7(Graph &graph, HashGraphmap &VHashMap,double preEdgeRelia);

//double subdecompoForFalse_generalV8(Graph &graph, HashGraphmap &VHashMap, double preEdgeRelia);
//double subdecompoForTrue_generalV8(Graph &graph, HashGraphmap &VHashMap,double preEdgeRelia);
set<int> NeiborsOfNode_bfs(Graph &graph,int nodeID);
void GraphReset_BasedBFS(Graph &graph,int startID);

void initial_nodeDegreeV2(Graph &graph);

void removed_redundancy_loopEdge_generalV7(Graph &graph);
void removed_redundancy_loopEdge_EdgeV1(Graph &graph);
void removed_redundancy_loopEdge_EdgeV2(Graph &graph);
void clean_redundancy_loopEdgeV1(Graph &graph);
inline void removed_redundancy_loopEdgeV2(Graph &graph);
void remove_branchEdgeV1(Graph &graph);
void removed_redundancy_loopEdge_generalV8(Graph &graph);
void serial_ana_generalV7(Graph &graph);
void serial_ana_generalEdge(Graph &graph);
void serial_ana_generalEdge(Graph &graph,double noderelia);
void serial_ana_generalV8(Graph &graph);
bool iscompleted(Graph &graph);

//inline unsigned long mergeinteger1(int a, int b);
inline int mergeinteger_int(int a, int b);
inline long long mergeinteger_longlong(long long a, long long b);

//Graph operation
void CreateGraph(Graph &graph,int row, int col, double p);
void InitialCompleteGraph(Graph &graph,int nodesize, double varfunctionp);
void InitialMeshGraph(Graph &graph,int row, int col, double varfunctionp);
void InitiallatticeGraph3D(Graph &graph,int row, int col,int hei, double varfunctionp);
void addEdge(Graph& graph,int start, int end, double relia);
void deleteEdge(Graph &graph,int start, int end);


void RDecomLayer(HashGraphmap*  phashgraphmapArray,int Layernumber,Graph &graph);

void RDecomLayerV2(vector<vector<HashGraphmap>> &phashgraphmap2DArray,
		HashGraphmap &phashgraphmap,Graph &G,list<GraphNode*> &redundantpGraphStack,int &usednodes) ;

void printBSVectorInfor(Graph &G);

void RDecomLayer2D(vector<vector<HashGraphmap>> &phashgraphmap2DArray,
		int Layernumber,int cols, Graph &G);

void expandStackGraph(list<Graph*> &redundantpGraphStack,int &allnodes);
void expandStackGraphV2(vector<GraphNode*> &redundantpGraphStack,int &allnodes);

bool Left_decom_finished2D(GraphNode* pleftGraphNode,GraphNode &graph,vector<vector<HashGraphmap>> &phashgraphmap2DArray,Graph &G);
bool Left_decom_finished2DV2(GraphNode* pleftGraphNode,GraphNode &graph,vector<vector<HashGraphmap>> &phashgraphmap2DArray,Graph &G);
inline bool Right_decom_finished2D(GraphNode* prightGraphNode,GraphNode &graph, vector<vector<HashGraphmap *>> &phashgraphmap2DArray,Graph &G);

inline bool Right_decom_finished2DV2(GraphNode* pRightGraphNode,GraphNode &gnode, vector<vector<HashGraphmap>> &phashgraphmap2DArray,Graph &graph);

void locateClusterNodeSetByTargetEdge19V6(Graph &graph,auxiliaryGraph &auxiliarygraph,Graph &G);


bool isLocateclusterSet_it19V6(Graph &G, int startID, int endID,auxiliaryGraph &auxiliarygraph);

void LocateclusterSet_itbyID19V6(Graph &G, int ID, bool isstartID,
		auxiliaryGraph &auxiliarygraph);
bool isLocateclusterSet_itbyTargetID(Graph &G, int ID, auxiliaryGraph &auxiliarygraph);

inline void VarGeneralEdgeTrue_TwoS19V6(Graph& G, int startID, int endID,auxiliaryGraph &auxiliarygraph);

inline void VarGeneralEdgeTrue_ZeroS19V6(Graph &G, int startID, int endID,
		auxiliaryGraph &auxiliarygraph);

inline void VarGeneralEdgeTrue_OneS19V6(Graph &graph, int startID, int endID,auxiliaryGraph &auxiliarygraph);
inline bool isBranchEdge19V6(Graph &graph);
inline bool isBranchEdge_general(Graph &G);

inline void removed_redundancy_loopGeneralEdge(Graph& G);

bool isFailedStartIDAndNoFailedEndIDContinue(Graph &G, int startID,
		int endID, auxiliaryGraph &auxiliarygraph);
bool isFailedEndIDandNoFailedStartIDContinue(Graph &G, int startID,
		int endID, auxiliaryGraph &auxiliarygraph);




void failed_nodeEdge_Ana(Graph & G,int faildNode,auxiliaryGraph &auxiliarygraph);

void degree_detection(Graph &graph);
inline void removeZerodegreeOfStartIDFromEdge19V6(Graph& G, int startID,
		auxiliaryGraph &auxiliarygraph);
inline void removeZerodegreeOfEndIDFromEdge19V6(Graph &G, int endID,
		auxiliaryGraph &auxiliarygraph);


inline void ClusterBListToCombineBounaryVectorV1(Graph &graph);

inline int mergeinteger_int(int a, int b);

inline void ClusterBListToCombineStringV1(GraphNode &graph,Graph& G,string &keystring);
inline void ClusterBListToCombineString2D(Graph& G,string &keystring,int &auxKey,int cols=500);

void UpdateGraphNode(Graph &G);

void DecodeClusterBlistFromString(GraphNode &graph,Graph &G, const string &keystring);
void expandBoundary(Graph &G);
bool isNodeTrueFailed(Graph &graph,int targetID);
void PrintNeiborsDegree(Graph &graph,int targetID);


void printmap_basic19V10(HashGraphmap *phashGraphmap,int len);
void printmap_basic_CapacityV2(HashGraphmap *phashGraphmap,int len);
inline void setTargetEdgeLoop19V6(Graph& graph,auxiliaryGraph &auxiliarygraph);
inline bool setTargetEdgeLoop19V7(Graph& graph,auxiliaryGraph &auxiliarygraph);
inline bool setTargetEdgeLoop19V8(Graph& graph,auxiliaryGraph &auxiliarygraph);

bool removedBranch_finished(GraphNode &graph, auxiliaryGraph &auxiliarygraph);

bool removeFailedStratID(Graph &graph,int startID,int endID,auxiliaryGraph&auxiliarygraph,bool isStartID_rundant);
bool removeFailedEndID(Graph &graph,int startID,int endID,auxiliaryGraph&auxiliarygraph,bool isEndID_rundant);
bool DetectBranch(GraphNode &graph,int startID,int endID,auxiliaryGraph&auxiliarygraph);

void zeroNodeDegreeFromBSDetection(Graph &graph,Graph &G);

inline bool isBranchEdge19V6(Graph &graph);
inline void Assign(Graph &G1,Graph& graph);
inline void CopyGraph(Graph &G1,Graph& graph);


inline void AssignGNode(GraphNode &G1,GraphNode& graph);
inline void CopyGNode(GraphNode &G1,GraphNode& graph);

inline void CopyGNodeFromGraph(GraphNode &G1,Graph& graph);


void set_DegreeVec(HashGraphmap &phashmap,Graph &G,GraphNode &Gnode);
void set_DegreeVecV2(HashGraphmap &phashmap,Graph &G,GraphNode &Gnode);

void record_BSVector(HashGraphmap &phashmap,Graph &G,GraphNode &Gnode);

void printmap_basicV16(HashGraphmap &hashGraphmap);
void clearV16(HashGraphmap &hashGraphmap);
void printmap_basicV17(HashGraphmap &hashGraphmap);
void clearV17(HashGraphmap &hashGraphmap);
inline unsigned long  mergeinteger1(unsigned long a, unsigned long b);

void resetNodeDegree(Graph &G,int oldMinID,int newMinID,int len);
void resetNodeDegreeForCompact(Graph &G,int oldMinID,int newMinID,int len);
void TruncationNodeDegree(Graph &graph,int oldMinID,int len);
void InitialBoundaryDegree(GraphNode &graphNode, int len,Graph& G);

void printClusterBounaryListBasic(Graph &graph);
void PrintBoundaryList(Graph& graph,list<clusterNodeSet> &mClusterBounaryList);
void PrintDegreeInfo(Graph &graph);

void DetectFailedNodeInClusterBounaryListBasic(Graph &graph);

void printClusterBounaryList_V5(Graph &graph);

void printLayerInfo(map<int,set<int>>& layermap,int fromNodeID);
void recordKeyInfoForNode(map<int, vector<int>> &NodeKeyInfomap,
		map<int, set<int>> &layermap, int targetID);

void Network_ring(Graph &graph, double varFunctionsp);

void Network1(Graph &graph, double edgefuncsp);
void Network11(Graph &graph, double edgefuncsp);

void Network2(Graph &graph, double varFunctionsp);
void RingNet(Graph &graph, int m,int len,double varFunctionsp);
void Network12(Graph &graph, double varFunctionsp);

void Network3(Graph &graph, double varFunctionsp);
void Network13(Graph &graph, double varFunctionsp);

void Network4(Graph &graph, double varFunctionsp);
void Network14(Graph &graph, double varFunctionsp);


void Network5(Graph &graph, double varFunctionsp);
void Network15(Graph &graph, double varFunctionsp);

void Network6(Graph &graph, double edgefuncsp);
void Network16(Graph &graph, double edgefuncsp);

void Network7(Graph &graph, double edgefuncsp);
void Network17(Graph &graph, double edgefuncsp);

void Network8(Graph &graph, double edgefuncsp);
void Network18(Graph &graph, double edgefuncsp);

void Network9(Graph &graph, double edgefuncsp);
void Network19(Graph &graph, double edgefuncsp);

void Network10(Graph &graph, double edgefuncsp);
void Network20(Graph &graph, double edgefuncsp);


void Network_debug(Graph &graph, double edgefuncsp);
void Network_debugV2(Graph &graph, double edgefuncsp);
void Network_debugV3(Graph &graph, double edgefuncsp);

void Network_Bridgedemo(Graph &graph, double varFunctionsp);
void Network_mesh3_3_2T(Graph &graph, double varFunctionsp);
void Network_mesh2_3_2T(Graph &graph, double varFunctionsp);

void Network_mesh4x4(Graph &graph, double varFunctionsp);
void Network_mesh7x7(Graph &graph, double varFunctionsp);
void LoadMesh7X7(Graph &graph);
void Network_mesh6x6(Graph &graph, double varFunctionsp);
void LoadMesh6X6(Graph &graph);


void Network_mesh8x8(Graph &graph, double varFunctionsp);

void Network_mesh8x8V2(Graph &graph, double varFunctionsp);

void Network_mesh9x9(Graph &graph, double varFunctionsp);
void Network_mesh10x10(Graph &graph, double varFunctionsp);
void Network_mesh11x11(Graph &graph, double varFunctionsp);
void Network_mesh21x11(Graph &graph, double varFunctionsp);
void Network_mesh31x11(Graph &graph, double varFunctionsp);




void Network_mesh12x12(Graph &graph, double varFunctionsp);
void Network_mesh13x13(Graph &graph, double varFunctionsp);
void Network_mesh100x8(Graph &graph, double varFunctionsp);
void Network_mesh200x8(Graph &graph, double varFunctionsp);
void Network_mesh300x8(Graph &graph, double varFunctionsp);
void Network_mesh500x8(Graph &graph, double varFunctionsp);
void Network_mesh1000x8(Graph &graph, double varFunctionsp);

void Network_mesh10000x3(Graph &graph, double varFunctionsp);
void Network_mesh8000x3(Graph &graph, double varFunctionsp);
void Network_mesh6000x3(Graph &graph, double varFunctionsp);
void Network_mesh4000x3(Graph &graph, double varFunctionsp);
void Network_mesh2000x3(Graph &graph, double varFunctionsp);
void Network_mesh1000x3(Graph &graph, double varFunctionsp);
void Network_mesh100x3(Graph &graph, double varFunctionsp);

void Network_mesh2000x7(Graph &graph, double varFunctionsp);
void Network_mesh1500x7(Graph &graph, double varFunctionsp);
void Network_mesh1200x7(Graph &graph, double varFunctionsp);
void Network_mesh1000x7(Graph &graph, double varFunctionsp);
void Network_mesh900x7(Graph &graph, double varFunctionsp);
void Network_mesh800x7(Graph &graph, double varFunctionsp);
void Network_mesh801x7(Graph &graph, double varFunctionsp);
void Network_mesh600x7(Graph &graph, double varFunctionsp);
void Network_mesh500x7(Graph &graph, double varFunctionsp);
void Network_mesh400x7(Graph &graph, double varFunctionsp);
void Network_mesh200x7(Graph &graph, double varFunctionsp);
void Network_mesh100x7(Graph &graph, double varFunctionsp);


void Network_mesh3000x6(Graph &graph, double varFunctionsp);
void Network_mesh2500x6(Graph &graph, double varFunctionsp);
void Network_mesh1800x6(Graph &graph, double varFunctionsp);
void Network_mesh2000x6(Graph &graph, double varFunctionsp);
void Network_mesh1000x6(Graph &graph, double varFunctionsp);
void Network_mesh100x6(Graph &graph, double varFunctionsp);
void Network_mesh200x6(Graph &graph, double varFunctionsp);
void Network_mesh400x6(Graph &graph, double varFunctionsp);
void Network_mesh600x6(Graph &graph, double varFunctionsp);
void Network_mesh800x6(Graph &graph, double varFunctionsp);
void Network_mesh1600x6(Graph &graph, double varFunctionsp);
void Network_mesh3200x6(Graph &graph, double varFunctionsp);


void Network_mesh20x6(Graph &graph, double varFunctionsp);

void loadGraphFromEdgelist(Graph &graph, string strFlle);
void loadGraphFromNodeKlist(Graph &graph, string strFlle);
void WriteToEdgelist(Graph &graph);
void WriteToEdgelistForDecom(Graph &graph,int step,set<int>& BS);
void WriteToOriginalEdgelist(Graph &graph);
void WriteToNodelist(Graph &graph);

void WriteToEdgelistV2(Graph &graph);

void LoadMesh100X3(Graph &graph);
void LoadDeltacomNet(Graph &graph);
void LoadUsCarriercomNet(Graph &graph);
void LoadNetwork4comNet(Graph &graph);
void LoadKDLNet(Graph &graph);
void LoadCogentcoNet(Graph &graph);
void LoadNetWork5(Graph &graph);
void LoadNetWork15(Graph &graph);
void LoadMesh3X3(Graph &graph);

void LoadMesh12X12(Graph &graph);
void LoadMesh10X10(Graph &graph);


void LoadNetWork5_2K(Graph &graph);
void removedRundantEdge(Graph &graph);
void LoadRedBestel(Graph &graph);
void LoadVtlWavenet2008(Graph &graph);
void LoadVtlWavenet2011(Graph &graph);
void LoadInteroute(Graph &graph);
void LoadION(Graph &graph);
void LoadDialtelecomCz(Graph &graph);
void LoadTataNld(Graph &graph);

void ringofring(Graph &graph, double varFunctionsp);


void LoadDodecahedralK2(Graph &graph);
void LoadIndia35K2(Graph &graph);
void LoadNewyorkK2(Graph &graph);
void LoadPioro40K2(Graph &graph);
void LoadGermany50K2(Graph &graph);
void LoadTa2K2(Graph &graph);
void LoadDodecahedralK2(Graph &graph);
void LoadMesh7X7K2(Graph &graph);
void LoadMesh10X10K2(Graph &graph);
void LoadMesh10X10KN(Graph &graph);




void Network_completeDim10(Graph &graph, double varFunctionsp);
void Network_completeDim14(Graph &graph, double varFunctionsp);
void Network_completeDim12(Graph &graph, double varFunctionsp);

void WriteToNodelist_final(Graph &graph);
void WriteToEdgelist_final(Graph &graph);

void KeyNodeLayerInfoCompute(map<int,vector<int>>&NodeKeyInfomap,Graph &graph, int &scanlinelen);
void KeyNodeLayerInfoComForTargetSet(map<int, vector<int>>&AbsorptionInfomap,map<int,int>& BreathInfomap,
		Graph &graph, set<int>&targetSet, set<int> &BoundarySet);

void ReliaCompute(Graph &graph);

void recordKeyInfoForNodeByBoundarySet(map<int, vector<int>> &NodeKeyInfomap,
		map<int, set<int>> &layermap, int targetID,set<int> BoundarySet);

void recordAbsorptionInfoForNodeByBoundarySet(map<int, vector<int>> &NodeKeyInfomap,
		map<int, set<int>> &layermap, int targetID,set<int> BoundarySet);

void recordBreadthInfoForNode(map<int,int> &BreadthInfomap,map<int, set<int>> &layermap, int targetID);
void recordBandTimeInfoForNode(map<int,vector<int>> &BandTimesInfomap,map<int, set<int>> &layermap, int targetID);
void ComputeForMultiS(Graph&graph,set<int>&BoundarySet,Edge &selectedEdge,map<int, vector<int>>&AbsorptionInfomap,map<int,int>&BreathInfomap);

set<int> intersectionSet(set<int>&A,set<int>&B);

void SelectedEdgebyScanline(Graph& graph,set<int>&visitedSet,set<int>&BoundarySet, int& nodemapindex);

#endif /* NETWORKDECOM_H_ */
