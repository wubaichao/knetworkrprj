/*
 * benchmarkNetworks.h
 *
 *  Created on: 2022年12月11日
 *      Author: Administrator
 */

#ifndef BENCHMARKNETWORKS_H_
#define BENCHMARKNETWORKS_H_

#include "Graph.h"


void LoadDodecahedralK2(Graph &graph);
void LoadIndia35K2(Graph &graph);
void LoadNewyorkK2(Graph &graph);
void LoadPioro40K2(Graph &graph);
void LoadGermany50K2(Graph &graph);
void LoadTa2K2(Graph &graph);
void LoadMesh7X7K2(Graph &graph);
void LoadMesh10X10K2(Graph &graph);
void Network_Complete(Graph &graph,int dim,int Keysize,double varFunctionsp);

void LoadDodecahedralKN2(Graph &graph);
void LoadDodecahedralKN(Graph &graph);
void LoadIndia35KN2(Graph &graph);
void LoadIndia35KN(Graph &graph);


void LoadNewyorkKN2(Graph &graph);
void LoadNewyorkKN(Graph &graph);


void LoadPioro40KN2(Graph &graph);

void LoadPioro40KN(Graph &graph);
void LoadGermany50KN2(Graph &graph);
void LoadGermany50KN(Graph &graph);
void LoadTa2KN2(Graph &graph);
void LoadTa2KN(Graph &graph);
void LoadMesh7X7KN2(Graph &graph);
void LoadMesh7X7KN(Graph &graph);
void LoadMesh10X10KN2(Graph &graph);
void LoadMesh10X10KN(Graph &graph);

void Loadgiul39K2(Graph &graph);
void Loadgiul39KN2(Graph &graph);
void Loadgiul39KN(Graph &graph);

void MeshNetworkK2(Graph &graph, int row,int col,double varFunctionsp);
void MeshNetworkKN2(Graph &graph, int row,int col,double varFunctionsp);

#endif /* BENCHMARKNETWORKS_H_ */
