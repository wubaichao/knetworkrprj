
#include "benchmarkNetworks.h"

void LoadDodecahedralK2(Graph &graph){
	string parStr = "G:\\reliability\\zlib\\results\\";
	string myfile = parStr + "dodecahedral.edgelist";
	loadGraphFromEdgelist(graph, myfile);

	graph.networkName="Dodecahedral";

	graph.KterminalSet.insert(1);
	graph.KterminalSet.insert(16);
	graph.sID =1;
	graph.mSClusterSize=-1;
}

void LoadDodecahedralKN2(Graph &graph){
	string parStr = "G:\\reliability\\zlib\\results\\";
	string myfile = parStr + "dodecahedral.edgelist";
	loadGraphFromEdgelist(graph, myfile);

	graph.networkName="DodecahedralKN2";

	for(int k=13;k<=18;k++){
		graph.KterminalSet.insert(k);
	}
	graph.KterminalSet.insert(5);
	graph.KterminalSet.insert(6);
	graph.KterminalSet.insert(1);


	graph.sID =1;
	graph.mSClusterSize=-1;
}


void LoadDodecahedralKN(Graph &graph){
	string parStr = "G:\\reliability\\zlib\\results\\";
	string myfile = parStr + "dodecahedral.edgelist";
	loadGraphFromEdgelist(graph, myfile);

	graph.networkName="DodecahedralKN2";

	for(int k=1;k<=20;k++){
		graph.KterminalSet.insert(k);
	}
//	graph.KterminalSet.insert(5);
//	graph.KterminalSet.insert(6);
//	graph.KterminalSet.insert(1);


	graph.sID =1;
	graph.mSClusterSize=-1;
}





void LoadMesh7X7K2(Graph &graph){
	string parStr = "G:\\reliability\\zlib\\results\\";
	string myfile = parStr + "MeshNetwork7X7.edgelist";
	loadGraphFromEdgelist(graph, myfile);

	graph.networkName="MeshNetwork7X7";

	graph.KterminalSet.insert(1);
	graph.KterminalSet.insert(49);
	graph.sID =1;
	graph.mSClusterSize=-1;
}



void LoadMesh7X7KN2(Graph &graph){
	string parStr = "G:\\reliability\\zlib\\results\\";
	string myfile = parStr + "MeshNetwork7X7.edgelist";
	loadGraphFromEdgelist(graph, myfile);

	graph.networkName="MeshNetwork7X7";


//
//	for(int k=1;k<=24;k++){
//		graph.KterminalSet.insert(k);
//	}


	for(int c=1;c<=3;c++){
		for(int r=1;r<=7;r++){
			int knode=7*(r-1)+c;
			cout<<"K=---------------"<<knode<<endl;
			graph.KterminalSet.insert(knode);
		}
	}

	graph.KterminalSet.insert(49);
	graph.KterminalSet.insert(32);
	graph.KterminalSet.insert(39);
	graph.KterminalSet.insert(46);
	graph.sID =1;
	graph.KterminalSet.insert(1);
	graph.mSClusterSize=-1;
}



void LoadMesh7X7KN(Graph &graph){
	string parStr = "G:\\reliability\\zlib\\results\\";
	string myfile = parStr + "MeshNetwork7X7.edgelist";
	loadGraphFromEdgelist(graph, myfile);

	graph.networkName="MeshNetwork7X7";


//
	for(int k=1;k<=49;k++){
		graph.KterminalSet.insert(k);
	}


	graph.sID =1;
	graph.mSClusterSize=-1;
}



void LoadIndia35K2(Graph &graph){
	string parStr = "G:\\reliability\\zlib\\results\\";
	string myfile = parStr + "india35.edgelist";
	loadGraphFromEdgelist(graph, myfile);

	graph.networkName="india35K2";

	graph.KterminalSet.insert(1);
	graph.KterminalSet.insert(32);
	graph.sID =1;
	graph.mSClusterSize=-1;
}

void LoadIndia35KN2(Graph &graph){
	string parStr = "G:\\reliability\\zlib\\results\\";
	string myfile = parStr + "india35.edgelist";
	loadGraphFromEdgelist(graph, myfile);

	graph.networkName="india35KN2";

	graph.KterminalSet.insert(1);
	graph.KterminalSet.insert(32);

    for(int k=2;k<=16;k++){
    	graph.KterminalSet.insert(k);
    }


    graph.mSClusterSize=-1;
	graph.sID =1;
}

void LoadIndia35KN(Graph &graph){
	string parStr = "G:\\reliability\\zlib\\results\\";
	string myfile = parStr + "india35.edgelist";
	loadGraphFromEdgelist(graph, myfile);

	graph.networkName="india35KN";

    for(int k=1;k<=35;k++){
    	graph.KterminalSet.insert(k);
    }


    graph.mSClusterSize=-1;
	graph.sID =1;
}



void LoadNewyorkK2(Graph &graph){
	string parStr = "G:\\reliability\\zlib\\results\\";
	string myfile = parStr + "newyork.edgelist";
	loadGraphFromEdgelist(graph, myfile);

	graph.networkName="newyorkK2";

	graph.KterminalSet.insert(1);
	graph.KterminalSet.insert(16);
	graph.sID =1;
	graph.mSClusterSize=-1;
}

void LoadNewyorkKN2(Graph &graph){
	string parStr = "G:\\reliability\\zlib\\results\\";
	string myfile = parStr + "newyork.edgelist";
	loadGraphFromEdgelist(graph, myfile);

	graph.networkName="newyorkKN2";

	graph.KterminalSet.insert(1);
	graph.KterminalSet.insert(16);

	 for(int k=2;k<=7;k++){
	    	graph.KterminalSet.insert(k);
	    }


	    graph.mSClusterSize=-1;


	graph.sID =1;
}

void LoadNewyorkKN(Graph &graph){
	string parStr = "G:\\reliability\\zlib\\results\\";
	string myfile = parStr + "newyork.edgelist";
	loadGraphFromEdgelist(graph, myfile);

	graph.networkName="newyorkKN";



	 for(int k=1;k<=16;k++){
	    	graph.KterminalSet.insert(k);
	    }


	    graph.mSClusterSize=-1;


	graph.sID =1;
}



void LoadPioro40K2(Graph &graph){
	string parStr = "G:\\reliability\\zlib\\results\\";
	string myfile = parStr + "Pioro40.edgelist";
	loadGraphFromEdgelist(graph, myfile);

	graph.networkName="pioro40KN2";

	graph.KterminalSet.insert(1);
	graph.KterminalSet.insert(40);
	graph.sID =1;



}


void LoadPioro40KN2(Graph &graph){
	string parStr = "G:\\reliability\\zlib\\results\\";
	string myfile = parStr + "Pioro40.edgelist";
	loadGraphFromEdgelist(graph, myfile);

	graph.networkName="pioro40KN2";

	graph.KterminalSet.insert(1);
	graph.KterminalSet.insert(40);
	graph.sID =1;
	for (int k = 2; k <= 19; k++) {
			graph.KterminalSet.insert(k);
		}

		graph.mSClusterSize = -1;
}


void LoadPioro40KN(Graph &graph){
	string parStr = "G:\\reliability\\zlib\\results\\";
	string myfile = parStr + "Pioro40.edgelist";
	loadGraphFromEdgelist(graph, myfile);

	graph.networkName="pioro40KN";

	graph.KterminalSet.insert(1);
	graph.KterminalSet.insert(40);
	graph.sID =1;
	for (int k = 1; k <= 40; k++) {
			graph.KterminalSet.insert(k);
		}

		graph.mSClusterSize = -1;
}


void LoadGermany50K2(Graph &graph){
	string parStr = "G:\\reliability\\zlib\\results\\";
	string myfile = parStr + "Germany50.edgelist";
	loadGraphFromEdgelist(graph, myfile);

	graph.networkName="Germany50K2";

	graph.KterminalSet.insert(1);
	graph.KterminalSet.insert(44);
	graph.sID =1;
//	for (int k = 2; k <= 24; k++) {
//		graph.KterminalSet.insert(k);
//	}

	graph.mSClusterSize = -1;
}

void LoadGermany50KN2(Graph &graph){
	string parStr = "G:\\reliability\\zlib\\results\\";
	string myfile = parStr + "Germany50.edgelist";
	loadGraphFromEdgelist(graph, myfile);

	graph.networkName="Germany50KN2";

	graph.KterminalSet.insert(1);
	graph.KterminalSet.insert(44);
	graph.sID =1;
	for (int k = 2; k <= 24; k++) {
			graph.KterminalSet.insert(k);
		}

	graph.mSClusterSize = -1;
}

void LoadGermany50KN(Graph &graph){
	string parStr = "G:\\reliability\\zlib\\results\\";
	string myfile = parStr + "Germany50.edgelist";
	loadGraphFromEdgelist(graph, myfile);

	graph.networkName="Germany50KN";

	graph.KterminalSet.insert(1);
	graph.KterminalSet.insert(44);
	graph.sID =1;
	for (int k = 1; k <= 50; k++) {
			graph.KterminalSet.insert(k);
		}

	graph.mSClusterSize = -1;
}



void LoadTa2K2(Graph &graph){
	string parStr = "G:\\reliability\\zlib\\results\\";
	string myfile = parStr + "ta2.edgelist";
	loadGraphFromEdgelist(graph, myfile);

	graph.networkName="Ta2K2";

	graph.KterminalSet.insert(1);
	graph.KterminalSet.insert(12);
	graph.sID =1;
	graph.mSClusterSize=-1;
}

void LoadTa2KN2(Graph &graph){
	string parStr = "G:\\reliability\\zlib\\results\\";
	string myfile = parStr + "ta2.edgelist";
	loadGraphFromEdgelist(graph, myfile);

	graph.networkName="Ta2KN2";

	graph.KterminalSet.insert(1);
	graph.KterminalSet.insert(12);
	graph.sID =1;
	for (int k = 2; k <= 33; k++) {
			graph.KterminalSet.insert(k);
		}

	graph.mSClusterSize = -1;
}

void LoadTa2KN(Graph &graph){
	string parStr = "G:\\reliability\\zlib\\results\\";
	string myfile = parStr + "ta2.edgelist";
	loadGraphFromEdgelist(graph, myfile);

	graph.networkName="Ta2KN";

	graph.sID =1;
	for (int k = 1; k <= 65; k++) {
			graph.KterminalSet.insert(k);
		}

	graph.mSClusterSize = -1;
}




void LoadMesh10X10K2(Graph &graph){
	string parStr = "G:\\reliability\\zlib\\results\\";
	string myfile = parStr + "MeshNetwork10X10.edgelist";
	loadGraphFromEdgelist(graph, myfile);

	graph.networkName="MeshNetwork10X10 K2";

	graph.KterminalSet.insert(1);
	graph.KterminalSet.insert(100);
	graph.sID =1;
	graph.mSClusterSize=-1;
}


void LoadMesh10X10KN2(Graph &graph){
	string parStr = "G:\\reliability\\zlib\\results\\";
	string myfile = parStr + "MeshNetwork10X10.edgelist";
	loadGraphFromEdgelist(graph, myfile);

	graph.networkName="MeshNetwork10X10 K2";

	graph.KterminalSet.insert(1);
	graph.KterminalSet.insert(100);

	for (int c = 1; c <= 5; c++) {
		for (int r = 1; r <= 10; r++) {
			int knode = 10* (r - 1) + c;
			cout << "K=---------------" << knode << endl;
			graph.KterminalSet.insert(knode);
		}
	}

	graph.KterminalSet.erase(95);

	graph.sID = 1;
	graph.KterminalSet.insert(1);
	graph.mSClusterSize = -1;








	graph.sID =1;
}


void LoadMesh10X10KN(Graph &graph){
	string parStr = "G:\\reliability\\zlib\\results\\";
	string myfile = parStr + "MeshNetwork10X10.edgelist";
	loadGraphFromEdgelist(graph, myfile);

	graph.networkName="MeshNetwork10X10 K2";

	graph.KterminalSet.insert(1);
	graph.KterminalSet.insert(100);

	for(int k=1;k<=100;k++){
		graph.KterminalSet.insert(k);
	}

	graph.sID = 1;
	graph.mSClusterSize = -1;


}


void Loadgiul39K2(Graph &graph){
	string parStr = "G:\\reliability\\zlib\\results\\";
	string myfile = parStr + "giul39.edgelist";
	loadGraphFromEdgelist(graph, myfile);

	graph.networkName="giul39 K2";

	graph.KterminalSet.insert(1);
	graph.KterminalSet.insert(37);

//	for(int k=1;k<=100;k++){
//		graph.KterminalSet.insert(k);
//	}

	graph.sID = 1;
	graph.mSClusterSize = -1;


}

void Loadgiul39KN2(Graph &graph){
	string parStr = "G:\\reliability\\zlib\\results\\";
	string myfile = parStr + "giul39.edgelist";
	loadGraphFromEdgelist(graph, myfile);

	graph.networkName="giul39 KN2";

	graph.KterminalSet.insert(1);
	graph.KterminalSet.insert(37);

	for(int k=1;k<=19;k++){
		graph.KterminalSet.insert(k);
	}

	graph.sID = 1;
	graph.mSClusterSize = -1;


}


void Loadgiul39KN(Graph &graph){
	string parStr = "G:\\reliability\\zlib\\results\\";
	string myfile = parStr + "giul39.edgelist";
	loadGraphFromEdgelist(graph, myfile);

	graph.networkName="giul39 KN2";

	graph.KterminalSet.insert(1);
	graph.KterminalSet.insert(37);

	for(int k=1;k<=39;k++){
		graph.KterminalSet.insert(k);
	}

	graph.sID = 1;
	graph.mSClusterSize = -1;


}



void Network_Complete(Graph &graph,int dim,int Keysize,double varFunctionsp) {

	graph.networkName="Network_Complete_D"+::to_string(dim)+"K"+::to_string(Keysize);
    InitialCompleteGraph(graph,dim, varFunctionsp);
	//add Kset
//    int sID=1;
//    int tID=nodesize;
//	graph.KterminalSet.insert(sID);
//    graph.KterminalSet.insert(tID);
//	graph.VisitedSet.insert(sID);
//    graph.VisitedSet.insert(tID);

    for(int k=1;k<=Keysize;k++){
    	graph.KterminalSet.insert(k);
    }

    graph.sID=1;


    graph.mSClusterSize=-1;






}



void MeshNetworkK2(Graph &graph, int row,int col,double varFunctionsp) {
	graph.networkName="MeshNetwork"+::to_string(row)+"X"+::to_string(col);
	InitialMeshGraph(graph,col, row, varFunctionsp);

	//add Kset
    int sID=1;
    int tID=row*col;
//    int tID=row;
//    graph.sID=sID;
	graph.KterminalSet.insert(sID);
    graph.KterminalSet.insert(tID);
//	graph.VisitedSet.insert(sID);
//    graph.VisitedSet.insert(tID);
    graph.mSClusterSize=-1;

   graph.sID=1;

    cout<<"finished!!!!!!!!!!!"<<endl;


}

void MeshNetworkKN2(Graph &graph, int row,int col,double varFunctionsp) {
	graph.networkName="MeshNetwork N2"+::to_string(row)+"X"+::to_string(col);
    InitialMeshGraph(graph,row, col, varFunctionsp);
	//add Kset
    int sID=1;
    int tID=row*col;
//    graph.sID=sID;
	graph.KterminalSet.insert(sID);
    graph.KterminalSet.insert(tID);
//	graph.VisitedSet.insert(sID);
//    graph.VisitedSet.insert(tID);
//    graph.SClusterSize=-1;
    int num=(row*col-1)/2;
    for(int k=1;k<=num;k++){
    	graph.KterminalSet.insert(k);
    }

   graph.sID=1;
   graph.mSClusterSize=-1;

    cout<<"finished!!!!!!!!!!!"<<endl;


}
