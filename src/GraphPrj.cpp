//============================================================================
// Name        : GraphPrj.cpp
// Author      : wu
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include "benchmarkNetworks.h"
#include "Networkdecom.h"
#include "Graph.h"
#include<time.h>
#include<map>
#include <iostream>
#include<cmath>

#include <set>


/*
the target network is as follows:###################################################
bride network
Error_interval---0
the rel :0.7600787280
the run time is:0.002s！
the rel Range from :0.7600787280, to 0.7600787280
the estimate rel :0.7600787280
****************the hashmap is as follows:**************
the outerlayer size:7
the graph node size:22
the hit times is:14
*/





/*
the target network is as follows:###################################################
the newtork type:Network_mesh1000x7
the rel :0.9753397974
the run time is:100.14s！
****************the hashmap is as follows:**************
the outerlayer size:779710
the graph node size:3842556
the hit times is:8936584
*/

/*
the target network is as follows:###################################################
the newtork type:Network_mesh100x3
the rel :0.8484160441
the run time is:0.012s！
****************the hashmap is as follows:**************
the outerlayer size:1277
the graph node size:1473
the hit times is:2441
*/
/*
the target network is as follows:###################################################
the newtork type:Network_mesh1000x3
the rel :0.2353912999
the run time is:0.152s！
****************the hashmap is as follows:**************
the outerlayer size:12977
the graph node size:14973
the hit times is:24941
*/
/*
the target network is as follows:###################################################
the newtork type:Network_mesh10000x3
the rel :0.0000006362
the run time is:9.235s！
****************the hashmap is as follows:**************
the outerlayer size:129977
the graph node size:149973
the hit times is:249941
*/


double Graph::NetRelia = 0.0;

vector<Edge> Graph::vectorEdge;
map<int,map<int,double> >Graph::adj_reliaV2;
map<int,map<int,double> >Graph::adj_relia;
vector<int>Graph:: node_degreeArray;
map<int,int>Graph::node_layer;

string Graph::networkName;

set<int> Graph::VisitedSet;
map<int,int>Graph::node_map;

list<clusterNodeSet> Graph::mClusterBounaryList;


int main() {

	clock_t start, finish;
	double varFunctionsp=0.9;
	Graph graph;




	Network_Bridgedemo(graph,varFunctionsp);



//	Network1(graph, varFunctionsp);
//	Network11(graph, varFunctionsp);

//	Network2(graph, varFunctionsp);
//	Network12(graph, varFunctionsp);

//	Network3(graph, varFunctionsp);
//	Network13(graph, varFunctionsp);

//	Network4(graph, varFunctionsp);
//	Network14(graph, varFunctionsp);
//
//	Network5(graph, varFunctionsp);
//	LoadNetWork5_2K(graph);
//	Network15(graph, varFunctionsp);


//	Network7(graph, varFunctionsp);
//	Network17(graph, varFunctionsp);

//	Network6(graph, varFunctionsp);
//	Network16(graph, varFunctionsp);

//	Network8(graph, varFunctionsp);
//	Network18(graph, varFunctionsp);

//	Network9(graph, varFunctionsp);
//	Network19(graph, varFunctionsp);


//		Network10(graph, varFunctionsp);
//		Network20(graph, varFunctionsp);


//		::MeshNetworkK2(graph, 10,10, varFunctionsp);
//		::MeshNetworkK2(graph, 11,11, varFunctionsp);
	//	::MeshNetworkK2(graph, 12,12, varFunctionsp);
	// ::MeshNetworkK2(graph, 13,13, varFunctionsp);
	//	::MeshNetworkK2(graph, 14,14, varFunctionsp);
	//	::MeshNetworkK2(graph, 1000,10, varFunctionsp);
	//	::MeshNetworkK2(graph,1200,10, varFunctionsp);



//	int cols=10;
//	int rows=10;
//	::MeshNetworkK2(graph, cols,rows, varFunctionsp);

	//
//		int dim=12,Keysize=2;
//		int dim=13,Keysize=2;
//		int dim=14,Keysize=2;
//		::Network_Complete(graph, dim, Keysize, varFunctionsp);

	//KDLNet
//	::LoadKDLNetV1(graph);

	removedRundantEdge(graph);
	serial_ana_generalEdge(graph,varFunctionsp);
	::GraphReset_BasedBFS(graph,graph.sID);


	ReliaCompute(graph);



	return 0;

}
